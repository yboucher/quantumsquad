using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class IntroSequencer : MonoBehaviour
{
    public string[] dialogue;
    public float textSpeed = 1.5f;
    public TextMeshPro line;
    public GameObject warImg;
    public GameObject interior;
    public GameObject crystal;
    public GameObject blackHole;
    public GameObject earth;
    public AudioClip music;

    public SpriteRenderer overlay;

    public int phase = 0;

    // Start is called before the first frame update
    void Start()
    {
        FindObjectOfType<MusicManager>().PlayMusic(music);
        StartCoroutine(Sequencer());
    }

    // Update is called once per frame
    void Update()
    {
        /*
        // Skip
        if (Input.GetKeyDown(KeyCode.Space))
		{
            FindObjectOfType<LevelLoader>().LoadLevelByName("MenuScene");
        }
        */
    }

    IEnumerator FadeIn()
	{
        for (int i = 0; i < 10; i++)
		{
            overlay.color = new Color(0, 0, 0, i / 10.0f);
            yield return new WaitForSeconds(0.1f);
		}
    }
    IEnumerator FadeOut()
    {
        for (int i = 0; i < 10; i++)
        {
            overlay.color = new Color(0, 0, 0, 1 - (i / 10.0f));
            yield return new WaitForSeconds(0.1f);
        }
        overlay.color = new Color(0, 0, 0, 0);
    }

    IEnumerator Sequencer()
	{
        GetComponent<TextWriter>().AddWriter(line, dialogue[0], textSpeed, true, null, 0);
        yield return new WaitForSeconds(10);
        GetComponent<TextWriter>().AddWriter(line, dialogue[1], textSpeed, true, null, 0);
        yield return new WaitUntil(() => phase == 1);

        GetComponent<TextWriter>().AddWriter(line, dialogue[2], textSpeed, true, null, 0);
        yield return FadeIn();
        earth.SetActive(false);
        warImg.SetActive(true);
        yield return FadeOut();

        yield return new WaitForSeconds(10);

        GetComponent<TextWriter>().AddWriter(line, dialogue[3], textSpeed, true, null, 0);
        yield return FadeIn();
        warImg.SetActive(false);
        interior.SetActive(true);
        yield return FadeOut();

        yield return new WaitForSeconds(10);
        GetComponent<TextWriter>().AddWriter(line, dialogue[4], textSpeed, true, null, 0);
        yield return new WaitForSeconds(10);

        GetComponent<TextWriter>().AddWriter(line, dialogue[5], textSpeed, true, null, 0);
        yield return new WaitForSeconds(5);
        yield return FadeIn();
        interior.SetActive(false);
        crystal.SetActive(true);
        yield return FadeOut();

        yield return new WaitForSeconds(10);
        GetComponent<TextWriter>().AddWriter(line, dialogue[6], textSpeed, true, null, 0);
        yield return new WaitForSeconds(10);
        GetComponent<TextWriter>().AddWriter(line, dialogue[7], textSpeed, true, null, 0);
        yield return new WaitForSeconds(10);

        blackHole.SetActive(true);

        yield return new WaitForSeconds(4);

        FindObjectOfType<LevelLoader>().LoadLevelByName("MenuScene");
    }
}
