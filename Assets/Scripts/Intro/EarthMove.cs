using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EarthMove : MonoBehaviour
{
    public Vector3 destination;
    public float duration = 3;

    public GameObject spaceship;
    public GameObject spaceship2;
    public GameObject spaceship3;
    public GameObject[] explosions;

    Vector3 initial;
    float startTime;
    bool reached = false;

    // Start is called before the first frame update
    void Start()
    {
        initial = transform.position;
        startTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if (reached)
            return;
        float t = Mathf.Min(1.02f, (Time.time - startTime) / duration);
        t = EasingFunction.EaseOutCubic(0, 1, t);
        float new_y = Vector3.Lerp(initial, destination, t).y;
        new_y -= new_y % 0.05f; // Pixel stairway effect
        transform.position = new Vector3(0, new_y, 0);

        if (t >= 1)
		{
            reached = true;
            StartCoroutine(ZoomOutAnim());
		}
    }

    IEnumerator ZoomOutAnim()
	{
        for (int i = 0; i < explosions.Length; i += 1)
		{
            explosions[i].SetActive(true);
            yield return new WaitForSeconds(0.2f);
        }

        yield return new WaitForSeconds(1);
        spaceship.SetActive(true);
        spaceship2.SetActive(true);
        spaceship3.SetActive(true);

        int iters = 100;
        for (int i = 0; i < iters; ++i)
		{
            float t = i / (float)iters;
            float val = EasingFunction.EaseOutSine(0.5f, 0.15f, t);
            transform.localScale = new Vector3(val, val, 1);

            yield return new WaitForSeconds(5.0f / 100);
		}

	}
}
