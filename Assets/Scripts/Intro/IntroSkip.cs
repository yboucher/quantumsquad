using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class IntroSkip : MonoBehaviour
{
    public TextMeshPro text;
    public SpriteRenderer icon;

    bool shown = false;
    float showTime = 100000;
    float hideTime = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time > hideTime)
            shown = false;

        if (Input.GetButtonDown("Fire1"))
		{
            if (!shown)
			{
                shown = true;
                showTime = Time.time;
                hideTime = showTime + 5;
			}
            else if (shown && Time.time - showTime > 0.5f) // Make sure the player doesn't accidentaly mash their way into skipping
			{
                FindObjectOfType<LevelLoader>().LoadLevelByName("MenuScene");
            }
		}

        float alpha = Mathf.Clamp(Mathf.Lerp(0, 1, (Time.time - showTime) / 1.0f), 0, 1);
        alpha *= Mathf.Clamp(Mathf.Lerp(1, 0, (Time.time - hideTime) / 1.0f), 0, 1);

        text.alpha = alpha;
        icon.color = new Color(icon.color.r, icon.color.g, icon.color.b, alpha);
    }
}
