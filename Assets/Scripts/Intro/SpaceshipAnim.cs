using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceshipAnim : MonoBehaviour
{
    public Vector3 destination;
    public float duration = 3;
    public bool justVibin = false;

    Vector3 initial;
    float startTime;
    float initialScale;
    bool reached = false;

    // Start is called before the first frame update
    void Start()
    {
        initial = transform.position;
        initialScale = transform.localScale.x;
        startTime = Time.time;
    }


    IEnumerator Anim()
    {
        yield return new WaitForSeconds(2);

        int iters = 100;
        for (int i = 0; i < iters; ++i)
        {
            float t = i / (float)iters;
            float val = EasingFunction.EaseOutSine(initialScale, 0.05f, t);
            transform.localScale = new Vector3(val, val, 1);

            float new_x = Vector3.Lerp(initial, destination, t).x;
            new_x -= new_x % 0.05f; // Pixel stairway effect

            transform.position = new Vector3(new_x, transform.position.y, 0);

            yield return new WaitForSeconds(5.0f / 100);
        }

        FindObjectOfType<IntroSequencer>().phase = 1;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(0, 0, Time.deltaTime * 10.0f);
        if (justVibin)
            return;

        if (reached)
            return;
        float t = Mathf.Min(1.02f, (Time.time - startTime) / duration);
        t = EasingFunction.EaseOutCubic(0, 1, t);
        float new_y = Vector3.Lerp(initial, destination, t).y;
        new_y -= new_y % 0.05f; // Pixel stairway effect

        transform.position = new Vector3(initial.x, new_y, 0);

        if (t >= 1)
        {
            reached = true;
            StartCoroutine(Anim());
        }
    }
}
