using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomTypeGenerator : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void GenerateRooms(SpaceshipMapGenerator map)
	{
        bool valid = true;
        int maxiterations = 1000;

        do
        {
            valid = true;

            Dictionary<System.Type, int> maxOccurences = new Dictionary<System.Type, int>();
            Dictionary<System.Type, int> occurences = new Dictionary<System.Type, int>();

            maxOccurences[typeof(HealRoom)] = 3;
            maxOccurences[typeof(TestCombatRoom)] = 8;
            maxOccurences[typeof(LootRoom)] = 4;
            maxOccurences[typeof(BuffRoom)] = 3;
            maxOccurences[typeof(CurioRoom)] = 5;

            for (int i = 0; i < map.roomCountSize.x; i++)
                for (int j = 0; j < map.roomCountSize.y; j++)
                {
                    var room = map.GetRoom(new Vector2Int(i, j));
                    if (room.type == SpaceshipMapGenerator.TileType.Room)
                    {
                        Room roomType;
                        do
                        {
                            //Debug.Log("Room : " + new Vector2Int(i, j) + " is " + room.type);
                            roomType = GenerateRoom(room);
                            // Make sure we don't exceed the max room type counts
                            if (roomType == null)
                                break;
                            else if (!occurences.ContainsKey(roomType.GetType())) // Create key
                                occurences.Add(roomType.GetType(), 0);
                        //} while (false);
                        } while (occurences[roomType.GetType()] + 1 > maxOccurences[roomType.GetType()]);

                        if (roomType != null)
                            occurences[roomType.GetType()]++;
                    }
                }

            int healRooms = 0;
            int buffRooms = 0;
            int emptyRooms = 0;
            int combatRooms = 0;
            for (int i = 0; i < map.roomCountSize.x; ++i)
                for (int j = 0; j < map.roomCountSize.y; ++j)
				{
                    var room = map.GetRoom(new Vector2Int(i, j));

                    // Dead end, spawn a bonus room
                    if ((room.connects[0]?1:0) + (room.connects[1] ? 1 : 0) +
                        (room.connects[2] ? 1 : 0) + (room.connects[3] ? 1 : 0) == 1)
					{
                        int rand = Random.Range(0, 100);

                        if (rand <= 25)
                            room.attributeSprite.GetComponent<RoomAttributeMarker>().roomObject = null;
                        else if (rand <= 50)
                            room.attributeSprite.GetComponent<RoomAttributeMarker>().roomObject = new BuffRoom();
                        else if (rand <= 75)
                            room.attributeSprite.GetComponent<RoomAttributeMarker>().roomObject = new LootRoom();
                        else
                            room.attributeSprite.GetComponent<RoomAttributeMarker>().roomObject = new HealRoom();
                    }

                    if (room.attributeSprite.GetComponent<RoomAttributeMarker>().roomObject?.GetType() == typeof(TestCombatRoom) &&
                        NeighborsOfType<TestCombatRoom>(map, new Vector2Int(i, j)) >= 2) // Means that there are 3 consecutive combat rooms
					{
                        valid = false; //Debug.Log("Many neighbors at " + new Vector2Int(i, j) + " : " + combatNeighbors);
                    }
                    if (room.attributeSprite.GetComponent<RoomAttributeMarker>().roomObject?.GetType() == typeof(HealRoom) &&
                        NeighborsOfType<HealRoom>(map, new Vector2Int(i, j)) >= 1) // Means that there are 2 consecutive heal rooms
                    {
                        valid = false; //Debug.Log("Many neighbors at " + new Vector2Int(i, j) + " : " + combatNeighbors);
                    }

                    if (room.attributeSprite.GetComponent<RoomAttributeMarker>().roomObject?.GetType() == typeof(HealRoom))
                        ++healRooms;
                    if (room.attributeSprite.GetComponent<RoomAttributeMarker>().roomObject?.GetType() == typeof(BuffRoom))
                        ++buffRooms;
                    if (room.attributeSprite.GetComponent<RoomAttributeMarker>().roomObject?.GetType() == typeof(TestCombatRoom))
                        ++combatRooms;
                    if (room.type == SpaceshipMapGenerator.TileType.Room && room.attributeSprite.GetComponent<RoomAttributeMarker>().roomObject == null)
                        ++emptyRooms;
                }

            // At least 2 heal rooms, no more than 3
            if (healRooms < 2 || healRooms > 3)
                valid = false;
            if (buffRooms == 0 || buffRooms > 3)
                valid = false;
            if (combatRooms < 5)
                valid = false;
            if (emptyRooms >= 6)
                valid = false;
        } while (!valid && maxiterations-- > 0);

        Debug.Log("Iterations : " + (1000 - maxiterations));

        Debug.Log("Room types generated");
	}

    int NeighborsOfType<Type>(SpaceshipMapGenerator map, Vector2Int pos)
	{
        int combatNeighbors = 0;
        var room = map.GetRoom(pos);

        int i = pos.x, j = pos.y;

        if (room.connects[0] && map.GetRoom(new Vector2Int(i, j + 1)).attributeSprite.GetComponent<RoomAttributeMarker>().roomObject?.GetType() == typeof(Type))
            ++combatNeighbors;
        if (room.connects[1] && map.GetRoom(new Vector2Int(i - 1, j)).attributeSprite.GetComponent<RoomAttributeMarker>().roomObject?.GetType() == typeof(Type))
            ++combatNeighbors;
        if (room.connects[2] && map.GetRoom(new Vector2Int(i, j - 1)).attributeSprite.GetComponent<RoomAttributeMarker>().roomObject?.GetType() == typeof(Type))
            ++combatNeighbors;
        if (room.connects[3] && map.GetRoom(new Vector2Int(i + 1, j)).attributeSprite.GetComponent<RoomAttributeMarker>().roomObject?.GetType() == typeof(Type))
            ++combatNeighbors;

        return combatNeighbors;
    }

    Room GenerateRoom(SpaceshipMapGenerator.Room room)
	{
        var attr = room.attributeSprite.GetComponent<RoomAttributeMarker>();

        int roll = Random.Range(0, 100);
        //Debug.Log("Roll " + roll);
        // 15% : empty room
        if (roll <= 15)
        {
            attr.roomObject = null;
            return null;
        }
        // 10% : buff
        else if (roll <= 25)
            attr.roomObject = new BuffRoom();
        // 15% : heal room
        else if (roll <= 40)
            attr.roomObject = new HealRoom();
        // 15% : curio
        else if (roll <= 55)
            attr.roomObject = new CurioRoom(Curio.RandomCurio());
        // 15% : loot 
        else if (roll <= 70)
            attr.roomObject = new LootRoom();
        // 30% : fight
        else
            attr.roomObject = new TestCombatRoom();

        return attr.roomObject;
	}

    // Update is called once per frame
    void Update()
    {
        
    }
}
