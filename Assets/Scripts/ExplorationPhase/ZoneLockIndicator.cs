using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoneLockIndicator : MonoBehaviour
{
    public GameObject lock2, lock3, lock4;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        lock2.SetActive(SharedData.bossesSlain < 1);
        lock3.SetActive(SharedData.bossesSlain < 2);
        lock4.SetActive(SharedData.bossesSlain < 3);
    }
}
