using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ScoutingButton : MonoBehaviour
{
    public TextMeshProUGUI text;
    public Button button;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void ButtonPressed()
	{
        FindObjectOfType<SquadIndicator>().DoScouting();
    }

    // Update is called once per frame
    void Update()
    {
        if (SharedData.scoutingCharges == 0)
		{
            text.color = Color.grey;
            text.text = "Rep�rage";
            button.interactable = false;
		}
        else
		{
            text.color = Color.white;
            text.text = "Rep�rage (" + SharedData.scoutingCharges + ")";

            button.interactable = true;
		}

        var colorBlock = button.colors;
        colorBlock.disabledColor = SharedData.scoutingCharges > 0 ? Color.white : Color.black;
        button.colors = colorBlock;

        if (Input.GetKeyDown(KeyCode.T))
            ++SharedData.scoutingCharges;
    }
}
