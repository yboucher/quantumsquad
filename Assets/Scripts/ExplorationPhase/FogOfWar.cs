using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FogOfWar : MonoBehaviour
{
    public Material fogMaterial;

    float[] tileData;
    int tileCount = 0;
    bool gridUpdated = false;

    HashSet<Vector2Int> revealedTiles;

    // Start is called before the first frame update
    void Start()
    {
        revealedTiles = new HashSet<Vector2Int>();
        tileData = new float[256 * 3];
        tileCount = 0;

        RevealGrid(new Vector2Int(0, 0));
        RevealGrid(new Vector2Int(1, 0));
        RevealGrid(new Vector2Int(2, 0));
        RevealGrid(new Vector2Int(3, 0));
        RevealGrid(new Vector2Int(-1, 0));
        RevealGrid(new Vector2Int(-2, 0));
        RevealGrid(new Vector2Int(-3, 0));
        RevealGrid(new Vector2Int(-4, 0));

        RevealGrid(new Vector2Int(0, 1));
        RevealGrid(new Vector2Int(0, 2));
        RevealGrid(new Vector2Int(0, 3));
        RevealGrid(new Vector2Int(0, 4));
        RevealGrid(new Vector2Int(0, 5));
        RevealGrid(new Vector2Int(0, 6));

        RevealGrid(new Vector2Int(0, -1));
        RevealGrid(new Vector2Int(0, -2));
        RevealGrid(new Vector2Int(0, -3));
        RevealGrid(new Vector2Int(0, -4));
        RevealGrid(new Vector2Int(0, -5));
        RevealGrid(new Vector2Int(0, -6));
    }

    public void RevealGrid(Vector2Int pos)
	{
        if (revealedTiles.Contains(pos))
            return;
        revealedTiles.Add(pos);

        tileData[tileCount*3] = pos.x;
        tileData[tileCount*3+1] = pos.y;
        tileData[tileCount*3+2] = Time.timeSinceLevelLoad;
        tileCount++;
        gridUpdated = true;
	}

    // Update is called once per frame
    void Update()
    {
        if (gridUpdated)
		{
            fogMaterial.SetFloatArray("_revealedTiles", tileData);
            fogMaterial.SetInt("_revealedTileCount", tileCount);
            gridUpdated = false;
		}           
    }
}
