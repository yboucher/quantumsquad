using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplorationManager : MonoBehaviour
{
    public AudioClip explorationMusic;
    public MusicManager musicManager;

    // Start is called before the first frame update
    void Start()
    {
        musicManager.PlayMusic(explorationMusic);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	private void OnDestroy()
	{
		// Reset the camera pos
        if (Camera.main)
            Camera.main.transform.localPosition = Vector3.zero;
	}
}
