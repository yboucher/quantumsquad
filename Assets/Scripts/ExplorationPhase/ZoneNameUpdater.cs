using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ZoneNameUpdater : MonoBehaviour
{
    public TextMeshPro text;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        text.text = "Zone :\n<color=green>";
        switch (SharedData.currentZone)
        {
            default:
            case 0:
                text.text += "Docks du vaisseau";
                break;
            case 1:
                text.text += "Quartiers des aliens";
                break;
            case 2:
                text.text += "QG de Megotron";
                break;
            case 3:
                text.text += "Labo corrompu";
                break;
            case 4:
                text.text += "Halls de l'empereur";
                break;
        }
    }
}
