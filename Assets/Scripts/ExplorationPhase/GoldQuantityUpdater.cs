using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GoldQuantityUpdater : MonoBehaviour
{
    SaveData saveData;

    float timeCounter = 0;

    // Start is called before the first frame update
    void Start()
    {
        saveData = SaveData.Load();
    }

    // Update is called once per frame
    void Update()
    {
        timeCounter += Time.deltaTime;
        // Reload save data every 0.5s
        if (timeCounter > 0.5f)
		{
            timeCounter = 0;
            saveData = SaveData.Load();
		}

        GetComponent<TextMeshPro>().text = "Gold : <color=yellow>" + saveData.gold + "</color> po";
    }
}
