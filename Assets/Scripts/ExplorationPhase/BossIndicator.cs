using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossIndicator : MonoBehaviour
{
    public int zoneID = 1;
    public GameObject sprites;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        sprites.SetActive(zoneID == SharedData.bossesSlain + 1 && !SharedData.bossRoomRevealed);
    }
}
