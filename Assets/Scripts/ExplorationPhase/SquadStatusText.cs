using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SquadStatusText : MonoBehaviour
{
    public TextMeshPro text;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (SharedData.soldiers == null)
            return;
        text.text = "Soldats :\n";
        foreach (var soldier in SharedData.soldiers)
            text.text += "  <color=blue>" + soldier.ActorName + "</color> : <color=green>" + soldier.HP + "/" + soldier.MaxHP + "</color>\n";
    }
}
