using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class SquadIndicator : MonoBehaviour
{
    public float movementSpeed = 0.3f;
    public FogOfWar fog;
    public SpaceshipMapGenerator mapZone1, mapZone2, mapZone3, mapZone4;
    public MessageBox msgbox;
    public DialogBox dialogBox;

    float cameraYOffset = -0.5f;
    float mouseCameraSpeed = 50.0f;
    bool moving = false;
    bool popupOpen = false;
    float movementStart;
    Vector3 from, to;
    Vector3 velocity;
    Vector2Int tilePos = Vector2Int.zero;
    Vector2Int roomPos = Vector2Int.zero;
    int lastSlainBossCount = 0;

    // Start is called before the first frame update
    void Start()
    {
        tilePos = new Vector2Int(6, 2);
        roomPos = new Vector2Int(6, 2);
        transform.position = mapZone1.RoomWorldCoordinates(roomPos);
        Camera.main.transform.position = new Vector3(transform.position.x, transform.position.y + cameraYOffset, Camera.main.transform.position.z);

        mapZone1.conversionOffset = Vector2Int.zero;
        mapZone2.conversionOffset = new Vector2Int(10, 0);
        mapZone3.conversionOffset = new Vector2Int(8, -1) - new Vector2Int(2, 6);

        // Handle the Boss Room
        mapZone1.GetRoom(mapZone1.BossRoomPos()).attributeSprite.GetComponent<RoomAttributeMarker>().roomObject = new BossRoom();
        mapZone2.GetRoom(mapZone2.BossRoomPos()).attributeSprite.GetComponent<RoomAttributeMarker>().roomObject = new BossRoom();
        mapZone3.GetRoom(mapZone3.BossRoomPos()).attributeSprite.GetComponent<RoomAttributeMarker>().roomObject = new BossRoom();

        // Handle locked starting rooms
        //mapZone2.GetRoom(mapZone2.StartRoomPos()).attributeSprite.GetComponent<RoomAttributeMarker>().roomObject = new LockedRoom();
        //mapZone3.GetRoom(mapZone3.StartRoomPos()).attributeSprite.GetComponent<RoomAttributeMarker>().roomObject = new LockedRoom();
        //mapZone2.GetRoom(mapZone2.StartRoomPos()).attributeSprite.GetComponent<RoomAttributeMarker>().RevealType();
        //mapZone3.GetRoom(mapZone3.StartRoomPos()).attributeSprite.GetComponent<RoomAttributeMarker>().RevealType();

        //fog.RevealGrid(new Vector2Int(2, -1));
        //fog.RevealGrid(mapZone2.StartRoomPos() + new Vector2Int(1, 0) - new Vector2Int(2, 2));
        // DEBUG
        //map.GetRoom(new Vector2Int(6, 4)).attributeSprite.GetComponent<RoomAttributeMarker>().roomObject = new BossRoom();

        /*
        // Adding test rooms
        if (map.GetRoom(new Vector2Int(4, 4)) != null && map.GetRoom(new Vector2Int(4, 4)).attributeSprite != null)
        {
            map.GetRoom(new Vector2Int(4, 4)).attributeSprite.GetComponent<RoomAttributeMarker>().roomObject = new BuffRoom();
        }

        if (map.GetRoom(new Vector2Int(5, 4)) != null && map.GetRoom(new Vector2Int(5, 4)).attributeSprite != null)
        {
            //var curio = new AlienArchivesCurio();
            var curio = new ControlConsoleCurio();
            map.GetRoom(new Vector2Int(5, 4)).attributeSprite.GetComponent<RoomAttributeMarker>().roomObject = new CurioRoom(curio);
        }
        if (map.GetRoom(new Vector2Int(5, 3)) != null && map.GetRoom(new Vector2Int(5, 3)).attributeSprite != null)
        {
            map.GetRoom(new Vector2Int(5, 3)).attributeSprite.GetComponent<RoomAttributeMarker>().roomObject = new HealRoom();
        }
        if (map.GetRoom(new Vector2Int(5, 0)) != null && map.GetRoom(new Vector2Int(5, 0)).attributeSprite != null)
		{
            map.GetRoom(new Vector2Int(5, 0)).attributeSprite.GetComponent<RoomAttributeMarker>().roomObject = new TestCombatRoom();
        }
        if (map.GetRoom(new Vector2Int(5, 1)) != null && map.GetRoom(new Vector2Int(5, 1)).attributeSprite != null)
        {
            map.GetRoom(new Vector2Int(5, 1)).attributeSprite.GetComponent<RoomAttributeMarker>().roomObject = new TestCombatRoom();
        }
        */

        // Comment out to disable automatic room type generation
        FindObjectOfType<RoomTypeGenerator>().GenerateRooms(mapZone1);
        FindObjectOfType<RoomTypeGenerator>().GenerateRooms(mapZone2);
        FindObjectOfType<RoomTypeGenerator>().GenerateRooms(mapZone3);

        RevealConnectedRooms();
    }

    public void DoScouting()
	{
        --SharedData.scoutingCharges;
        UnityEngine.EventSystems.EventSystem.current.SetSelectedGameObject(null);

        FindObjectOfType<SquadIndicator>().RevealConnectedRooms(2); // Reveal connected rooms with some range
        FindObjectOfType<SquadIndicator>().RevealConnectedRoomsType(); // Reveal the type of the connected rooms
    }

    bool debugRoomsRevealed = false;
    public void DebugToggleRoomsRevealed()
	{
        var map = GetMap(roomPos);
        if (map == null)
            return;

        for (int i = 0; i < map.roomCountSize.x; ++i)
            for (int j = 0; j < map.roomCountSize.y; ++j)
            {
                if (map.GetRoom(new Vector2Int(i, j)) != null && map.GetRoom(new Vector2Int(i, j)).attributeSprite)
                {
                    if (!debugRoomsRevealed)
                        map.GetRoom(new Vector2Int(i, j)).attributeSprite.GetComponent<RoomAttributeMarker>().RevealType();
                    else
                        map.GetRoom(new Vector2Int(i, j)).attributeSprite.GetComponent<RoomAttributeMarker>().Unreveal();
                }
            }
        debugRoomsRevealed = !debugRoomsRevealed;
    }

    IEnumerator ShowExitMsg()
	{
        popupOpen = true;

        dialogBox.text.text = "Voulez vous quitter le jeu ?\n\n";

        dialogBox.acceptedText.text = "Quitter le jeu";
        dialogBox.refusedText.text = "Annuler";

        dialogBox.pressed = false;
        dialogBox.Show();

        yield return new WaitUntil(() => dialogBox.pressed);
        dialogBox.Close();
        if (dialogBox.accepted)
            Application.Quit();

        popupOpen = false;
    }
    SpaceshipMapGenerator GetMap(Vector2Int pos)
	{
        // We're in zone 2
        if (pos.x > 9)
        {
            if (mapZone2.GetRoomWorldCoords(pos) != null)
                return mapZone2;
        }
        // We're in zone 3
        if (pos.y < 0)
		{
            if (mapZone3.GetRoomWorldCoords(pos) != null)
                return mapZone3;
		}

        if (mapZone1.GetRoomWorldCoords(pos) != null)
            return mapZone1;
        else
            return null;
	}

    bool IsOnCenterCross(Vector2Int pos)
	{
        if (pos.y == 2) // Vertical center
            return pos.x >= 7 && pos.x <= 9;
        if (pos.x == 8) // Horizontal center
            return pos.y <= 4 && pos.y >= 0;
        return false;
	}

    IEnumerator ShowPopup(string msg)
	{
        popupOpen = true;

        msgbox.text.text = msg;
        msgbox.Show();

        yield return new WaitUntil(() => msgbox.closing);

        popupOpen = false;
	}

    // Update is called once per frame
    void Update()
    {
        if (SharedData.scoutingCharges > 0 && Input.GetButtonDown("Scout"))
            DoScouting();

        if (!popupOpen && Input.GetButtonDown("Cancel"))
		{
            StartCoroutine(ShowExitMsg());
		}

        // A new boss has been slain!
        if (SharedData.bossesSlain != lastSlainBossCount)
		{
            lastSlainBossCount = SharedData.bossesSlain;
            StartCoroutine(ShowPopup("F�licitations, vous avez vaincu ce boss !\nNouvel objectif : " + GameObject.FindObjectOfType<ObjectiveIndicator>().GetObjectiveText()));
        }

        if (GetMap(roomPos) == null)
            SharedData.currentZone = 0;
        else if (GetMap(roomPos) == mapZone1)
            SharedData.currentZone = 1;
        else if (GetMap(roomPos) == mapZone2)
            SharedData.currentZone = 2;
        else if (GetMap(roomPos) == mapZone3)
            SharedData.currentZone = 3;
        else if (GetMap(roomPos) == mapZone4)
            SharedData.currentZone = 4;

        //Debug.Log("Current pos : " + roomPos);
        if (!FindObjectOfType<LevelLoader>().loading && !moving && !popupOpen && (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0))
        {
            Vector2Int offset = Vector2Int.zero;
            if (Input.GetAxis("Horizontal") > 0)
                offset = new Vector2Int(1, 0);
            else if (Input.GetAxis("Horizontal") < 0)
                offset = new Vector2Int(-1, 0);
            else if (Input.GetAxis("Vertical") > 0)
                offset = new Vector2Int(0, 1);
            else if (Input.GetAxis("Vertical") < 0)
                offset = new Vector2Int(0, -1);

            var map = GetMap(roomPos);
            var nextMap = GetMap(roomPos + offset);
            // Will we enter a locked zone entry point ?
            Debug.Log(roomPos + offset);
            if (roomPos + offset == new Vector2Int(9, 2) && SharedData.bossesSlain < 1) // Zone 2 lock
			{
                offset = Vector2Int.zero; // Can't move to a locked zone !
                StartCoroutine(ShowPopup("Vous devez vaincre le boss des <color=green>quartiers des aliens</color> pour entrer dans le <color=red>QG de Megotron</color> !"));
			}
            else if (roomPos + offset == new Vector2Int(8, 1) && SharedData.bossesSlain < 2) // Zone 3 lock
            {
                offset = Vector2Int.zero; // Can't move to a locked zone !
                StartCoroutine(ShowPopup("Vous devez vaincre le boss du <color=green>QG de Megotron</color> pour entrer dans le <color=red>labo corrompu</color> !"));
            }
            else if (roomPos + offset == new Vector2Int(8, 3) && SharedData.bossesSlain < 3) // Zone 4 lock
            {
                offset = Vector2Int.zero; // Can't move to a locked zone !
                StartCoroutine(ShowPopup("Vous devez vaincre le boss du <color=green>labo corrompu</color> pour entrer dans les <color=red>halles de l'Empereur</color> !"));
            }
            else if (roomPos + offset == new Vector2Int(8, 4)) // Beta version lock!
            {
                offset = Vector2Int.zero; // Can't move to a locked zone !
                StartCoroutine(ShowPopup("F�licitations, vous avez vaincu les 3 premiers boss !\n" + 
                                        "Le boss final sera disponible pour la version finale.\n" + 
                                        "Merci encore d'avoir jou� � notre jeu ! :)"));
            }
            // Will we enter on center cross territory ?
            if (IsOnCenterCross(roomPos + offset))
			{
                Debug.Log("About to enter center cross");
                map = null;
			}
            // Will we leave cross territory to map territory ?
            else if (IsOnCenterCross(roomPos) && nextMap != null && nextMap.GetRoomWorldCoords(roomPos + offset) != null)
			{
                map = nextMap;
            }
            else if (map == null)
			{
                offset = Vector2Int.zero;
			}
            else if (map.GetRoomWorldCoords(roomPos) != null)
            {
                var room = map.GetRoomWorldCoords(roomPos);
                if (!room.connects[3] && Input.GetAxis("Horizontal") > 0)
                    offset = Vector2Int.zero;
                else if (!room.connects[1] && Input.GetAxis("Horizontal") < 0)
                    offset = Vector2Int.zero;
                else if (!room.connects[0] && Input.GetAxis("Vertical") > 0)
                    offset = Vector2Int.zero;
                else if (!room.connects[2] && Input.GetAxis("Vertical") < 0)
                    offset = Vector2Int.zero;
            }

            if (offset != Vector2Int.zero)
            {
                int multiplier = 2;

                from = transform.position;
                to = transform.position + new Vector3(offset.x*0.5f*multiplier, offset.y * 0.5f * multiplier, 0);
                velocity = Vector3.zero;
                movementStart = Time.time;
                moving = true;

                roomPos += offset;

                offset.x *= -1;

                fog.RevealGrid(tilePos + offset - new Vector2Int(2, 2));
                fog.RevealGrid(tilePos + offset * 2 - new Vector2Int(2, 2));

                tilePos += offset*2; // opposite coordinate system

                if (map != null)
                {
                    if (map.GetRoomWorldCoords(roomPos).attributeSprite != null)
                        map.GetRoomWorldCoords(roomPos).attributeSprite.GetComponent<RoomAttributeMarker>().Hide();

                    if (map.GetRoomWorldCoords(roomPos).attributeSprite != null && map.GetRoomWorldCoords(roomPos).attributeSprite.GetComponent<RoomAttributeMarker>().roomObject != null &&
                        !map.GetRoomWorldCoords(roomPos).attributeSprite.GetComponent<RoomAttributeMarker>().roomObject.Entered())
                    {
                        popupOpen = true;
                        ++SharedData.enteredRooms;
                        map.GetRoomWorldCoords(roomPos).attributeSprite.GetComponent<RoomAttributeMarker>().roomObject.Enter();
                    }

                    RevealConnectedRooms();
                }
            }
        }

        if (moving)
		{
            transform.position = Vector3.SmoothDamp(transform.position, to, ref velocity, movementSpeed);
            Camera.main.transform.position = new Vector3(transform.position.x, transform.position.y + cameraYOffset, Camera.main.transform.position.z);
        }
        else if (Input.GetMouseButton(2)) // Middle mouse button held
        {
            Camera.main.transform.position += new Vector3(-Input.GetAxisRaw("Mouse X") * Time.deltaTime * mouseCameraSpeed, -Input.GetAxisRaw("Mouse Y") * Time.deltaTime * mouseCameraSpeed, 0f);
        }
        if (moving)
		{
            if ((Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0) && velocity.magnitude < 0.26 ||
                velocity.magnitude < 0.06)
            {
                moving = false;
                transform.position = to;
            }
		}
    }

    public void LeftRoom()
	{
        Assert.IsTrue(popupOpen);
        popupOpen = false;
	}

    void RevealRoomType(Vector2Int pos)
	{
        var map = GetMap(pos);
        var room = map.GetRoomWorldCoords(pos);
        if (room.attributeSprite == null)
            return;

        room.attributeSprite.GetComponent<RoomAttributeMarker>().RevealType();
    }

    public void RevealConnectedRoomsType()
	{
        var map = GetMap(roomPos);
        var room = map.GetRoomWorldCoords(roomPos);
        Assert.IsNotNull(room);
        Assert.IsTrue(room.type != SpaceshipMapGenerator.TileType.Void);
        //Debug.Log("Pos : " + pos + ", " + room.type);
        if (room.connects[0])
        {
            RevealRoomType(roomPos + new Vector2Int(0, 1));
        }
        if (room.connects[1])
        {
            RevealRoomType(roomPos + new Vector2Int(-1, 0));
        }
        if (room.connects[2])
        {
            RevealRoomType(roomPos + new Vector2Int(0, -1));
        }
        if (room.connects[3])
        {
            RevealRoomType(roomPos + new Vector2Int(1, 0));
        }
    }

    public void RevealConnectedRooms(int recursionLevel = 1)
	{
        RevealConnectedRooms(roomPos, tilePos, recursionLevel);
	}

    public void RevealConnectedRooms(Vector2Int pos, Vector2Int tilePos, int recursionLevel = 1)
	{
        if (recursionLevel <= 0)
            return;

        Debug.Log(tilePos + " / " + pos + " / converted : " + (pos - GetMap(pos).conversionOffset));

        var map = GetMap(pos);
        var room = map.GetRoomWorldCoords(pos);
        Assert.IsNotNull(room);
        Assert.IsTrue(room.type != SpaceshipMapGenerator.TileType.Void);
        if (room.type == SpaceshipMapGenerator.TileType.BossRoom)
            SharedData.bossRoomRevealed = true;

        if (room.connects[0])
        {
            fog.RevealGrid(tilePos + new Vector2Int(0, 1) - new Vector2Int(2, 2));
            fog.RevealGrid(tilePos + new Vector2Int(0, 2) - new Vector2Int(2, 2));
            RevealConnectedRooms(pos + new Vector2Int(0, 1), tilePos + new Vector2Int(0, 2), recursionLevel - 1);
        }
        if (room.connects[1])
        {
            fog.RevealGrid(tilePos + new Vector2Int(1, 0) - new Vector2Int(2, 2));
            fog.RevealGrid(tilePos + new Vector2Int(2, 0) - new Vector2Int(2, 2));
            RevealConnectedRooms(pos + new Vector2Int(-1, 0), tilePos + new Vector2Int(2, 0), recursionLevel - 1);
        }
        if (room.connects[2])
        {
            fog.RevealGrid(tilePos + new Vector2Int(0, -1) - new Vector2Int(2, 2));
            fog.RevealGrid(tilePos + new Vector2Int(0, -2) - new Vector2Int(2, 2));
            RevealConnectedRooms(pos + new Vector2Int(0, -1), tilePos + new Vector2Int(0, -2), recursionLevel - 1);
        }
        if (room.connects[3])
        {
            fog.RevealGrid(tilePos + new Vector2Int(-1, 0) - new Vector2Int(2, 2));
            fog.RevealGrid(tilePos + new Vector2Int(-2, 0) - new Vector2Int(2, 2));
            RevealConnectedRooms(pos + new Vector2Int(1, 0), tilePos + new Vector2Int(-2, 0), recursionLevel - 1);
        }
    }
}
