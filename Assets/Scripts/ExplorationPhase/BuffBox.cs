using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class BuffBox : MessageBox
{
	public GameObject dialogButtons;
	public TextMeshPro buff1Text;
	public TextMeshPro buff2Text;
	public TextMeshPro buff3Text;
	public SpriteRenderer icon1, icon2, icon3;

	public int buffPressed = 0;

	public BuffBox()
	{
		canBeClosed = false;
	}

	protected override void ShowImpl()
	{
	}

	protected override void FullyOpen()
	{
		dialogButtons.SetActive(true);

		EventSystem.current.GetComponent<EventSystem>().SetSelectedGameObject(null);
		if (dialogButtons.transform.Find("Buff1Button") != null)
			dialogButtons.transform.Find("Buff1Button").GetComponent<Button>().Select();
		if (dialogButtons.transform.Find("Rune1Button") != null)
			dialogButtons.transform.Find("Rune1Button").GetComponent<Button>().Select();
	}

	protected override void CloseImpl()
	{
		dialogButtons.SetActive(false);
	}

	public void BuffPressed(int idx)
	{
		buffPressed = idx;
	}

}
