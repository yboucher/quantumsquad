using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CombatDifficulty
{
    NotSet,
    Easy,
    Medium,
    Tough,
    Boss
}

public class CombatGenerator : MonoBehaviour
{
    public void GenerateAndEnter()
    {
        CombatDifficulty difficulty = CombatDifficulty.Easy;

        // Generate difficulty according to the distance to the boss room and/or the zone?
        if (SharedData.currentZone == 1)
		{
            int roll = Random.Range(0, 100);
            if (roll <= 30)
                difficulty = CombatDifficulty.Easy;
            else if (roll <= 80)
                difficulty = CombatDifficulty.Medium;
            else
                difficulty = CombatDifficulty.Tough;

		}
        else if (SharedData.currentZone == 2)
        {
            int roll = Random.Range(0, 100);
            if (roll <= 50)
                difficulty = CombatDifficulty.Medium;
            else
                difficulty = CombatDifficulty.Tough;

        }
        else if (SharedData.currentZone == 3)
        {
            int roll = Random.Range(0, 100);
            if (roll <= 30)
                difficulty = CombatDifficulty.Medium;
            else
                difficulty = CombatDifficulty.Tough;

        }

        GenerateAndEnter(difficulty);
    }
    public void GenerateAndEnter(CombatDifficulty difficulty)
    {
        SharedData.nextEnemyParty = null;
        SharedData.nextCombatDifficulty = difficulty;
        GameObject.FindObjectOfType<LevelLoader>().LoadLevelByName("CombatScene", true);
    }
    public void CombatPhaseGeneration()
	{
        Debug.Log("Generating fight of difficulty " + SharedData.nextCombatDifficulty);

        SharedData.nextEnemyParty = new List<Enemy>();
        switch (SharedData.nextCombatDifficulty)
		{
            case CombatDifficulty.NotSet:
                SharedData.nextEnemyParty = null;
                break;
            default:
            case CombatDifficulty.Easy:
                GenerateEasyEncounter();
                break;
            case CombatDifficulty.Medium:
                GenerateMediumEncounter();
                break;
            case CombatDifficulty.Tough:
                GenerateToughEncounter();
                break;
            case CombatDifficulty.Boss:
                GenerateBossEncounter();
                break;
        }
    }

    public void GenerateEasyEncounter()
	{
        int roll = Random.Range(0, 100);
        if (roll <= 33)
		{
            SharedData.nextEnemyParty.Add(Zone1Enemy1.Create());
            SharedData.nextEnemyParty.Add(Zone1Enemy2.Create());
        }
        else if (roll <= 33)
        {
            SharedData.nextEnemyParty.Add(Ecorcheur.Create());
            SharedData.nextEnemyParty.Add(Zone1Enemy1.Create());
        }
        else
        {
            SharedData.nextEnemyParty.Add(SentinelEnemy.Create());
            SharedData.nextEnemyParty.Add(Zone1Enemy2.Create());
        }
    }

    public void GenerateMediumEncounter()
    {
        int roll = Random.Range(0, 100);
        if (SharedData.currentZone == 1)
        {
            if (roll <= 20)
            {
                SharedData.nextEnemyParty.Add(Zone1Enemy1.Create());
                SharedData.nextEnemyParty.Add(Zone1Enemy2.Create());
                SharedData.nextEnemyParty.Add(SentinelEnemy.Create());
            }
            else if (roll <= 40)
            {
                SharedData.nextEnemyParty.Add(Zone1Enemy1.Create());
                SharedData.nextEnemyParty.Add(Zone1Enemy2.Create());
                SharedData.nextEnemyParty.Add(Ecorcheur.Create());
            }
            else if (roll <= 60)
            {
                SharedData.nextEnemyParty.Add(Zone1Enemy1.Create());
                SharedData.nextEnemyParty.Add(Zone1Enemy2.Create());
                SharedData.nextEnemyParty.Add(Zone1Enemy1.Create());
            }
            else if (roll <= 80)
            {
                SharedData.nextEnemyParty.Add(Zone1Enemy1.Create());
                SharedData.nextEnemyParty.Add(Ecorcheur.Create());
                SharedData.nextEnemyParty.Add(Ecorcheur.Create());
            }
            else
            {
                SharedData.nextEnemyParty.Add(Zone1Enemy1.Create());
                SharedData.nextEnemyParty.Add(SentinelEnemy.Create());
                SharedData.nextEnemyParty.Add(Ecorcheur.Create());
            }
        }
        else if (SharedData.currentZone == 2) // Zone 2
		{
            if (roll <= 20)
            {
                SharedData.nextEnemyParty.Add(Zone1Enemy1.Create());
                SharedData.nextEnemyParty.Add(Zone1Enemy2.Create());
                SharedData.nextEnemyParty.Add(RavageurEnemy.Create());
            }
            else if (roll <= 40)
            {
                SharedData.nextEnemyParty.Add(CommanderEnemy.Create());
                SharedData.nextEnemyParty.Add(ProtectorEnemy.Create());
                SharedData.nextEnemyParty.Add(Zone1Enemy2.Create());
            }
            else if (roll <= 60)
            {
                SharedData.nextEnemyParty.Add(SentinelEnemy.Create());
                SharedData.nextEnemyParty.Add(ProtectorEnemy.Create());
                SharedData.nextEnemyParty.Add(Zone1Enemy1.Create());
            }
            else if (roll <= 80)
            {
                SharedData.nextEnemyParty.Add(CommanderEnemy.Create());
                SharedData.nextEnemyParty.Add(Ecorcheur.Create());
                SharedData.nextEnemyParty.Add(ProtectorEnemy.Create());
            }
            else
            {
                SharedData.nextEnemyParty.Add(CommanderEnemy.Create());
                SharedData.nextEnemyParty.Add(SentinelEnemy.Create());
                SharedData.nextEnemyParty.Add(RavageurEnemy.Create());
            }
        }
        else // Zone 3
        {
            if (roll <= 20)
            {
                SharedData.nextEnemyParty.Add(Zone1Enemy1.Create());
                SharedData.nextEnemyParty.Add(RadBeastEnemy.Create());
                SharedData.nextEnemyParty.Add(RadBeastEnemy.Create());
            }
            else if (roll <= 40)
            {
                SharedData.nextEnemyParty.Add(RadBeastEnemy.Create());
                SharedData.nextEnemyParty.Add(LeechEnemy.Create());
                SharedData.nextEnemyParty.Add(Zone1Enemy2.Create());
            }
            else if (roll <= 60)
            {
                SharedData.nextEnemyParty.Add(ProtectorEnemy.Create());
                SharedData.nextEnemyParty.Add(LeechEnemy.Create());
                SharedData.nextEnemyParty.Add(LeechEnemy.Create());
            }
            else if (roll <= 80)
            {
                SharedData.nextEnemyParty.Add(LeechEnemy.Create());
                SharedData.nextEnemyParty.Add(RadBeastEnemy.Create());
                SharedData.nextEnemyParty.Add(RavageurEnemy.Create());
            }
            else
            {
                SharedData.nextEnemyParty.Add(Ecorcheur.Create());
                SharedData.nextEnemyParty.Add(LeechEnemy.Create());
                SharedData.nextEnemyParty.Add(LeechEnemy.Create());
            }
        }
    }

    public void GenerateToughEncounter()
    {
        int roll = Random.Range(0, 100);
        if (SharedData.currentZone == 1)
        {
            if (roll <= 33)
            {
                SharedData.nextEnemyParty.Add(Zone1Enemy1.Create());
                SharedData.nextEnemyParty.Add(Zone1Enemy2.Create());
                SharedData.nextEnemyParty.Add(Zone1Enemy1.Create());
                SharedData.nextEnemyParty.Add(Zone1Enemy2.Create());
            }
            else if (roll <= 33)
            {
                SharedData.nextEnemyParty.Add(Zone1Enemy1.Create());
                SharedData.nextEnemyParty.Add(Zone1Enemy2.Create());
                SharedData.nextEnemyParty.Add(SentinelEnemy.Create());
                SharedData.nextEnemyParty.Add(Ecorcheur.Create());
            }
            else
            {
                SharedData.nextEnemyParty.Add(Zone1Enemy1.Create());
                SharedData.nextEnemyParty.Add(Ecorcheur.Create());
                SharedData.nextEnemyParty.Add(SentinelEnemy.Create());
                SharedData.nextEnemyParty.Add(Ecorcheur.Create());
            }
        }
        else if (SharedData.currentZone == 2) // Zone 2
        {
            if (roll <= 33)
            {
                SharedData.nextEnemyParty.Add(CommanderEnemy.Create());
                SharedData.nextEnemyParty.Add(Zone1Enemy2.Create());
                SharedData.nextEnemyParty.Add(Zone1Enemy2.Create());
                SharedData.nextEnemyParty.Add(ProtectorEnemy.Create());
            }
            else if (roll <= 33)
            {
                SharedData.nextEnemyParty.Add(ProtectorEnemy.Create());
                SharedData.nextEnemyParty.Add(SentinelEnemy.Create());
                SharedData.nextEnemyParty.Add(SentinelEnemy.Create());
                SharedData.nextEnemyParty.Add(Zone1Enemy1.Create());
            }
            else
            {
                SharedData.nextEnemyParty.Add(RavageurEnemy.Create());
                SharedData.nextEnemyParty.Add(RavageurEnemy.Create());
                SharedData.nextEnemyParty.Add(Zone1Enemy2.Create());
                SharedData.nextEnemyParty.Add(Zone1Enemy2.Create());
            }
        }
        else // Zone 3
        {
            if (roll <= 33)
            {
                SharedData.nextEnemyParty.Add(CommanderEnemy.Create());
                SharedData.nextEnemyParty.Add(LeechEnemy.Create());
                SharedData.nextEnemyParty.Add(LeechEnemy.Create());
                SharedData.nextEnemyParty.Add(RadBeastEnemy.Create());
            }
            else if (roll <= 33)
            {
                SharedData.nextEnemyParty.Add(LeechEnemy.Create());
                SharedData.nextEnemyParty.Add(Zone1Enemy1.Create());
                SharedData.nextEnemyParty.Add(LeechEnemy.Create());
                SharedData.nextEnemyParty.Add(Ecorcheur.Create());
            }
            else
            {
                SharedData.nextEnemyParty.Add(RavageurEnemy.Create());
                SharedData.nextEnemyParty.Add(Zone1Enemy1.Create());
                SharedData.nextEnemyParty.Add(Zone1Enemy2.Create());
                SharedData.nextEnemyParty.Add(RadBeastEnemy.Create());
            }
        }
    }

    public void GenerateBossEncounter()
    {
        if (SharedData.currentZone == 1)
        {
            SharedData.nextEnemyParty.Add(Boss1.Create());
            // Easy mode is too easy if the boss is all alone
            SaveData saveData = SaveData.Load();
            if (saveData.difficultyMode == 0)
            {
                SharedData.nextEnemyParty.Add(Zone1Enemy1.Create());
                SharedData.nextEnemyParty.Add(Ecorcheur.Create());
            }
        }
        else if (SharedData.currentZone == 2) // Zone 2
        {
            SharedData.nextEnemyParty.Add(Boss2.Create());
            SharedData.nextEnemyParty.Add(CommanderEnemy.Create());
            //SharedData.nextEnemyParty.Add(ProtectorEnemy.Create());
        }
        else // Zone 3
        {
            SharedData.nextEnemyParty.Add(Boss3.Create());
            //SharedData.nextEnemyParty.Add(LeechEnemy.Create());
            SharedData.nextEnemyParty.Add(RadBeastEnemy.Create());
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
