using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomAttributeMarker : MonoBehaviour
{
    public float hideStartTime = -1;
    public float hideDuration = 0.5f;
    public float revealStartTime = -1;

    public Room roomObject;

    //bool revealed = false;
    Sprite interrogationSprite;
    Color interrogationColor;

    // Start is called before the first frame update
    void Start()
    {
        interrogationSprite = GetComponent<SpriteRenderer>().sprite;
        interrogationColor = GetComponent<SpriteRenderer>().color;
    }

    // Update is called once per frame
    void Update()
    {
        if (hideStartTime >= 0)
		{
            GetComponent<SpriteRenderer>().color = new Color(GetComponent<SpriteRenderer>().color.r,
                                                             GetComponent<SpriteRenderer>().color.g,
                                                             GetComponent<SpriteRenderer>().color.b,
                                                             Mathf.Lerp(1, 0, Mathf.Max(0, (Time.time - hideStartTime)/hideDuration)));
            if (Time.time - hideStartTime > hideDuration)
            {
                Destroy(gameObject);
            }
        }
        if (revealStartTime >= 0)
        {
            GetComponent<SpriteRenderer>().color = new Color(GetComponent<SpriteRenderer>().color.r,
                                                             GetComponent<SpriteRenderer>().color.g,
                                                             GetComponent<SpriteRenderer>().color.b,
                                                             Mathf.Lerp(0, 1, Mathf.Max(0, (Time.time - revealStartTime) / hideDuration)));

            if (Time.time - revealStartTime > hideDuration)
            {
                revealStartTime = -1;
            }
        }
    }

    // Show the room type icon instead of the interrogation mark
    public void RevealType()
	{
        //if (revealed)
        //    return;

        //if (roomObject.icon == null)
        //    return;
        if (roomObject != null && roomObject.icon != null)
        {
            GetComponent<SpriteRenderer>().sprite = roomObject.icon;
            GetComponent<SpriteRenderer>().color = roomObject.iconColor;
            revealStartTime = Time.time;
            //revealed = true;
        }
        else
		{
            Hide();
		}
	}

    public void Unreveal()
	{
        GetComponent<SpriteRenderer>().sprite = interrogationSprite;
        GetComponent<SpriteRenderer>().color = interrogationColor;
    }

    public void Hide()
	{
        hideStartTime = Time.time;
	}
}
