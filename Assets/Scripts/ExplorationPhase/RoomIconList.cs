using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomIconList : MonoBehaviour
{
    public Sprite healIcon;
    public Sprite combatIcon;
    public Sprite curioIcon;
    public Sprite buffIcon;
    public Sprite skullIcon;
    public Sprite moneyIcon;
    public Sprite fireIcon;
    public Sprite lootIcon;
    public Sprite eyeIcon;
    public Sprite signpostIcon;
    public Sprite lockedIcon;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
