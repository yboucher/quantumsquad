using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.Assertions;

public class SpaceshipMapGenerator : MonoBehaviour
{
    public Tilemap tilemap;
    public Tile roomTile;
    public Tile straightCorridor;
    public Tile bossTile;
    public Tile elbowTile;
    public Tile TTile;
    public Tile crossTile;
    public RectInt generationRect;
    public GameObject attributeSpriteBase;
    public Orientation bossRoomSide;
    public Vector2Int conversionOffset = Vector2Int.zero;
    public enum Orientation
	{
        North = 0,
        West = 1,
        South = 2,
        East = 3
	}

    public enum TileType
	{
        Void,
        Room,
        EntranceRoom,
        BossRoom,
        Connector
	}

    public class Room
	{
        public bool[] connects = new bool[4];
        public TileType type = TileType.Void;
        public bool explored = false;

        public bool traversedByAlgorithm = false;
        public int markedDistance = 9999;
        public GameObject attributeSprite;
	}

    public Vector2Int roomCountSize;
    private Room[,] rooms;
    private Vector2Int startingPos;
    private Vector2Int bossPos;
    private Queue<Vector2Int> bfsQueue = new Queue<Vector2Int>();

    public Vector2Int StartRoomPos()
    { return startingPos; }
    public Vector2Int BossRoomPos()
    { return bossPos; }

    public Room GetRoomWorldCoords(Vector2Int pos)
	{
        pos -= conversionOffset;
        return GetRoom(pos);
	}
    public Room GetRoom(Vector2Int pos)
	{
        if (pos.x < 0 || pos.y < 0 || pos.x >= roomCountSize.x || pos.y >= roomCountSize.y)
            return null;

        return rooms[pos.x, pos.y];
	}

    public Vector3 RoomWorldCoordinates(Vector2Int pos)
	{
        if (GetRoom(pos) == null)
            return Vector3.zero;
        var tilePos = new Vector3Int(pos.x * 2 + generationRect.xMin, pos.y * 2 + generationRect.yMin, 0);
        return tilemap.GetCellCenterWorld(new Vector3Int(tilePos.x, tilePos.y, 0));
    }

    // Start is called before the first frame update
    void Start()
    {
        roomCountSize = new Vector2Int((generationRect.width+1)/2, (generationRect.height+1)/2);

        rooms = new Room[roomCountSize.x, roomCountSize.y];

        for (int i = 0; i < roomCountSize.x; ++i)
            for (int j = 0; j < roomCountSize.y; ++j)
            {
                var room = rooms[i, j] = new Room();
                room.connects[0] = j < roomCountSize.y - 1; // North
                room.connects[1] = i > 0; // West
                room.connects[2] = j > 0; // South
                room.connects[3] = i < roomCountSize.x - 1; // East
                room.type = TileType.Room;
                room.traversedByAlgorithm = false;
                room.attributeSprite = GameObject.Instantiate(attributeSpriteBase, transform);
                room.attributeSprite.SetActive(true);
                room.attributeSprite.transform.position = RoomWorldCoordinates(new Vector2Int(i, j));
                room.attributeSprite.GetComponent<SpriteRenderer>().enabled = false;
            }

        var timeStart = Time.realtimeSinceStartup;
        int iterations = 0;
        do
        {
            RandomGeneration();
            ++iterations;
        }
        while (!GeneratedMapValid());
        var timeTaken = Time.realtimeSinceStartup - timeStart;
        Debug.Log("Time taken : " + timeTaken + ", Iterations : " + iterations + ", Paths : " + CountPaths(bossPos, new Vector2Int(roomCountSize.x - 1, 2)));

        GenerateTiles();
        GenerateCorridors();
    }

    int Connections(Room room)
	{
        return (room.connects[0] ? 1 : 0) + (room.connects[1] ? 1 : 0) +
               (room.connects[2] ? 1 : 0) + (room.connects[3] ? 1 : 0);
    }

    bool GeneratedMapValid()
	{
        // Boss room should be accessible from two different sides at least
        if (Connections(rooms[bossPos.x, bossPos.y]) < 2)
            return false;

        // No room should be further than four times the largest side of the area away from the boss
        // Also test if some rooms have 3 consecutive fights
        MarkDistances(bossPos);
        for (int i = 0; i < roomCountSize.x; ++i)
            for (int j = 0; j < roomCountSize.y; ++j)
            {
                if (rooms[i, j].type != TileType.Room)
                    continue;
                if (rooms[i, j].markedDistance > 4 * Mathf.Max(roomCountSize.x, roomCountSize.y))
                    return false;
            }

        // Random generation criteria:
        // Have at least 14 different paths to the boss tile
        if (CountPaths(bossPos, startingPos) < 14)
            return false;

        // TODO : test that there is enough distance between two nodes connected to the boss room

        // Each boss corridor should be reachable independantly, without going through the boss tile
        
        rooms[bossPos.x, bossPos.y].traversedByAlgorithm = true;
        if (rooms[bossPos.x, bossPos.y].connects[0])
            if (CountPaths(bossPos + new Vector2Int(0, 1), startingPos) == 0)
                return false;
        rooms[bossPos.x, bossPos.y].traversedByAlgorithm = true;
        if (rooms[bossPos.x, bossPos.y].connects[1])
            if (CountPaths(bossPos + new Vector2Int(-1, 0), startingPos) == 0)
                return false;
        rooms[bossPos.x, bossPos.y].traversedByAlgorithm = true;
        if (rooms[bossPos.x, bossPos.y].connects[2])
            if (CountPaths(bossPos + new Vector2Int(0, -1), startingPos) == 0)
                return false;
        rooms[bossPos.x, bossPos.y].traversedByAlgorithm = true;
        if (rooms[bossPos.x, bossPos.y].connects[3])
            if (CountPaths(bossPos + new Vector2Int(1, 0), startingPos) == 0)
                return false;
        

        foreach (var room in rooms)
            room.traversedByAlgorithm = false;

        return true;
	}

    void RandomGeneration()
	{
        for (int i = 0; i < roomCountSize.x; ++i)
            for (int j = 0; j < roomCountSize.y; ++j)
            {
                var room = rooms[i, j];
                room.connects[0] = j < roomCountSize.y - 1; // North
                room.connects[1] = i > 0; // West
                room.connects[2] = j > 0; // South
                room.connects[3] = i < roomCountSize.x - 1; // East
                room.type = TileType.Room;
                room.traversedByAlgorithm = false;
            }

        switch (bossRoomSide)
		{
            case Orientation.South:
                bossPos = new Vector2Int(Random.Range(0, generationRect.size.x - 1) / 2, 0);
                startingPos = new Vector2Int(((generationRect.size.x - 1) / 2) / 2, roomCountSize.y - 1);
                break;
            case Orientation.North:
                bossPos = new Vector2Int(Random.Range(0, generationRect.size.x - 1) / 2, roomCountSize.y - 1);
                startingPos = new Vector2Int(((generationRect.size.x - 1) / 2) / 2, 0);
                break;
            case Orientation.East:
                bossPos = new Vector2Int(0, Random.Range(0, generationRect.size.y - 1) / 2);
                startingPos = new Vector2Int((generationRect.size.x - 1) / 2, ((generationRect.size.y - 1) / 2) / 2);
                break;
            case Orientation.West:
                bossPos = new Vector2Int(roomCountSize.x - 1, Random.Range(0, generationRect.size.y - 1) / 2);
                startingPos = new Vector2Int(0, ((generationRect.size.y - 1) / 2) / 2);
                break;
        }
        rooms[bossPos.x, bossPos.y].type = TileType.BossRoom;
        rooms[startingPos.x, startingPos.y].type = TileType.EntranceRoom;

        int iters = 20;
        while (iters > 0)
        {
            --iters;

            int i = Random.Range(0, roomCountSize.x-1);
            int j = Random.Range(0, roomCountSize.y-1);
            if (rooms[i, j].type == TileType.Room)
            {
                // Make sure we never remove the starting position!
                if (Random.Range(0, 3) == 0 && new Vector2Int(i, j) != startingPos)
                    RemoveRoom(new Vector2Int(i, j));
                else
				{
                    // Remove connection if at least 1 connection will remain
                    if (Connections(rooms[i, j]) >= 2)
                        RemoveConnection(new Vector2Int(i, j), (Orientation)Random.Range(0, 3));
                }
            }
        }
        //Debug.Log("Iters : " + iters);

        // Convert some rooms to connectors
        int corridors = Random.Range(3, 6);
        int iterationLimit = 1000;
        do
        {
            int i = Random.Range(0, roomCountSize.x - 1);
            int j = Random.Range(0, roomCountSize.y - 1);

            // don't turn the starting position into a corridor either
            if (Connections(rooms[i, j]) >= 2 && rooms[i, j].type == TileType.Room && new Vector2Int(i, j) != startingPos)
            {
                rooms[i, j].type = TileType.Connector;
                --corridors;
            }
        } while (corridors > 0 && --iterationLimit > 0);
    }
    void RemoveConnection(Vector2Int roomCoord, Orientation dir)
	{
        Room room = rooms[roomCoord.x,roomCoord.y];
        switch (dir)
		{
            case Orientation.North:
                if (room.connects[0])
				{
                    var neighbor = rooms[roomCoord.x, roomCoord.y + 1];
                    //Assert.IsTrue(neighbor.connects[2]);
                    room.connects[0] = neighbor.connects[2] = false;
				}
                break;
            case Orientation.South:
                if (room.connects[2])
                {
                    var neighbor = rooms[roomCoord.x, roomCoord.y - 1];
                    //Assert.IsTrue(neighbor.connects[0]);
                    room.connects[2] = neighbor.connects[0] = false;
                }
                break;
            case Orientation.West:
                if (room.connects[1])
                {
                    var neighbor = rooms[roomCoord.x - 1, roomCoord.y];
                    //Assert.IsTrue(neighbor.connects[3]);
                    room.connects[1] = neighbor.connects[3] = false;
                }
                break;
            case Orientation.East:
                if (room.connects[3])
                {
                    var neighbor = rooms[roomCoord.x + 1, roomCoord.y];
                    //Assert.IsTrue(neighbor.connects[1]);
                    room.connects[3] = neighbor.connects[1] = false;
                }
                break;
        }
	}
    
    // BFS algorithm to compute distance between two rooms
    void MarkDistances(Vector2Int from)
	{
        foreach (var room in rooms)
        {
            room.markedDistance = 9999;
            room.traversedByAlgorithm = false;
        }

        var queue = bfsQueue;
        queue.Clear();
        queue.Enqueue(from);

        int distance = 0;
        while (queue.Count > 0)
        {
            var tilePos = queue.Dequeue();
            if (rooms[tilePos.x, tilePos.y].traversedByAlgorithm)
                continue;
            rooms[tilePos.x, tilePos.y].markedDistance = distance;

            rooms[tilePos.x, tilePos.y].traversedByAlgorithm = true;

            if (rooms[tilePos.x, tilePos.y].connects[0])
                queue.Enqueue(tilePos + new Vector2Int(0, 1));
            if (rooms[tilePos.x, tilePos.y].connects[1])
                queue.Enqueue(tilePos + new Vector2Int(-1, 0));
            if (rooms[tilePos.x, tilePos.y].connects[2])
                queue.Enqueue(tilePos + new Vector2Int(0, -1));
            if (rooms[tilePos.x, tilePos.y].connects[3])
                queue.Enqueue(tilePos + new Vector2Int(1, 0));

            ++distance;
        }

        foreach (var room in rooms)
            room.traversedByAlgorithm = false;
    }
    int CountPaths(Vector2Int from, Vector2Int to)
	{
        int counter = 0;
        CountPathsRecurse(from, to, ref counter);

        foreach (var room in rooms)
            room.traversedByAlgorithm = false;

        return counter;
	}

    void CountPathsRecurse(Vector2Int from, Vector2Int to, ref int counter)
	{
        var room = rooms[from.x, from.y];
        if (from == to)
            ++counter;
        else
        {
            //Assert.IsFalse(room.traversedByAlgorithm);

            room.traversedByAlgorithm = true;

            if (room.connects[0] && !rooms[from.x, from.y + 1].traversedByAlgorithm)
                CountPathsRecurse(from + new Vector2Int(0, 1), to, ref counter);
            if (room.connects[1] && !rooms[from.x - 1, from.y].traversedByAlgorithm)
                CountPathsRecurse(from + new Vector2Int(-1, 0), to, ref counter);
            if (room.connects[2] && !rooms[from.x, from.y - 1].traversedByAlgorithm)
                CountPathsRecurse(from + new Vector2Int(0, -1), to, ref counter);
            if (room.connects[3] && !rooms[from.x + 1, from.y].traversedByAlgorithm)
                CountPathsRecurse(from + new Vector2Int(1, 0), to, ref counter);
        }
        room.traversedByAlgorithm = false;
    }

    void RemoveRoom(Vector2Int roomCoord)
	{
        RemoveConnection(roomCoord, Orientation.North);
        RemoveConnection(roomCoord, Orientation.West);
        RemoveConnection(roomCoord, Orientation.South);
        RemoveConnection(roomCoord, Orientation.East);
        rooms[roomCoord.x, roomCoord.y].type = TileType.Void;
    }

    void GenerateCorridors()
	{
        for (int i = 0; i < roomCountSize.x; ++i)
            for (int j = 0; j < roomCountSize.y; ++j)
            {
                // For each tile, generate right and up connections
                if (rooms[i,j].connects[0])
				{
                    var pos = new Vector3Int(i * 2 + generationRect.xMin, 1 + j * 2 + generationRect.yMin, 0);
                    tilemap.SetTile(pos, straightCorridor);
                    tilemap.SetTransformMatrix(pos, Matrix4x4.Rotate(Quaternion.Euler(0, 0f, 0f)));
                }
                if (rooms[i, j].connects[3])
                {
                    var pos = new Vector3Int(1 + i * 2 + generationRect.xMin, j * 2 + generationRect.yMin, 0);
                    tilemap.SetTile(pos, straightCorridor);
                    tilemap.SetTransformMatrix(pos, Matrix4x4.Rotate(Quaternion.Euler(0, 0f, 90f)));
                }
            }
    }

    void GenerateConnectionTile(Vector2Int roomPos)
    {
        var pos = new Vector3Int(roomPos.x * 2 + generationRect.xMin, roomPos.y * 2 + generationRect.yMin, 0);
        var room = rooms[roomPos.x, roomPos.y];

        if (room.connects[0] && room.connects[1] && room.connects[2] && room.connects[3])
        { 
            tilemap.SetTile(pos, crossTile);
        }
        // T, west unconnected
        else if (room.connects[0] && !room.connects[1] && room.connects[2] && room.connects[3])
		{
            tilemap.SetTile(pos, TTile);
            tilemap.SetTransformMatrix(pos, Matrix4x4.Rotate(Quaternion.Euler(0, 0f, 0f)));
        }
        // T, north unconnected
        else if (!room.connects[0] && room.connects[1] && room.connects[2] && room.connects[3])
        {
            tilemap.SetTile(pos, TTile);
            tilemap.SetTransformMatrix(pos, Matrix4x4.Rotate(Quaternion.Euler(0, 0f, 270f)));
        }
        // T, east unconnected
        else if (room.connects[0] && room.connects[1] && room.connects[2] && !room.connects[3])
        {
            tilemap.SetTile(pos, TTile);
            tilemap.SetTransformMatrix(pos, Matrix4x4.Rotate(Quaternion.Euler(0, 0f, 180f)));
        }
        // T, south unconnected
        else if (room.connects[0] && room.connects[1] && !room.connects[2] && room.connects[3])
        {
            tilemap.SetTile(pos, TTile);
            tilemap.SetTransformMatrix(pos, Matrix4x4.Rotate(Quaternion.Euler(0, 0f, 90f)));
        }
        // Elbow, connects SE
        else if (!room.connects[0] && !room.connects[1] && room.connects[2] && room.connects[3])
        {
            tilemap.SetTile(pos, elbowTile);
            tilemap.SetTransformMatrix(pos, Matrix4x4.Rotate(Quaternion.Euler(0, 0f, 0f)));
        }
        // Elbow, connects SW
        else if (!room.connects[0] && room.connects[1] && room.connects[2] && !room.connects[3])
        {
            tilemap.SetTile(pos, elbowTile);
            tilemap.SetTransformMatrix(pos, Matrix4x4.Rotate(Quaternion.Euler(0, 0f, 270f)));
        }
        // Elbow, connects NW
        else if (room.connects[0] && room.connects[1] && !room.connects[2] && !room.connects[3])
        {
            tilemap.SetTile(pos, elbowTile);
            tilemap.SetTransformMatrix(pos, Matrix4x4.Rotate(Quaternion.Euler(0, 0f, 180f)));
        }
        // Elbow, connects NE
        else if (room.connects[0] && !room.connects[1] && !room.connects[2] && room.connects[3])
        {
            tilemap.SetTile(pos, elbowTile);
            tilemap.SetTransformMatrix(pos, Matrix4x4.Rotate(Quaternion.Euler(0, 0f, 90f)));
        }
        // Straifght, NS
        else if (room.connects[0] && !room.connects[1] && room.connects[2] && !room.connects[3])
        {
            tilemap.SetTile(pos, straightCorridor);
            tilemap.SetTransformMatrix(pos, Matrix4x4.Rotate(Quaternion.Euler(0, 0f, 0f)));
        }
        // Straifght, EW
        else if (!room.connects[0] && room.connects[1] && !room.connects[2] && room.connects[3])
        {
            tilemap.SetTile(pos, straightCorridor);
            tilemap.SetTransformMatrix(pos, Matrix4x4.Rotate(Quaternion.Euler(0, 0f, 90f)));
        }
        else
		{
            Debug.LogWarning("Unsupported connection type at coordinates " + roomPos + " : " + room.connects[0] + ", " + room.connects[1] + ", " +
                                                                                        room.connects[2] + ", " + room.connects[3]);
            tilemap.SetTile(pos, null);
		}
	}

    void GenerateTiles()
	{

        // Generate rooms
        for (int i = 0; i < roomCountSize.x; ++i)
            for (int j = 0; j < roomCountSize.y; ++j)
			{
                var pos = new Vector3Int(i * 2 + generationRect.xMin, j * 2 + generationRect.yMin, 0);
                switch(rooms[i,j].type)
				{
                    case TileType.EntranceRoom:
                    case TileType.Room:
                        if (rooms[i, j].type != TileType.EntranceRoom) // not the starting area
                            rooms[i, j].attributeSprite.GetComponent<SpriteRenderer>().enabled = true;
                        tilemap.SetTile(pos, roomTile);
                        break;
                    case TileType.BossRoom:
                        tilemap.SetTile(pos, bossTile);
                        break;
                    case TileType.Void:
                        tilemap.SetTile(pos, null);
                        break;
                    case TileType.Connector:
                        GenerateConnectionTile(new Vector2Int(i, j));
                        break;
                }
			}

    }

    // Update is called once per frame
    void Update()
    {
    }
}
