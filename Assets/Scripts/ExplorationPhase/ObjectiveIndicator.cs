using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ObjectiveIndicator : MonoBehaviour
{
    public TextMeshPro text;

    // Start is called before the first frame update
    void Start()
    {

    }

    public string GetObjectiveText()
	{
        string text = "";
        switch (SharedData.bossesSlain)
        {
            default:
            case 0:
                text += "Vaincre le <color=red>Seigneur de guerre</color> aux <color=green>Quartiers des aliens";
                break;
            case 1:
                text += "Vaincre le <color=red>Getal Mear</color> au <color=green>QG de Megotron";
                break;
            case 2:
                text += "Vaincre le <color=red>Nayzaka</color> dans le <color=green>Labo corrompu";
                break;
            case 3:
                text += "Vaincre l'<color=red>empereur quantique</color> dans les <color=green>Halls de l'empereur";
                break;
            case 4:
                text += "F�licitations, vous avez sauv� l'univers (rien que �a) !";
                break;
        }

        return text;
    }

    // Update is called once per frame
    void Update()
    {
        text.text = "Objectif :\n" + GetObjectiveText();
    }
}
