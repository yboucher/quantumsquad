using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using System.Linq;

public class BossRoom : Room
{
	RuneBox runeBox;
	List<BossRune> masterRuneList, runeList;

	public BossRoom()
	{
		masterRuneList = new List<BossRune>();

		masterRuneList.Add(new BerserkRune()); // OK Works
		masterRuneList.Add(new ComboRune()); // OK works
		masterRuneList.Add(new VengeanceRune()); // OK Works
		masterRuneList.Add(new CoffeeRune()); // OK Works
		masterRuneList.Add(new LastBreathRune()); // OK works
		masterRuneList.Add(new VigilanceRune()); //
		masterRuneList.Add(new WisdomRune()); // OK works
		masterRuneList.Add(new AssaultRune()); // OK works
		masterRuneList.Add(new DoubleEdgeRune()); // OK Works

		runeBox = GameObject.FindObjectOfType<RuneBox>();

		icon = GameObject.FindObjectOfType<RoomIconList>().buffIcon;
		iconColor = Color.green;
	}

	public override void EnterImpl()
	{
		Assert.IsTrue(masterRuneList.Count > 0);
		var combatRune = masterRuneList.FindAll(x => x.Type == RuneType.Combat).OrderBy(a => Random.Range(0, 100000000)).FirstOrDefault();
		var survivalRune = masterRuneList.FindAll(x => x.Type == RuneType.Survival).OrderBy(a => Random.Range(0, 100000000)).FirstOrDefault();
		var tacticsRune = masterRuneList.FindAll(x => x.Type == RuneType.Tactics).OrderBy(a => Random.Range(0, 100000000)).FirstOrDefault();
		runeList = new List<BossRune> { combatRune, survivalRune, tacticsRune };

		runeBox.buff1Text.text = "<color=red><align=center>" + combatRune.Name + "</align></color>\n\n";
		runeBox.buff1Text.text += combatRune.Description;

		runeBox.buff2Text.text = "<color=red><align=center>" + survivalRune.Name + "</align></color>\n\n";
		runeBox.buff2Text.text += survivalRune.Description;

		runeBox.buff3Text.text = "<color=red><align=center>" + tacticsRune.Name + "</align></color>\n\n";
		runeBox.buff3Text.text += tacticsRune.Description;

		runeBox.buffPressed = -1;
		runeBox.Show();

		PlaceIcon(runeBox.buff1Text, runeBox.icon1);
		PlaceIcon(runeBox.buff2Text, runeBox.icon2);
		PlaceIcon(runeBox.buff3Text, runeBox.icon3);
		runeBox.icon1.sprite = GameObject.FindObjectOfType<RoomIconList>().skullIcon; runeBox.icon1.color = Color.white;
		runeBox.icon2.sprite = GameObject.FindObjectOfType<RoomIconList>().healIcon; runeBox.icon2.color = Color.red;
		runeBox.icon3.sprite = GameObject.FindObjectOfType<RoomIconList>().buffIcon; runeBox.icon3.color = Color.blue;

		GameObject.Find("GlobalScripts").GetComponent<CoroutineUtil>().StartCoroutine(WaitUntilClosed());
	}

	void PlaceIcon(TMPro.TextMeshPro text, SpriteRenderer icon)
	{
		text.ForceMeshUpdate(true, true);
		text.GetTextInfo(text.text); // Force the textInfo field to be regenerated
		Vector3 localCharPos = (text.textInfo.characterInfo[0].topLeft + text.textInfo.characterInfo[0].bottomLeft) / 2
						- new Vector3(0.30f, 0, 0);
		icon.transform.position = text.transform.TransformPoint(localCharPos);
	}

	IEnumerator WaitUntilClosed()
	{
		yield return new WaitUntil(() => runeBox.buffPressed > 0);

		Debug.Log("Chose rune " + runeList[runeBox.buffPressed - 1].Name);

		foreach (var soldier in SharedData.soldiers)
		{
			soldier.BossRunes.Clear();
			soldier.BossRunes.Add(runeList[runeBox.buffPressed - 1]);
		}

		runeBox.Close();

		var loader = GameObject.FindObjectOfType<LevelLoader>();

		GameObject.FindObjectOfType<CombatGenerator>().GenerateAndEnter(CombatDifficulty.Boss);

		Leave();
	}

	/*
	public override void EnterImpl()
	{
		msgBox.text.text = "Dans cette salle se trouve le boss de la zone. Pr�parez-vous au combat !";
		msgBox.Show();

		GameObject.Find("GlobalScripts").GetComponent<CoroutineUtil>().StartCoroutine(WaitUntilClosed());
	}

	IEnumerator WaitUntilClosed()
	{
		yield return new WaitUntil(() => msgBox.closing);

		GameObject.FindObjectOfType<CombatGenerator>().GenerateAndEnter(CombatDifficulty.Boss);

		Leave();
	}
	*/
}

public enum RuneType
{
	Combat,
	Survival,
	Tactics
}

public class BossRune
{
	public RuneType Type;
	public string Name;
	public string Description;
	public BossRune(RuneType Type, string Name, string Description)
	{
		this.Type = Type;
		this.Name = Name;
		this.Description = Description;
	}
}

public class BerserkRune : BossRune
{
	public BerserkRune() : base(RuneType.Combat, "Berserk", "Lorsqu�un personnage poss�de moins de 20% de ses HP max., celui-ci inflige 40% de d�gats supp.")
	{ }
}

public class ComboRune : BossRune
{
	public ComboRune() : base(RuneType.Combat, "Encha�nement", "Chaque attaque cons�cutive octroie � l��quipe un bonus de d�g�ts de 5%. Le boss obtient un bonus de +15% de d�gats si un coup est rat�.")
	{ }
}

public class VengeanceRune : BossRune
{
	public VengeanceRune() : base(RuneType.Combat, "Vengeance", "+10 / +20 / +40 % de d�g�ts suppl�mentaires si 1 / 2 / 3 soldats sont morts respectivement. -20 % de d�g�ts si l��quipe est au complet.")
	{ }
}

public class CoffeeRune : BossRune
{
	public CoffeeRune() : base(RuneType.Survival, "Pause caf�", "Soigne 20 PV � tous les membres de l��quipe.")
	{ }
}

public class LastBreathRune : BossRune
{
	public LastBreathRune() : base(RuneType.Survival, "Dernier souffle", "Si un alli� subit un coup fatal : le personnage reste en vie avec 20% de ses PV.")
	{ }
}
public class VigilanceRune : BossRune
{
	public VigilanceRune() : base(RuneType.Survival, "Vigilance", "100% de chance d�esquiver le 1er coup critique du boss, mais celui-ci voit sa chance de coup critique augment�e de 5%.")
	{ }
}

public class WisdomRune : BossRune
{
	public WisdomRune() : base(RuneType.Tactics, "Sagesse", "Bonus de +20% de pr�cision, malus de -10% de d�gats inflig� � l�ennemi.")
	{ }
}

public class AssaultRune : BossRune
{
	public AssaultRune() : base(RuneType.Tactics, "Assaut", "Le boss subit 15% de d�gats suppl�mentaires si celui-ci poss�de un d�buff.")
	{ }
}

public class DoubleEdgeRune : BossRune
{
	public DoubleEdgeRune() : base(RuneType.Tactics, "Double tranchant", "Les attaques �l�mentaires infligent 10% de d�g�ts suppl�mentaires. Si le joueur n�a effectu� aucune attaque �l�mentaire dans le tour, tous les membres de l��quipesubissent 50 d�g�ts.")
	{ }
}