using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockedRoom : Room
{
	public LockedRoom()
	{
        icon = GameObject.FindObjectOfType<RoomIconList>().lockedIcon;
		iconColor = Color.red;
	}

	public override bool CanEnter()
	{
        return false;
	}

	public override void EnterImpl()
	{
	}
}
