using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class Room
{
	public string Name = "<MISSINGNAME>";
	public Sprite icon = null;
	public Color iconColor = Color.white;

	protected MessageBox msgBox;
	protected MessageBox lootMsgBox;
	protected DialogBox dialogBox;
	bool entered = false;

	public Room()
	{
		msgBox = GameObject.Find("MessageBox").GetComponent<MessageBox>();
		lootMsgBox = GameObject.Find("LootMsgBox").GetComponent<MessageBox>();
		dialogBox = GameObject.Find("DialogBox").GetComponent<DialogBox>();
		Assert.IsNotNull(msgBox);
		Assert.IsNotNull(dialogBox);
	}

	public bool Entered()
	{
		return entered;
	}
	public void Enter()
	{
		entered = true;

		dialogBox.acceptedText.text = "Utiliser";
		dialogBox.refusedText.text = "Partir";

		EnterImpl();
	}

	public virtual void EnterImpl()
	{ }

	public virtual bool CanEnter()
	{ return true;  }

	protected void Leave()
	{
		// Ensure that all dead soldiers as a result of the room's effects are removed
		SharedData.soldiers.RemoveAll(x => x.HP <= 0);

		GameObject.FindObjectOfType<SquadIndicator>().LeftRoom();
	}

	protected virtual void LeaveImpl()
	{ }
}
