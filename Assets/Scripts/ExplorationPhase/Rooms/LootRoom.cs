using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootRoom : Room
{
	public LootRoom()
	{
		icon = GameObject.FindObjectOfType<RoomIconList>().lootIcon;
		iconColor = Color.yellow;
	}
	public override void EnterImpl()
	{
        GameObject.Find("CurioSuccess").GetComponent<AudioSource>().Play();

        string text = "";
        List<Item> items = new List<Item>();

        // Generate money
        int money = Random.Range(80, 250);
        var saveData = SaveData.Load();
        saveData.gold += money;
        saveData.Save();

        // Generate NFT
        if (Random.Range(0, 100) <= 50)
            items.Add(new NFTItem());
        // Generate healing
        if (Random.Range(0, 100) <= 40)
        {
            if (Random.Range(0, 100) <= 70)
                items.Add(new MedKitItem());
            else
                items.Add(new HealHerbs());
        }
        // Generate extra
        if (Random.Range(0, 100) <= 60)
        {
            items.Add(Item.GenerateRandomItem());
        }

        text = "Vous trouvez un coffre rempli d'objets intéressants :\n";

        if (money > 0)
            text += "\n<color=yellow>+" + money + " cryptomonnaie\n";
        foreach (var item in items)
        {
            text += "\n<color=green>+1 " + item.name + "</color>";
            SharedData.sharedInventory.Add(item);
        }

        lootMsgBox.text.text = text;
        lootMsgBox.Show();

		GameObject.Find("GlobalScripts").GetComponent<CoroutineUtil>().StartCoroutine(WaitUntilClosed());
	}

	IEnumerator WaitUntilClosed()
	{
        yield return new WaitUntil(() => lootMsgBox.closing);

		Leave();
	}
}
