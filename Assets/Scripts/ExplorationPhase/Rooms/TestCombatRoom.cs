using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestCombatRoom : Room
{
	public TestCombatRoom()
	{
		icon = GameObject.FindObjectOfType<RoomIconList>().combatIcon;
		iconColor = Color.white;
	}
	public override void EnterImpl()
	{
		GameObject.Find("AlarmSound").GetComponent<AudioSource>().Play();

		msgBox.text.text = "Des aliens patrouillent dans cette salle.\nC'est l'heure du combat !";
		msgBox.Show();

		GameObject.Find("GlobalScripts").GetComponent<CoroutineUtil>().StartCoroutine(WaitUntilClosed());
	}

	IEnumerator WaitUntilClosed()
	{
		yield return new WaitUntil(() => msgBox.closing);

		GameObject.FindObjectOfType<CombatGenerator>().GenerateAndEnter();

		Leave();
	}
}
