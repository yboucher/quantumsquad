using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealRoom : Room
{
	const float chancePenality = 20;
	float rollChance = 80;

	public HealRoom()
	{
		icon = GameObject.FindObjectOfType<RoomIconList>().healIcon;
		iconColor = Color.red;
	}

	public override void EnterImpl()
	{
		Debug.Log("It is entered");

		rollChance = Mathf.Min(100, 80 + SharedData.healRoomChanceBonus);

		UpdateText();

		dialogBox.pressed = false;
		dialogBox.Show();

		GameObject.Find("GlobalScripts").GetComponent<CoroutineUtil>().StartCoroutine(WaitUntilClosed());
	}

	void UpdateText()
	{
		dialogBox.text.text = "Votre �quipe d�couvre un dispositif alien m�dical complexe...\n" +
					  "Vous devriez pouvoir soigner vos blessures, mais toute erreur de manipulation pourrait faire exploser la machine !\n";
		dialogBox.text.text += "Chance de succ�s : " + rollChance + "%\n\n";

		foreach (var soldier in SharedData.soldiers)
		{
			dialogBox.text.text += soldier.ActorName + " : " + "<color=green>" + soldier.HP + "/" + soldier.MaxHP + "</color>\n";
		}
	}

	IEnumerator WaitUntilClosed()
	{

		while (true)
		{
			yield return new WaitUntil(() => dialogBox.pressed);
			Debug.Log("Button : " + dialogBox.accepted);
			if (!dialogBox.accepted)
			{
				dialogBox.Close();
				Leave();
				yield break;
			}
			else // Roll
			{
				float roll = Random.Range(0, 100);
				// Heal
				if (roll < rollChance)
				{
					Debug.Log("Heal");
					GameObject.Find("HealSpray").GetComponent<AudioSource>().Play();

					foreach (var soldier in SharedData.soldiers)
						soldier.HP = Mathf.Min(soldier.HP + 25, soldier.MaxHP);

					rollChance = Mathf.Max(rollChance - chancePenality, 0);

					UpdateText();
					dialogBox.pressed = false;
				}
				// Fail
				else
				{
					Debug.Log("Fail");
					GameObject.Find("ElecArc").GetComponent<AudioSource>().Play();
					GameObject.Find("GlassBreak").GetComponent<AudioSource>().Play();

					foreach (var soldier in SharedData.soldiers)
						soldier.HP = Mathf.Max(soldier.HP - 25, 0);
					SharedData.soldiers.RemoveAll(x => x.HP <= 0);

					UpdateText();

					msgBox.text.text = "Le mat�riel alien a fini par casser, et a bless� vos soldats pour 25HP...";
					msgBox.Show();

					// TODO : Screenshake

					yield return new WaitUntil(() => msgBox.closing);

					dialogBox.Close();
					Leave();
					yield break;
				}
			}
		}
	}
}
