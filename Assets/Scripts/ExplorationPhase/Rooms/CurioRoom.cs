using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;	

public class CurioRoom : Room
{
	Curio curio;
	Item usedItem;

	public CurioRoom(Curio curio)
	{
		this.curio = curio;

		icon = GameObject.FindObjectOfType<RoomIconList>().curioIcon;
		iconColor = Color.white;
	}

	public override void EnterImpl()
	{
		dialogBox.text.text = "<u><align=\"center\">" + curio.Name() + "</align></u>\n\n";
		dialogBox.text.text += curio.Description();

		dialogBox.acceptedText.text = curio.AcceptText();
		dialogBox.refusedText.text = curio.RefuseText();

		usedItem = SharedData.sharedInventory.Find(x => x.GetType() == curio.CounterItem());
		if (usedItem != null && usedItem.name.Length >= 1)
		{
			dialogBox.useItemButton.gameObject.SetActive(true);
			string name = char.ToLower(usedItem.name[0]) + usedItem.name.Substring(1); // minuscule premi�re lettre
			dialogBox.useItemText.text = "Utiliser " + name;
		}
		else
		{
			dialogBox.useItemButton.gameObject.SetActive(false);
		}

		dialogBox.pressed = false;
		dialogBox.Show();

		GameObject.Find("GlobalScripts").GetComponent<CoroutineUtil>().StartCoroutine(WaitUntilClosed());
	}

	IEnumerator WaitUntilClosed()
	{
		yield return new WaitUntil(() => dialogBox.pressed);
		if (dialogBox.accepted)
		{
			Debug.Log("Last character pos : " + dialogBox.transform.TransformPoint(dialogBox.text.textInfo.characterInfo[5].topLeft));

			int roll = Random.Range(0, 100);
			roll = Mathf.Max(0, roll - SharedData.curioBonus);
			if (dialogBox.itemUsed)
			{
				roll = 0; // Force a perfect roll if the item was used.
				SharedData.sharedInventory.Remove(usedItem);
			}

			var msg = curio.DoAction(roll);
			msgBox.text.text = msg;
			msgBox.Show();


			yield return new WaitUntil(() => msgBox.closing);

			if (curio.enterCombat)
				GameObject.FindObjectOfType<CombatGenerator>().GenerateAndEnter();
		}

		dialogBox.useItemButton.gameObject.SetActive(false);
		dialogBox.Close();
		Leave();
	}
}

public abstract class Curio
{
	public bool enterCombat = false;

	public abstract string Name();
	public abstract string Description();
	public abstract string AcceptText();
	public abstract string RefuseText();
	public abstract string DoAction(int roll);

	// Type of the item that can counter this Curio
	public virtual System.Type CounterItem()
	{ return typeof(int); }

	public static Curio RandomCurio()
	{
		var list = new List<Curio> { new MysteriousFlowerCurio(), new AlienArchivesCurio(), new ControlConsoleCurio(), new RadioactiveRoomCurio() };
		int idx = Random.Range(0, list.Count);
		return list[idx];
	}
}

public class MysteriousFlowerCurio : Curio
{
	public override string Name()
	{
		return "Fleur myst�rieuse";
	}
	public override string Description()
	{
		return "Une fleur est plant�e au milieu de cette salle, ses tortueuses racines s'�tendent aux quatres coins de la pi�ce...";
	}
	public override string AcceptText()
	{
		return "Toucher la fleur";
	}
	public override string RefuseText()
	{
		return "Quitter la pi�ce";
	}

	public override System.Type CounterItem()
	{ return typeof(GourdeItem); }

	// Returns the message to be shown
	public override string DoAction(int roll)
	{
		string msg;
		if (roll <= 25)
		{
			msg = "La fleur a des propri�t�s m�dicinales, vous la prenez dans votre inventaire.\n" +
				  "R�colte '<color=green>herbe m�dicinale</color>'";
			SharedData.sharedInventory.Add(new HealHerbs());

			GameObject.Find("CurioSuccess").GetComponent<AudioSource>().Play();
		}
		else if (roll <= 50)
		{
			msg = "La fleur d�gage des spores m�dicinales tr�s potentes : le soldat le plus bless� de votre groupe regagne <color=green>100HP</color>.\n";
			var lowestSoldier = SharedData.soldiers.OrderBy(x => x.HP).First();
			lowestSoldier.HP = Mathf.Min(lowestSoldier.MaxHP, lowestSoldier.HP + 100);

			GameObject.Find("CurioSuccess").GetComponent<AudioSource>().Play();
		}
		else if (roll <= 75)
		{
			msg = "De la fleur suinte une s�ve n�faste ! Les membres de votre �quipe sont atteints de <color=red>saignement</color>.\n";
			foreach (var soldier in SharedData.soldiers)
				soldier.AddEffect(new BleedingEffect(10));

			GameObject.Find("Grunt").GetComponent<AudioSource>().Play();
		}
		else
		{
			msg = "Cette fleur se nourrit de la radiation �manant du vaisseau ! Vos soldats sont atteints d'<color=red>irradiation</color>.\n";
			foreach (var soldier in SharedData.soldiers)
				soldier.AddEffect(new RadioactiveEffect(10));

			GameObject.Find("Grunt").GetComponent<AudioSource>().Play();
		}

		return msg;
	}
}

public class AlienArchivesCurio : Curio
{
	public override string Name()
	{
		return "Archives alien";
	}
	public override string Description()
	{
		return "Des dizaines de terminaux sont reli�s � un ordinateur central alien... En y acc�dant, vous pourriez d�couvrir des informations biom�dicales capitales.";
	}
	public override string AcceptText()
	{
		return "Inspecter";
	}
	public override string RefuseText()
	{
		return "Quitter la pi�ce";
	}

	public override System.Type CounterItem()
	{ return typeof(BadgeItem); }

	// Returns the message to be shown
	public override string DoAction(int roll)
	{
		string msg;
		if (roll <= 50)
		{
			msg = "Vous d�couvrez des informations sur le corps humain et les soins. Votre �quipe est enti�rement <color=green>soign�e.</color>.\n";
			foreach (var soldier in SharedData.soldiers)
				soldier.HP = soldier.MaxHP;

			GameObject.Find("CurioSuccess").GetComponent<AudioSource>().Play();
		}
		else if (roll <= 75)
		{
			msg = "Vous tombez sur des descriptions de la fin de l'Univers. Votre �quipe d�prime et subit un <color=red>malus de pr�cision</color> de -15%.\n";
			foreach (var soldier in SharedData.soldiers)
				soldier.AddEffect(new PrecisionEffect(-15));

			GameObject.Find("Dramatic").GetComponent<AudioSource>().Play();
		}
		else
		{
			msg = "Vous tombez sur des descriptions de la fin de l'Univers. Votre �quipe d�prime et subit un <color=red>malus d'esquive</color> de -15%.\n";
			foreach (var soldier in SharedData.soldiers)
				soldier.AddEffect(new ExoModEffect(-15));

			GameObject.Find("Dramatic").GetComponent<AudioSource>().Play();
		}

		return msg;
	}
}

public class ControlConsoleCurio : Curio
{
	public override string Name()
	{
		return "Console de contr�le";
	}
	public override string Description()
	{
		return "Cette salle contient un terminal alien administrateur. Y acc�der pourrait r�v�ler des informations capitales pour votre mission.";
	}
	public override string AcceptText()
	{
		return "Inspecter";
	}
	public override string RefuseText()
	{
		return "Quitter la pi�ce";
	}

	public override System.Type CounterItem()
	{ return typeof(AccessCardItem); }

	// Returns the message to be shown
	public override string DoAction(int roll)
	{
		string msg;
		if (roll <= 50)
		{
			GameObject.Find("CurioSuccess").GetComponent<AudioSource>().Play();

			msg = "Un alien a oubli� de se d�connecter de sa session, vous d�couvrez un plan du vaisseau. <color=green>+1 Rep�rage</color>.\n";
			++SharedData.scoutingCharges;
		}
		else
		{
			GameObject.Find("AlarmSound").GetComponent<AudioSource>().Play();

			msg = "Vous essayez de pirater le terminal, mais vous n'y comprenez rien : <color=red>l'alarme sonne!</color>.\n";
			enterCombat = true;
		}

		return msg;
	}
}

public class RadioactiveRoomCurio : Curio
{
	public override string Name()
	{
		return "Section irradi�e";
	}
	public override string Description()
	{
		return "Vous arrivez dans un compartiment qui semble �tre � l�abandon. Au fur � mesure que vous avancez, vous apercevez des particules vertes en suspension dans l�air...";
	}
	public override string AcceptText()
	{
		return "Avancer";
	}
	public override string RefuseText()
	{
		return "Quitter la pi�ce";
	}

	public override System.Type CounterItem()
	{ return typeof(GeigerItem); }

	// Returns the message to be shown
	public override string DoAction(int roll)
	{
		string msg;
		if (roll <= 33)
		{
			msg = "Vous trouvez de l'�quipement abandonn� :";

			// Generate items
			for (int i = 0; i < 3; ++i)
			{
				var item = Item.GenerateRandomItem();
				msg += "\n<color=green>+1 " + item.name + "</color>";
				SharedData.sharedInventory.Add(item);
			}

			GameObject.Find("CurioSuccess").GetComponent<AudioSource>().Play();
		}
		else
		{
			msg = "Vous ne trouvez rien, et vos soldats souffrent � pr�sent de <color=red>radiation</color>.\n";
			foreach (var soldier in SharedData.soldiers)
				soldier.AddEffect(new RadioactiveEffect(10));

			GameObject.Find("Geiger").GetComponent<AudioSource>().Play();
		}

		return msg;
	}
}