using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using System.Linq;

public class BuffRoom : Room
{
	BuffBox buffBox;
	List<Buff> masterBuffList, buffList;

	public BuffRoom()
	{
		masterBuffList = new List<Buff>();

		masterBuffList.Add(new BerserkBuff());
		masterBuffList.Add(new RegenBuff());
		masterBuffList.Add(new GreedBuff());
		masterBuffList.Add(new AdrenalineBuff());
		masterBuffList.Add(new LynxEyeBuff());
		masterBuffList.Add(new VitalityBuff());
		masterBuffList.Add(new MedecineBuff());
		masterBuffList.Add(new ElementalBuff());
		masterBuffList.Add(new TemeriteBuff());
		masterBuffList.Add(new BrutalityBuff());

		buffBox = GameObject.FindObjectOfType<BuffBox>();

		icon = GameObject.FindObjectOfType<RoomIconList>().buffIcon;
		iconColor = Color.green;
	}

	public override void EnterImpl()
	{
		GameObject.Find("CurioSuccess").GetComponent<AudioSource>().Play();

		Assert.IsTrue(masterBuffList.Count > 0);
		// Randomly choose 3 buffs to give
		buffList = masterBuffList.OrderBy(a => Random.Range(0, 100000000)).ToList();
		// Remove buffs that are already active and non-stackable
		foreach (var soldier in SharedData.soldiers)
			foreach (var buff in soldier.Buffs)
				if (buffList.Find(x => x.Name == buff.Name) != null && !buff.canStack)
					buffList.RemoveAll(x => x.Name == buff.Name);

		// If for some reason the buff list is empty, add the first buff from the master list
		if (buffList.Count <= 0)
			buffList.Add(masterBuffList[0]);
		while (buffList.Count < 3)
			buffList.Add(buffList[0]);

		var buff1 = buffList[0];
		var buff2 = buffList[1];
		var buff3 = buffList[2];

		buffBox.buff1Text.text = "<color=red><align=center><u>" + buff1.Name + "</u></align></color>\n\n";
		buffBox.buff1Text.text += buff1.Description;

		buffBox.buff2Text.text = "<color=red><align=center><u>" + buff2.Name + "</u></align></color>\n\n";
		buffBox.buff2Text.text += buff2.Description;

		buffBox.buff3Text.text = "<color=red><align=center><u>" + buff3.Name + "</u></align></color>\n\n";
		buffBox.buff3Text.text += buff3.Description;

		buffBox.buffPressed = -1;
		buffBox.Show();

		PlaceIcon(buffBox.buff1Text, buffBox.icon1);
		PlaceIcon(buffBox.buff2Text, buffBox.icon2);
		PlaceIcon(buffBox.buff3Text, buffBox.icon3);
		buffBox.icon1.sprite = buff1.sprite; buffBox.icon1.color = buff1.color;
		buffBox.icon2.sprite = buff2.sprite; buffBox.icon2.color = buff2.color;
		buffBox.icon3.sprite = buff3.sprite; buffBox.icon3.color = buff3.color;

		GameObject.Find("GlobalScripts").GetComponent<CoroutineUtil>().StartCoroutine(WaitUntilClosed());
	}

	void PlaceIcon(TMPro.TextMeshPro text, SpriteRenderer icon)
	{
		text.ForceMeshUpdate(true, true);
		text.GetTextInfo(text.text); // Force the textInfo field to be regenerated
		Vector3 localCharPos = (text.textInfo.characterInfo[0].topLeft + text.textInfo.characterInfo[0].bottomLeft) / 2
						- new Vector3(0.30f, 0, 0);
		icon.transform.position = text.transform.TransformPoint(localCharPos);
	}

	IEnumerator WaitUntilClosed()
	{
		yield return new WaitUntil(() => buffBox.buffPressed > 0);

		Debug.Log("Chose buff " + buffList[buffBox.buffPressed-1].Name);

		buffList[buffBox.buffPressed - 1].ApplyEffect();

		if (!buffList[buffBox.buffPressed - 1].Immediate())
		{
			foreach (var soldier in SharedData.soldiers)
				soldier.Buffs.Add(buffList[buffBox.buffPressed - 1]);
		}

		buffBox.Close();
		Leave();
	}
}

public class Buff
{
	public string Name;
	public string Description;
	public Sprite sprite;
	public Color color;
	public bool canStack;
	public Buff(string Name, string Description, Sprite sprite, Color color, bool canStack)
	{
		this.Name = Name;
		this.Description = Description;
		this.sprite = sprite;
		this.color = color;
		this.canStack = canStack;
	}

	public virtual void ApplyEffect()
	{ }

	public virtual bool Immediate()
	{ return false; }
}

public class BerserkBuff : Buff
{
	public BerserkBuff() : base("Berserk", "Lorsqu�un personnage poss�de moins de 20% de ses HP max., celui-ci inflige 40% de d�gats supp.",
			GameObject.FindObjectOfType<RoomIconList>().skullIcon, Color.white, false)
	{

	}
}

public class RegenBuff : Buff
{
	public RegenBuff() : base("R�g�n�ration", "Chaque membre de l��quipe r�g�n�re 5% d�HP de plus apr�s chaque combat.",
	GameObject.FindObjectOfType<RoomIconList>().healIcon, Color.red, true)
	{

	}
}

public class GreedBuff : Buff
{
	public GreedBuff() : base("Avidit�", "Bonus de +30% de cryptomonnaie re�ue en plus.",
GameObject.FindObjectOfType<RoomIconList>().moneyIcon, Color.yellow, true)
	{

	}
}

public class AdrenalineBuff : Buff
{
	readonly static int bonus = Tweakables.GetSingle("adrenaline_buff_bonus", 5);

	public AdrenalineBuff() : base("Adr�naline", bonus + "% d�esquive � chaque membre de l��quipe.",
GameObject.FindObjectOfType<RoomIconList>().fireIcon, Color.green, true)
	{

	}

	public override bool Immediate()
	{
		return true;
	}

	public override void ApplyEffect()
	{
		foreach (var soldier in SharedData.soldiers)
			soldier.SetBaseDodge(soldier.GetBaseDodgeChance() + bonus);
	}
}

public class LynxEyeBuff : Buff
{
	public LynxEyeBuff() : base("Oeil de lynx", "Obtient un rep�rage suppl�mentaire.",
GameObject.FindObjectOfType<RoomIconList>().signpostIcon, Color.white, true)
	{

	}

	public override bool Immediate()
	{
		return true;
	}

	public override void ApplyEffect()
	{
		++SharedData.scoutingCharges;
	}
}

public class VitalityBuff : Buff
{
	readonly static int bonus = Tweakables.GetSingle("vitality_buff_bonus", 7);

	public VitalityBuff() : base("Vitalit�", "+" + bonus + "% d�HP max pour chaque membre de l��quipe.",
GameObject.FindObjectOfType<RoomIconList>().healIcon, Color.red, true)
	{

	}

	public override bool Immediate()
	{
		return true;
	}

	public override void ApplyEffect()
	{
		foreach (var soldier in SharedData.soldiers)
		{
			int increase = Mathf.RoundToInt(soldier.MaxHP * (bonus/100.0f));
			soldier.MaxHP += increase;
			soldier.HP += increase;
		}
	}
}

public class MedecineBuff : Buff
{
	readonly static int bonus = Tweakables.GetSingle("medecine_buff_bonus", 4);

	public MedecineBuff() : base("M�decine", "+" + bonus + "% de chance de succ�s durant la salle \"soins\".",
GameObject.FindObjectOfType<RoomIconList>().healIcon, Color.green, true)
	{

	}

	public override bool Immediate()
	{
		return true;
	}

	public override void ApplyEffect()
	{
		SharedData.healRoomChanceBonus += bonus;
	}
}

public class ElementalBuff : Buff
{
	readonly static int bonus = Tweakables.GetSingle("elemental_buff_bonus", 5);

	public ElementalBuff() : base("Ma�trise �l�mentaire", "+" + bonus + "% de d�g�ts �l�mentaires.",
GameObject.FindObjectOfType<RoomIconList>().fireIcon, Color.yellow, true)
	{

	}

	public override bool Immediate()
	{
		return true;
	}

	public override void ApplyEffect()
	{
		SharedData.elementalDamageBonus += bonus;
	}
}

public class TemeriteBuff : Buff
{
	readonly static int bonus = Tweakables.GetSingle("temerite_buff_bonus", 8);

	public TemeriteBuff() : base("T�m�rit�", "+" + bonus + "% de chance de r�ussite pour les �v�nements al�atoires.",
GameObject.FindObjectOfType<RoomIconList>().curioIcon, Color.white, true)
	{

	}

	public override bool Immediate()
	{
		return true;
	}

	public override void ApplyEffect()
	{
		SharedData.curioBonus += bonus;
	}
}

public class BrutalityBuff : Buff
{
	readonly static int bonus = Tweakables.GetSingle("brutality_buff_bonus", 2);

	public BrutalityBuff() : base("Brutalit�", "+" + bonus + "% de chance de coup critique.",
GameObject.FindObjectOfType<RoomIconList>().combatIcon, Color.yellow, true)
	{

	}

	public override bool Immediate()
	{
		return true;
	}

	public override void ApplyEffect()
	{
		SharedData.critBonus += bonus;
	}
}