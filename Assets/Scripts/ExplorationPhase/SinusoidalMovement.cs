using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SinusoidalMovement : MonoBehaviour
{
    public float amplitude = 1;
    public float speed = 1;
    public Vector3 direction = Vector3.right;
    [System.NonSerialized]
    public Transform newParent;

    private bool tweening = false;
    private Vector3 _startPosition;

    void Start()
    {
        _startPosition = transform.localPosition;
    }

    void Update()
    {
        // Don't move if currently tweening
        if (LeanTween.isTweening(gameObject))
            tweening = true;
        else
        {
            // Did we finish tweening? Reset the base pos
            if (tweening)
            {
                _startPosition = transform.localPosition;
            }
            if (newParent == null)
                transform.localPosition = _startPosition + Mathf.Sin(Time.time * speed) * direction * amplitude;
            else
                transform.localPosition = newParent.position + Mathf.Sin(Time.time * speed) * direction * amplitude;
            tweening = false;
        }
    }
}
