using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UpgradeButton : MonoBehaviour, ISelectHandler
{
    public int skillIdx;
    public int skillLevel;

    public void OnSelect(BaseEventData eventData)
    {
        if (eventData.selectedObject == gameObject)
        {
            FindObjectOfType<SkillTree>().SkillHovered(skillIdx, skillLevel);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        var cols = GetComponent<Button>().colors;

        if (EventSystem.current.GetComponent<EventSystem>().currentSelectedGameObject == gameObject)
            cols.highlightedColor = new Color(cols.highlightedColor.r, cols.highlightedColor.g, cols.highlightedColor.b, 254/255.0f);
        else
            cols.highlightedColor = new Color(cols.highlightedColor.r, cols.highlightedColor.g, cols.highlightedColor.b, 0);

        GetComponent<Button>().colors = cols;
    }
}
