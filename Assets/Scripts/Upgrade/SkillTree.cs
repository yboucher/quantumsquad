using System.Collections;
using System;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Tilemaps;
using UnityEngine.Assertions;
using UnityEngine.EventSystems;

public class SkillTree : MonoBehaviour
{
    class SkillInfo
	{
        public string id;
        public string name;
        public string desc;
        public List<ActionMainStatAttribute> attrs = new List<ActionMainStatAttribute>();
        public int level = 0;
        public string usedByClass;
	}

    int[] goldCosts;

    public Tilemap tilemap;
    public Tile unpickedTile, pickedTile, connectorTile;
    public Vector3Int topLeftCorner;
    public GameObject skillHighlighter;
    public TextMeshPro skillDesc;
    public TextMeshPro goldText;
    public GameObject msgBoxOfThePoor;
    public GameObject classIndicator;
    public TextMeshPro classIndicatorText;
    public UpgradeButton topLeftHighlightButton;

    GameObject[,] highlightButtons;
    TextMeshPro[] skillNames;
    List<SkillInfo> skillInfo;
    SaveData saveData;
    Vector3 highlighterBasePos;
    Vector3 lastMousePos;
    int maxUnlockableLevel = 1;
    int skillPageOffset = 0;
    int beratingFrame = 0;
    bool popupOpen = false;

    // Start is called before the first frame update
    void Start()
    {
        highlighterBasePos = skillHighlighter.transform.position;

        saveData = SaveData.Load();

        // Limit the max skill unlockable based on the highest beaten boss of this save
        SetMaxUnlockableLevel(saveData.highestBeatenBoss + 2);

        goldCosts = new int[5] { 500, 500, 1000, 1000, 1500 };
        skillNames = new TextMeshPro[6];
        skillInfo = new List<SkillInfo>();
        for (int i = 0; i < 6; i++)
        {
            skillNames[i] = GameObject.Find("SkillName" + (i + 1)).GetComponent<TextMeshPro>();
        }

        // List Ability subclasses
        var subclassTypes = Assembly
           .GetAssembly(typeof(Action))
           .GetTypes()
           .Where(t => t.IsSubclassOf(typeof(Action)));

        int skillIdx = 0;
        foreach (var type in subclassTypes)
        {
            var attrs = System.Attribute.GetCustomAttributes(type, typeof(ActionDataAttribute));
            if (attrs.Length == 0)
                continue;
            var attr = (ActionDataAttribute)attrs[0];
            Debug.Log(type.Name + " / " + attr.name);
            ++skillIdx;

            SkillInfo info = new SkillInfo();
            info.id = type.Name;
            info.name = attr.name;
            info.desc = attr.desc;

            var mainStats = type.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance)
                                .Where(prop => prop.IsDefined(typeof(ActionMainStatAttribute), false));
            foreach (var stat in mainStats)
			{
                var statAttr = stat.GetCustomAttribute<ActionMainStatAttribute>();
                Debug.Log(stat.Name + " / " + statAttr.name + " : " + statAttr.baseValue + " (" + statAttr.step + ")");
                info.attrs.Add(statAttr);
			}

            var usedByClass = "";
            foreach (var pair in ClassInfo.Classes())
			{
                if (!saveData.unlockedClasses.Contains(pair.Key))
                    continue;
                if (pair.Value.Abilities.Contains(type.Name))
				{
                    usedByClass = pair.Key;
                    break;
				}
			}
            // This skill is used by no class
            if (usedByClass == "")
                continue;
            //Debug.Log("Skill " + info.name + " is used by class " + usedByClass);
            info.usedByClass = usedByClass;

            skillInfo.Add(info);
        }
        // Sort skills by alphabetical order
        skillInfo.Sort((x, y) => x.name.CompareTo(y.name));
        // Load levels from save data
        foreach (var savedSkill in saveData.skillLevels)
		{
            var info = skillInfo.Find(x => x.id == savedSkill.Name);
            if (info == null)
			{
                Debug.Log("Uknown skill " + savedSkill.Name + " from save data");
                continue;
			}
            info.level = savedSkill.Level;
		}

        goldText.text = String.Format("Gold : <color=yellow><u>{0} po</u></color>", saveData.gold);

        // Create the highlight buttons
        highlightButtons = new GameObject[6, 5];
        highlightButtons[0, 0] = topLeftHighlightButton.gameObject;
        highlightButtons[0, 0].GetComponent<Button>().onClick.AddListener(() => AttemptToPick(0, 0));
        for (int i = 0; i < 6; ++i)
            for (int j = 0; j < 5; ++j)
			{
                if (i == 0 && j == 0)
                    continue;
                highlightButtons[i, j] = GameObject.Instantiate(topLeftHighlightButton.gameObject, topLeftHighlightButton.transform.parent);
                highlightButtons[i, j].transform.localPosition = new Vector3(topLeftHighlightButton.transform.localPosition.x + j * 100,
                                                                             topLeftHighlightButton.transform.localPosition.y - i * 100);
                highlightButtons[i, j].GetComponent<UpgradeButton>().skillIdx = i;
                highlightButtons[i, j].GetComponent<UpgradeButton>().skillLevel = j;
                highlightButtons[i, j].GetComponent<Button>().onClick.RemoveAllListeners();
                int skillIdxCopy = i;
                int skillLevelCopy = j;
                highlightButtons[i, j].GetComponent<Button>().onClick.AddListener(() => AttemptToPick(skillIdxCopy, skillLevelCopy));
            }

        LoadPage(0);
    }

    private void ChangeClass(SkillInfo skill)
    {
        var className = skill.usedByClass;
        if (className == "")
		{
            classIndicator.SetActive(false);
            return;
		}

        classIndicator.SetActive(true);
        classIndicatorText.text = className;

        //Debug.Log("class : " + className);
        var anim = FindObjectOfType<AnimList>().anims.Find(x => x.name == className);
        classIndicator.GetComponent<Animator>().runtimeAnimatorController = anim.anim;
        classIndicator.GetComponent<Animator>().Update(0.0f);
    }

    void LoadPage(int pageOffset)
	{
        skillPageOffset = pageOffset;

        // Load skill info
        for (int i = 0; i < 6; ++i)
        {
            bool hidden = false;
            if (i + skillPageOffset < skillInfo.Count)
            {
                skillNames[i].gameObject.SetActive(true);
                skillNames[i].text = skillInfo[i + skillPageOffset].name;
            }
            else
            {
                skillNames[i].gameObject.SetActive(false);
                hidden = true;
            }

            for (int k = 0; k < 5 * 2; k++)  // 1-based indexing as 0 means the skill wasn't picked
            {
                // Selectable tile position, set according to current skill level
                if (k % 2 == 0 && !hidden)
                {
                    var tile = (k / 2) < skillInfo[i + skillPageOffset].level ? pickedTile : unpickedTile;
                    tilemap.SetTile(topLeftCorner + new Vector3Int(k, -i * 2, 0), tile);
                }
                if (k % 2 == 0)
				{
                    highlightButtons[i, k / 2].SetActive(!hidden);
				}

                // Enable tile coloring
                tilemap.SetTileFlags(topLeftCorner + new Vector3Int(k, -i * 2, 0), TileFlags.None);

                var col = tilemap.GetColor(topLeftCorner + new Vector3Int(k, -i * 2, 0));
                if (k == 4 * 2)
                    col = Color.red; // Ultimate Tier
                col.a = hidden ? 0 : 255;
                tilemap.SetColor(topLeftCorner + new Vector3Int(k, -i * 2, 0), col);
            }
        }
    }

    public void UpdateGoldCount()
	{
        var newData = SaveData.Load();
        saveData.gold = newData.gold;
        goldText.text = String.Format("Gold : <color=yellow><u>{0} po</u></color>", saveData.gold);
    }
    public void ResetSkills()
	{
        foreach (var skill in skillInfo)
		{
            //Debug.Log("Could remove " + CalculateGoldCost(0, skill.level) + " for " + skill.id);
            saveData.gold += CalculateGoldCost(0, skill.level); // Refund the gold
            skill.level = 0; // Reset the skill level
        }
        // Reload the page to refresh the displayed tree
        LoadPage(skillPageOffset);
        goldText.text = String.Format("Gold : <color=yellow><u>{0} po</u></color>", saveData.gold);
    }

    void SetMaxUnlockableLevel(int maxLevel)
	{
        maxUnlockableLevel = maxLevel;

        --maxLevel; // switch to 0-based indexing

        for (int j = 0; j < 5*2 + 1; ++j)
		{
            for (int i = 0; i < 4*2+1; ++i)
			{
                // Enable tile coloring
                tilemap.SetTileFlags(topLeftCorner + new Vector3Int(i, -j, 0), TileFlags.None);
                // unlockable
                if (i < maxLevel*2+1)
				{
                    if (i == 4*2) // Ultimate rank
                        tilemap.SetColor(topLeftCorner + new Vector3Int(i, -j, 0), Color.red);
                    else
                        tilemap.SetColor(topLeftCorner + new Vector3Int(i, -j, 0), Color.white);
                }
                else // not unlockable
				{
                    if (i == 4*2) // Ultimate rank
                        tilemap.SetColor(topLeftCorner + new Vector3Int(i, -j, 0), new Color(64, 0, 0)); // dark red
                    else
                        tilemap.SetColor(topLeftCorner + new Vector3Int(i, -j, 0), Color.grey);
                }
			}
		}
	}

    public void PrevPage()
	{
        if (skillPageOffset > 0)
            LoadPage(Mathf.Max(0, skillPageOffset - skillNames.Length));
    }

    public void NextPage()
	{
        if ((skillPageOffset + skillNames.Length) < skillInfo.Count)
            LoadPage(Mathf.Min(skillPageOffset + skillNames.Length, skillInfo.Count - 1));
    }

    // Update is called once per frame
    void Update()
    {
        // Paging
        if (/*Input.GetKeyDown(KeyCode.LeftArrow) ||*/ Input.GetButtonDown("SwitchLeft"))
            PrevPage();
        else if (/*Input.GetKeyDown(KeyCode.RightArrow) ||*/ Input.GetButtonDown("SwitchRight"))
            NextPage();

        Vector3 mouseWorldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector3Int tileCoordinate = tilemap.WorldToCell(mouseWorldPos);

        if (mouseWorldPos != lastMousePos && tilemap.GetTile(tileCoordinate) != null /*&& tilemap.GetTile(tileCoordinate).name == unpickedTile.name*/ &&
            tilemap.GetColor(tileCoordinate).a > 0 && (tileCoordinate - topLeftCorner).x <= (maxUnlockableLevel-1)*2)
		{
            //Debug.Log("Hovering " + (-(tileCoordinate - topLeftCorner).y / 2) + " / " + (tileCoordinate - topLeftCorner).x / 2);
            highlightButtons[-(tileCoordinate - topLeftCorner).y / 2, (tileCoordinate - topLeftCorner).x / 2].GetComponent<Button>().Select();
            SkillHovered(-(tileCoordinate - topLeftCorner).y / 2, (tileCoordinate - topLeftCorner).x / 2);
		}
        else if (EventSystem.current.GetComponent<EventSystem>().currentSelectedGameObject != null &&
                 EventSystem.current.GetComponent<EventSystem>().currentSelectedGameObject.GetComponent<UpgradeButton>() == null)
		{
            skillHighlighter.SetActive(false);
            classIndicator.SetActive(false);
            skillDesc.text = "Survolez un niveau de comp�tence pour afficher sa description.";
		}

        lastMousePos = mouseWorldPos;
    }

    int CalculateGoldCost(int currentLevel, int desiredLevel)
	{
        return goldCosts.Skip(currentLevel).Take(desiredLevel - currentLevel).Sum();
	}
    void PickSkillTier(int skillIdx, int skillLevel)
	{
        for (int i = 0; i <= skillLevel; i++)
        {
            tilemap.SetTile(topLeftCorner + new Vector3Int(i * 2, -skillIdx * 2, 0), pickedTile);
            tilemap.SetTileFlags(topLeftCorner + new Vector3Int(i * 2, -skillIdx * 2, 0), TileFlags.None);
            if (i == 4)
                tilemap.SetColor(topLeftCorner + new Vector3Int(i * 2, -skillIdx * 2, 0), Color.red);
        }

        //saveData.gold -= goldCosts[skillLevel];
        saveData.gold -= CalculateGoldCost(skillInfo[skillIdx + skillPageOffset].level, skillLevel + 1);
        goldText.text = String.Format("Gold : <color=yellow><u>{0} po</u></color>", saveData.gold);

        skillInfo[skillIdx + skillPageOffset].level = skillLevel + 1; // == 0 <=> no skill, >0 <=> picked at least once

        GetComponent<AudioSource>().Play();

        // Update the text by calling SkillHovered again
        SkillHovered(skillIdx, skillLevel);
    }

    public void AttemptToPick(int skillIdx, int skillLevel)
	{
        if (popupOpen || Time.frameCount == beratingFrame || skillLevel >= maxUnlockableLevel)
            return;

        int goldCost = CalculateGoldCost(skillInfo[skillIdx + skillPageOffset].level, skillLevel + 1);

        if (goldCost > 0)
            if (goldCost > saveData.gold)
                StartCoroutine(BerateForBeingPoor());
            else
                PickSkillTier(skillIdx, skillLevel);
    }
    public void SkillHovered(int skillIdx, int skillLevel)
	{
        //skillHighlighter.SetActive(true);
        //skillHighlighter.transform.position = highlighterBasePos + new Vector3Int(skillLevel, -skillIdx, 0);

        ChangeClass(skillInfo[skillIdx + skillPageOffset]);

        int goldCost = CalculateGoldCost(skillInfo[skillIdx + skillPageOffset].level, skillLevel + 1);

        Assert.IsTrue(skillIdx + skillPageOffset < skillInfo.Count);
        var info = skillInfo[skillIdx + skillPageOffset];
        skillDesc.text = info.name + " Tier " + (skillLevel+1) + "\n\n";
        skillDesc.text += info.desc + "\n\n";
        foreach (var stat in info.attrs)
        {
            skillDesc.text += stat.name + " : " + (stat.baseValue + stat.step * (skillLevel+1));
            skillDesc.text += " (" + (stat.step < 0 ? "-" : "+") + (stat.step * (skillLevel+1)) + ")";
            skillDesc.text += "\n";
        }
        skillDesc.text += "\n\n";
        if (goldCost > 0)
            skillDesc.text += String.Format("Co�t : <color=yellow><u>{0} po</u></color>", goldCost);
        else
            skillDesc.text += String.Format("<color=green>Vous poss�dez d�ja ce niveau de comp�tence.</color>", goldCost);

        highlightButtons[skillIdx, skillLevel].GetComponent<Button>().Select();

        if (Input.GetMouseButtonDown(0))
            AttemptToPick(skillIdx, skillLevel);
    }

    IEnumerator BerateForBeingPoor()
	{
        popupOpen = true;

        var msg = GameObject.Instantiate(msgBoxOfThePoor, transform.parent);
        msg.GetComponent<MessageBox>().Show();
        yield return new WaitUntil(() => msg.GetComponent<MessageBox>().closing);
        msg.GetComponent<MessageBox>().Close();
        beratingFrame = Time.frameCount;

        popupOpen = false;
	}

    public void GoBack()
	{
        Save();
        FindObjectOfType<LevelLoader>().UnloadCurrentLevel();
	}

    void Save()
	{
        // OnDestroy might be called before Start()!
        if (saveData == null)
            return;

        saveData.skillLevels.Clear();

        foreach (var skill in skillInfo)
        {
            saveData.skillLevels.Add(new SaveData.SaveDataSkillInfo(skill.id, skill.level));
        }

        // Update soldier skills on the fly
        foreach (var soldier in SharedData.soldiers)
            foreach (var skill in soldier.Abilities)
            {
                var level = saveData.skillLevels.Find(x => x.Name == skill.GetType().Name);
                if (level != null)
                    skill.SetTier(level.Level);
			}

        saveData.Save();
    }

	void OnDestroy()
	{
        Save();
	}
}
