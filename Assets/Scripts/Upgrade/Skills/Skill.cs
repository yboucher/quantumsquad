using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Skill
{
    public struct Tier
	{
        public Tier(string d, int c)
		{
            desc = d; cost = c;
		}

        public string desc;
        public int cost;
	}

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public abstract Tier[] Tiers();

    // Update is called once per frame
    void Update()
    {
        
    }
}
