using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillRegen : Skill
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public override Tier[] Tiers()
	{
        return new Tier[]
        {
            new Tier("Régénération Tier 1",
                    60),
            new Tier("Régénération Tier 2", 
                    120),
            new Tier("Régénération Tier 3",
                    180),
            new Tier("Régénération Tier 4",
                    260),
            new Tier("Régénération Tier 5",
                    500),
        };
	}

    // Update is called once per frame
    void Update()
    {
        
    }
}
