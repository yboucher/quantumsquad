using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SessionReportScript : MonoBehaviour
{
	public GameObject radxIcon, cyborgIcon, yakaIcon, guardianIcon;
	public TextMeshPro desc, nftText1, nftText2;

	public float scrambleTime = 3;

	float startTime = 0;

	List<string> items = new List<string>();

	// Start is called before the first frame update
	void Start()
	{
		startTime = Time.time;

		GenerateNFTs();

		SaveData save = SaveData.Load();

		desc.text = System.String.Format(desc.text, SharedData.bossesSlain, SharedData.ennemiesSlain, SharedData.combatsFought, SharedData.deadPlayers, SharedData.enteredRooms,
			save.gold - SharedData.goldAtSessionStart);


		if (SharedData.newlyBeatenZones != null)
		{
			if (SharedData.newlyBeatenZones.Contains(2))
			{
				yakaIcon.SetActive(true);
				guardianIcon.SetActive(true);
			}
			else if (SharedData.newlyBeatenZones.Contains(1))
			{
				radxIcon.SetActive(true);
				cyborgIcon.SetActive(true);
			}
		}
	}

	void GenerateNFTs()
	{
		SaveData save = SaveData.Load();

		items = new List<string>();
		foreach (var item in SharedData.sharedInventory.FindAll(x => x.GetType() == typeof(NFTItem)))
		//for (int j = 0; j < 8; ++j)
		{
			int roll = Random.Range(0, 100);
			if (roll <= 35)
			{
				items.Add("La physique quantique pour les nuls (30po)");
				save.gold += 30;
			}
			else if (roll <= 62)
			{
				items.Add("Dessin de singe spatial (80po)");
				save.gold += 80;
			}
			else if (roll <= 82)
			{
				items.Add("Carte � collectionner (150po)");
				save.gold += 150;
			}
			else if (roll <= 94)
			{
				items.Add("Hologramme de L2-B2 (300po)");
				save.gold += 300;
			}
			else if (roll <= 100)
			{
				items.Add("Vaisseau de citoyen des �toiles (500po)");
				save.gold += 500;
			}
		}

		save.Save();
	}

	float counter = 0;
	// Update is called once per frame
	void Update()
	{
		counter += Time.deltaTime;

		if (counter > 0.05f)
		{
			counter = 0;

			nftText1.text = "";
			nftText2.text = "";

			for (int i = 0; i < items.Count; i++)
			{
				float itemStartTime = startTime + i * 0.5f;

				char[] itemName = items[i].ToCharArray();
				float progress = Mathf.Clamp((Time.time - itemStartTime) / scrambleTime, 0, 1);
				progress = EasingFunction.EaseInSine(0, 1, progress);
				int randLimit = Mathf.RoundToInt(itemName.Length * (1.0f - progress));
				for (int j = 0; j < randLimit; ++j)
				{
					itemName[j] = (char)(Random.Range(0x20, 0x7F)); // Random char effect, a la Matrix
				}

				string str = "<mspace=0.5em><color=yellow>" + new string(itemName) + "</color></mspace>";
				if (i != items.Count - 1)
				{
					str += "\n";
				}

				if (i % 2 == 0)
				{
					nftText1.text += str;

				}
				else
				{
					nftText2.text += str;
				}
			}
		}
	}
}
