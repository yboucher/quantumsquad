using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundFXList : MonoBehaviour
{
    public AudioClip plasma;
    public AudioClip machineGun;
    public AudioClip grenadeThrow;
    public AudioClip grenadeBoom;
    public AudioClip slash;
    public AudioClip swoosh;
    public AudioClip spray;
    public AudioClip shieldPowerup;
    public AudioClip moveSwoosh;
    public AudioClip demonScream;
    public AudioClip alienBattlecry;
    public AudioClip robotSounds;
    public AudioClip chargedCannon;
    public AudioClip fireball;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
