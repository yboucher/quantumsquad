using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarpEffect : MonoBehaviour
{
    public float elapsed = 0;
    public float duration = 3;
    public float scalingFactor = 720;

    Vector3 baseScale;
    // Start is called before the first frame update
    void Start()
    {
        baseScale = transform.localScale;
    }

    // Update is called once per frame
    void Update()
    {
        elapsed += Time.deltaTime;
        if (elapsed > duration)
            elapsed = duration;

        float progress = EasingFunction.EaseInCirc(0, 1, elapsed / duration);
        transform.localScale = new Vector3(progress, progress, progress)*scalingFactor + baseScale;
        transform.Rotate(0, 0, 90 * Time.deltaTime);
    }
}
