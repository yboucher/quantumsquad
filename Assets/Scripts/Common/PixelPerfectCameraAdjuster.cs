using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

public class PixelPerfectCameraAdjuster : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        // Disable pixel-perfect adjustments for resolutions that are too small (lower than 1080p)
        if (Screen.height < 1080 || Screen.width < 1920)
        {
            GetComponent<PixelPerfectCamera>().enabled = false;
            GetComponent<Camera>().orthographicSize = 3.9f;
        }
        else
            GetComponent<PixelPerfectCamera>().enabled = true;
    }
}
