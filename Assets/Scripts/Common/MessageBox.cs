using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MessageBox : MonoBehaviour
{
    public bool showOnStartup = false;
    public float popSpeed = 0.3f;
    public Vector2 size = new Vector2(10, 4);
    public TextMeshPro text;
    public bool closing = false;
    public bool open = false;
    public bool canBeClosed = true;
    public float autoCloseDelay = 4.0f;

    float openTime = -1;
    float startTime = -1;
    int showFrame;

    void Start()
    {
        if (showOnStartup)
        {
            GetComponent<SpriteRenderer>().enabled = true;
            text.gameObject.SetActive(true);
        }
        else if (startTime < 0) // Not yet been ordered to show
        {
            GetComponent<SpriteRenderer>().enabled = false;
            text.gameObject.SetActive(false);
        }
    }

    public void Show()
	{
        closing = false;
        open = true;
        openTime = Time.time;
        startTime = Time.time;
        GetComponent<SpriteRenderer>().enabled = true;
        text.gameObject.SetActive(false);
        ShowImpl();
        showFrame = Time.frameCount;
    }

    protected virtual void ShowImpl()
    { }

    public void Close()
	{
        GetComponent<SpriteRenderer>().enabled = true;
        text.gameObject.SetActive(false);
        closing = true;
        open = false;
        startTime = Time.time;
        CloseImpl();
    }

    protected virtual void CloseImpl()
    { }

    protected virtual void FullyOpen()
    { 
    }

    // Update is called once per frame
    void Update()
    {
        if (canBeClosed && open)
        { 
            if (Time.frameCount > showFrame && Input.GetButtonDown("Fire1") || Time.time - openTime >= autoCloseDelay)
                Close();
        }
        if (startTime > 0)
        {
            if (!closing)
                GetComponent<SpriteRenderer>().size = Vector2.Lerp(Vector2.zero, size, Mathf.Min(1, (Time.time - startTime) / popSpeed));
            else
                GetComponent<SpriteRenderer>().size = Vector2.Lerp(size, Vector2.zero, Mathf.Min(1, (Time.time - startTime) / popSpeed));
            if (Time.time - startTime >= popSpeed)
			{
                text.gameObject.SetActive(!closing);
                startTime = -1;
                if (closing)
                {
                    closing = false;
                    GetComponent<SpriteRenderer>().enabled = false;
                }
                else
				{
                    FullyOpen();
                }
			}
        }
    }
}
