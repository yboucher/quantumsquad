using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public class SharedData
{
	static public string universeName = "#37AF-42";
	static public List<Soldier> soldiers = null;
	static public List<Item> sharedInventory = null;
	static public int scoutingCharges = 1;
	static public int currentZone = 1; // 1-indexed
	static public int healRoomChanceBonus = 0;
	static public int elementalDamageBonus = 0;
	static public int curioBonus = 0;
	static public int critBonus = 0;
	static public int deadPlayers = 0;
	static public int bossesSlain = 0;
	static public int ennemiesSlain = 0;
	static public bool bossRoomRevealed = false;
	static public List<ShopItem> shopItems = null;
	static public List<int> newlyBeatenZones = new List<int>();
	static public int combatsFought = 0;
	static public int goldAtSessionStart = 0;
	static public int enteredRooms = 0;
	static public bool firstTime = true;

	static public List<Enemy> nextEnemyParty = null;
	static public CombatDifficulty nextCombatDifficulty = CombatDifficulty.NotSet;

	private static object GetDefault(System.Type type)
	{
		if (type.IsValueType)
		{
			return System.Activator.CreateInstance(type);
		}

		return null;
	}
	static public void Reset()
	{
		// Reset all static fields
		var fields = typeof(SharedData).GetFields(BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy | BindingFlags.Public);
		foreach (var fieldInfo in fields)
		{
			fieldInfo.SetValue(null, GetDefault(fieldInfo.FieldType));
		}
	}
}
