using UnityEngine;
using UnityEngine.UI;
using System.Collections;

 
public class ImageFadeIn : MonoBehaviour {
    
    public int secondsToWait;
    public int animationLength;
    Image image; 
    public void Start()
    {
        // fades the image out when you click
        image = GetComponent<Image>();
        var tempColor = image.color;
        tempColor.a = 0f;
        image.color = tempColor;
        StartCoroutine(FadeIn());
    }
 
    IEnumerator FadeIn()
    {
        yield return new WaitForSeconds(secondsToWait);
        for (float i = 0; i <= animationLength; i += Time.deltaTime)
        {
            // set color with i as alpha
            var tempColor = image.color;
            tempColor.a = (i / animationLength);
            image.color = tempColor;
            yield return null;
        }
    }
}