using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Assertions;

public class LevelLoader : MonoBehaviour
{
    public bool loading = false;
    public bool returnedToThisScene = false;
    public Animator transition;
    public MusicManager musicManager = null;
    public TransitionSwipeScript swipe = null;
    public bool useSwipe = false;
    static Stack<string> loadedSceneNames = new Stack<string>();
    static Stack<LevelLoader> previousLoaders = new Stack<LevelLoader>();

	public void Start()
	{
		if (useSwipe)
            swipe = FindObjectOfType<TransitionSwipeScript>();
	}

	public void LoadLevelByName(string name)
    {
        if (loading)
            return;
        loading = true;
        StartCoroutine(LoadLevelName(name, false));
    }
    public void LoadLevelByNameAdditive(string name)
    {
        if (loading)
            return;
        loading = true;
        StartCoroutine(LoadLevelName(name, true));
    }
    public void LoadLevelByName(string name, bool additive)
    {
        if (loading)
            return;
        loading = true;
        StartCoroutine(LoadLevelName(name, additive));
    }

    public void UnloadCurrentLevel()
	{
        if (loading)
            return;
        loading = true;
        StartCoroutine(UnloadLevel());
	}

    void FadeMusicManagers()
	{
        if (musicManager != null)
            musicManager.FadeMusic();
	}

    IEnumerator LoadLevelName(string name, bool additive = false)
    {
        Debug.Log("Loading level " + name);
        // Crossfade enable
        //transform.Find("CrossFade").gameObject.SetActive(true);

        // Reset cursor
        Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);

        FadeMusicManagers();

        if (swipe != null)
        {
            DontDestroyOnLoad(swipe.transform.parent);
            swipe.Swipe();
        }

        transition.SetTrigger("Start");
        var async = SceneManager.LoadSceneAsync(name, additive ? LoadSceneMode.Additive : LoadSceneMode.Single);
        async.allowSceneActivation = false;
        if (swipe != null)
            yield return new WaitForSeconds(1);
        else
            yield return new WaitForSeconds(2);
        async.allowSceneActivation = true;
        while (!async.isDone)
            yield return null;

        Debug.Log("size : " + loadedSceneNames.Count);

        if (additive)
		{
            loadedSceneNames.Push(name);
            // Mark current scene as inactive
            previousLoaders.Push(this);
            gameObject.SetActive(false);
        }
    }

    IEnumerator UnloadLevel()
    {
        Debug.Log("size : " + loadedSceneNames.Count);
        Assert.IsTrue(loadedSceneNames.Count > 0);
        Assert.IsTrue(loadedSceneNames.Count == previousLoaders.Count);

        FadeMusicManagers();

        Debug.Log("Unloading current level");
        if (swipe != null)
        {
            DontDestroyOnLoad(swipe.transform.parent);
            swipe.Swipe();
        }
        transition.SetTrigger("Start");

        yield return new WaitForSeconds(1);

        // Mark the previous scene as active again
        var prev = previousLoaders.Pop();
        prev.gameObject.SetActive(true);
        prev.Reloaded();
        //prev.gameObject.transform.Find("CrossFade").gameObject.SetActive(false); // Reset the crossfade

        var async = SceneManager.UnloadSceneAsync(loadedSceneNames.Pop());
        prev.Reloaded();
        async.allowSceneActivation = false;

        if (swipe != null)
            yield return new WaitForSeconds(1);
        else
            yield return new WaitForSeconds(2);

        async.allowSceneActivation = true;
        while (!async.isDone)
            yield return null;

        // Reset the crossfade of the previous scene
    }

    void Reloaded()
	{
        //transform.Find("CrossFade").gameObject.SetActive(false);
        if (musicManager != null)
            musicManager.ReplayLastClip();
        Debug.Log("Reloaded");
        returnedToThisScene = true;
        loading = false;
        // Reset the cursor
        Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
    }
}
