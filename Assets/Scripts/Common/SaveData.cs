using System.Collections;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;


[System.Serializable]
public class SaveData
{
	[System.Serializable]
	public class SaveDataSkillInfo
	{
		public SaveDataSkillInfo(string in_name, int in_level, string in_prettyName = "")
		{
			Name = in_name; Level = in_level; PrettyName = in_prettyName;
		}
		public string Name;
		public int Level;
		[System.NonSerialized]
		public string PrettyName;
	}

	public bool introPlayed = false;
	public int difficultyMode = 0;
	public int gold = 0;
	public int highestBeatenBoss = 0;
	public int unlockedPlayers = 2;
	public List<string> unlockedClasses = new List<string>{ "Shinobi", "Mastodonte" };
	public List<SaveDataSkillInfo> skillLevels = new List<SaveDataSkillInfo>();
	public bool introTutorialShown = false;
	public bool effectTutorialShown = false;
	public bool vulnTutorialShown = false;

	// Default initialization
	public SaveData()
	{
		// Load skill data
		var subclassTypes = Assembly
		   .GetAssembly(typeof(Action))
		   .GetTypes()
		   .Where(t => t.IsSubclassOf(typeof(Action)));
		foreach (var type in subclassTypes)
		{
			var attrs = System.Attribute.GetCustomAttributes(type, typeof(ActionDataAttribute));
			if (attrs.Length == 0)
				continue;
			var attr = (ActionDataAttribute)attrs[0];

			// Default to skills at level 0
			skillLevels.Add(new SaveData.SaveDataSkillInfo(type.Name, 0, attr.name));
		}
		skillLevels.Sort((x, y) => x.PrettyName.CompareTo(y.PrettyName));
	}

	public void Save()
	{
		string filepath = Application.persistentDataPath + "/save.json";
		Debug.Log("Persistent data saved to " + filepath);
		using (StreamWriter sw = new StreamWriter(filepath))
		{
			sw.WriteLine(JsonUtility.ToJson(this));
		}
	}

	public static SaveData Load()
	{
		string filepath = Application.persistentDataPath + "/save.json";
		if (!File.Exists(filepath))
			return new SaveData(); // File wasn't created yet

		using (StreamReader sr = new StreamReader(filepath))
		{
			string saveJSON = "";

			string line;
			while ((line = sr.ReadLine()) != null)
			{
				saveJSON += line;
			}

			return JsonUtility.FromJson<SaveData>(saveJSON);
		}
	}
}
