using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;

public class ButtonClickScript : MonoBehaviour
{
    [SerializeField] AudioClip _audioClip;
    [SerializeField][Range(0.0f, 1.0f)] float _volume = 1;
    AudioSource _audioSource;

    HashSet<Button> buttonsModified = new HashSet<Button>();

    void Awake()
    {
        _audioSource = gameObject.AddComponent<AudioSource>();
        if (_audioClip != null)
            _audioSource.clip = _audioClip;
        _audioSource.playOnAwake = false;
        _audioSource.volume = _volume;

    }

    void Start()
	{
        foreach (var button in FindObjectsOfType<Button>())
        {
            button.onClick.AddListener(() => _audioSource.Play());
            buttonsModified.Add(button);
        }
    }

	void Update()
	{
        foreach (var button in FindObjectsOfType<Button>())
        {
            if (!buttonsModified.Contains(button))
            {
                button.onClick.AddListener(() => _audioSource.Play());
                buttonsModified.Add(button);
            }
        }
    }
}
