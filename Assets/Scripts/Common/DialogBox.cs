using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class DialogBox : MessageBox
{
	public GameObject dialogButtons;
	public TextMeshProUGUI acceptedText;
	public TextMeshProUGUI refusedText;
	public TextMeshProUGUI useItemText;
	public Button useItemButton;

	public bool pressed = false;
	public bool accepted = false;
	public bool itemUsed = false;

	public DialogBox()
	{
		canBeClosed = false;
	}

	protected override void ShowImpl()
	{
		if (dialogButtons.transform.Find("AcceptButton") != null)
			dialogButtons.transform.Find("AcceptButton").GetComponent<Button>().Select();
	}

	protected override void FullyOpen()
	{
		dialogButtons.SetActive(true);
		
		if (dialogButtons.transform.Find("AcceptButton") != null)
		{
			// Reset the selected item (Unity doesn't like selecting a button just after activating it)
			EventSystem.current.GetComponent<EventSystem>().SetSelectedGameObject(null);
			dialogButtons.transform.Find("AcceptButton").GetComponent<Button>().Select();
		}
	}

	protected override void CloseImpl()
	{
		dialogButtons.SetActive(false);
	}

	public void UseItemPressed()
	{
		pressed = true;
		accepted = true;
		itemUsed = true;
	}
	public void AcceptPressed()
	{
		pressed = true;
		accepted = true;
		itemUsed = false;
	}

	public void RefusePressed()
	{
		pressed = true;
		accepted = false;
		itemUsed = false;
	}
}
