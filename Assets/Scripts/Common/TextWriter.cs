using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
//using UnityEngine.InputSystem;
using TMPro;

public class TextWriter : MonoBehaviour
{
	public char screenShakeChar = '*';
	public char invisibleDelayChar = '+';
    public AudioSource talkingSound;

    private TMP_Text tmp_Text;
    private string textToWrite;
    private int index;
    private float timer;
    private float timePerCharacter;
    private bool invisibleCharacters;
    private AudioSource animaleseSound;
    private float delay = 0.0f;

    private void Start()
    {
        animaleseSound = GetComponent<AudioSource>();
    }
    
    public bool Done()
    {
    	return tmp_Text == null;
    }

	public void Reset()
	{
        tmp_Text = null;
	}

	public void AddWriter(TMP_Text _tmp_Text, string _textToWrite, float _timePerCharacter, bool _invisibleCharacters, AudioSource _talkingSound, float pitch)
    {
        tmp_Text = _tmp_Text;
        textToWrite = _textToWrite;
        timePerCharacter = _timePerCharacter;
        //timePerCharacter /= PlayerPrefs.GetFloat("dialg_speed", 1);
        invisibleCharacters = _invisibleCharacters;
        //talkingSound = _talkingSound;
        index = 0;
        timer = 0;
        delay = 0.0f;
    }

    private void Update()
    {
        if (tmp_Text != null)
        {
            timer -= Time.deltaTime;
            while (timer <= 0f)
            {
                string text = "";

                timer += timePerCharacter;
                // Si il y a un d�lai actif, on attend, sinon on affiche le prochain char
                if (delay > 0.0f)
            	{
                    if (talkingSound.isPlaying)
                        talkingSound.Stop();
            		delay -= timePerCharacter;
            		continue;
            	}
                if (!talkingSound.isPlaying)
                    talkingSound.Play();
                // Si le charact�re � afficher est de la ponctuation, on ajoute un petit d�lai
                if (textToWrite[index] == ',' || textToWrite[index] == ';' || textToWrite[index] == ':')
                	delay = 0.25f;
                if (textToWrite[index] == '.' || textToWrite[index] == '!' ||textToWrite[index] == '?' || textToWrite[index] == '\n'
                   || textToWrite[index] == invisibleDelayChar)
                {
                	// Pas de d�lai si il s'agit d'une s�rie de charact�res de ponctuation identiques (style ??? ou ...)
                	if (index+1 < textToWrite.Length && textToWrite[index+1] == textToWrite[index])
                	    delay = 0;
                	else
                		delay = 0.5f;
                }
                
                index++;

                // Si le caract�re est un tag, on l'affiche imm�diatement
                if (index < textToWrite.Length && textToWrite[index] == '<')
                {
                    while (textToWrite[index] != '>')
                    {
                        text += textToWrite[index++];
                    }
                    text += textToWrite[index++];
                }

                if (index < textToWrite.Length && textToWrite[index] == screenShakeChar)
                {
                	//GetComponent<ScreenShake>().Shake();
                }
                text = textToWrite.Substring(0, index).Replace("*", "").Replace(invisibleDelayChar.ToString(), "");
                var textWithoutTags = textToWrite.Substring(index);
                textWithoutTags = Regex.Replace(textWithoutTags, "<[^>]*>", "");


                if (invisibleCharacters)
                {
                    text += "<color=#00000000>" + textWithoutTags + "</color>";
                }
                tmp_Text.text = text;
                //talkingSound.Play();
                if (index >= textToWrite.Length)
                {
                    tmp_Text = null;
                    return;
                }
            }
        }
        else
		{
            if (talkingSound.isPlaying)
                talkingSound.Stop();
        }
    }

    public void EndWriter()
    {
        if (tmp_Text != null)
        {
            index = textToWrite.Length - 1;
            animaleseSound.Stop();
        }
    }
}