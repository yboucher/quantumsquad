using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
	AudioSource audioSource;
    
    float baseVolume = 1.0f;

    bool fading = false;
    float fadeDuration = 1.0f;
    float fadeStart = -1;

    void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        baseVolume = audioSource.volume /* * PlayerPrefs.GetFloat("volume", 1) */;
    }
    
    public void SetRelativeVolume(float vol)
    {
        audioSource.volume = vol*baseVolume;
    }
    
    public void ReplayLastClip()
	{
        fading = false;
        SetRelativeVolume(1);

        audioSource.Play();
	}
    public void PlayMusic(AudioClip clip)
    {
        fading = false;
        SetRelativeVolume(1);

        audioSource.loop = true;
    	audioSource.clip = clip;
    	audioSource.Play();
    }

    public void PlayJingle(AudioClip clip)
    {
        fading = false;
        SetRelativeVolume(1);

        audioSource.loop = false;
        audioSource.clip = clip;
        audioSource.Play();
    }

    public void FadeMusic()
	{
        fading = true;
        fadeStart = Time.time;
	}

    public void PauseMusic()
    {
    	audioSource.Pause();
    }
    
    public void StopMusic()
    {
    	audioSource.Stop();
    }

    // Update is called once per frame
    void Update()
    {
        if (fading && Time.time < fadeStart + fadeDuration)
		{
            SetRelativeVolume(Mathf.Lerp(1, 0, (Time.time - fadeStart) / fadeDuration));
		}
    }
}
