using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using SimpleJSON;
public class Tweakables
{
    public class Range
	{
		public Range(float lo, float hi)
		{
			this.lo = lo;
			this.hi = hi;
		}
        public float lo, hi;
	}

    static Dictionary<string, Range> tweakables = new Dictionary<string, Range>();
	public static int GetSingle(string id, int defaultVal)
	{
		if (!tweakables.ContainsKey(id))
			tweakables.Add(id, new Range(defaultVal, defaultVal));
		return (int)tweakables[id].lo;
	}

	public static float GetSingle(string id, float defaultVal)
	{
		if (!tweakables.ContainsKey(id))
			tweakables.Add(id, new Range(defaultVal, defaultVal));
		return tweakables[id].lo;
	}
	public static Range GetRange(string id, float defaultValLo, float defaultValHi)
	{
		if (!tweakables.ContainsKey(id))
			tweakables.Add(id, new Range(defaultValLo, defaultValHi));
		return tweakables[id];
	}

	public static void Load()
	{
		System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
		customCulture.NumberFormat.NumberDecimalSeparator = ".";
		System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

		SaveData save = SaveData.Load();

		string filepath = Application.dataPath + "/.." + "/tweaks.json";
		if (save.difficultyMode == 1) // Hardcore mode
			filepath = Application.dataPath + "/.." + "/tweaks_hard.json";
		if (!File.Exists(filepath))
			return; // File wasn't created yet

		var jsonString = File.ReadAllText(filepath);
		JSONNode data = JSON.Parse(jsonString);
		foreach (KeyValuePair<string, JSONNode> record in data)
		{
			//Debug.Log("nombre: " + record + ", is array : " + record.Value.IsArray);
			if (record.Value.IsArray)
				tweakables[record.Key] = new Range(record.Value[0].AsFloat, record.Value[1].AsFloat);
			else
				tweakables[record.Key] = new Range(record.Value.AsFloat, record.Value.AsFloat);
		}
	}
	public static void Save()
	{
		SaveData save = SaveData.Load();


		string data = "{\n";
		foreach (var pair in tweakables)
		{
			data += "    \"" + pair.Key + "\" : ";
			if (pair.Value.lo == pair.Value.hi)
				data += pair.Value.lo;
			else
				data += "[" + pair.Value.lo + ", " + pair.Value.hi + "]";
			data += ",\n";
		}
		data += "}";

		Debug.Log("Saving : " + data);

		string filepath = Application.dataPath + "/.." + "/tweaks.json";
		if (save.difficultyMode == 1) // Hardcore mode
			filepath = Application.dataPath + "/.." + "/tweaks_hard.json";

		Debug.Log("Persistent data saved to " + filepath);
		using (StreamWriter sw = new StreamWriter(filepath))
		{
			sw.WriteLine(data);
		}
	}
}
