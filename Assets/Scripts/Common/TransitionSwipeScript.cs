using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TransitionSwipeScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
    }

	private void OnEnable()
	{
        if (GetComponent<Image>() != null)
            GetComponent<Image>().material.SetFloat("_StartTime", Time.timeSinceLevelLoad);
        if (GetComponent<SpriteRenderer>() != null)
            GetComponent<SpriteRenderer>().material.SetFloat("_StartTime", Time.timeSinceLevelLoad);
    }

    public void Swipe()
	{
        GetComponent<Image>().enabled = true;
        GetComponent<Image>().material.SetFloat("_StartTime", Time.time);

        //Destroy(gameObject, 2.0f);
    }

	// Update is called once per frame
	void Update()
    {
        GetComponent<Image>().material.SetFloat("_GlobalTime", Time.time);
    }
}
