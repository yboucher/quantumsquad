using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LootMessageBox : MessageBox
{
	int money;
	List<Item> items;

	public LootMessageBox(int money, List<Item> items, string title = "Butin r�cup�r� :\n")
	{
		canBeClosed = true;

		this.money = money;
		this.items = items;

		text.text = title;
		if (money > 0)
			text.text += "\n<color=yellow>+" + money + " $";
		foreach (var item in items)
		{
			text.text += "\n<color=green>+1 " + item.name + "</color>";
			SharedData.sharedInventory.Add(item);
		}
	}

	protected override void ShowImpl()
	{

	}

	protected override void FullyOpen()
	{
		
	}

	protected override void CloseImpl()
	{
		
	}
}
