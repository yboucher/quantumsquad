using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClassInfo
{
	public int HP;
	public int dodge;
	public int crit;
	public List<string> Abilities;
	public string desc;
	// TODO : sprite

	static public Dictionary<string, ClassInfo> Classes()
	{
		ClassInfo mastodonte = new ClassInfo();
		mastodonte.HP = Tweakables.GetSingle("mastodonte_HP", 200);
		mastodonte.dodge = Tweakables.GetSingle("mastodonte_dodge", 0);
		mastodonte.crit = Tweakables.GetSingle("mastodonte_crit", 6);
		mastodonte.Abilities = new List<string>{ "BulletFlurryAction", "HumanShieldAction", "ZapAction" };
		mastodonte.desc = "Un humain compos� de chair, d�os ... et de m�tal. Ce cyborg utilise son blindage pour couvrir ses alli�.";

		ClassInfo shinobi = new ClassInfo();
		shinobi.HP = Tweakables.GetSingle("shinobi_HP", 75);
		shinobi.dodge = Tweakables.GetSingle("shinobi_dodge", 15);
		shinobi.crit = Tweakables.GetSingle("shinobi_crit", 12);
		shinobi.Abilities = new List<string> { "SlashAction", "FireSlashAction", "BleedingSlashAction" };
		shinobi.desc = "Ce ma�tre du combat rapproch� a d�pens� une fortune dans une lame � incandescence permettant de reproduire les attaques de son h�ros de manga favori.";

		ClassInfo savon = new ClassInfo();
		savon.HP = Tweakables.GetSingle("savon_HP", 125);
		savon.dodge = Tweakables.GetSingle("savon_dodge", 8);
		savon.crit = Tweakables.GetSingle("savon_crit", 5);
		savon.Abilities = new List<string> { "ShootAction", "FocusedShotAction", "FlashAction" };
		savon.desc = "Le couteau suisse de l��quipe capable de mener des offensives et de d�stabiliser l�ennemi, �quip� d�un fusil plasma.";

		ClassInfo radx = new ClassInfo();
		radx.HP = Tweakables.GetSingle("radx_HP", 100);
		radx.dodge = Tweakables.GetSingle("radx_dodge", 10);
		radx.crit = Tweakables.GetSingle("radx_crit", 10);
		radx.Abilities = new List<string> { "GammaShootAction", "HealSprayAction", "WeakeningRayAction" };
		radx.desc = "Le dernier bruit que vos ennemis entendront sera le gr�sillement de son compteur Geiger.";

		ClassInfo cyborg = new ClassInfo();
		cyborg.HP = Tweakables.GetSingle("cyborg_HP", 150);
		cyborg.dodge = Tweakables.GetSingle("cyborg_dodge", 6);
		cyborg.crit = Tweakables.GetSingle("cyborg_crit", 7);
		cyborg.Abilities = new List<string> { "CyborgAoEAction", "CyborgPunchAction", "CyborgIntimidateAction" };
		cyborg.desc = "Ce soldat a remplac� ses membres par des implants �lectro-quantiques lui permettant de contr�ler le feu et l'�lectricit�.";

		ClassInfo yaka = new ClassInfo();
		yaka.HP = Tweakables.GetSingle("yaka_HP", 80);
		yaka.dodge = Tweakables.GetSingle("yaka_dodge", 10);
		yaka.crit = Tweakables.GetSingle("yaka_crit", 11);
		yaka.Abilities = new List<string> { "AimBotAction", "YakaAction", "NanomachineAction" };
		yaka.desc = "L'ing�nieur utilise ses comp�tences techniques pour laisser ses machines infliger des attaques d�vastatrices sans avoir � se salir les mains.";

		Dictionary<string, ClassInfo> classes = new Dictionary<string,ClassInfo>();
		classes["Mastodonte"] = mastodonte;
		classes["Shinobi"] = shinobi;
		classes["Savon"] = savon;
		classes["RadX"] = radx;
		classes["Cyborg"] = cyborg;
		classes["Ing�nieur"] = yaka;

		return classes;
	}

	static public void UnlockNewClass()
	{
		// TODO : implement
	}
}
