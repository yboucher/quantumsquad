using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using System.Linq;
using System.Reflection;

public class DebugUtils : MonoBehaviour
{
    class SkillInfo
    {
        public string id;
        public string name;
        public string desc;
        public List<ActionMainStatAttribute> attrs = new List<ActionMainStatAttribute>();
        public int level = 0;
        public string usedByClass;
    }

    static public bool godmode = false;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);

        Tweakables.Load();

        var saveData = SaveData.Load();
        Debug.Log("Save data : " + JsonUtility.ToJson(saveData));

        // Initialize the default party
        if (SharedData.soldiers == null)
		{
            SharedData.sharedInventory = new List<Item>();
            //SharedData.sharedInventory.Add(new NFTItem());
            //SharedData.sharedInventory.Add(new NFTItem());
            //SharedData.sharedInventory.Add(new NFTItem());
            //SharedData.sharedInventory.Add(new NFTItem());
            //SharedData.sharedInventory.Add(new Bandage());

            SharedData.soldiers = new List<Soldier>();
            SharedData.soldiers.Add(CreateSoldier("Mastodonte")); SharedData.soldiers.Add(CreateSoldier("Shinobi"));
            SharedData.soldiers.Add(CreateSoldier("RadX")); SharedData.soldiers.Add(CreateSoldier("Ingénieur"));
            //SharedData.sharedInventory.Add(new AccessCardItem());
            SharedData.scoutingCharges = saveData.highestBeatenBoss + 1;
        }

        LoadSkills();
    }

    void LoadSkills()
	{
        SaveData saveData = SaveData.Load();

        List<SkillInfo> skillInfo = new List<SkillInfo>();

        // List Ability subclasses
        var subclassTypes = Assembly
           .GetAssembly(typeof(Action))
           .GetTypes()
           .Where(t => t.IsSubclassOf(typeof(Action)));

        int skillIdx = 0;
        foreach (var type in subclassTypes)
        {
            var attrs = System.Attribute.GetCustomAttributes(type, typeof(ActionDataAttribute));
            if (attrs.Length == 0)
                continue;
            var attr = (ActionDataAttribute)attrs[0];
            Debug.Log(type.Name + " / " + attr.name);
            ++skillIdx;

            SkillInfo info = new SkillInfo();
            info.id = type.Name;
            info.name = attr.name;
            info.desc = attr.desc;

            var mainStats = type.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance)
                                .Where(prop => prop.IsDefined(typeof(ActionMainStatAttribute), false));
            foreach (var stat in mainStats)
            {
                var statAttr = stat.GetCustomAttribute<ActionMainStatAttribute>();
                Debug.Log(stat.Name + " / " + statAttr.name + " : " + statAttr.baseValue + " (" + statAttr.step + ")");
                info.attrs.Add(statAttr);
            }

            var usedByClass = "";
            foreach (var pair in ClassInfo.Classes())
            {
                if (!saveData.unlockedClasses.Contains(pair.Key))
                    continue;
                if (pair.Value.Abilities.Contains(type.Name))
                {
                    usedByClass = pair.Key;
                    break;
                }
            }
            // This skill is used by no class
            if (usedByClass == "")
                continue;
            //Debug.Log("Skill " + info.name + " is used by class " + usedByClass);
            info.usedByClass = usedByClass;

            skillInfo.Add(info);
        }
        // Sort skills by alphabetical order
        skillInfo.Sort((x, y) => x.name.CompareTo(y.name));
        // Load levels from save data
        foreach (var savedSkill in saveData.skillLevels)
        {
            var info = skillInfo.Find(x => x.id == savedSkill.Name);
            if (info == null)
            {
                Debug.Log("Uknown skill " + savedSkill.Name + " from save data");
                continue;
            }
            info.level = savedSkill.Level;
        }

        saveData.skillLevels.Clear();

        foreach (var skill in skillInfo)
        {
            saveData.skillLevels.Add(new SaveData.SaveDataSkillInfo(skill.id, skill.level));
        }

        // Update soldier skills on the fly
        foreach (var soldier in SharedData.soldiers)
            foreach (var skill in soldier.Abilities)
            {
                var level = saveData.skillLevels.Find(x => x.Name == skill.GetType().Name);
                if (level != null)
                    skill.SetTier(level.Level);
            }

        saveData.Save();
    }

	private void OnDestroy()
	{
        Tweakables.Save();
	}

	// Update is called once per frame
	void Update()
    {
        //return;


        // Toggle god mode
        if (Input.GetKeyDown(KeyCode.G))
		{
            godmode = !godmode;
		}
        // Switch to skills menu
        if (Input.GetKeyDown(KeyCode.X))
        {
            FindObjectOfType<LevelLoader>().LoadLevelByName("UpgradePhase", true); // Additive load
        }
        // Spawn an enemy
        if (Input.GetKeyDown(KeyCode.Percent))
        {
            if (FindObjectOfType<CombatManager>() != null)
                FindObjectOfType<CombatManager>().SpawnEnemy(Zone1Enemy2.Create());
        }
        // Switch to shop menu
        if (Input.GetKeyDown(KeyCode.Y))
        {
            FindObjectOfType<LevelLoader>().LoadLevelByName("ShopScene", true); // Additive load
        }
        // Pretend we killed the current boss
        if (Input.GetKeyDown(KeyCode.O))
        {
            ++SharedData.bossesSlain;
        }
        // Switch to combat
        if (Input.GetKeyDown(KeyCode.C))
        {
            FindObjectOfType<LevelLoader>().LoadLevelByName("CombatScene", false);
        }
        // Switch to Exploration
        if (Input.GetKeyDown(KeyCode.E))
        {
            FindObjectOfType<LevelLoader>().LoadLevelByName("ExplorationPhase", true);
        }
        // Unload current scene
        if (Input.GetKeyDown(KeyCode.U))
        {
            FindObjectOfType<LevelLoader>().UnloadCurrentLevel();
        }
        // Reset save data
        if (Input.GetKeyDown(KeyCode.M))
        {
            SaveData saveData = new SaveData();
            saveData.Save();
        }
        // Reveal all rooms in the exploration scene
        // Debug commands
        if (Input.GetKeyDown(KeyCode.H) && FindObjectOfType<SquadIndicator>())
        {
            FindObjectOfType<SquadIndicator>().DebugToggleRoomsRevealed();
        }
        // List all items in the console
        if (Input.GetKeyDown(KeyCode.I))
		{
            Debug.Log("Common Items : " + System.String.Join(" / ", Item.ItemsByRarity(ItemRarity.Common)));
            Debug.Log("Rare Items : " + System.String.Join(" / ", Item.ItemsByRarity(ItemRarity.Rare)));
            Debug.Log("Legendary Items : " + System.String.Join(" / ", Item.ItemsByRarity(ItemRarity.Legendary)));
        }
        // Regenerate the rooms
        if (Input.GetKeyDown(KeyCode.P))
		{
            foreach (var map in FindObjectsOfType<SpaceshipMapGenerator>())
                FindObjectOfType<RoomTypeGenerator>().GenerateRooms(map);
		}
        // Ajoute 500 de gold
        if (Input.GetKeyDown(KeyCode.B))
        {
            var save = SaveData.Load();
            save.gold += 500;
            save.Save();
            if (FindObjectOfType<SkillTree>() != null)
                FindObjectOfType<SkillTree>().UpdateGoldCount();
            if (FindObjectOfType<ShopScript>() != null)
                FindObjectOfType<ShopScript>().UpdateGoldCount();
        }
    }

    static void LoadClassInfo(Soldier soldier)
    {
        var classInfoDict = ClassInfo.Classes();
        Assert.IsTrue(classInfoDict.ContainsKey(soldier.Class));
        var classInfo = classInfoDict[soldier.Class];
        var saveData = SaveData.Load();

        soldier.MaxHP = soldier.HP = classInfo.HP;
        soldier.SetBaseCrit(classInfo.crit);
        soldier.SetBaseDodge(classInfo.dodge);

        Dictionary<string, Action> tempDict = new Dictionary<string, Action>();
        foreach (var skill in saveData.skillLevels)
        {
            System.Type type = System.Type.GetType(skill.Name);
            if (type == null)
            {
                Debug.Log("Invalid skill " + skill.Name + " read from save data");
                continue;
            }
            Action act = (Action)System.Activator.CreateInstance(type);
            if (act == null)
            {
                Debug.Log("Unable to instantiate skill " + skill.Name + " read from save data");
                continue;
            }
            act.SetTier(skill.Level);
            //Debug.Log("Has skill " + type + " at tier " + skill.Level + ", charges " + act.Charges);

            // Only add the abilities that the class can possess
            if (classInfo.Abilities.Contains(skill.Name))
                tempDict[skill.Name] = act;
        }
        // Order the abilities according to the ClassInfo order
        foreach (var classAbility in classInfo.Abilities)
		{
            if (!tempDict.ContainsKey(classAbility))
                continue;
            soldier.Abilities.Add(tempDict[classAbility]);
        }

        soldier.Abilities.Add(new GrenadeAction()); // Grenades are for everyone
        soldier.Abilities.Add(new UseItemAction());
    }

    public static Soldier CreateSoldier(string Class)
    {
        Soldier testSoldier = new Soldier();
        testSoldier.Class = Class;
        testSoldier.MaxHP = 100;
        testSoldier.HP = 80;
        testSoldier.MaxArmor = 20;
        testSoldier.Armor = 20;
        testSoldier.Actions = testSoldier.DefaultActions = Tweakables.GetSingle("soldier_actions", 2);
        testSoldier.ActorName = "Soldier" + (SharedData.soldiers.Count+1);
        testSoldier.Enemy = false;
        testSoldier.Skills.Add(new ActorSkillInfo("Tirer", 2));
        testSoldier.Items = SharedData.sharedInventory;
        Weapon wp = new Weapon();
        wp.Name = "Glamdring";
        testSoldier.Weapons.Add(wp);
        /*
        var shootAction = new ShootAction(); shootAction.accuracy = 85;
        testSoldier.Abilities.Add(shootAction);
        testSoldier.Abilities.Add(new GrenadeAction());
        testSoldier.Abilities.Add(new FocusedShotAction());
        testSoldier.Abilities.Add(new FlashAction());
        testSoldier.Abilities.Add(new HumanShieldAction());
        testSoldier.Abilities.Add(new HealSprayAction());
        testSoldier.Abilities.Add(new ZapAction());
        testSoldier.Abilities.Add(new BleedingSlashAction());
        */
        LoadClassInfo(testSoldier);
        //testSoldier.AddEffect(new BleedingEffect(10));

        return testSoldier;
    }
}
