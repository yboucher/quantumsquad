using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class BeforeGameScript : MonoBehaviour
{
    public TextMeshProUGUI enterText;

    // Start is called before the first frame update
    void Start()
    {
        enterText.text = string.Format(enterText.text, SharedData.universeName);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
