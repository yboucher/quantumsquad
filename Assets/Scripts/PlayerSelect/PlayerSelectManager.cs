using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Linq;

public class PlayerSelectManager : MonoBehaviour
{
    public TextMeshProUGUI[] soldierNames;
    public TextMeshProUGUI[] classNames;
    public TextMeshProUGUI[] desc;
    public Animator[] animators;
    public GameObject[] crosses;
    public GameObject[] buttons;
    public TextAsset nameListAsset;
    public GameObject blackHole;

    int[] classIdx = new int[4] { 0, 0, 0, 0 };

    List<ClassInfo> classes = new List<ClassInfo>();
    List<string> classInfoNames = new List<string>();
    string[] nameList;
    SaveData saveData;

    int MaxPlayers()
	{
        int maxPlayers = saveData.highestBeatenBoss + 2;
        if (saveData.difficultyMode == 1)
            maxPlayers = Mathf.Max(3, saveData.highestBeatenBoss + 2); // 3 Players min in hard mode
        if (maxPlayers > 4)
            maxPlayers = 4;
        return maxPlayers;
    }

    // Start is called before the first frame update
    void Start()
    {
        saveData = SaveData.Load();

        SharedData.goldAtSessionStart = saveData.gold;

        nameList = nameListAsset.text.Split(new[] { "\r\n", "\r", "\n" }, System.StringSplitOptions.None);
        // Order the list randomly
        nameList = nameList.OrderBy(a => Random.Range(0, 1000000)).ToArray();

        foreach (var info in ClassInfo.Classes())
        {
            if (!saveData.unlockedClasses.Contains(info.Key))
                continue;
            classes.Add(info.Value);
            classInfoNames.Add(info.Key);
        }

        List<int> randomShuffle = new List<int>();
        for (int i = 0; i < classes.Count; i++)
            randomShuffle.Add(i);
        randomShuffle = randomShuffle.OrderBy(a => Random.Range(0, 1000000)).ToList();

        for (int i = 0; i < 4; ++i)
		{
            if (i < MaxPlayers()) // 1st boss defeated : 3 players; 2nd boss defeated : 4 players
			{
                crosses[i].SetActive(false);
                ChangeClass(i, randomShuffle[i % randomShuffle.Count]); // Set class at random
			}
            else
			{
                animators[i].GetComponent<SpriteRenderer>().enabled = false;
                desc[i].gameObject.SetActive(false);
                crosses[i].SetActive(true);
                buttons[i].SetActive(false);
                soldierNames[i].gameObject.SetActive(false);
                classNames[i].text = "Verrouillé";
            }
		}
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void NextClass(int idx)
    {
        ChangeClass(idx, 1);
    }
    public void PrevClass(int idx)
    {
        ChangeClass(idx, classes.Count-1); // Equivalent to (-1), but works with C#'s modulo operator
    }
    public void ChangeClass(int idx, int direction)
    { 
        int nextClassIdx = (classIdx[idx] + direction) % classes.Count;
        classIdx[idx] = nextClassIdx;
        var classInfo = classes[classIdx[idx]];
        var className = classInfoNames[classIdx[idx]];

        // Give a different name to the classes of this player slot
        soldierNames[idx].text = nameList[idx*classes.Count + classIdx[idx]];

        classNames[idx].text = "<color=#00A9FF>" + className;
        desc[idx].text = classInfo.desc;

        var anim = FindObjectOfType<AnimList>().anims.Find(x => x.name == className);
        animators[idx].runtimeAnimatorController = anim.anim;
        animators[idx].Update(0.0f);
    }

    public void EnterExploration()
	{
        SharedData.soldiers = new List<Soldier>();
        for (int i = 0; i < MaxPlayers(); ++i)
        {
            var soldier = DebugUtils.CreateSoldier(classInfoNames[classIdx[i]]);
            soldier.ActorName = soldierNames[i].text;
            SharedData.soldiers.Add(soldier);
        }

        StartCoroutine(EnterExplorationAnim());
	}

    IEnumerator EnterExplorationAnim()
	{
        blackHole.SetActive(true);
        yield return new WaitForSeconds(2);

        FindObjectOfType<LevelLoader>().LoadLevelByName("ExplorationPhase");
    }
}
