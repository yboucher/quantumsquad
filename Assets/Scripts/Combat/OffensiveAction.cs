using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public abstract class OffensiveAction : Action
{
	public abstract Tuple<int, int> DamageRange();
}
