using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using TMPro;

public class SceneMouseHoverMonitor : MonoBehaviour
{
    public Material hoverMaterial;
    public Texture2D targetCursor;
    public GameObject pickTargetBox;
    public TextMeshProUGUI pickTargetText;

    Dictionary<SpriteRenderer, Material> savedMaterials = new Dictionary<SpriteRenderer, Material>();

    bool targetSelectionMode = false;
    public string targetTag = "Enemy";
    GameObject hoveredObject = null;
    GameObject targetObject = null;
    int targetModeActivationFrame = 0;
    Color defaultOutlineColor;
    public GameObject HoveredObject()
	{
        return hoveredObject;
	}

    // Start is called before the first frame update
    void Start()
    {
        defaultOutlineColor = new Color(0, 0xA9 / 255.0f, 0xFF / 255.0f);
        hoverMaterial.SetColor("_OutlineColor", defaultOutlineColor);
        TargetSelectionMode("Enemy");
        ExitTargetMode();
    }

    public void TargetSelectionMode(string inTargetTag)
	{
        //Debug.Log("Entered frame " + Time.frameCount);
        targetModeActivationFrame = Time.frameCount;

        targetTag = inTargetTag;
        targetSelectionMode = true;
        targetObject = null;
        pickTargetBox.SetActive(true);
        if (inTargetTag == "Enemy")
        {
            hoverMaterial.SetColor("_OutlineColor", Color.red);
            pickTargetText.text = "S�lectionner une cible";
        }
        else
        {
            hoverMaterial.SetColor("_OutlineColor", Color.green);
            pickTargetText.text = "S�lectionner un alli�";
        }

        FindObjectOfType<TargetIndicator>().ResetSelection();

        Cursor.SetCursor(targetCursor, Vector2.zero, CursorMode.Auto);
    }

    public void ExitTargetMode()
	{
        targetSelectionMode = false;
        hoverMaterial.SetColor("_OutlineColor", defaultOutlineColor);
        targetObject = null;
        pickTargetBox.SetActive(false);

        Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
    }

    public GameObject SelectedTarget()
	{
        return targetObject;
	}

    void ResetMaterials()
	{
        SpriteRenderer[] components = GameObject.FindObjectsOfType<SpriteRenderer>();
        foreach (var renderer in components)
            // if the sprite has a collider <=> hoverable
            if (renderer.gameObject.GetComponent<PolygonCollider2D>())
            {
                if (savedMaterials.ContainsKey(renderer))
                    renderer.material = savedMaterials[renderer];
                else
                    savedMaterials.Add(renderer, renderer.material);
            }
    }

    void HighlightObject(GameObject selectedObject)
	{
        if (!targetSelectionMode || selectedObject.CompareTag(targetTag))
        {
            if (targetSelectionMode)
                FindObjectOfType<TargetIndicator>().SelectEnemy(selectedObject);

            ResetMaterials();
            selectedObject.GetComponent<SpriteRenderer>().material = hoverMaterial;
            hoveredObject = selectedObject;
            if (Input.GetButtonDown("Fire1") && targetSelectionMode && Time.frameCount != targetModeActivationFrame)
            {
                targetObject = selectedObject;
                pickTargetBox.SetActive(false);
                Debug.Log("Hit frame " + Time.frameCount);
                Debug.Log(selectedObject.name + ", " + selectedObject.tag);
            }
        }
    }

    void Update()
    {
        FindObjectOfType<TargetIndicator>().gameObject.GetComponent<SpriteRenderer>().enabled = targetSelectionMode;

        if (!targetSelectionMode)
        { 
            ResetMaterials(); // Latch the hover effect only for the targeting mode
            hoveredObject = null;
        }

        var cam = Camera.main;
        Vector3 mousePosition = cam.ScreenToWorldPoint(Input.mousePosition);
        mousePosition.z = -10;
        RaycastHit2D[] hits = Physics2D.RaycastAll(mousePosition, cam.transform.position - mousePosition, 0, 001);
        if (hits.Length == 0)
        {
            if (targetSelectionMode && FindObjectOfType<TargetIndicator>().SelectedEnemy() != null)
                HighlightObject(FindObjectOfType<TargetIndicator>().SelectedEnemy());
            return;
        }
        // Get the hit corresponding to the object on top
        RaycastHit2D hit = hits.OrderBy(x => x.collider.gameObject.GetComponent<SpriteRenderer>().sortingOrder).Last();

        if (hit && hit.collider.gameObject.GetComponent<SpriteRenderer>() && 
            ((!targetSelectionMode && hit.collider.gameObject.CompareTag("Soldier")) || hit.collider.gameObject.CompareTag("Enemy")))
        {
            //Debug.Log("Hovering " + hit.collider.gameObject);
            GameObject selectedObject = hit.collider.gameObject;
            HighlightObject(selectedObject);
        }
        else if (targetSelectionMode && FindObjectOfType<TargetIndicator>().SelectedEnemy() != null)
		{
            HighlightObject(FindObjectOfType<TargetIndicator>().SelectedEnemy());
		}
    }
}
