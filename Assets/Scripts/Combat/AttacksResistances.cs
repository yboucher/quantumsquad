using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AttackType
{
	Ballistic,
	Electric,
	Blast,
	Melee,
	Radioactive,
	Fire,
	Bleeding,
	Poison,
	Unavoidable, // Damage that cannot in any way be avoided
	Harmless
}

public class AttackTypeUtils
{
	public static string TypeToVulnerabilityString(AttackType type)
	{
		switch (type)
		{
			default:
				return type.ToString();
			case AttackType.Ballistic:
				return "aux armes � feu";
				case AttackType.Electric:
				return "� l'�lectricit�";
				case AttackType.Blast:
				return "aux explosions";
				case AttackType.Melee:
				return "au corps-�-corps";
				case AttackType.Radioactive:
				return "aux radiations";
				case AttackType.Fire:
				return "au feu";
				case AttackType.Bleeding:
				return "aux saignements";
				case AttackType.Poison:
				return "au poison";
		}
	}
	public static string TypeToAttackString(AttackType type)
	{
		switch (type)
		{
			default:
				return type.ToString();
			case AttackType.Ballistic:
				return "ballistique";
			case AttackType.Electric:
				return "�lectrique";
			case AttackType.Blast:
				return "explosif";
			case AttackType.Melee:
				return "m�l�e";
			case AttackType.Radioactive:
				return "radioactif";
			case AttackType.Fire:
				return "feu";
			case AttackType.Bleeding:
				return "saignement";
			case AttackType.Poison:
				return "poison";
		}
	}
	public static string TypeToColorString(AttackType type)
	{
		switch (type)
		{
			default:
				return "#ffffff";
			case AttackType.Electric:
				return "#00A9FF";
			case AttackType.Radioactive:
				return "yellow";
			case AttackType.Fire:
				return "#FF7400";
			case AttackType.Bleeding:
				return "red";
			case AttackType.Poison:
				return "green";
		}
	}
	public static bool BypassesArmor(AttackType type)
	{
		switch(type)
		{
			case AttackType.Radioactive:
			case AttackType.Bleeding:
			case AttackType.Poison:
			case AttackType.Unavoidable:
				return true;
			default:
				return false;
		}
	}

	public static bool Elemental(AttackType type)
	{
		switch(type)
		{
			case AttackType.Electric:
			case AttackType.Fire:
			case AttackType.Radioactive:
			case AttackType.Poison:
				return true;
			default:
				return false;
		}
	}
}

public enum ActorType
{
	Organic,
	Robotic
}