using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

public class ActionButton : MonoBehaviour
{
    public Action action;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (action != null && FindObjectOfType<CombatManager>().activeAction == action)
		{
            //GetComponentInChildren<TMPro.TextMeshProUGUI>().color = Color.green;
            var colorBlock = GetComponent<Button>().colors;
            colorBlock.normalColor = Color.yellow;
            //GetComponent<Button>().colors = colorBlock;
        }
    }

    public void SetInteractable(bool interactable)
    {
        if (action == null)
            GetComponent<Button>().interactable = interactable;
        else
            GetComponent<Button>().interactable = action.CanUse() && interactable;
    }

    public void SetAction(Action in_action)
	{
        Assert.IsNotNull(in_action);
        action = in_action;
        GetComponentInChildren<TMPro.TextMeshProUGUI>().text = action.name;
        if (!action.CanUse() && action.Charges != 0)
            GetComponentInChildren<TMPro.TextMeshProUGUI>().text += " (" + action.CooldownRemaining + ")";
        GetComponent<Button>().interactable = action.CanUse();

        var colorBlock = GetComponent<Button>().colors;
        colorBlock.normalColor = new Color(0.55f, 0.55f, 0.55f);
        colorBlock.disabledColor = new Color(0, 0, 0, 0.55f);
        //colorBlock.disabledColor = action.CanUse() ? Color.white : Color.black;
        GetComponent<Button>().colors = colorBlock;
        if (action.CanUse())
            GetComponentInChildren<TMPro.TextMeshProUGUI>().color = Color.white;
        else
            GetComponentInChildren<TMPro.TextMeshProUGUI>().color = Color.grey;
    }

    public void Pressed()
	{
        if (action != null && FindObjectOfType<CombatManager>().interactable)
        {
            var colorBlock = GetComponent<Button>().colors;
            colorBlock.normalColor = Color.yellow;
            colorBlock.disabledColor = Color.yellow;
            GetComponent<Button>().colors = colorBlock;
            action.Use();
        }
	}
}
