﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CombatManager : MonoBehaviour
{
	public MusicManager musicManager;
	public GameObject ball;
	public GameObject explosion, poison;
	public TextMeshProUGUI turnText;
	public TextMeshProUGUI activePartyText;
	public TextMeshPro actionsLeftText;
	public GameObject hitMsg;
	public GameObject missMsg;
	public GameObject healMsg;
	public GameObject critMsg;
	public GameObject reviveMsg;
	public GameObject counterattackMsg;

	public Material spriteMaterial;

	public GameObject blackHole;
	public GameObject globalArrowThing;
	public GameObject enemyTurnOverlay;

	public GameObject[] enemyObjs;

	public TextAsset testJSON;
	public ItemButton[] itemButtons;
	public GameObject actionButtonsRoot;
	public GameObject itemButtonsRoot;

	public MessageBox introTutorialBox;
	public MessageBox effectTutorialBox;
	public MessageBox resistVulnTutorialBox;

	public AudioClip battleMusic;
	public AudioClip bossMusic;
	public AudioClip victoryJingle;
	public AudioClip defeatJingle;

	public Action activeAction = null;

	public bool revealWeaknesses = false;

	List<Actor> playerParty;
	List<Actor> enemyParty;
	List<Actor> activeParty;

	SaveData saveData;

	ActionButton[] actionButtons;
	public Soldier selectedSoldier;
	int turnNumber = 1;
	int selectionCycleIndex = 0;
	int consecutiveAttackCounter = 0;
	List<Actor> actorsMissedThisAction = new List<Actor>();

	bool elementalAttacksThisTurn = false;
	bool actionFinished = false;
	public bool interactable = true;
	bool counterattack = false;

	bool lastAttackVuln = false;
	bool lastAttackResist = false;
	bool lastAttackImmune = false;
	AttackType lastAttackType;

	public List<Actor> EnemyParty()
	{ return enemyParty; }
	public List<Actor> PlayerParty()
	{ return playerParty; }

	// Start is called before the first frame update
	void Start()
	{
		// Reset the fury effect material parameter
		Shader.SetGlobalFloat("_FuryMode", 0.0f);

		saveData = SaveData.Load();

		playerParty = new List<Actor>();
		enemyParty = new List<Actor>();

		actionButtons = new ActionButton[8];
		for (int i = 0; i < 8; ++i)
		{
			actionButtons[i] = GameObject.Find("ActionButton" + (i + 1)).GetComponent<ActionButton>();
			Assert.IsNotNull(actionButtons[i]);
		}

		if (SharedData.sharedInventory == null)
		{
			SharedData.sharedInventory = new List<Item>();
			SharedData.sharedInventory.Add(new Bandage());
			//SharedData.sharedInventory.Add(new MedKitItem());
		}

		InitSoldiers();

		FindObjectOfType<CombatGenerator>().CombatPhaseGeneration();

		foreach (var obj in enemyObjs)
			obj.SetActive(false);

		if (SharedData.nextEnemyParty == null)
		{
			SharedData.sharedInventory.Add(new MedKitItem());
			SharedData.currentZone = 3;
			SpawnEnemy(RadBeastEnemy.Create());
			SpawnEnemy(RadBeastEnemy.Create());
			SpawnEnemy(Ecorcheur.Create());
			SpawnEnemy(Boss1.Create());
#if false
			SpawnEnemy(CommanderEnemy.Create());
			SpawnEnemy(ProtectorEnemy.Create());
			SpawnEnemy(LeechEnemy.Create());
			SpawnEnemy(RavageurEnemy.Create());
#else
			//SpawnEnemy(Boss2.Create());
			//SpawnEnemy(Boss2.Create());
			//SpawnEnemy(CommanderEnemy.Create());
			//SpawnEnemy(ProtectorEnemy.Create());
			//SpawnEnemy(Zone1Enemy2.Create());
			//SpawnEnemy(Boss3.Create());
#endif
			//SpawnEnemy(RadBeastEnemy.Create());
		}
		else
		{
			foreach (var enemy in SharedData.nextEnemyParty)
				SpawnEnemy(enemy);
		}

		// Special scenario : is this Boss 2? Then adjust his position in ordero to have him fit on the screen
		var boss2 = enemyParty.Find(x => x.ActorName == "Getal Mear");
		if (boss2 != null)
		{
			boss2.obj.transform.localPosition = new Vector3(3.7f, 1.3f);
			enemyObjs[1].transform.localPosition = new Vector3(7.26f, -0.48f);
			enemyObjs[2].transform.localPosition = new Vector3(6.94f, 1.12f);
			enemyObjs[3].transform.localPosition = new Vector3(1.52f, 1.21f);
		}

		activeParty = playerParty;

		// Apply random animation offsets
		ApplyRandomAnimationOffsets();

		UpdateBuffs();

		SelectSoldier((Soldier)playerParty[0]);
		UpdateActionText();

		InitMusic();

		// Apply rune bonuses
		foreach (var soldier in playerParty)
			if (soldier.BossRunes.Find(x => x.Name == "Vigilance") != null)
			{
				// Flat crit increases for bosses with this rune
				foreach (var enemy in enemyParty.FindAll(x => ((Enemy)x).isBoss))
					enemy.SetBaseCrit(enemy.GetBaseCritChance() + 5);

				break;
			}

		foreach (var enemy in enemyParty)
			if (enemy.aiController != null)
				enemy.aiController.PreCombatInit();

		if (SharedData.sharedInventory.Find(x => x.GetType() == typeof(EnemyScanItem)) != null)
			revealWeaknesses = true;

		Debug.Log("Current zone is " + SharedData.currentZone);

		foreach (var ennemy in enemyParty)
		{
			int zoneDiff = SharedData.currentZone - ennemy.zone;
			float hpMultiplier = 1;
			if (zoneDiff == 1)
				hpMultiplier = 1.4f;
			else if (zoneDiff == 2)
				hpMultiplier = 1.8f;
			ennemy.MaxHP = Mathf.RoundToInt(ennemy.MaxHP * hpMultiplier);
			ennemy.HP = ennemy.MaxHP;
		}

		if (!saveData.introTutorialShown)
		{
			StartCoroutine(ShowIntroText());
			saveData.introTutorialShown = true;
			saveData.Save();
		}
	}

	IEnumerator ShowIntroText()
	{
		EnterActionAnimationState();

		yield return new WaitForSeconds(0.5f);

		introTutorialBox.Show();

		yield return new WaitUntil(() => introTutorialBox.closing);

		interactable = true;
		SelectSoldier((Soldier)playerParty[0]);
	}

	void InitMusic()
	{
		if (SharedData.nextCombatDifficulty == CombatDifficulty.Boss)
			musicManager.PlayMusic(bossMusic);
		else
			musicManager.PlayMusic(battleMusic);
	}
	public int TurnNumber()
	{ return turnNumber; }

	void InitSoldiers()
	{
		Assert.IsNotNull(SharedData.soldiers);

		for (int i = 0; i < 4; i++)
		{
			GameObject.Find("Soldier" + (i + 1)).SetActive(i < SharedData.soldiers.Count);
		}
		for (int i = 0; i < SharedData.soldiers.Count; i++)
		{
			var anim = FindObjectOfType<AnimList>().anims.Find(x => x.name == SharedData.soldiers[i].Class);
			Assert.IsNotNull(anim);
			var obj = GameObject.Find("Soldier" + (i + 1));
			playerParty.Add(SharedData.soldiers[i]); SharedData.soldiers[i].obj = obj.GetComponent<ActorSpriteUpdater>(); SharedData.soldiers[i].obj.actor = SharedData.soldiers[i];
			obj.GetComponent<Animator>().runtimeAnimatorController = anim.anim;
			obj.GetComponent<Animator>().Update(0.0f);

			obj.GetComponent<PixelCollider2D>().Regenerate();
		}
		foreach (var soldier in playerParty)
		{
			soldier.party = playerParty;
			soldier.oppositeParty = enemyParty;
		}

		// Reset the item cooldowns
		foreach (var soldier in playerParty)
		{
			foreach (var ability in soldier.Abilities)
				ability.Reset();
		}
	}
	public void SpawnEnemy(Enemy enemy)
	{
		// Full, do not spawn
		if (enemyParty.Count >= 4)
		{
			Debug.LogWarning("Attempted to add an enemy to a full party");
			return;
		}
		enemyParty.Add(enemy);
		enemy.party = enemyParty;
		enemy.oppositeParty = playerParty;

		// Try to find an actor object to the scene that's not active
		GameObject obj = enemyObjs.ToList().Find(x => !x.activeSelf);

		if (obj == null)
		{
			Debug.LogWarning("Attempted to add an enemy to a scene with no actor objects left");
			return;
		}

		obj.SetActive(true);

		var anim = FindObjectOfType<AnimList>().anims.Find(x => x.name == enemy.ActorName);
		if (anim == null)
			Debug.LogWarning("Could not find the animation for ennemy " + enemy.ActorName);
		else
		{
			obj.GetComponent<Animator>().runtimeAnimatorController = anim.anim;
			obj.GetComponent<Animator>().Update(0.0f);
		}

		obj.GetComponent<PixelCollider2D>().Regenerate();
		enemy.obj = obj.GetComponent<ActorSpriteUpdater>(); enemy.obj.actor = enemy;
		// Set Sorting order based on actor name
		int order = -int.Parse(enemy.obj.gameObject.name.Last().ToString());
		if (activeParty != enemyParty)
			order -= 4;
		obj.GetComponent<SpriteRenderer>().sortingOrder = order;

	}

	void ApplyRandomAnimationOffsets()
	{
		foreach (var animator in FindObjectsOfType<Animator>())
			if (animator.parameters.Any(x => x.name == "Offset"))
				animator.SetFloat("Offset", Random.Range(0.0f, 1.0f));
	}
	void EnterActionAnimationState()
	{
		interactable = false;
		for (int i = 0; i < actionButtons.Length; ++i)
		{
			actionButtons[i].SetInteractable(false);
		}
	}

	public void HealActor(Actor effector, Actor act, int HP)
	{
		Assert.IsNotNull(act);
		Assert.IsNotNull(act.obj);

		// Roll for critical heal
		int critRoll = Random.Range(0, 100);
		// Don't let enemies crit heal, in order not to frustrate the player too much
		if (effector != null && !effector.Enemy && critRoll <= effector.EffectiveCritChance())
		{
			HP *= 2; // Double heal on crits
			GameObject.Instantiate(critMsg, act.obj.transform.position + new Vector3(0.0f, 0.8f, 0.0f), act.obj.transform.rotation);
		}

		Debug.Log("Actor " + act.ActorName + " was healed by " + effector.ActorName + " for " + HP + " HP");

		int prevHP = act.HP;
		act.HP = Mathf.Min(act.MaxHP, act.HP + HP);
		int actuallyHealed = act.HP - prevHP;

		GameObject.Instantiate(healMsg, act.obj.transform).GetComponent<TextMeshPro>().text = "+" + actuallyHealed + "HP";
	}

	// Returns true if the attack hit, false otherwise
	public bool AttackActor(Actor effector, Actor act, int damage, float hitChance, AttackType type) // 0 to 100
	{
		if (effector != null && !effector.Enemy && AttackTypeUtils.Elemental(type))
			elementalAttacksThisTurn = true;

		hitChance = Mathf.Max(0, hitChance - act.EffectiveDodgeChance());

		// See if we hit
		float roll = Random.Range(0, 100);
		Debug.Log("Rolled " + roll + " against " + hitChance);
		if (roll <= hitChance)
		{
			DamageActor(effector, act, damage, type);
			return true;
		}
		else
		{
			consecutiveAttackCounter = -1; // Combo break
			MissActor(act);
			return false;
		}
	}

	void UpdateBuffs()
	{
		foreach (var soldier in playerParty)
		{
			if (soldier.Buffs.Find(x => x.Name == "Berserk") != null ||
				soldier.BossRunes.Find(x => x.Name == "Berserk") != null)
			{
				// Add the berserk effect
				if ((float)soldier.HP / soldier.MaxHP <= 0.2f) // 20%
					soldier.AddEffect(new HurtDamageBoost(1.4f));
				else
					soldier.RemoveEffect<HurtDamageBoost>();
			}

			var rune = soldier.BossRunes.Find(x => x.Name == "Pause café");
			if (rune != null)
			{
				HealActor(soldier, soldier, 20);
				soldier.BossRunes.Remove(rune);
			}

			rune = soldier.BossRunes.Find(x => x.Name == "Sagesse");
			if (rune != null)
			{
				soldier.AddEffect(new WisdomEffect());
				soldier.BossRunes.Remove(rune);
			}
		}
	}

	public void MissActor(Actor act)
	{
		GameObject.Instantiate(missMsg, act.obj.transform.position, act.obj.transform.rotation);
		actorsMissedThisAction.Add(act);
	}

	public void DamageActor(Actor effector, Actor act, int damage, AttackType type)
	{
		Assert.IsNotNull(act);
		Assert.IsNotNull(act.obj);

		bool crit = false;
		int critRoll = Random.Range(0, 100);
		if (effector != null && !effector.Enemy)
			critRoll = Mathf.Max(0, critRoll - SharedData.critBonus);
		if (effector != null && critRoll <= effector.EffectiveCritChance() && type != AttackType.Harmless)
		{
			crit = true;
			damage = Mathf.RoundToInt(damage * Tweakables.GetSingle("crit_damage_multiplier", 1.50f)); // Increase damage on crits
			GameObject.Instantiate(critMsg, act.obj.transform.position + new Vector3(0.0f, 0.8f, 0.0f), act.obj.transform.rotation);
		}

		// Apply rune bonuses
		foreach (var soldier in playerParty)
			if (soldier.BossRunes.Find(x => x.Name == "Vigilance") != null && crit && !act.Enemy
				&& effector != null && effector.Enemy && ((Enemy)effector).isBoss && ((Enemy)effector).critsDealt <= 0)
			{
				// Cancel this attack
				damage = 0;
			}

		if (effector != null && effector.Enemy && crit)
			((Enemy)effector).critsDealt++;

		// Rune "Vengeance"
		foreach (var soldier in playerParty)
			if (soldier.BossRunes.Find(x => x.Name == "Vengeance") != null && act.Enemy
				&& effector != null && !effector.Enemy)
			{
				float mod = 0.8f;
				if (SharedData.deadPlayers == 1)
					mod = 1.1f;
				if (SharedData.deadPlayers == 2)
					mod = 1.2f;
				if (SharedData.deadPlayers >= 3)
					mod = 1.4f;
				damage = Mathf.RoundToInt(damage * mod);
				break;
			}

		// Rune "Assaut"
		foreach (var soldier in playerParty)
			if (soldier.BossRunes.Find(x => x.Name == "Assaut") != null && act.Enemy && act.GetEffects().Count > 0)
			{
				damage = Mathf.RoundToInt(damage * Tweakables.GetSingle("assault_rune_bonus", 1.15f));
				break;
			}

		// Rune "Enchaînement"
		foreach (var soldier in playerParty)
			if (soldier.BossRunes.Find(x => x.Name == "Enchaînement") != null && effector != null)
			{
				if (!effector.Enemy && consecutiveAttackCounter > 0)
					damage = Mathf.RoundToInt(damage * (1.0f + 0.05f*consecutiveAttackCounter));
				else if (effector.Enemy && consecutiveAttackCounter < 0) // There was a combo break
					damage = Mathf.RoundToInt(damage * 1.15f);
				break;
			}

		damage = Mathf.RoundToInt(damage * act.EffectiveReceivedDamageMultiplier());
		if (effector != null)
			damage = Mathf.RoundToInt(damage * effector.EffectiveInflictedDamageMultiplier());

		// Handle damage redirection
		if (act.GetEffects().Find(x => x is DamageRedirectEffect) != null)
		{
			var effect = (DamageRedirectEffect)act.GetEffects().Find(x => x is DamageRedirectEffect);
			int redirectedDamage = Mathf.RoundToInt(effect.receivedDamageMult * damage);
			damage *= Mathf.RoundToInt(1.0f - effect.receivedDamageMult);

			ApplyDamage(act, effect.benefactor, redirectedDamage, type, false); // do not carry over crits to the defender
		}

		ApplyDamage(effector, act, damage, type, crit);

		if (act.Enemy && consecutiveAttackCounter >= 0)
			++consecutiveAttackCounter;
	}

	void ApplyDamage(Actor effector, Actor act, int damage, AttackType type, bool crit)
	{
		// Don't receive damage in godmode (or inflict immense damage if the effector is a player
		if (DebugUtils.godmode)
		{
			if (!effector.Enemy && act.Enemy)
			{
				damage = 1000000;
			}
			else if (effector.Enemy && !act.Enemy)
				return;
		}

		// Fire attack? Remove the 3rd's boss fury
		if (type == AttackType.Fire)
			act.RemoveEffect<Boss3FuryEffect>();

		// Adjust the damage of ennemies based on the current zone
		if (effector != null && effector.Enemy)
		{
			int zoneDiff = SharedData.currentZone - effector.zone;
			float dmgMultiplier = 1;
			if (zoneDiff == 1)
				dmgMultiplier = 1.25f;
			else if (zoneDiff == 2)
				dmgMultiplier = 1.50f;

			damage = Mathf.RoundToInt(damage * dmgMultiplier);
		}

		// Elementary bonus buff
		if (effector != null && !effector.Enemy && AttackTypeUtils.Elemental(type))
			damage += SharedData.elementalDamageBonus;

		bool blockedByGetalMearArmor = false;
		bool GetalMearArmorDamaged = false;
		// Is armor worn off by hits and not damage?
		if (act.ArmorIsInHitNumber && act.Armor > 0)
		{
			if (act.Vulnerabilities.Find(x => x.type == type) != null || crit)
			{
				act.Armor -= 2;
				GetalMearArmorDamaged = true;
			}
			else if (damage >= 30)
			{
				act.Armor -= 1;
				GetalMearArmorDamaged = true;
			}
			if (act.Armor < 0)
				act.Armor = 0;

			damage = 0;
			blockedByGetalMearArmor = true;
		}

		// Does that element heal this actor?
		if (act.HealElements.Contains(type))
		{
			HealActor(effector, act, damage);
			return;
		}

		// Apply resistance/vulnerability multipliers
		if (act.Resistances.Find(x => x.type == type) != null && act.Vulnerabilities.Find(x => x.type == type) == null)
			damage -= Mathf.RoundToInt(act.Resistances.Find(x => x.type == type).percentage / 100.0f * damage); // 40%
		if (act.Resistances.Find(x => x.type == type) == null && act.Vulnerabilities.Find(x => x.type == type) != null)
			damage += Mathf.RoundToInt(act.Vulnerabilities.Find(x => x.type == type).percentage / 100.0f * damage); // 40%
		if (act.Invicibility.Contains(type))
			damage = 0; // Nullify damage

		// Crits bypass armor
		if (!crit && !AttackTypeUtils.BypassesArmor(type))
		{
			// Hit the armor
			int absorbedByArmor = Mathf.Min(act.Armor, damage);
			damage -= absorbedByArmor;
			act.Armor -= absorbedByArmor;
		}
		// Elemental damage do extra
		if (act.ArmorVulnerableToElements && AttackTypeUtils.Elemental(type))
			act.Armor -= 50;


			act.Armor = Mathf.Max(0, act.Armor);
		Debug.Log("Actor " + act.ActorName + " was damaged for " + damage + " HP");

		var msg = GameObject.Instantiate(hitMsg, act.obj.transform.position, act.obj.transform.rotation);
		msg.GetComponent<TextMeshPro>().text = damage.ToString() + "!";
		// Apply resistance/vulnerability effects to the hit message
		if (act.Resistances.Find(x => x.type == type) != null && act.Vulnerabilities.Find(x => x.type == type) == null)
		{
			msg.GetComponent<TextMeshPro>().color = Color.gray;
			msg.GetComponent<TextMeshPro>().text = "Résistance\n" + damage.ToString() + "...";

			if (act.Enemy)
				lastAttackResist = true;
		}
		if (act.Resistances.Find(x => x.type == type) == null && act.Vulnerabilities.Find(x => x.type == type) != null)
		{
			msg.GetComponent<TextMeshPro>().color = Color.magenta;
			msg.GetComponent<TextMeshPro>().text = "Vulnérable !\n" + damage.ToString() + "!";

			if (act.Enemy)
				lastAttackVuln = true;
		}
		if (act.Invicibility.Contains(type) || (!GetalMearArmorDamaged && blockedByGetalMearArmor))
		{
			msg.GetComponent<TextMeshPro>().color = Color.gray;
			msg.GetComponent<TextMeshPro>().text = "Bloqué"; // Nullify damage

			if (act.Enemy)
				lastAttackImmune = true;
		}

		lastAttackType = type;

		// Armor destroyed? Remove any possible shield VFX child
		if (act.Armor <= 0)
		{
			foreach (Transform child in act.obj.transform)
				if (child.CompareTag("Shield"))
					GameObject.Destroy(child.gameObject);
		}

		act.HP = Mathf.Max(0, act.HP - damage);
		if (damage > 0)
			act.obj.GetComponent<DamagedShake>().Hit();
		else
			act.obj.GetComponent<DamagedShake>().Shake();

		if (act.HP == 0)
		{
			// Rune "Dernier souffle"
			var rune = act.BossRunes.Find(x => x.Name == "Dernier souffle");
			if (rune != null)
			{
				act.HP = Mathf.RoundToInt(act.MaxHP * 0.20f);
				foreach (var soldier in playerParty)
					soldier.BossRunes.RemoveAll(x => x.Name == "Dernier souffle");

				GameObject.Instantiate(reviveMsg, act.obj.transform.position + new Vector3(1.6f, 0.8f, 0.0f), act.obj.transform.rotation);
			}
			else
				HandleActorDeath(act);
		}

		// Handle buff updates post-damage
		UpdateBuffs();
	}

	public void DamageParty(Actor effector, List<Actor> actors, int damage, AttackType type)
	{
		List<Actor> listCopy = new List<Actor>(actors); // Make a copy of the list object (not its content) to iterate over it, as DamageActor might delete actors which might break the iteration
		foreach (var actor in listCopy)
			DamageActor(effector, actor, damage, type);
	}

	void HandleActorDeath(Actor act)
	{
		Debug.Log("Actor " + act.ActorName + " died");
		act.RemoveNonPermanentEffects();
		act.party.Remove(act);
		if (!act.Enemy)
		{
			SharedData.soldiers.Remove((Soldier)act);
			++SharedData.deadPlayers;
		}
		else
		{
			++SharedData.ennemiesSlain;
		}
		var dup = GameObject.Instantiate(act.obj.gameObject);
		dup.transform.position = act.obj.transform.position;
		act.obj.gameObject.SetActive(false);
		SplitEffect.SplitSprite(dup.GetComponent<SpriteRenderer>());
		//GameObject.Destroy(act.obj.gameObject);
	}

	void SetupActionButtons(Soldier soldier)
	{
		for (int i = 0; i < Mathf.Min(soldier.Abilities.Count, actionButtons.Length); ++i)
		{
			actionButtons[i].gameObject.SetActive(true);
			actionButtons[i].SetInteractable(true);
			soldier.Abilities[i].effector = soldier;
			actionButtons[i].SetAction(soldier.Abilities[i]);
		}
		for (int i = soldier.Abilities.Count; i < actionButtons.Length; ++i)
		{
			actionButtons[i].gameObject.SetActive(false);
		}
	}

	void MoveGlobalArrow(GameObject dest)
	{ 
		LeanTween.cancel(globalArrowThing.gameObject);
		LeanTween.move(globalArrowThing, dest.transform.position, 0.25f).setEaseInOutQuad();
		globalArrowThing.GetComponent<SinusoidalMovement>().newParent = dest.transform;
	}

	void SelectSoldier(Soldier soldier)
	{
		selectedSoldier = soldier;

		// Unselect all other soldiers
		foreach (var s in playerParty)
			s.active = false;

		selectedSoldier.active = true;
		SetupActionButtons(soldier);
		UpdateActionText();

		MoveGlobalArrow(selectedSoldier.obj.arrow);
		//Debug.Log("Slide to " + selectedSoldier.obj.arrow.transform.position);

		EventSystem.current.SetSelectedGameObject(null);
		actionButtons[0].GetComponent<Button>().Select();
	}

	public void ActionStarted(Action act)
	{
		// Only update the actions when in the interactive player phase
		if (activeParty == enemyParty)
			return;

		activeAction = act;

		//Debug.Log("Combat manager reports action " + act.name + " started");
		EnterActionAnimationState();

		//act.target = enemyParty[0];
		if (!counterattack)
		{
			//Assert.IsTrue(selectedSoldier.Actions > 0);
			//Assert.IsTrue(activeParty == playerParty);

			if (activeParty == playerParty) // TODO : ugly
				act.effector = selectedSoldier;
			if (act.turnEnding)
				selectedSoldier.Actions = 0;
			else
				selectedSoldier.Actions -= act.ActionCost;
			if (selectedSoldier.Actions < 0)
				selectedSoldier.Actions = 0;
		}
	}

	void GenerateLoot()
	{
		string text = "";
		List<Item> items = new List<Item>();

		int money = 0;
		bool bossBeatenFirstTime = false;
		if (SharedData.nextCombatDifficulty == CombatDifficulty.Boss)
		{
			// Generate money
			money = 300;
			var saveData = SaveData.Load();
			saveData.gold += money;
			saveData.Save();

			// Generate NFT
			items.Add(new NFTItem());
			// Generate healing
			if (Random.Range(0, 100) <= 50)
			{
				if (Random.Range(0, 100) <= 70)
					items.Add(new MedKitItem());
				else
					items.Add(new HealHerbs());
			}
			// Generate extra
			if (Random.Range(0, 100) <= 30)
			{
				items.Add(Item.GenerateRandomItem());
			}

			// Reset boss bookkeeping stuff
			++SharedData.bossesSlain;
			SharedData.bossRoomRevealed = false;

			Debug.Log("Boss beaten in zone " + SharedData.currentZone + " / " + saveData.highestBeatenBoss);
			if (saveData.highestBeatenBoss < SharedData.currentZone)
			{
				bossBeatenFirstTime = true;
				saveData.highestBeatenBoss = SharedData.currentZone;
			}
			saveData.Save();
		}
		else
		{
			// Generate money
			money = Random.Range(60, 150);
			var saveData = SaveData.Load();
			saveData.gold += money;
			saveData.Save();

			// Generate NFT
			if (Random.Range(0, 100) <= 20)
				items.Add(new NFTItem());
			// Generate healing
			if (Random.Range(0, 100) <= 10)
			{
				if (Random.Range(0, 100) <= 70)
					items.Add(new MedKitItem());
				else
					items.Add(new HealHerbs());
			}
			// Generate extra
			if (Random.Range(0, 100) <= 20)
			{
				items.Add(Item.GenerateRandomItem());
			}
		}

		// More money if we have the "Avidité" perk
		if (playerParty.Count > 0 && playerParty[0].Buffs.Find(x => x.Name == "Avidité") != null)
			money = Mathf.RoundToInt(money * 1.3f);

			text = "";
		if (SharedData.nextCombatDifficulty == CombatDifficulty.Boss)
		{
			text += "Boss vaincu !\n\n";
		}
		text += "Félicitations !\nRécompenses obtenues :\n";

		if (money > 0)
			text += "\n<color=yellow>+" + money + " cryptomonnaie";
		foreach (var item in items)
		{
			text += "\n<color=green>+1 " + item.name + "</color>";
			SharedData.sharedInventory.Add(item);
		}
		if (bossBeatenFirstTime)
		{
			saveData = SaveData.Load();

			text += "\n<color=green>+1 Repérage";
			text += "\n<color=green>+1 Upgrade max de compétence";
			if (saveData.highestBeatenBoss == 1 && !saveData.unlockedClasses.Contains("RadX"))
			{
				text += "\n<color=green>Classe débloquée : RadX";
				text += "\n<color=green>Classe en boutique : Cyborg";
				saveData.unlockedClasses.Add("RadX");
			}
			else if (saveData.highestBeatenBoss == 2 && !saveData.unlockedClasses.Contains("Ingénieur"))
			{
				text += "\n<color=green>Classe débloquée : Ingénieur";
				text += "\n<color=green>Classe en boutique : Guardian";
				saveData.unlockedClasses.Add("Ingénieur");
			}

			saveData.Save();

			if (SharedData.newlyBeatenZones == null)
				SharedData.newlyBeatenZones = new List<int>();
			SharedData.newlyBeatenZones.Add(saveData.highestBeatenBoss);

			ClassInfo.UnlockNewClass();
		}
		if (SharedData.nextCombatDifficulty == CombatDifficulty.Boss)
			++SharedData.scoutingCharges;

		GameObject.Find("VictoryBox").GetComponent<MessageBox>().text.text = text;
	}

	bool helpShown = false;
	IEnumerator ShowHelpCoroutine()
	{
		helpShown = true;
		GameObject.Find("HelpMsgBox").GetComponent<MessageBox>().Show();

		yield return new WaitUntil(() => GameObject.Find("HelpMsgBox").GetComponent<MessageBox>().closing);
		helpShown = false;
	}
	public void ShowHelp()
	{
		if (!helpShown)
			StartCoroutine(ShowHelpCoroutine());
	}

	bool berating = false;
	IEnumerator BerateForBeingACoward()
	{
		berating = true;
		GameObject.Find("LootMsgBox").GetComponent<MessageBox>().text.text = "Le dieu quantique refuse votre lâche tentative de fuir votre destinée...\n\n" +
			"Pour pouvoir capituler, il faut avoir effectué au moins 3 combats !";
		GameObject.Find("LootMsgBox").GetComponent<MessageBox>().Show();

		yield return new WaitUntil(() => GameObject.Find("LootMsgBox").GetComponent<MessageBox>().closing);
		berating = false;
	}

	public void Capitulate()
	{
		if (SharedData.combatsFought < 3)
		{
			if (!berating)
				StartCoroutine(BerateForBeingACoward());
		}
		else
			EndOfCombat(false);
	}

	void EndOfCombat(bool playersWon)
	{
		interactable = false;
		EnterActionAnimationState();
		GameObject.FindObjectOfType<SceneMouseHoverMonitor>().ExitTargetMode();

		Debug.Log("End of Combat! Player victory : " + playersWon);
		if (!playersWon)
		{
			musicManager.PlayJingle(defeatJingle);
			GameObject.Find("VictoryBox").GetComponent<MessageBox>().text.text = "Vous avez échoué ! Cet univers a perdu tout espoir...\n\nIl est temps de sauter à nouveau dans le <color=#00A9FF>multivers</color> et de retenter votre chance !"; ;
		}
		else
		{
			musicManager.PlayJingle(victoryJingle);
			GenerateLoot();
		}
		GameObject.Find("VictoryBox").GetComponent<MessageBox>().Show();

		// Reset the effects
		foreach (var soldier in playerParty)
		{
			soldier.Actions = soldier.DefaultActions;
			soldier.RemoveNonPermanentEffects();
			soldier.BossRunes.Clear();
		}

		// Apply the post-combat buffs
		foreach (var soldier in playerParty)
		{
			if (soldier.Buffs.Find(x => x.Name == "Régénération") != null)
				soldier.HP = Mathf.CeilToInt(Mathf.Min(soldier.HP + 0.05f * soldier.MaxHP, soldier.MaxHP));
		}

		++SharedData.combatsFought;

		StartCoroutine(WaitEndMessageClosed(playersWon));
	}

	IEnumerator WaitEndMessageClosed(bool playersWon)
	{
		yield return new WaitUntil(() => GameObject.Find("VictoryBox").GetComponent<MessageBox>().closing);

		if (!playersWon) // back to menu
			yield return DefeatAnimation();
		else
			FindObjectOfType<LevelLoader>().UnloadCurrentLevel();

		yield return new WaitForSeconds(10000); // Idle until the previous level is loaded
	}

	IEnumerator DefeatAnimation()
	{
		blackHole.SetActive(true);
		yield return new WaitForSeconds(2);
		FindObjectOfType<LevelLoader>().useSwipe = false;
		FindObjectOfType<LevelLoader>().swipe = null;
		FindObjectOfType<LevelLoader>().LoadLevelByName("SessionReportScene");
	}

	bool HasActionsLeft(Actor act)
	{
		return act.Actions > 0 && act.Abilities.Any(x => x.CanUse());
	}

	bool CheckEndOfCombat()
	{
		// End of combat ?
		if (enemyParty.Count == 0 || playerParty.Count == 0)
		{
			EndOfCombat(enemyParty.Count == 0);
			return true;
		}
		return false;
	}

	public IEnumerator HandleCounterattacks(Actor enemy, Actor target)
	{
		// Pick an ability
		var action = enemy.aiController.PickCounterattack();
		if (action == null)
			yield break;

		Assert.IsFalse(counterattack);
		enemyTurnOverlay.SetActive(true);
		counterattack = true;
		GameObject.Instantiate(counterattackMsg, new Vector3(0, 3.5f), Quaternion.identity);


		action.effector = enemy;
		Debug.Log("Effector is " + action.effector.ActorName);

		action.target = target;

		Assert.IsTrue(action.effector.Enemy);
		actionFinished = false;
		action.Use();
		yield return new WaitUntil(() => actionFinished);

		counterattack = false;
		if (activeParty == playerParty)
			enemyTurnOverlay.SetActive(false);
	}

	public IEnumerator ActionFinished(Action act)
	{
		actionFinished = true;
		act.target = null;
		activeAction = null;

		if (CheckEndOfCombat())
			yield break;

		if (act.effector != null && !act.effector.Enemy && activeParty == playerParty)
		{
			// Find counterattack candidates
			Actor counterAttackEnemy = actorsMissedThisAction.Find(x => x.Enemy && x.CanCounterattack());
			if (counterAttackEnemy != null)
				yield return HandleCounterattacks(counterAttackEnemy, act.effector);
		}
		interactable = true;

		actorsMissedThisAction.Clear();

		// the enemies play in an automated manned, no UI to update here
		if (activeParty == enemyParty)
			yield break;

		// did the selected soldier die as a result of the counterattack?
		if (selectedSoldier.HP == 0 || !playerParty.Contains((Actor)selectedSoldier))
		{
			Debug.Log("Player died after counterattack");
			DoActualSwitchPlayer();
		}


		TemporaryEffect effect = null;
		foreach (var soldier in playerParty)
		{
			if (soldier.GetEffects().Find(y => !y.permanent) != null)
			{
				effect = soldier.GetEffects().Find(y => !y.permanent);
			}
		}
		foreach (var enemy in enemyParty)
		{
			if (enemy.GetEffects().Find(y => !y.permanent) != null)
			{
				effect = enemy.GetEffects().Find(y => !y.permanent);
			}
		}

		if (!saveData.effectTutorialShown && effect != null)
		{
			effectTutorialBox.text.text = string.Format(effectTutorialBox.text.text, effect.Name);
			effectTutorialBox.Show();

			yield return new WaitUntil(() => effectTutorialBox.closing);

			saveData.effectTutorialShown = true;
			saveData.Save();
		}

		if (!saveData.vulnTutorialShown && (lastAttackVuln || lastAttackResist || lastAttackImmune))
		{
			string effectType = "<color=#ff00ff>vulnérable</color>";
			if (lastAttackResist)
				effectType = "<color=grey>résistant</color>";
			if (lastAttackImmune)
				effectType = "<color=red>immunisé</color>";

			resistVulnTutorialBox.text.text = string.Format(resistVulnTutorialBox.text.text, effectType, "<color=" + AttackTypeUtils.TypeToColorString(lastAttackType) + ">" + 
				AttackTypeUtils.TypeToAttackString(lastAttackType) + "</color>");
			resistVulnTutorialBox.Show();

			yield return new WaitUntil(() => resistVulnTutorialBox.closing);
			saveData.vulnTutorialShown = true;
			saveData.Save();
		}


		//Debug.Log("Combat manager reports action " + act.name + " finished");
		// Rebuild the action buttons, as some might be unavailable now
		SetupActionButtons(selectedSoldier);

		// Select another soldier
		if (!HasActionsLeft(selectedSoldier))
		{
			//Debug.Log("Depleted actions for " + selectedSoldier.ActorName);

			bool selectableSoldierFound = false;
			foreach (var soldier in selectedSoldier.party)
			{
				if (HasActionsLeft(soldier))
				{
					SelectSoldier((Soldier)soldier);
					selectableSoldierFound = true;
					break;
				}
			}

			// No more soldiers selectable
			if (!selectableSoldierFound)
			{
				EndOfPlayerTurn();
			}
		}

		UpdateActionText();

		actionButtons[0].GetComponent<Button>().Select();
	}

	void UpdateActionText()
	{
		actionsLeftText.text = "Actions : <color=red>";
		for (int i = 0; i < selectedSoldier.Actions; i++)
			actionsLeftText.text += "■";
	}

	void EndOfPlayerTurn()
	{
		consecutiveAttackCounter = 0;

		Assert.IsTrue(activeParty == playerParty);
		foreach (var soldier in playerParty)
		{
			foreach (var ability in soldier.Abilities)
				ability.EndOfTurn();

			soldier.active = false;
			soldier.Actions = soldier.DefaultActions;
		}
		if (CheckEndOfCombat())
			return;

		EnterActionAnimationState();
		activePartyText.text = "<color=red>Aliens";
		activeParty = enemyParty;

		// Invert the sort orders
		foreach (var soldier in playerParty)
			soldier.obj.GetComponent<SpriteRenderer>().sortingOrder -= 4;
		foreach (var enemy in enemyParty)
			enemy.obj.GetComponent<SpriteRenderer>().sortingOrder += 4;

		if (!elementalAttacksThisTurn)
		{
			var playerPartyCopy = new List<Actor>(playerParty); // Needed because we might modify the party contents as we damage our actors
			foreach (var soldier in playerPartyCopy)
			{
				if (soldier.BossRunes.Find(x => x.Name == "Double tranchant") != null)
					DamageActor(null, soldier, 50, AttackType.Unavoidable);
			}
		}
		elementalAttacksThisTurn = false;

		enemyTurnOverlay.SetActive(true);

		List<Actor> enemyPartyCopy = new List<Actor>(enemyParty);
		foreach (var actor in enemyPartyCopy)
			actor.ApplyEffectsEnemyTurnStart();

		StartCoroutine(BaddiesTurn());
	}

	void EndOfEnemyTurn()
	{
		Assert.IsTrue(activeParty == enemyParty);
		foreach (var enemy in enemyParty)
		{
			foreach (var ability in enemy.Abilities)
				ability.EndOfTurn();

			enemy.active = false;
			enemy.Actions = enemy.DefaultActions;
		}
		if (CheckEndOfCombat())
			return;

		enemyTurnOverlay.SetActive(false);

		// Invert the sort orders
		foreach (var soldier in playerParty)
			soldier.obj.GetComponent<SpriteRenderer>().sortingOrder += 4;
		foreach (var enemy in enemyParty)
			enemy.obj.GetComponent<SpriteRenderer>().sortingOrder -= 4;

		EndOfTurn();
	}

	void EndOfTurn()
	{
		++turnNumber;
		turnText.text = "<mspace=0.55em>Tour " + Mathf.Min(99, turnNumber);
		activePartyText.text = "<color=#00A9FF>Humains";
		activeParty = playerParty;

		var playerPartyCopy = new List<Actor>(playerParty); // Make a copy as some effects might destroy the actors we're iterating on
		var enemyPartyCopy = new List<Actor>(enemyParty);
		foreach (var actor in playerPartyCopy)
			actor.ApplyEffects();
		foreach (var actor in enemyPartyCopy)
			actor.ApplyEffects();

		if (CheckEndOfCombat())
			return;

		// Players can play again!
		interactable = true;

		// Select the first available solider in the party
		bool playerFound = false;
		for (int i = 0; i < playerPartyCopy.Count; i++)
		{
			if (playerParty[i].Actions > 0)
			{
				playerFound = true;
				SelectSoldier((Soldier)playerParty[i]);
				break;
			}
		}
		// Nobody can play? Enemy turn then
		if (!playerFound)
			EndOfPlayerTurn();
	}

	IEnumerator BaddiesTurn()
	{
		Debug.Log("It's the baddies' turn now!");
		// Baddies action loop
		while (true)
		{
			Actor activeEnemy = enemyParty.Find(x => HasActionsLeft(x));
			if (activeEnemy == null)
				break; // All baddies have played, stop here

			// Mark this enemy as active
			foreach (var s in enemyParty)
				s.active = false;
			activeEnemy.active = true;
			MoveGlobalArrow(activeEnemy.obj.arrow);

			// Pick an ability
			var actionInfo = activeEnemy.aiController.PickAction();
			Assert.IsNotNull(actionInfo); // This enemy is supposed to be able to act

			Action action = actionInfo.action;
			action.effector = activeEnemy;

			action.target = actionInfo.target;

			Assert.IsTrue(action.effector.Enemy);
			actionFinished = false;
			action.Use();
			yield return new WaitUntil(() => actionFinished);

			if (action.turnEnding)
				activeEnemy.Actions = 0;
			else
				activeEnemy.Actions--;

			//Debug.Log("Enemy " + activeEnemy.ActorName + ", action finished");
		}

		Debug.Log("Baddies have finished their turn");
		EndOfEnemyTurn();
	}

	void DoActualSwitchPlayer(int offset = +1)
	{
		int iterationLimit = 100; // Safeguard
		Soldier s;
		do
		{
			if (--iterationLimit <= 0) // We don't want infinite loops, even if it's very unlikely to occur
			{
				s = selectedSoldier;
				break;
			}

			selectionCycleIndex = (selectionCycleIndex + offset) % playerParty.Count;
			if (selectionCycleIndex < 0) // Correct negative modulos
				selectionCycleIndex += playerParty.Count;
			s = (Soldier)playerParty[selectionCycleIndex];
		}
		while (s == selectedSoldier || s.Actions == 0);
		SelectSoldier(s);
	}

	public void SwitchPlayer(int offset = +1)
	{
		if (activeParty != playerParty)
			return;
		if (!interactable)
			return;
		if (playerParty.Count <= 1)
			return;

		DoActualSwitchPlayer(offset);
	}

	public void SkipTurn()
	{
		if (activeParty != playerParty)
			return;
		if (!interactable)
			return;
		EndOfPlayerTurn();
	}

	// Update is called once per frame
	void Update()
	{
		foreach (var actor in playerParty)
			actor.obj.UpdateState();
		foreach (var actor in enemyParty)
			actor.obj.UpdateState();

		if (interactable)
		{
			// Force next turn
			if (Input.GetKeyDown(KeyCode.T))
			{
				EndOfPlayerTurn();
			}
			// Select other soldier
			//if (Input.GetKeyDown(KeyCode.Tab))
			if (Input.GetButtonDown("SwitchRight"))
			{
				GetComponent<AudioSource>().Play();
				SwitchPlayer(+1);
			}
			else if (Input.GetButtonDown("SwitchLeft"))
			{
				GetComponent<AudioSource>().Play();
				SwitchPlayer(-1);
			}
		}
	}
}
