using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class FloatingText : MonoBehaviour
{
    public float aliveTime = 0.75f;
    public float fadeTime = 1.5f;
    public float moveSpeed = 0.75f;

    float elapsedTime = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        elapsedTime += Time.deltaTime;
        transform.Translate(0, moveSpeed * Time.deltaTime, 0);


        if (elapsedTime > aliveTime && elapsedTime < (aliveTime + fadeTime))
		{
            Color col = GetComponent<TextMeshPro>().color;
            Color col_transparent = new Color(col.r, col.g, col.b, 0);
            GetComponent<TextMeshPro>().color = Color.Lerp(col, col_transparent, (elapsedTime - aliveTime) / (fadeTime));
		}
        if (elapsedTime > aliveTime + fadeTime)
            Destroy(gameObject);
    }
}
