using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class RadBeastEnemy : AIController
{
	public static Enemy Create(List<Actor> playerParty = null, List<Actor> enemyParty = null)
	{
        Enemy enemy = new Enemy();
        enemy.MaxHP = enemy.HP = Tweakables.GetSingle("radbeast_hp", 230);
        enemy.SetBaseDodge(Tweakables.GetSingle("radbeast_dodge", 0));
        enemy.SetBaseCrit(Tweakables.GetSingle("radbeast_crit", 8));
        enemy.MaxArmor = enemy.Armor = Tweakables.GetSingle("radbeast_armor", 0);
        enemy.Actions = enemy.DefaultActions = 1;
        enemy.ActorName = "Cr�ature irradi�e";
        enemy.Enemy = true;
        enemy.aiController = new RadBeastEnemy(enemy);
        enemy.zone = 3;

        enemy.Abilities.Add(new HeavyPunchAction());
        enemy.Abilities.Add(new RadSprayAction());
        enemy.HealElements.Add(AttackType.Radioactive);
        enemy.Vulnerabilities.Add(new AttackTypeRate(AttackType.Fire, 70));
        enemy.Resistances.Add(new AttackTypeRate(AttackType.Bleeding, 20));
        enemy.party = enemyParty;
        enemy.oppositeParty = playerParty;

        return enemy;
    }


    public RadBeastEnemy(Actor actor) : base(actor)
    { }

	public override ActionInfo PickAction()
	{
        // Get usable abilities
        var usable = actor.Abilities.FindAll(x => x.CanUse());
        if (usable.Count == 0)
            return null; // No action

        // Pick one ability at random
        var ability = usable[Random.Range(0, usable.Count)];

        // No healing abilities, so pick the opponent with the lowest health
        var target = PickTarget();

        return new ActionInfo(ability, target);
	}
}
