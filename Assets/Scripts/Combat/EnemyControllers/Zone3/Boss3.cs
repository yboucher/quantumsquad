using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Boss3 : AIController
{
	public static Enemy Create(List<Actor> playerParty = null, List<Actor> enemyParty = null)
	{
        Enemy enemy = new Enemy();
        enemy.MaxHP = enemy.HP = Tweakables.GetSingle("boss3_hp", 800);
        enemy.SetBaseDodge(Tweakables.GetSingle("boss3_dodge", 12));
        enemy.SetBaseCrit(Tweakables.GetSingle("boss3_crit", 10));
        enemy.ArmorIsInHitNumber = true;
        enemy.Actions = enemy.DefaultActions = 2;
        enemy.ActorName = "Nayzaka";
        enemy.Enemy = true;
        enemy.aiController = new Boss3(enemy);
        enemy.isBoss = true;
        enemy.zone = 3;

        var shootAction = new ShootAction
		{
			useRange = true,
			damageLo = Tweakables.GetSingle("boss3_atk1_min", 25),
			damageHi = Tweakables.GetSingle("boss3_atk1_max", 30),
			accuracy = Tweakables.GetSingle("boss3_atk1_accuracy", 88)
		}; enemy.Abilities.Add(shootAction);
        var multizapAction = new MultiZapAction
        {
            useRange = true,
            damageMin = Tweakables.GetSingle("boss3_zap_min", 7),
            damageMax = Tweakables.GetSingle("boss3_zap_max", 12),
            accuracy = Tweakables.GetSingle("boss3_zap_accuracy", 95)
        }; enemy.Abilities.Add(multizapAction);
        enemy.Abilities.Add(new FuryActivationAction());

        enemy.Resistances.Add(new AttackTypeRate(AttackType.Electric, 80));
        enemy.Vulnerabilities.Add(new AttackTypeRate(AttackType.Fire, 50));
        enemy.party = enemyParty;
        enemy.oppositeParty = playerParty;

        return enemy;
    }


    public Boss3(Actor actor) : base(actor)
    {
    }

	public override void PreCombatInit()
	{
        // This boss is tall, shift him downwards some bit
        actor.obj.transform.position += new Vector3(0, -0.8f, 0);
    }

    public override ActionInfo PickAction()
	{
        // Get usable abilities
        var usable = actor.Abilities.FindAll(x => x.CanUse());
        // No shield on first turn
        //if (GameObject.FindObjectOfType<CombatManager>().TurnNumber() <= 1)
        //    usable.Remove(actor.Abilities[2]);
        if (usable.Count == 0)
            return null; // No action

        // Pick one ability at random
        var ability = usable[Random.Range(0, usable.Count)];

        // No healing abilities, so pick the opponent with the lowest health
        var target = PickTarget();

        return new ActionInfo(ability, target);
	}

    public override Action PickCounterattack()
    {
        // No healing abilities, so pick the opponent with the lowest health
        var target = PickTarget();

        var shootAction = new ShootAction
        {
            useRange = true,
            damageLo = Tweakables.GetSingle("boss3_counteratk_min", 22),
            damageHi = Tweakables.GetSingle("boss3_counteratk_max", 27),
            accuracy = Tweakables.GetSingle("boss3_atk1_accuracy", 88)
        };

        return shootAction;
    }
}
