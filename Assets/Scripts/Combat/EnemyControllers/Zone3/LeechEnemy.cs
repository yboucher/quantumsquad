using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class LeechEnemy : AIController
{
	public static Enemy Create(List<Actor> playerParty = null, List<Actor> enemyParty = null)
	{
        Enemy enemy = new Enemy();
        enemy.MaxHP = enemy.HP = Tweakables.GetSingle("mutant_hp", 160);
        enemy.SetBaseDodge(Tweakables.GetSingle("mutant_dodge", 8));
        enemy.SetBaseCrit(Tweakables.GetSingle("mutant_crit", 8));
        enemy.MaxArmor = enemy.Armor = Tweakables.GetSingle("mutant_armor", 0);
        enemy.Actions = enemy.DefaultActions = 1;
        enemy.ActorName = "Mutant sangsue";
        enemy.Enemy = true;
        enemy.aiController = new LeechEnemy(enemy);

        var slashAction = new BleedingSlashAction();
        slashAction.useRange = true;
        slashAction.damageLo = Tweakables.GetSingle("mutant_slash_dmg_min", 27);
        slashAction.damageHi = Tweakables.GetSingle("mutant_slash_dmg_max", 32);
        slashAction.accuracy = Tweakables.GetSingle("mutant_slash_accuracy", 80);
        slashAction.bleedingChance = Tweakables.GetSingle("mutant_slash_bleed_prob", 60);
        slashAction.bleedTurns = Tweakables.GetSingle("mutant_slash_bleed_turns", 3);
        enemy.Abilities.Add(slashAction);
        enemy.zone = 3;

        var piqureAction = new LeechAttackAction(enemy);
        piqureAction.accuracy = Tweakables.GetSingle("piqure_accuracy", 90);
        piqureAction.damage = Tweakables.GetSingle("piqure_dmg", 27);
        enemy.Abilities.Add(piqureAction);

        enemy.Vulnerabilities.Add(new AttackTypeRate(AttackType.Fire, 50));
        enemy.Resistances.Add(new AttackTypeRate(AttackType.Bleeding, 70));
        enemy.party = enemyParty;
        enemy.oppositeParty = playerParty;

        return enemy;
    }


    public LeechEnemy(Actor actor) : base(actor)
    { }

	public override ActionInfo PickAction()
	{
        // Get usable abilities
        var usable = actor.Abilities.FindAll(x => x.CanUse());
        if (usable.Count == 0)
            return null; // No action

        // Pick one ability at random
        var ability = usable[Random.Range(0, usable.Count)];

        // No healing abilities, so pick the opponent with the lowest health
        var target = PickTarget();

        return new ActionInfo(ability, target);
	}
}
