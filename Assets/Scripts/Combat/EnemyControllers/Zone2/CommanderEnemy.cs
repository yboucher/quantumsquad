using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class CommanderEnemy : AIController
{
	public static Enemy Create(List<Actor> playerParty = null, List<Actor> enemyParty = null)
	{
        Enemy enemy = new Enemy();
        enemy.MaxHP = enemy.HP = Tweakables.GetSingle("commandant_hp", 120);
        enemy.SetBaseDodge(Tweakables.GetSingle("commandant_dodge", 15));
        enemy.SetBaseCrit(Tweakables.GetSingle("commandant_crit", 0));
        enemy.MaxArmor = enemy.Armor = Tweakables.GetSingle("commandant_armor", 0);
        enemy.Actions = enemy.DefaultActions = 1;
        enemy.ActorName = "Commandant";
        enemy.Enemy = true;
        enemy.aiController = new ProtectorEnemy(enemy);
        enemy.zone = 2;

        enemy.Abilities.Add(new WarCryAction());
        enemy.Abilities.Add(new EnemySpawnAction(enemy, 3, () => Zone1Enemy2.Create()));

        enemy.Vulnerabilities.Add(new AttackTypeRate(AttackType.Bleeding, 30));
        enemy.party = enemyParty;
        enemy.oppositeParty = playerParty;

        return enemy;
    }


    public CommanderEnemy(Actor actor) : base(actor)
    { }

	public override ActionInfo PickAction()
	{
        // Get usable abilities
        var usable = actor.Abilities.FindAll(x => x.CanUse());
        if (usable.Count == 0)
            return null; // No action

        // Pick one ability at random
        var ability = usable[Random.Range(0, usable.Count)];

        // No healing abilities, so pick the opponent with the lowest health
        var target = PickTarget();

        return new ActionInfo(ability, target);
	}
}
