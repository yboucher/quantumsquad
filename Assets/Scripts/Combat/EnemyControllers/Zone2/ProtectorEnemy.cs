using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ProtectorEnemy : AIController
{
	public static Enemy Create(List<Actor> playerParty = null, List<Actor> enemyParty = null)
	{
        Enemy enemy = new Enemy();
        enemy.MaxHP = enemy.HP = Tweakables.GetSingle("protector_hp", 200);
        enemy.SetBaseDodge(Tweakables.GetSingle("protector_dodge", 0));
        enemy.SetBaseCrit(Tweakables.GetSingle("protector_crit", 2));
        enemy.MaxArmor = enemy.Armor = Tweakables.GetSingle("protector_armor", 0);
        enemy.Actions = enemy.DefaultActions = 1;
        enemy.ActorName = "Protecteur";
        enemy.Enemy = true;
        enemy.aiController = new ProtectorEnemy(enemy);
        enemy.zone = 2;

        var shootAction = new SlashAction(); shootAction.accuracy = Tweakables.GetSingle("protector_accuracy", 75); shootAction.damage = Tweakables.GetSingle("protector_atk_damage", 24);
        enemy.Abilities.Add(shootAction);
        enemy.Abilities.Add(new ProtectorAction());
        enemy.Invicibility.Add(AttackType.Bleeding);
        enemy.Vulnerabilities.Add(new AttackTypeRate(AttackType.Electric, 40));
        enemy.party = enemyParty;
        enemy.oppositeParty = playerParty;

        return enemy;
    }


    public ProtectorEnemy(Actor actor) : base(actor)
    { }

	public override ActionInfo PickAction()
	{
        // Get usable abilities
        var usable = actor.Abilities.FindAll(x => x.CanUse());
        if (usable.Count == 0)
            return null; // No action

        // Pick one ability at random
        var ability = usable[Random.Range(0, usable.Count)];

        // No healing abilities, so pick the opponent with the lowest health
        var target = PickTarget();

        return new ActionInfo(ability, target);
	}
}
