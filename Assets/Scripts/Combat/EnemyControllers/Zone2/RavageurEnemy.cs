using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class RavageurEnemy : AIController
{
	public static Enemy Create(List<Actor> playerParty = null, List<Actor> enemyParty = null)
	{
        Enemy enemy = new Enemy();
        enemy.MaxHP = enemy.HP = Tweakables.GetSingle("ravageur_hp", 160);
        enemy.SetBaseDodge(Tweakables.GetSingle("ravageur_dodge", 12));
        enemy.SetBaseCrit(Tweakables.GetSingle("ravageur_crit", 3));
        enemy.MaxArmor = enemy.Armor = Tweakables.GetSingle("ravageur_armor", 0);
        enemy.Actions = enemy.DefaultActions = 1;
        enemy.ActorName = "Ravageur";
        enemy.Enemy = true;
        enemy.aiController = new RavageurEnemy(enemy);

        var shootAction = new ShootAction();
        shootAction.accuracy = Tweakables.GetSingle("ravageur_shot_accuracy", 95);
        shootAction.useRange = true;
        shootAction.damageLo = Tweakables.GetSingle("ravageur_shot_damage_min", 17);
        shootAction.damageHi = Tweakables.GetSingle("ravageur_shot_damage_max", 25);
        enemy.Abilities.Add(shootAction);
        enemy.zone = 2;

        var chargedAction = new RavageurChargedAction();
        chargedAction.accuracy = Tweakables.GetSingle("ravageur_attack_accuracy", 83);
        chargedAction.damage = Tweakables.GetSingle("ravageur_attack_damage", 35);
        enemy.Abilities.Add(chargedAction);

        enemy.Resistances.Add(new AttackTypeRate(AttackType.Fire, 50));
        enemy.Vulnerabilities.Add(new AttackTypeRate(AttackType.Electric, 50));
        enemy.party = enemyParty;
        enemy.oppositeParty = playerParty;

        return enemy;
    }


    public RavageurEnemy(Actor actor) : base(actor)
    { }

	public override ActionInfo PickAction()
	{
        // Get usable abilities
        var usable = actor.Abilities.FindAll(x => x.CanUse());
        if (usable.Count == 0)
            return null; // No action

        // Pick one ability at random
        var ability = usable[Random.Range(0, usable.Count)];

        // No healing abilities, so pick the opponent with the lowest health
        var target = PickTarget();

        return new ActionInfo(ability, target);
	}
}
