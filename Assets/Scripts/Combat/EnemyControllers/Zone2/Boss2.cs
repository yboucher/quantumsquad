using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Boss2 : AIController
{
	public static Enemy Create(List<Actor> playerParty = null, List<Actor> enemyParty = null)
	{
        Enemy enemy = new Enemy();
        enemy.MaxHP = enemy.HP = Tweakables.GetSingle("boss2_hp", 420);
        enemy.SetBaseDodge(Tweakables.GetSingle("boss2_dodge", 0));
        enemy.SetBaseCrit(Tweakables.GetSingle("boss2_crit", 11));
        enemy.MaxArmor = enemy.Armor = 7;
        enemy.ArmorIsInHitNumber = true;
        enemy.Actions = enemy.DefaultActions = 2;
        enemy.ActorName = "Getal Mear";
        enemy.Enemy = true;
        enemy.aiController = new Boss2(enemy);
        enemy.isBoss = true;
        enemy.zone = 2;

        var slashAction = new BulletFlurryAction
		{
			useRange = true,
			damageMin = Tweakables.GetSingle("boss2_gatling_min", 13),
			damageMax = Tweakables.GetSingle("boss2_gatling_max", 17),
			accuracy = Tweakables.GetSingle("boss2_gatling_accuracy", 80)
		}; enemy.Abilities.Add(slashAction);
		var zapAction = new ZapAction
		{
			useRange = true,
			accuracy = Tweakables.GetSingle("boss2_zap_accuracy", 92),
			damageMin = Tweakables.GetSingle("boss2_zap_damage_min", 10),
            damageMax = Tweakables.GetSingle("boss2_zap_damage_max", 15)
        };
		enemy.Abilities.Add(zapAction);
        enemy.Abilities.Add(new SliceMountainAction(enemy));

        enemy.Vulnerabilities.Add(new AttackTypeRate(AttackType.Electric, 50));
        enemy.party = enemyParty;
        enemy.oppositeParty = playerParty;

        return enemy;
    }


    public Boss2(Actor actor) : base(actor)
    {
    }

	public override void PreCombatInit()
	{
        // This boss is tall, shift him downwards some bit
        actor.obj.transform.position += new Vector3(0, -0.8f, 0);
    }

    public override ActionInfo PickAction()
	{
        // Priority : using the slice action if available
        if (actor.Abilities[2].CanUse())
		{
            return new ActionInfo(actor.Abilities[2], actor);
        }

        // Get usable abilities
        var usable = actor.Abilities.FindAll(x => x.CanUse());
        // No shield on first turn
        //if (GameObject.FindObjectOfType<CombatManager>().TurnNumber() <= 1)
        //    usable.Remove(actor.Abilities[2]);
        if (usable.Count == 0)
            return null; // No action

        // Pick one ability at random
        var ability = usable[Random.Range(0, usable.Count)];

        // No healing abilities, so pick the opponent with the lowest health
        var target = PickTarget();

        return new ActionInfo(ability, target);
	}
}
