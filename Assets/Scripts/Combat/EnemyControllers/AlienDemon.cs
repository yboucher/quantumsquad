using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class AlienDemon : AIController
{
	public static Enemy Create(List<Actor> playerParty, List<Actor> enemyParty)
	{
        Enemy enemy = new Enemy();
        enemy.MaxHP = 100;
        enemy.HP = 80;
        enemy.MaxArmor = 10;
        enemy.Armor = 10;
        enemy.Actions = enemy.DefaultActions = 1;
        enemy.ActorName = "Alien Demon";
        enemy.Enemy = true;
        enemy.aiController = new AlienDemon(enemy);
        enemy.Skills.Add(new ActorSkillInfo("Grenade", 1));

        var shootAction = new ShootAction(); shootAction.accuracy = 50;
        enemy.Abilities.Add(shootAction);
        enemy.Abilities.Add(new SlashAction());
        enemy.Abilities.Add(new GrenadeAction());
        enemy.party = enemyParty;
        enemy.oppositeParty = playerParty;

        return enemy;
    }


    public AlienDemon(Actor actor) : base(actor)
    { }

	public override ActionInfo PickAction()
	{
        // Get usable abilities
        var usable = actor.Abilities.FindAll(x => x.CanUse());
        if (usable.Count == 0)
            return null; // No action

        // Pick one ability at random
        var ability = usable[Random.Range(0, usable.Count)];

        // No healing abilities, so pick the opponent with the lowest health
        var target = actor.oppositeParty.OrderBy(x => x.HP).FirstOrDefault();

        return new ActionInfo(ability, target);
	}
}
