using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public abstract class AIController
{
	protected Actor actor;

	public class ActionInfo
	{
		public ActionInfo(Action action, Actor target)
		{
			this.action = action;
			this.target = target;
		}
		public Action action;
		public Actor target;
	}

	public AIController(Actor actor)
	{
		this.actor = actor;
	}

	public virtual void PreCombatInit()
	{ }
	protected virtual Actor PickTarget()
	{
		var orderedList = actor.oppositeParty.OrderBy(x => x.HP).ToList();
		// Weighted targeting : random, but more often than not the weakest enemy
		while (orderedList.Count > 0)
		{
			int roll = Random.Range(0, 100);
			if (roll <= 65)
				return orderedList.FirstOrDefault();
			else
				orderedList.RemoveAt(0);
		}

		// Fallback
		return actor.oppositeParty.OrderBy(x => x.HP).FirstOrDefault();
	}

	public abstract ActionInfo PickAction();

	public virtual Action PickCounterattack()
	{ return null; }
}
