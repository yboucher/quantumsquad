using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Boss1 : AIController
{
	public static Enemy Create(List<Actor> playerParty = null, List<Actor> enemyParty = null)
	{
        Enemy enemy = new Enemy();
        enemy.MaxHP = enemy.HP = Tweakables.GetSingle("boss1_hp", 300);
        enemy.SetBaseDodge(Tweakables.GetSingle("boss1_dodge", 4));
        enemy.SetBaseCrit(Tweakables.GetSingle("boss1_crit", 6));
        enemy.MaxArmor = enemy.Armor = 0;
        enemy.Actions = enemy.DefaultActions = 2;
        enemy.ActorName = "Seigneur de guerre";
        enemy.Enemy = true;
        enemy.aiController = new Boss1(enemy);
        enemy.isBoss = true;
        enemy.zone = 1;

        var slashAction = new ShootAction(); slashAction.damage = Tweakables.GetSingle("boss1_damage", 35); slashAction.accuracy = Tweakables.GetSingle("accuracy_damage", 90);
        enemy.Abilities.Add(slashAction);
        enemy.Abilities.Add(new MarkAction());
        enemy.Abilities.Add(new BossArmorAction());
        enemy.party = enemyParty;
        enemy.oppositeParty = playerParty;

        return enemy;
    }


    public Boss1(Actor actor) : base(actor)
    {
    }

	public override void PreCombatInit()
	{
        // This boss is tall, shift him downwards some bit
        actor.obj.transform.position += new Vector3(0, -0.8f, 0);
    }

    public override ActionInfo PickAction()
	{
        // No more armor, get our crit chance back
        if (actor.Armor <= 0)
		{
            actor.SetBaseCrit(Tweakables.GetSingle("boss1_crit", 6));
        }

        // Priority : using the armor action if available, but not on first turn!
        if (actor.Abilities[2].CanUse() /*&& GameObject.FindObjectOfType<CombatManager>().TurnNumber() > 1*/)
		{
            return new ActionInfo(actor.Abilities[2], actor);
        }

        // Get usable abilities
        var usable = actor.Abilities.FindAll(x => x.CanUse());
        // No shield on first turn
        //if (GameObject.FindObjectOfType<CombatManager>().TurnNumber() <= 1)
        //    usable.Remove(actor.Abilities[2]);
        if (usable.Count == 0)
            return null; // No action

        // Pick one ability at random
        var ability = usable[Random.Range(0, usable.Count)];

        // No healing abilities, so pick the opponent with the lowest health
        var target = PickTarget();

        return new ActionInfo(ability, target);
	}
}
