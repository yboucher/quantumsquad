using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Zone1Enemy2 : AIController
{
	public static Enemy Create(List<Actor> playerParty = null, List<Actor> enemyParty = null)
	{
        Enemy enemy = new Enemy();
        enemy.MaxHP = enemy.HP = Tweakables.GetSingle("enemy2_hp", 90);
        enemy.SetBaseDodge(Tweakables.GetSingle("enemy2_dodge", 8));
        enemy.SetBaseCrit(Tweakables.GetSingle("enemy2_crit", 8));  
        enemy.MaxArmor = enemy.Armor = 0;
        enemy.Actions = enemy.DefaultActions = 1;
        enemy.ActorName = "Garde contaminé";
        enemy.Enemy = true;
        enemy.aiController = new Zone1Enemy2(enemy);
        enemy.zone = 1;

        var shootAction = new ShootAction(); shootAction.accuracy = Tweakables.GetSingle("enemy2_shoot_accuracy", 90); shootAction.damage = Tweakables.GetSingle("enemy2_shoot_damage", 15);
        enemy.Abilities.Add(shootAction);
        var gammaAction = new GammaShootAction();
        gammaAction.accuracy = Tweakables.GetSingle("enemy2_gamma_accuracy", 80);
        enemy.Abilities.Add(gammaAction);
        enemy.Invicibility.Add(AttackType.Radioactive);
        enemy.party = enemyParty;
        enemy.oppositeParty = playerParty;

        return enemy;
    }


    public Zone1Enemy2(Actor actor) : base(actor)
    { }

	public override ActionInfo PickAction()
	{
        // Get usable abilities
        var usable = actor.Abilities.FindAll(x => x.CanUse());
        if (usable.Count == 0)
            return null; // No action

        // Pick one ability at random
        var ability = usable[Random.Range(0, usable.Count)];

        // No healing abilities, so pick the opponent with the lowest health
        var target = PickTarget();

        return new ActionInfo(ability, target);
	}
}
