using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Zone1Enemy1 : AIController
{
	public static Enemy Create(List<Actor> playerParty = null, List<Actor> enemyParty = null)
	{
        Enemy enemy = new Enemy();
        enemy.MaxHP = enemy.HP = Tweakables.GetSingle("enemy1_hp", 130);
        enemy.SetBaseDodge(Tweakables.GetSingle("enemy1_dodge", 5));
        enemy.SetBaseCrit(Tweakables.GetSingle("enemy1_crit", 12));
        enemy.MaxArmor = enemy.Armor = 0;
        enemy.Actions = enemy.DefaultActions = 1;
        enemy.ActorName = "Artilleur";
        enemy.Enemy = true;
        enemy.aiController = new Zone1Enemy1(enemy);
        enemy.zone = 1;

        var shootAction = new ShootAction(); shootAction.accuracy = Tweakables.GetSingle("enemy1_accuracy", 85); shootAction.damage = Tweakables.GetSingle("enemy1_shoot_damage", 20);
        enemy.Abilities.Add(shootAction);
        var fireAction = new FireShootAction();
        fireAction.damage = Tweakables.GetSingle("enemy1_fire_shot_damage", 40);
        fireAction.accuracy = Tweakables.GetSingle("enemy1_fire_shot_accuracy", 80);
        enemy.Abilities.Add(fireAction);
        enemy.Resistances.Add(new AttackTypeRate(AttackType.Fire, 20));
        enemy.party = enemyParty;
        enemy.oppositeParty = playerParty;

        return enemy;
    }


    public Zone1Enemy1(Actor actor) : base(actor)
    { }

	public override ActionInfo PickAction()
	{
        // Get usable abilities
        var usable = actor.Abilities.FindAll(x => x.CanUse());
        if (usable.Count == 0)
            return null; // No action

        // Pick one ability at random
        var ability = usable[Random.Range(0, usable.Count)];

        // No healing abilities, so pick the opponent with the lowest health
        var target = PickTarget();

        return new ActionInfo(ability, target);
	}
}
