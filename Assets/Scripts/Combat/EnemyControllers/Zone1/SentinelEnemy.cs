using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class SentinelEnemy : AIController
{
	public static Enemy Create(List<Actor> playerParty = null, List<Actor> enemyParty = null)
	{
        Enemy enemy = new Enemy();
        enemy.MaxHP = enemy.HP = Tweakables.GetSingle("sentinel_hp", 200);
        enemy.SetBaseDodge(Tweakables.GetSingle("sentinel_dodge", 12));
        enemy.SetBaseCrit(Tweakables.GetSingle("sentinel_crit", 10));
        enemy.MaxArmor = enemy.Armor = Tweakables.GetSingle("sentinel_armor", 10);
        enemy.Actions = enemy.DefaultActions = 1;
        enemy.ActorName = "Sentinelle";
        enemy.Enemy = true;
        enemy.aiController = new SentinelEnemy(enemy);
        enemy.zone = 1;

        var shootAction = new ShootAction(); shootAction.accuracy = Tweakables.GetSingle("sentinel_accuracy", 95); shootAction.damage = Tweakables.GetSingle("sentinel_shoot_damage", 20);
        enemy.Abilities.Add(shootAction);
        var zapAction = new ZapAction();
        zapAction.damage = Tweakables.GetSingle("sentinel_zap_damage", 10);
        enemy.Abilities.Add(zapAction);
        enemy.Invicibility.Add(AttackType.Bleeding);
        enemy.Vulnerabilities.Add(new AttackTypeRate(AttackType.Electric, 40));
        enemy.party = enemyParty;
        enemy.oppositeParty = playerParty;

        return enemy;
    }


    public SentinelEnemy(Actor actor) : base(actor)
    { }

	public override ActionInfo PickAction()
	{
        // Get usable abilities
        var usable = actor.Abilities.FindAll(x => x.CanUse());
        if (usable.Count == 0)
            return null; // No action

        // Pick one ability at random
        var ability = usable[Random.Range(0, usable.Count)];

        // No healing abilities, so pick the opponent with the lowest health
        var target = PickTarget();

        return new ActionInfo(ability, target);
	}
}
