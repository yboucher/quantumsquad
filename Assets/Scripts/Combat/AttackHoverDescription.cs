using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Assertions;
using TMPro;

public class AttackHoverDescription : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, ISelectHandler
{
    [TextArea]
    public string attackDescription;

    public TextMeshPro entityDescriptionTMP;
    public TextMeshPro attackDescriptionTMP;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void OnSelect(BaseEventData eventData)
	{
        if (GetComponent<ActionButton>() == null && GetComponent<ItemButton>() == null)
            return;

        if (eventData.selectedObject == gameObject)
            ShowDesc();
        else
            HideDesc();
	}

    void ShowDesc()
	{
        //Debug.Log("Hey, desc : " + attackDescription);
        //attackDescriptionTMP.text = attackDescription;
        if (GetComponent<ActionButton>())
        {
            attackDescriptionTMP.text = "";
            if (!GetComponent<ActionButton>().action.Damage().Equals(new System.Tuple<int, int>(0, 0)))
            {
                if (GetComponent<ActionButton>().action.Damage().Item1 == GetComponent<ActionButton>().action.Damage().Item2 &&
                    GetComponent<ActionButton>().action.Damage().Item1 != 0)
                {
                    attackDescriptionTMP.text += "<color=#FF7400><size=200%><sprite=1></size> <voffset=0.3em>" + GetComponent<ActionButton>().action.Damage().Item1 + "</voffset></color>  ";
                }
                else
                {
                    attackDescriptionTMP.text += "<color=#FF7400><size=200%><sprite=1></size> <voffset=0.3em>" + GetComponent<ActionButton>().action.Damage().Item1 + "-" + GetComponent<ActionButton>().action.Damage().Item2 + "</voffset></color>  ";
                }
            }
            if (GetComponent<ActionButton>().action.Accuracy() > 0)
            {
                attackDescriptionTMP.text += "<color=green><size=200%><sprite=0></size> <voffset=0.3em>" + GetComponent<ActionButton>().action.Accuracy() + "%</voffset></color>  ";
            }
            if (GetComponent<ActionButton>().action.CooldownTime > 0)
                attackDescriptionTMP.text += "<color=#00A9FF><size=200%><sprite=2></size> <voffset=0.3em>" + GetComponent<ActionButton>().action.CooldownTime + "</voffset></color>  ";
            if (attackDescriptionTMP.text != "")
                attackDescriptionTMP.text += "\n";
            if (GetComponent<ActionButton>().action.DamageType() != AttackType.Harmless)
            {
                attackDescriptionTMP.text += "Attaque de type <color=" + AttackTypeUtils.TypeToColorString(GetComponent<ActionButton>().action.DamageType()) + ">"
                    + AttackTypeUtils.TypeToAttackString(GetComponent<ActionButton>().action.DamageType()) + "</color>\n\n";
            }
            attackDescriptionTMP.text += GetComponent<ActionButton>().action.Description();
        }
        else
            attackDescriptionTMP.text = GetComponent<ItemButton>().item.Description();

        //entityDescriptionTMP.gameObject.SetActive(false);
        attackDescriptionTMP.gameObject.SetActive(true);
    }

    void HideDesc()
	{
        //Debug.Log("Hey, desc out");
        //entityDescriptionTMP.gameObject.SetActive(true);
        attackDescriptionTMP.gameObject.SetActive(false);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        ShowDesc();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        HideDesc();
    }
}
