using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatusBar : MonoBehaviour
{
    public Transform bar;

    float baseBarScale;
    float ratio = 1;
    float _value = 1;
    float _maxValue = 1;

    public float value
	{
        get => _value;
        set
		{
            _value = value;
            ratio = _value / _maxValue;
            UpdateBar();
		}
    }
    public float maxValue
    {
        get => _maxValue;
        set
        {
            _maxValue = Mathf.Max(value, 1);
            ratio = _value / _maxValue;
            UpdateBar();
        }
    }

    void UpdateBar()
	{
        bar.localScale = new Vector3(baseBarScale * ratio, bar.localScale.y, bar.localScale.z);
	}

    // Start is called before the first frame update
    void Start()
    {
        baseBarScale = 1;

        maxValue = 100;
        value = 10;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
