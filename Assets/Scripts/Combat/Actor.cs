using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[Serializable]
public class ActorSkillInfo
{
    public ActorSkillInfo(string name, int level)
	{
        SkillName = name;
        SkillLevel = level;
	}

    public string SkillName;
    public int SkillLevel;
}

public class AttackTypeRate
{
    public AttackType type;
    public float percentage;
    public AttackTypeRate(AttackType type, float percentage)
	{
        this.type = type;
        this.percentage = percentage;
	}
}

[Serializable]
public class Actor
{
    public string ActorName = "<MISSINGNO>";

    public string Class;

    public int Actions = 0;
    public int DefaultActions = 2;
    public bool turnEnded = false;
    public bool active = false;
    public bool Enemy = false;
    public int HP = 0;
    public int MaxHP = 0;
    public int Armor = 0;
    public int MaxArmor = 0;
    public int zone = 0;
    public bool ArmorIsInHitNumber = false; // Armor isn't HP but number of hits before wearing off
    public bool ArmorVulnerableToElements = false;
    public List<AttackType> Invicibility = new List<AttackType>();
    public List<AttackTypeRate> Resistances = new List<AttackTypeRate>();
    public List<AttackTypeRate> Vulnerabilities = new List<AttackTypeRate>();
    public List<AttackType> HealElements = new List<AttackType>();
    public List<Item> Items = new List<Item>();
    public List<Weapon> Weapons = new List<Weapon>();
    // Abilities can be inferred from the class and the skills
    public List<ActorSkillInfo> Skills = new List<ActorSkillInfo>();

    [System.NonSerialized]
    public List<Action> Abilities = new List<Action>();
    [System.NonSerialized]
    List<TemporaryEffect> Effects = new List<TemporaryEffect>(); // private, should only be accessed using AddEffect/RemoveEffect
    [System.NonSerialized]
    public List<Buff> Buffs = new List<Buff>();
    [System.NonSerialized]
    public List<BossRune> BossRunes = new List<BossRune>();
    [System.NonSerialized]
    public ActorSpriteUpdater obj;
    [System.NonSerialized]
    public List<Actor> party;
    [System.NonSerialized]
    public List<Actor> oppositeParty;
    [System.NonSerialized]
    public AIController aiController; // For AI characters, of course
    [System.NonSerialized]
    public bool movedForward = false;
    [System.NonSerialized]
    public Vector3 basePos = Vector3.zero; // Valid only if 'movedForward' is true

    // Base fields that should directly be used by gameplay code
    [SerializeField]
    private float BaseDodgeChance = 0;
    [SerializeField]
    private float BaseCritChance = 5;
    [SerializeField]
    private float AccuracyModifier = 100;

    public void SetBaseDodge(float chance)
	{
        BaseDodgeChance = chance;
	}

    public float GetBaseDodgeChance()
	{
        return BaseDodgeChance;
	}
    public void SetBaseCrit(float chance)
    {
        BaseCritChance = chance;
    }

    public float GetBaseCritChance()
	{
        return BaseCritChance;
	}
    public void SetBaseAccuracy(float accuracy)
    {
        AccuracyModifier = accuracy;
    }

    public bool CanCounterattack()
	{
        return Actions > 0 && Effects.Any(x => x.canCounterattack);
	}

    public List<TemporaryEffect> GetEffects()
	{
        return Effects;
	}

    public void AddEffect(TemporaryEffect effect, bool canStack = false)
	{
        //if (!canStack && Effects.Find(x => x.GetType() == effect.GetType()) != null)
        //    return; // Effect already exists and doesn't stack
        // If the effect is already present, remove the one in the effect list and replace it by the one provided in the parameters
        if (!canStack)
        {
            foreach (var e in Effects)
                if (e.GetType().Equals(effect.GetType()))
                    e.DoExpireCleanup(this);
            Effects.RemoveAll(x => x.GetType() == effect.GetType());
        }
        Effects.Add(effect);
	}
    // Returns the number of removed effects
    public int RemoveEffect<T>()
	{
        foreach (var effect in Effects)
            if (effect.GetType().Equals(typeof(T)))
                effect.DoExpireCleanup(this);
        return Effects.RemoveAll(x => x.GetType().Equals(typeof(T)));
	}
    public void RemoveNonPermanentEffects()
	{
        foreach (var effect in Effects)
            if (!effect.permanent)
                effect.DoExpireCleanup(this);
        Effects.RemoveAll(x => !x.permanent);
    }

    public void ApplyEffectsEnemyTurnStart()
    {
        List<TemporaryEffect> copy = new List<TemporaryEffect>(Effects); // Copy because some effects might be removed or added during the iterations
        foreach (var effect in copy)
        {
            if (effect.EffectAppliedOnEnemyTurnStart() && Enemy)
                effect.DoEffect(this);
        }
        foreach (var effect in Effects)
            if (effect.Expired())
                effect.DoExpireCleanup(this);
        Effects.RemoveAll(x => x.Expired()); // Remove all expired effects
    }
    public void ApplyEffects()
	{
        List<TemporaryEffect> copy = new List<TemporaryEffect>(Effects); // Copy because some effects might be removed or added during the iterations
        foreach (var effect in copy)
        {
            if (effect.EffectAppliedOnEnemyTurnStart() && Enemy)
                continue;
            effect.DoEffect(this);
        }
        foreach (var effect in Effects)
            if (effect.Expired())
                effect.DoExpireCleanup(this);
        Effects.RemoveAll(x => x.Expired()); // Remove all expired effects
	}

    public float EffectiveCritChance()
	{
        return BaseCritChance + Effects.Sum(x => x.critMod);
	}
    public float EffectiveDodgeChance()
	{
        return BaseDodgeChance + Effects.Sum(x => x.dodgeMod);
	}

    public float EffectiveAccuracyModifier()
	{
        return Effects.Sum(x => x.accuracyMod);
	}

    public float EffectiveReceivedDamageMultiplier()
	{
        float mult = 1;
        foreach (var effect in Effects)
            mult *= effect.receivedDamageMult;
        return mult;
	}
    public float EffectiveInflictedDamageMultiplier()
    {
        float mult = 1;
        foreach (var effect in Effects)
            mult *= effect.inflictedDamageMult;
        return mult;
    }

}
