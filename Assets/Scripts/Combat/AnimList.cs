using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimList : MonoBehaviour
{
	[System.Serializable]
	public class SpritePair
	{
		public string name;
		public RuntimeAnimatorController anim;
	}

	public List<SpritePair> anims;

	void Start()
	{

	}

	void Update()
	{

	}
}
