using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class SplitEffect : MonoBehaviour
{
    float timeToLive = 1.5f;
    Vector3 direction = Vector3.up;
    float startTime = -1;

    // Start is called before the first frame update
    void Start()
    {
        startTime = Time.time;
    }

    public static void SplitSprite(SpriteRenderer sprite)
	{
        var upper = GameObject.Instantiate(GameObject.Find("SlicedSprite"), sprite.transform.position, sprite.transform.rotation);

        upper.GetComponent<SplitEffect>().enabled = true;
        upper.GetComponent<SpriteRenderer>().sprite = sprite.sprite;
        upper.GetComponent<SpriteRenderer>().sortingOrder = sprite.sortingOrder;
        upper.GetComponent<SpriteRenderer>().maskInteraction = SpriteMaskInteraction.VisibleOutsideMask;
        upper.GetComponent<SplitEffect>().direction = Quaternion.Euler(0, 0, 0) * Vector3.up;

        // Create the other half
        var dup = GameObject.Instantiate(upper);
        dup.GetComponent<SpriteRenderer>().maskInteraction = SpriteMaskInteraction.VisibleInsideMask;
        dup.GetComponent<SplitEffect>().direction = Quaternion.Euler(0, 0, 180) * dup.GetComponent<SplitEffect>().direction; // go the other way

        upper.GetComponent<SpriteRenderer>().sortingOrder += 10;
        upper.GetComponent<SpriteMask>().isCustomRangeActive = true;
        upper.GetComponent<SpriteMask>().backSortingOrder = 0;
        upper.GetComponent<SpriteMask>().frontSortingOrder = upper.GetComponent<SpriteRenderer>().sortingOrder;

        // Delete the sprite that was split
        GameObject.Destroy(sprite.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        if (startTime < 0)
            return;
        if (Time.time - startTime > timeToLive)
        {
            Destroy(gameObject);
            return;
        }

        float speed = EasingFunction.EaseInCubic(1, 0.2f, (Time.time - startTime) / timeToLive);
        transform.Translate(direction * speed * Time.deltaTime);

        Color col = GetComponent<SpriteRenderer>().color;
        col.a = EasingFunction.EaseInCubic(1, 0, Mathf.Min(1, (Time.time - startTime) / timeToLive));
        GetComponent<SpriteRenderer>().color = col;
    }
}
