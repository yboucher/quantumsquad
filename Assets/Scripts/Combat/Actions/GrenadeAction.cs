using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using System.Linq;

[ActionData("Grenade", "Lance une grenade touchant tous les ennemis.")]
public class GrenadeAction : Action
{
	GameObject effect;

	[ActionMainStat("D�g�ts", 30, +6)]
	int damage = 20;
	public GrenadeAction()
	{
		Reset();
	}

	public override AttackType DamageType()
	{
		return AttackType.Blast;
	}
	public override System.Tuple<int, int> Damage()
	{
		return new System.Tuple<int, int>(damage, damage);
	}
	public override int Accuracy()
	{
		return 100;
	}
	protected override void ResetImpl()
	{
		name = "Grenade";

		Charges = 2;
		CooldownTime = 1 + Tweakables.GetSingle("grenade_cooldown", 1);

		effect = GameObject.Find("EffectPrefabRoot").transform.Find("GrenadePrefab").gameObject;
	}

	public override string Description()
	{
		return "Lance une grenade touchant tous les ennemis, faisant " + damage + " d�g�ts.";
	}

	protected override void UseImpl()
	{
		//Debug.Log("Grenade action called : " + name);
		GameObject.Find("GlobalScripts").GetComponent<CoroutineUtil>().StartCoroutine(GrenadeAnimation());
	}

	IEnumerator GrenadeAnimation()
	{
		// Target either the center of enemy party or the center of the player party
		var positionTransform = GameObject.Find(effector.Enemy ? "PlayerPartyCenter" : "EnemyPartyCenter").transform;

		effector.obj.GetComponent<AudioSource>().PlayOneShot(GameObject.FindObjectOfType<SoundFXList>().grenadeThrow);
		if (ActorAnimator().parameters.Any(x => x.name == "Throw"))
			ActorAnimator().SetTrigger("Throw");
		yield return new WaitForSeconds(0.9f);

		effector.obj.GetComponent<AudioSource>().PlayOneShot(GameObject.FindObjectOfType<SoundFXList>().grenadeBoom);
		var obj = GameObject.Instantiate(effect, positionTransform);
		obj.SetActive(true);

		Assert.IsNotNull(effector);
		GameObject.FindObjectOfType<CombatManager>().DamageParty(effector, effector.oppositeParty, damage, AttackType.Blast);

		yield return new WaitForSeconds(2);
		ActionOver();
	}
}
