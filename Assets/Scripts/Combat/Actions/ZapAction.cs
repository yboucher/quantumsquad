using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using System.Linq;

[ActionData("Zap", "Inflige 25 pts de d�g�ts. " +
			"�tourdit la cible pendant 1 tour.")]
public class ZapAction : Action
{
	GameObject projectile;
	GameObject stunnedMsg;

	[ActionMainStat("D�g�ts", 25, +5)]
	public int damage = 25;

	public int accuracy = 85;
	public bool useRange = false;
	public int damageMin = 25;
	public int damageMax = 25;
	public int stunTurns = 1;

	public ZapAction()
	{
		Reset();
	}

	public override AttackType DamageType()
	{
		return AttackType.Electric;
	}

	public override System.Tuple<int, int> Damage()
	{
		return new System.Tuple<int, int>(damage, damage);
	}
	public override int Accuracy()
	{
		return (int)accuracy;
	}

	protected override void ResetImpl()
	{
		name = "Zap";

		Charges = -1;
		CooldownTime = 1 + Tweakables.GetSingle("zap_cooldown", 2);

		projectile = GameObject.Find("EffectPrefabRoot").transform.Find("ProjectilePrefab").gameObject;
		stunnedMsg = GameObject.Find("EffectPrefabRoot").transform.Find("StunnedMsg").gameObject;
	}

	public override string Description()
	{
		return "Inflige " + damage + " pts de d�g�ts. " +
			"�tourdit la cible pendant 1 tour.";
	}

	protected override void UseImpl()
	{
		GameObject.Find("GlobalScripts").GetComponent<CoroutineUtil>().StartCoroutine(SlashAnimation());
	}

	IEnumerator SlashAnimation()
	{
		var monitor = GameObject.FindObjectOfType<SceneMouseHoverMonitor>();
		bool UIPickedTarget = false;
		if (target == null) // Ask the user for a target using the UI
		{
			UIPickedTarget = true;
			GameObject.FindObjectOfType<SceneMouseHoverMonitor>().TargetSelectionMode("Enemy");

			// wait until a target has been picked
			yield return new WaitUntil(() => monitor.SelectedTarget() != null);
			target = monitor.SelectedTarget().GetComponent<ActorSpriteUpdater>().actor;
		}

		//GameObject.Instantiate(projectile).SetActive(true);
		if (ActorAnimator().parameters.Any(x => x.name == "Zap"))
		{
			ActorAnimator().SetTrigger("Zap");
			yield return new WaitForSeconds(0.4f);
		}
		else
			GameObject.Instantiate(projectile).SetActive(true);

		effector.obj.GetComponent<AudioSource>().PlayOneShot(GameObject.FindObjectOfType<SoundFXList>().plasma);
		yield return new WaitForSeconds(0.675f);

		int actualDamage = damage;
		if (useRange)
		{
			actualDamage = Random.Range(damageMin, damageMax);
		}

		Assert.IsNotNull(target);
		bool hit = GameObject.FindObjectOfType<CombatManager>().AttackActor(effector, target, actualDamage, accuracy + effector.EffectiveAccuracyModifier(),
			AttackType.Electric);
		if (hit)
		{
			GameObject.Instantiate(stunnedMsg, target.obj.transform).SetActive(true);
			target.AddEffect(new StunnedEffect(stunTurns));
			target.Actions = 0; // Stun
		}

		yield return new WaitForSeconds(2);
		if (UIPickedTarget)
			monitor.ExitTargetMode();
		ActionOver();
	}
}
