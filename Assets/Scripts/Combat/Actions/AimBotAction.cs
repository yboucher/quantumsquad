using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using System.Linq;

[ActionData("Aimbot", "Attaque pr�cise, inflige 15-20 points de d�g�ts.")]
public class AimBotAction : Action
{
	GameObject projectile;

	public float accuracy = Tweakables.GetSingle("aimbot_accuracy", 107);

	[ActionMainStat("D�g�ts minimum", 15, +5)]
	public int damage = 15;

	public AimBotAction()
	{
		Reset();
	}

	public override AttackType DamageType()
	{
		return AttackType.Ballistic;
	}
	public override System.Tuple<int, int> Damage()
	{
		return new System.Tuple<int, int>(damage, damage + 5);
	}
	public override int Accuracy()
	{
		return (int)accuracy;
	}
	protected override void ResetImpl()
	{
		name = "Aimbot";

		Charges = -1;
		CooldownTime = 0 + Tweakables.GetSingle("aimbot_cooldown", 0);

		projectile = GameObject.Find("EffectPrefabRoot").transform.Find("YakaProjectilePrefab").gameObject;
	}

	public override string Description()
	{
		return "Attaque pr�cise qui inflige " + damage + "-" + (damage+5) + "pts de d�g�ts plasma.";
	}

	protected override void UseImpl()
	{
		GameObject.Find("GlobalScripts").GetComponent<CoroutineUtil>().StartCoroutine(ShootAnimation());
	}

	IEnumerator ShootAnimation()
	{
		if (effector.movedForward)
		{
			// Move back
			LeanTween.move(effector.obj.gameObject, effector.basePos, 0.75f).setEaseInOutQuad();
			yield return new WaitUntil(() => !LeanTween.isTweening(effector.obj.gameObject));
			effector.movedForward = false;
		}

		var monitor = GameObject.FindObjectOfType<SceneMouseHoverMonitor>();
		bool UIPickedTarget = false;
		if (target == null) // Ask the user for a target using the UI
		{
			UIPickedTarget = true;
			GameObject.FindObjectOfType<SceneMouseHoverMonitor>().TargetSelectionMode("Enemy");

			// wait until a target has been picked
			yield return new WaitUntil(() => monitor.SelectedTarget() != null);
			target = monitor.SelectedTarget().GetComponent<ActorSpriteUpdater>().actor;
		}


		effector.obj.GetComponent<AudioSource>().PlayOneShot(GameObject.FindObjectOfType<SoundFXList>().plasma);
		if (ActorAnimator().parameters.Any(x => x.name == "Shoot"))
		{
			ActorAnimator().SetTrigger("Shoot");
			yield return new WaitForSeconds(0.1f);
			GameObject.Instantiate(projectile, effector.obj.transform).SetActive(true);
			yield return new WaitForSeconds(0.3f);
		}
		else
			GameObject.Instantiate(projectile).SetActive(true);

		int actualDamage = damage + Random.Range(0, 6);

		Assert.IsNotNull(target);
		GameObject.FindObjectOfType<CombatManager>().AttackActor(effector, target, actualDamage, accuracy + effector.EffectiveAccuracyModifier(),
			AttackType.Ballistic);

		yield return new WaitForSeconds(2);
		if (UIPickedTarget)
			monitor.ExitTargetMode();
		ActionOver();
	}
}
