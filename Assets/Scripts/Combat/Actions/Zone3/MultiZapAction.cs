using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class MultiZapAction : Action
{
	GameObject projectile;
	GameObject stunnedMsg;

	[ActionMainStat("D�g�ts", 10, +5)]
	public int damage = 10;

	public bool useRange = false;
	public int damageMin = 10;
	public int damageMax = 10;
	public int accuracy = 75;

	public MultiZapAction()
	{
		Reset();
	}

	public override System.Tuple<int, int> Damage()
	{
		return new System.Tuple<int, int>(damage, damage);
	}
	public override int Accuracy()
	{
		return (int)accuracy;
	}

	public override AttackType DamageType()
	{
		return AttackType.Electric;
	}
	protected override void ResetImpl()
	{
		name = "Multizap";

		Charges = -1;
		CooldownTime = 1 + Tweakables.GetSingle("boss3_zap_cooldown", 1);

		projectile = GameObject.Find("EffectPrefabRoot").transform.Find("Boss3SlashEffect").gameObject;
		stunnedMsg = GameObject.Find("EffectPrefabRoot").transform.Find("StunnedMsg").gameObject;
	}

	public override string Description()
	{
		return "Inflige " + damage + "pt de d�g�ts � tous les ennemis.";
	}

	protected override void UseImpl()
	{
		GameObject.Find("GlobalScripts").GetComponent<CoroutineUtil>().StartCoroutine(SlashAnimation());
	}

	IEnumerator SlashAnimation()
	{
		Vector3 basePos = effector.obj.gameObject.transform.position;
		LeanTween.cancel(effector.obj.gameObject);

		Vector3 direction = (target.obj.transform.position - effector.obj.transform.position).normalized;
		// Go near the target, but not right on top of it
		// Move to
		LeanTween.move(effector.obj.gameObject, target.obj.transform.position - 1.0f * direction, 0.75f).setEaseInOutQuad();
		yield return new WaitUntil(() => !LeanTween.isTweening(effector.obj.gameObject));

		ActorAnimator().SetTrigger("Slash");
		effector.obj.GetComponent<AudioSource>().PlayOneShot(GameObject.FindObjectOfType<SoundFXList>().slash);
		yield return new WaitForSeconds(0.1f);

		int actualDamage = damage;
		if (useRange)
			actualDamage = Random.Range(damageMin, damageMax);

		// Roll for each opponent and see if we affect them
		List<Actor> listCopy = new List<Actor>(effector.oppositeParty); // Make a copy of the list object (not its content) to iterate over it, as DamageActor might delete actors which might break the iteration
		foreach (var actor in listCopy)
		{
			bool hit = GameObject.FindObjectOfType<CombatManager>().AttackActor(effector, actor, actualDamage, accuracy + effector.EffectiveAccuracyModifier(), AttackType.Electric); // 0 Damage, just see if we hit them or not
			if (hit)
			{
				GameObject.Instantiate(projectile, actor.obj.transform).SetActive(true); // Spawn a slash effect on top of them
				int roll = Random.Range(0, 100);
				if (roll <= Tweakables.GetSingle("boss3_zap_stun_chance", 33))
				{
					GameObject.Instantiate(stunnedMsg, actor.obj.transform).SetActive(true);
					actor.AddEffect(new StunnedEffect(1));
					actor.Actions = 0; // Stun
				}
			}
		}
		yield return new WaitForSeconds(1);

		// Move back
		LeanTween.move(effector.obj.gameObject, basePos, 0.75f).setEaseInOutQuad();
		yield return new WaitUntil(() => !LeanTween.isTweening(effector.obj.gameObject));

		yield return new WaitForSeconds(0.5f);

		ActionOver();
	}
}
