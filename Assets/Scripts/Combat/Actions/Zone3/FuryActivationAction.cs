using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using System.Linq;

public class FuryActivationAction : Action
{
	public FuryActivationAction()
	{
		Reset();
	}

	protected override void ResetImpl()
	{
		name = "Furie";

		Charges = -1;
		CooldownTime = 1 + Tweakables.GetSingle("fury_action_cooldown", 3);
	}

	public override AttackType DamageType()
	{
		return AttackType.Harmless;
	}

	public override System.Tuple<int, int> Damage()
	{
		return new System.Tuple<int, int>(0, 0);
	}
	public override int Accuracy()
	{
		return 0;
	}

	public override string Description()
	{
		return "";
	}

	public override bool CanUseImpl()
	{
		return GameObject.FindObjectOfType<CombatManager>().TurnNumber() >= 3; // Cannot be used for the first two turns
	}

	protected override void UseImpl()
	{
		GameObject.Find("GlobalScripts").GetComponent<CoroutineUtil>().StartCoroutine(FuryAnimation());
	}

	IEnumerator FuryAnimation()
	{
		if (ActorAnimator().parameters.Any(x => x.name == "Fury"))
		{
			ActorAnimator().SetTrigger("Fury");
			yield return new WaitForSeconds(0.3f);
		}

		GameObject.Instantiate(GameObject.Find("EffectPrefabRoot").transform.Find("FuryMsg").gameObject, effector.obj.transform).SetActive(true);
		effector.AddEffect(new Boss3FuryEffect());

		yield return new WaitForSeconds(1.5f);

		ActionOver();
	}
}
