using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using System.Linq;

[ActionData("Taillaide", "Inflige 30 pt de d�g�ts. " +
			"Inflige l��tat 'Saignement' pendant deux tours (non cumulable).")]
public class BleedingSlashAction : Action
{
	GameObject projectile;

	[ActionMainStat("D�g�ts d'attaque", 30, +6)]
	public int damage = 30;
	[ActionMainStat("D�g�ts de saignement", 10, +5)]
	int bleedDamage = 10;

	public int accuracy = 80;

	public bool useRange = false;
	public int damageLo = 10;
	public int damageHi = 15;
	public int bleedTurns = 2;

	public int bleedingChance = 100;
	public BleedingSlashAction()
	{
		Reset();
	}

	public override AttackType DamageType()
	{
		return AttackType.Bleeding;
	}

	public override System.Tuple<int, int> Damage()
	{
		return new System.Tuple<int, int>(damage, damage);
	}
	public override int Accuracy()
	{
		return (int)accuracy;
	}
	protected override void ResetImpl()
	{
		name = "Taillaide";

		Charges = -1;
		CooldownTime = 1 + Tweakables.GetSingle("taillade_cooldown", 0);

		projectile = GameObject.Find("EffectPrefabRoot").transform.Find("BleedSlashLinear").gameObject;
	}

	public override string Description()
	{
		return "Inflige " + damage + " pt de d�g�ts. " +
			"Inflige l��tat 'Saignement' pendant deux tours (non cumulable).";
	}

	protected override void UseImpl()
	{
		GameObject.Find("GlobalScripts").GetComponent<CoroutineUtil>().StartCoroutine(SlashAnimation());
	}

	IEnumerator SlashAnimation()
	{
		var monitor = GameObject.FindObjectOfType<SceneMouseHoverMonitor>();
		bool UIPickedTarget = false;
		if (target == null) // Ask the user for a target using the UI
		{
			UIPickedTarget = true;
			GameObject.FindObjectOfType<SceneMouseHoverMonitor>().TargetSelectionMode("Enemy");

			// wait until a target has been picked
			yield return new WaitUntil(() => monitor.SelectedTarget() != null);
			target = monitor.SelectedTarget().GetComponent<ActorSpriteUpdater>().actor;
		}

		Vector3 basePos = effector.obj.gameObject.transform.position;
		LeanTween.cancel(effector.obj.gameObject);

		Vector3 direction = (target.obj.transform.position - effector.obj.transform.position).normalized;
		// Go near the target, but not right on top of it
		// Move to
		LeanTween.move(effector.obj.gameObject, target.obj.transform.position - 1.0f*direction, 0.75f).setEaseInOutQuad();
		yield return new WaitUntil(() => !LeanTween.isTweening(effector.obj.gameObject));

		// Attack
		if (ActorAnimator().parameters.Any(x => x.name == "Slash"))
		{
			ActorAnimator().SetTrigger("Slash");
			yield return new WaitForSeconds(0.4f);
		}

		int actualDamage = damage;
		if (useRange)
			actualDamage = Random.Range(damageLo, damageHi);

		bool hit = GameObject.FindObjectOfType<CombatManager>().AttackActor(effector, target, actualDamage, accuracy + effector.EffectiveAccuracyModifier(), AttackType.Bleeding);
		if (hit)
		{
			effector.obj.GetComponent<AudioSource>().PlayOneShot(GameObject.FindObjectOfType<SoundFXList>().slash);
			GameObject.Instantiate(projectile, target.obj.transform.position, Quaternion.identity).SetActive(true);
		}
		else
		{
			effector.obj.GetComponent<AudioSource>().PlayOneShot(GameObject.FindObjectOfType<SoundFXList>().swoosh);
		}

		if (hit)
		{
			int roll = Random.Range(0, 100);
			if (roll <= bleedingChance)
				target.AddEffect(new BleedingEffect(bleedDamage, bleedTurns)); // Bleeding effect
		}

		yield return new WaitForSeconds(1);

		// Move back
		LeanTween.move(effector.obj.gameObject, basePos, 0.75f).setEaseInOutQuad();
		yield return new WaitUntil(() => !LeanTween.isTweening(effector.obj.gameObject));

		yield return new WaitForSeconds(0.5f);
		if (UIPickedTarget)
			monitor.ExitTargetMode();
		ActionOver();
	}
}
