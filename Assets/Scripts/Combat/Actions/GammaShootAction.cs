using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using System.Linq;

[ActionData("Pistolet Gamma", "Inflige 17-22 pt de d�gats.\nIrradie la cible (15 de d�gats/tour) pendant 2 tours.")]
public class GammaShootAction : Action
{
	GameObject projectile;

	public float accuracy = 100;

	[ActionMainStat("D�g�ts d'attaque", 17, +5)]
	public int hitDamage = 17;
	[ActionMainStat("D�g�ts radioactifs", 15, +5)]
	public int radDamage = 15;

	public GammaShootAction()
	{
		Reset();
	}

	public override AttackType DamageType()
	{
		return AttackType.Radioactive;
	}
	public override System.Tuple<int, int> Damage()
	{
		return new System.Tuple<int, int>(hitDamage, hitDamage + 5);
	}
	public override int Accuracy()
	{
		return (int)accuracy;
	}

	protected override void ResetImpl()
	{
		name = "Pistolet Gamma";

		Charges = -1;
		CooldownTime = 0 + Tweakables.GetSingle("gamma_cooldown", 0);

		projectile = GameObject.Find("EffectPrefabRoot").transform.Find("ProjectilePrefab").gameObject;
	}

	public override string Description()
	{
		return "Inflige " + hitDamage + "-" + (hitDamage + 5) + "pt de d�gats.\nIrradie la cible (" + radDamage + " de d�gats/tour) pendant 2 tours.";
	}

	protected override void UseImpl()
	{
		GameObject.Find("GlobalScripts").GetComponent<CoroutineUtil>().StartCoroutine(ShootAnimation());
	}

	IEnumerator ShootAnimation()
	{
		var monitor = GameObject.FindObjectOfType<SceneMouseHoverMonitor>();
		bool UIPickedTarget = false;
		if (target == null) // Ask the user for a target using the UI
		{
			UIPickedTarget = true;
			GameObject.FindObjectOfType<SceneMouseHoverMonitor>().TargetSelectionMode("Enemy");

			// wait until a target has been picked
			yield return new WaitUntil(() => monitor.SelectedTarget() != null);
			target = monitor.SelectedTarget().GetComponent<ActorSpriteUpdater>().actor;
		}

		//GameObject.Instantiate(projectile).SetActive(true);
		if (ActorAnimator().parameters.Any(x => x.name == "Shoot"))
		{
			ActorAnimator().SetTrigger("Shoot");
			yield return new WaitForSeconds(0.4f);
		}
		else
			GameObject.Instantiate(projectile).SetActive(true);
		effector.obj.GetComponent<AudioSource>().PlayOneShot(GameObject.FindObjectOfType<SoundFXList>().plasma);
		yield return new WaitForSeconds(0.4f);


		Assert.IsNotNull(target);
		bool hit = GameObject.FindObjectOfType<CombatManager>().AttackActor(effector, target, hitDamage + Random.Range(0, 6), accuracy + effector.EffectiveAccuracyModifier(),
			AttackType.Radioactive);
		double roll = Random.Range(0, 100);
		if (hit && roll <= 100) // 100% to inflict radiation effect
		{
			GameObject.Instantiate(GameObject.Find("EffectPrefabRoot").transform.Find("RadMsg").gameObject, target.obj.transform).SetActive(true);
			target.AddEffect(new RadioactiveEffect(radDamage)); // 15 damage radiation
		}

		yield return new WaitForSeconds(2);
		if (UIPickedTarget)
			monitor.ExitTargetMode();
		ActionOver();
	}
}
