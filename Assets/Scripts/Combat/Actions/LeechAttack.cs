using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using System.Linq;

public class LeechAttackAction : Action
{
	GameObject projectile;

	public int accuracy = 90;

	public int damage = 20;

	public LeechAttackAction(Actor effector)
	{
		this.effector = effector;
		Reset();
	}

	public override AttackType DamageType()
	{
		return AttackType.Bleeding;
	}

	public override System.Tuple<int, int> Damage()
	{
		return new System.Tuple<int, int>(damage, damage);
	}
	public override int Accuracy()
	{
		return (int)accuracy;
	}

	public override bool CanUseImpl()
	{
		return effector.HP <= Tweakables.GetSingle("mutant_piqure_hp_threshold", 50);
	}

	protected override void ResetImpl()
	{
		name = "Taillaide";

		Charges = -1;
		CooldownTime = 1 + Tweakables.GetSingle("mutant_piqure_cooldown", 0);

		projectile = GameObject.Find("EffectPrefabRoot").transform.Find("BleedSlashLinear").gameObject;
	}

	public override string Description()
	{
		return "";
	}

	protected override void UseImpl()
	{
		GameObject.Find("GlobalScripts").GetComponent<CoroutineUtil>().StartCoroutine(SlashAnimation());
	}

	IEnumerator SlashAnimation()
	{
		var monitor = GameObject.FindObjectOfType<SceneMouseHoverMonitor>();
		bool UIPickedTarget = false;
		if (target == null) // Ask the user for a target using the UI
		{
			UIPickedTarget = true;
			GameObject.FindObjectOfType<SceneMouseHoverMonitor>().TargetSelectionMode("Enemy");

			// wait until a target has been picked
			yield return new WaitUntil(() => monitor.SelectedTarget() != null);
			target = monitor.SelectedTarget().GetComponent<ActorSpriteUpdater>().actor;
		}

		Vector3 basePos = effector.obj.gameObject.transform.position;
		LeanTween.cancel(effector.obj.gameObject);

		Vector3 direction = (target.obj.transform.position - effector.obj.transform.position).normalized;
		// Go near the target, but not right on top of it
		// Move to
		LeanTween.move(effector.obj.gameObject, target.obj.transform.position - 1.0f*direction, 0.75f).setEaseInOutQuad();
		yield return new WaitUntil(() => !LeanTween.isTweening(effector.obj.gameObject));

		// Attack
		if (ActorAnimator().parameters.Any(x => x.name == "Sting"))
		{
			ActorAnimator().SetTrigger("Sting");
			yield return new WaitForSeconds(0.1f);
		}

		bool hit = GameObject.FindObjectOfType<CombatManager>().AttackActor(effector, target, damage, accuracy + effector.EffectiveAccuracyModifier(), AttackType.Bleeding);
		if (hit) // sluuurp
		{
			effector.obj.GetComponent<AudioSource>().PlayOneShot(GameObject.FindObjectOfType<SoundFXList>().slash);
			GameObject.Instantiate(projectile, target.obj.transform.position, Quaternion.identity).SetActive(true);
			GameObject.FindObjectOfType<CombatManager>().HealActor(effector, effector, Random.Range(50, 65));
			target.AddEffect(new CritModEffect(-10));
		}
		else
		{
			effector.obj.GetComponent<AudioSource>().PlayOneShot(GameObject.FindObjectOfType<SoundFXList>().swoosh);
		}

		yield return new WaitForSeconds(1);

		// Move back
		LeanTween.move(effector.obj.gameObject, basePos, 0.75f).setEaseInOutQuad();
		yield return new WaitUntil(() => !LeanTween.isTweening(effector.obj.gameObject));

		yield return new WaitForSeconds(0.5f);
		if (UIPickedTarget)
			monitor.ExitTargetMode();
		ActionOver();
	}
}
