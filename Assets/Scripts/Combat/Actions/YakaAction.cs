using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

[ActionData("Fl�che Yaka", "Lance une fl�che autoguid�e qui inflige 20-30 d�g�ts � tous les ennemis, avec une chance de saignement.")]
public class YakaAction : Action
{
	GameObject projectile;

	[ActionMainStat("D�g�ts minimum", 20, +5)]
	public int damage = 20;

	public int accuracy = Tweakables.GetSingle("yaka_accuracy", 87);

	public YakaAction()
	{
		Reset();
	}

	public override AttackType DamageType()
	{
		return AttackType.Melee;
	}
	public override System.Tuple<int, int> Damage()
	{
		return new System.Tuple<int, int>(damage, damage + 10);
	}
	public override int Accuracy()
	{
		return (int)accuracy;
	}
	protected override void ResetImpl()
	{
		name = "Fl�che Yaka";

		Charges = -1;
		CooldownTime = 1 + Tweakables.GetSingle("yaka_cooldown", 1);

		projectile = GameObject.Find("EffectPrefabRoot").transform.Find("ProjectilePrefab").gameObject;
	}

	public override string Description()
	{
		return "Lance une fl�che autoguid�e qui inflige " + damage + "-" + (damage+10) + " d�g�ts � tous les ennemis, avec une chance de saignement.";
	}

	protected override void UseImpl()
	{
		GameObject.Find("GlobalScripts").GetComponent<CoroutineUtil>().StartCoroutine(ShootAnimation());
	}

	IEnumerator ShootAnimation()
	{
		ActorAnimator().SetTrigger("Yaka");
		yield return new WaitForSeconds(0.1f);
		effector.obj.GetComponent<AudioSource>().PlayOneShot(GameObject.FindObjectOfType<SoundFXList>().slash);
		yield return new WaitForSeconds(0.1f);
		effector.obj.GetComponent<AudioSource>().PlayOneShot(GameObject.FindObjectOfType<SoundFXList>().slash);
		yield return new WaitForSeconds(0.1f);
		effector.obj.GetComponent<AudioSource>().PlayOneShot(GameObject.FindObjectOfType<SoundFXList>().slash);
		yield return new WaitForSeconds(0.1f);

		int actualDamage = damage + Random.Range(0, 11);

		// Roll for each opponent and see if we affect them
		List<Actor> listCopy = new List<Actor>(effector.oppositeParty); // Make a copy of the list object (not its content) to iterate over it, as DamageActor might delete actors which might break the iteration
		foreach (var actor in listCopy)
		{
			bool hit = GameObject.FindObjectOfType<CombatManager>().AttackActor(effector, actor, actualDamage, accuracy + effector.EffectiveAccuracyModifier(), AttackType.Melee);
			int roll = Random.Range(0, 100);
			if (hit && roll <= 50)
				actor.AddEffect(new BleedingEffect(15));
		}

		yield return new WaitForSeconds(2);

		ActionOver();
	}
}
