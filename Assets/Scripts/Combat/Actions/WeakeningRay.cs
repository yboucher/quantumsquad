using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

[ActionData("Particules affaiblissantes", "Affaiblit un ennemi pendant 1 tour, lui faisant subir 50% de d�g�ts en plus.")]
public class WeakeningRayAction : Action
{
	GameObject projectile;
	[ActionMainStat("Pourcentage d'affaiblissement", 50, +10)]
	int weakenPercentage = 50;

	public WeakeningRayAction()
	{
		Reset();
	}

	public override AttackType DamageType()
	{
		return AttackType.Harmless;
	}

	public override System.Tuple<int, int> Damage()
	{
		return new System.Tuple<int, int>(0, 0);
	}
	public override int Accuracy()
	{
		return 90;
	}
	protected override void ResetImpl()
	{
		name = "Particules affaiblissantes";

		Charges = -1;
		CooldownTime = 1 + Tweakables.GetSingle("particules_affaiblissantes_cooldown", 1);

		projectile = GameObject.Find("EffectPrefabRoot").transform.Find("ProjectilePrefab").gameObject;
	}

	public override string Description()
	{
		return "Affaiblit un ennemi pendant 1 tour, lui faisant subir " + weakenPercentage + "% de d�g�ts en plus.";
	}

	protected override void UseImpl()
	{
		GameObject.Find("GlobalScripts").GetComponent<CoroutineUtil>().StartCoroutine(ShootAnimation());
	}

	IEnumerator ShootAnimation()
	{
		var monitor = GameObject.FindObjectOfType<SceneMouseHoverMonitor>();
		bool UIPickedTarget = false;
		if (target == null) // Ask the user for a target using the UI
		{
			UIPickedTarget = true;
			GameObject.FindObjectOfType<SceneMouseHoverMonitor>().TargetSelectionMode("Enemy");

			// wait until a target has been picked
			yield return new WaitUntil(() => monitor.SelectedTarget() != null);
			target = monitor.SelectedTarget().GetComponent<ActorSpriteUpdater>().actor;
		}

		//GameObject.Instantiate(projectile).SetActive(true);
		ActorAnimator().SetTrigger("Spray");
		effector.obj.GetComponent<AudioSource>().PlayOneShot(GameObject.FindObjectOfType<SoundFXList>().spray);
		yield return new WaitForSeconds(0.5f);

		Assert.IsNotNull(target);
		bool hit = GameObject.FindObjectOfType<CombatManager>().AttackActor(effector, target, 0, 90 + effector.EffectiveAccuracyModifier(),
			AttackType.Harmless);
		if (hit)
			target.AddEffect(new WeakeningEffect((weakenPercentage + 100) / 100.0f));

		yield return new WaitForSeconds(2);
		if (UIPickedTarget)
			monitor.ExitTargetMode();
		ActionOver();
	}
}
