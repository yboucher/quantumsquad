using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using System.Linq;

public class FireShootAction : Action
{
	GameObject projectile;

	public float accuracy = 100;

	[ActionMainStat("D�g�ts", 40, +5)]
	public int damage = 40;

	public bool useRange = false;
	public int damageLo = 10;
	public int damageHi = 15;

	public FireShootAction()
	{
		Reset();
	}

	public override System.Tuple<int, int> Damage()
	{
		return new System.Tuple<int, int>(damage, damage);
	}
	public override int Accuracy()
	{
		return (int)accuracy;
	}

	public override AttackType DamageType()
	{
		return AttackType.Fire;
	}
	protected override void ResetImpl()
	{
		name = "Tir de feu";

		Charges = -1;
		CooldownTime = 1 + Tweakables.GetSingle("fire_shoot_cooldown", 2);

		projectile = GameObject.Find("EffectPrefabRoot").transform.Find("ProjectilePrefab").gameObject;
	}

	public override string Description()
	{
		return "Inflige " + damage + "pts de d�gats de feu.";
	}

	protected override void UseImpl()
	{
		GameObject.Find("GlobalScripts").GetComponent<CoroutineUtil>().StartCoroutine(ShootAnimation());
	}

	IEnumerator ShootAnimation()
	{
		if (effector.movedForward)
		{
			// Move back
			LeanTween.move(effector.obj.gameObject, effector.basePos, 0.75f).setEaseInOutQuad();
			yield return new WaitUntil(() => !LeanTween.isTweening(effector.obj.gameObject));
			effector.movedForward = false;
		}

		var monitor = GameObject.FindObjectOfType<SceneMouseHoverMonitor>();
		bool UIPickedTarget = false;
		if (target == null) // Ask the user for a target using the UI
		{
			UIPickedTarget = true;
			GameObject.FindObjectOfType<SceneMouseHoverMonitor>().TargetSelectionMode("Enemy");

			// wait until a target has been picked
			yield return new WaitUntil(() => monitor.SelectedTarget() != null);
			target = monitor.SelectedTarget().GetComponent<ActorSpriteUpdater>().actor;
		}

		effector.obj.GetComponent<AudioSource>().PlayOneShot(GameObject.FindObjectOfType<SoundFXList>().plasma);
		if (ActorAnimator().parameters.Any(x => x.name == "FireShoot"))
		{
			ActorAnimator().SetTrigger("FireShoot");
			yield return new WaitForSeconds(0.6f);
		}
		else
			GameObject.Instantiate(projectile).SetActive(true);

		int actualDamage = damage;
		if (useRange)
			actualDamage = Random.Range(damageLo, damageHi);

		Assert.IsNotNull(target);
		GameObject.FindObjectOfType<CombatManager>().AttackActor(effector, target, actualDamage, accuracy + effector.EffectiveAccuracyModifier(),
			AttackType.Fire);

		yield return new WaitForSeconds(2);
		if (UIPickedTarget)
			monitor.ExitTargetMode();
		ActionOver();
	}
}
