using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using System.Linq;

[ActionData("Rafale", "Inflige 40 pts de d�gats plasma.")]
public class ShootAction : Action
{
	GameObject projectile;

	public float accuracy = 100;

	[ActionMainStat("D�g�ts", 40, +8)]
	public int damage = 40;

	public bool useRange = false;
	public int damageLo = 10;
	public int damageHi = 15;

	public ShootAction()
	{
		Reset();
	}

	public override AttackType DamageType()
	{
		return AttackType.Ballistic;
	}
	public override System.Tuple<int, int> Damage()
	{
		return new System.Tuple<int, int>(damage, damage);
	}
	public override int Accuracy()
	{
		return (int)accuracy;
	}

	protected override void ResetImpl()
	{
		name = "Rafale";

		Charges = -1;
		CooldownTime = 0 + Tweakables.GetSingle("tirer_cooldown", 0);

		projectile = GameObject.Find("EffectPrefabRoot").transform.Find("ProjectilePrefab").gameObject;
	}

	public override string Description()
	{
		return "Inflige " + damage + "pts de d�gats plasma.";
	}

	protected override void UseImpl()
	{
		Debug.Log("Shooting action called : " + name);
		GameObject.Find("GlobalScripts").GetComponent<CoroutineUtil>().StartCoroutine(ShootAnimation());
	}

	IEnumerator ShootAnimation()
	{
		if (effector.movedForward)
		{
			// Move back
			LeanTween.move(effector.obj.gameObject, effector.basePos, 0.75f).setEaseInOutQuad();
			yield return new WaitUntil(() => !LeanTween.isTweening(effector.obj.gameObject));
			effector.movedForward = false;
		}

		var monitor = GameObject.FindObjectOfType<SceneMouseHoverMonitor>();
		bool UIPickedTarget = false;
		if (target == null) // Ask the user for a target using the UI
		{
			UIPickedTarget = true;
			GameObject.FindObjectOfType<SceneMouseHoverMonitor>().TargetSelectionMode("Enemy");

			// wait until a target has been picked
			yield return new WaitUntil(() => monitor.SelectedTarget() != null);
			target = monitor.SelectedTarget().GetComponent<ActorSpriteUpdater>().actor;
		}

		effector.obj.GetComponent<AudioSource>().PlayOneShot(GameObject.FindObjectOfType<SoundFXList>().plasma);
		if (ActorAnimator().parameters.Any(x => x.name == "Shoot"))
		{
			ActorAnimator().SetTrigger("Shoot");
			yield return new WaitForSeconds(0.4f);
		}
		else
			GameObject.Instantiate(projectile).SetActive(true);

		int actualDamage = damage;
		if (useRange)
			actualDamage = Random.Range(damageLo, damageHi);

		Assert.IsNotNull(target);
		GameObject.FindObjectOfType<CombatManager>().AttackActor(effector, target, actualDamage, accuracy + effector.EffectiveAccuracyModifier(),
			AttackType.Ballistic);

		yield return new WaitForSeconds(2);
		if (UIPickedTarget)
			monitor.ExitTargetMode();
		ActionOver();
	}
}
