using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

public class UseItemAction  : Action
{
	ItemButton[] itemButtons;
	List<ItemPair> itemsCounts;
	public UseItemAction()
	{
		Reset();
	}

	protected override void ResetImpl()
	{
		name = "Inventaire";

		ActionCost = 0;
	}

	public override AttackType DamageType()
	{
		return AttackType.Harmless;
	}
	public override System.Tuple<int, int> Damage()
	{
		return new System.Tuple<int, int>(0, 0);
	}
	public override int Accuracy()
	{
		return (int)0;
	}
	class ItemPair
	{
		public ItemPair(Item item, int count)
		{
			this.item = item;
			this.count = count;
		}
		public Item item;
		public int count = 0;
	}

	void SetupItemButtons()
	{
		Assert.IsNotNull(effector);
		Assert.IsNotNull(effector.Items);
		Assert.IsNotNull(itemButtons);

		for (int i = 0; i < itemButtons.Length - 1; ++i)
		{
			itemButtons[i].gameObject.SetActive(false);
		}

		// TODO : handle the case with multiple instances of the same item (use SetCount) !!


		itemsCounts = new List<ItemPair>();

		foreach (var item in effector.Items)
		{
			if (item.Usage() != ItemUsage.DuringCombat)
				continue;
			// Already exists in the dicitonary
			var existingItem = itemsCounts.Find(x => x.item.GetType() == item.GetType());
			if (existingItem != null)
				existingItem.count++; // Increase count
			else
				itemsCounts.Add(new ItemPair(item, 1));
		}

		for (int i = 0; i < Mathf.Min(itemsCounts.Count, itemButtons.Length - 1); ++i)
		{
			itemButtons[i].gameObject.SetActive(true);
			itemButtons[i].SetInteractable(true);
			itemsCounts[i].item.action = this;
			itemButtons[i].SetItem(itemsCounts[i].item, itemsCounts[i].count);
		}

		itemButtons[itemButtons.Length - 1].gameObject.SetActive(true);
		itemButtons[itemButtons.Length - 1].SetInteractable(true);
		var dummy = new DummyItem(); dummy.action = this;
		itemButtons[itemButtons.Length - 1].SetItem(dummy, 1); // Bouton Retour
		itemButtons[itemButtons.Length - 1].GetComponent<Button>().Select();
	}
	public override string Description()
	{
		return "Ouvre l'inventaire.";
	}

	public void ItemUsed()
	{
		// Purge used items
		/*
		foreach (var x in effector.Items.FindAll(x => x.used))
		{
			x.used = false;
			var pair = itemsCounts.Find(i => x == i.item);
			Assert.IsNotNull(pair);
			--pair.count;
			Debug.Log("New count : " + pair.count);
			if (pair.count <= 0)
				x.used = true;
		}*/
		effector.Items.Remove(effector.Items.Find(x => x.used));
		foreach (var item in effector.Items.FindAll(x => x.used))
			item.used = item.animationActive = false;

		effector.Actions--; // Eat the action cost
		GameObject.FindObjectOfType<CombatManager>().actionButtonsRoot.SetActive(true);
		GameObject.FindObjectOfType<CombatManager>().itemButtonsRoot.SetActive(false);
		ActionOver();
	}

	public void Cancel()
	{
		Debug.Log("Inventory cancelled");

		GameObject.FindObjectOfType<CombatManager>().actionButtonsRoot.SetActive(true);
		GameObject.FindObjectOfType<CombatManager>().itemButtonsRoot.SetActive(false); // Show the inventory menu instead
		ActionOver();
	}

	protected override void UseImpl()
	{
		itemButtons = GameObject.FindObjectOfType<CombatManager>().itemButtons;
		Assert.IsNotNull(itemButtons);

		GameObject.FindObjectOfType<CombatManager>().actionButtonsRoot.SetActive(false);
		GameObject.FindObjectOfType<CombatManager>().itemButtonsRoot.SetActive(true); // Show the inventory menu instead

		SetupItemButtons();

		Debug.Log("Open inventory");
	}
}
