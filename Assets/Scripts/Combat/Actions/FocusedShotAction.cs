using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

[ActionData("Tir group�", "Effectue des tirs successifs sur 1 ennemitant que le tir touche la cible.\n5 pt de d�gats par coup.\nA chaque tir cons�cutif, la pr�cision est r�duite de 5%.")]
public class FocusedShotAction : Action
{
	GameObject projectile;

	public float baseAccuracy = 100;
	public float shotPenality = 4; // 4% less chance for each shot

	[ActionMainStat("D�g�ts", 7, +2)]
	int damage = 5;

	int consecutiveShots = 0;

	public FocusedShotAction()
	{
		Reset();
	}

	public override AttackType DamageType()
	{
		return AttackType.Ballistic;
	}
	public override System.Tuple<int, int> Damage()
	{
		return new System.Tuple<int, int>(damage, damage);
	}
	public override int Accuracy()
	{
		return (int)(baseAccuracy - shotPenality * consecutiveShots);
	}

	protected override void ResetImpl()
	{
		name = "Tir group�";

		Charges = -1;
		CooldownTime = 1 + Tweakables.GetSingle("tir_groupe_cooldown", 0);

		projectile = GameObject.Find("EffectPrefabRoot").transform.Find("ProjectilePrefab").gameObject;
	}

	public override string Description()
	{
		return "Effectue des tirs successifs sur 1 ennemi tant que le tir touche la cible.\nA chaque tir cons�cutif, la pr�cision est r�duite de 5%.";
	}

	protected override void UseImpl()
	{
		GameObject.Find("GlobalScripts").GetComponent<CoroutineUtil>().StartCoroutine(ShootAnimation());
	}

	protected override void EndOfTurnImpl()
	{
		consecutiveShots = 0;
	}

	IEnumerator ShootAnimation()
	{
		var monitor = GameObject.FindObjectOfType<SceneMouseHoverMonitor>();
		bool UIPickedTarget = false;
		if (target == null) // Ask the user for a target using the UI
		{
			UIPickedTarget = true;
			GameObject.FindObjectOfType<SceneMouseHoverMonitor>().TargetSelectionMode("Enemy");

			// wait until a target has been picked
			yield return new WaitUntil(() => monitor.SelectedTarget() != null);
			target = monitor.SelectedTarget().GetComponent<ActorSpriteUpdater>().actor;
		}

		//GameObject.Instantiate(projectile).SetActive(true);
		ActorAnimator().SetTrigger("Shoot");
		effector.obj.GetComponent<AudioSource>().PlayOneShot(GameObject.FindObjectOfType<SoundFXList>().plasma);
		yield return new WaitForSeconds(0.4f);

		Assert.IsNotNull(target);
		bool hit = GameObject.FindObjectOfType<CombatManager>().AttackActor(effector, target, damage, baseAccuracy - shotPenality*consecutiveShots + effector.EffectiveAccuracyModifier(), AttackType.Ballistic);

		yield return new WaitForSeconds(2);
		if (UIPickedTarget)
			monitor.ExitTargetMode();

		if (hit)
		{
			effector.Actions++;
			CooldownRemaining = 0; // Can reuse immediately
			++consecutiveShots;
		}
		else
			consecutiveShots = 0;

		ActionOver();
	}
}
