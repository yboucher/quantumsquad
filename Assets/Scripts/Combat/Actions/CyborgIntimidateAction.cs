using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using System.Linq;

[ActionData("Intimidation", "Intimide un ennemi pendant un tour, r�duisant ses d�g�ts de 30%.")]
public class CyborgIntimidateAction : Action
{
	GameObject projectile;

	[ActionMainStat("R�duction", 30, +6)]
	public int debuff = 30;

	public int accuracy = 85;

	public CyborgIntimidateAction()
	{
		Reset();
	}

	public override AttackType DamageType()
	{
		return AttackType.Harmless;
	}

	public override System.Tuple<int, int> Damage()
	{
		return new System.Tuple<int, int>(0, 0);
	}
	public override int Accuracy()
	{
		return (int)accuracy;
	}
	protected override void ResetImpl()
	{
		name = "Intimidation";

		Charges = -1;
		CooldownTime = 1 + Tweakables.GetSingle("intimidate_cooldown", 1);

		projectile = GameObject.Find("EffectPrefabRoot").transform.Find("ProjectilePrefab").gameObject;
	}

	public override string Description()
	{
		return "Intimide un ennemi pendant un tour, r�duisant ses d�g�ts de " + debuff + "%.";
	}

	protected override void UseImpl()
	{
		GameObject.Find("GlobalScripts").GetComponent<CoroutineUtil>().StartCoroutine(SlashAnimation());
	}

	IEnumerator SlashAnimation()
	{
		var monitor = GameObject.FindObjectOfType<SceneMouseHoverMonitor>();
		bool UIPickedTarget = false;
		if (target == null) // Ask the user for a target using the UI
		{
			UIPickedTarget = true;
			GameObject.FindObjectOfType<SceneMouseHoverMonitor>().TargetSelectionMode("Enemy");

			// wait until a target has been picked
			yield return new WaitUntil(() => monitor.SelectedTarget() != null);
			target = monitor.SelectedTarget().GetComponent<ActorSpriteUpdater>().actor;
		}

		//GameObject.Instantiate(projectile).SetActive(true);
		if (ActorAnimator().parameters.Any(x => x.name == "Intimidate"))
		{
			ActorAnimator().SetTrigger("Intimidate");
			yield return new WaitForSeconds(0.4f);
		}
		else
			GameObject.Instantiate(projectile).SetActive(true);

		Assert.IsNotNull(target);
		bool hit = GameObject.FindObjectOfType<CombatManager>().AttackActor(effector, target, 0, accuracy + effector.EffectiveAccuracyModifier(),
			AttackType.Harmless);
		if (hit)
		{
			GameObject.Instantiate(GameObject.Find("EffectPrefabRoot").transform.Find("IntimidatedMsg").gameObject, target.obj.transform).SetActive(true);
			target.AddEffect(new IntimidatedEffect(debuff));
		}

		yield return new WaitForSeconds(2);
		if (UIPickedTarget)
			monitor.ExitTargetMode();
		ActionOver();
	}
}
