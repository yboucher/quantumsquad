using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using System.Linq;

public class BossArmorAction : Action
{
	GameObject shieldSprite;

	public BossArmorAction()
	{
		Reset();
	}

	public override AttackType DamageType()
	{
		return AttackType.Harmless;
	}

	public override System.Tuple<int, int> Damage()
	{
		return new System.Tuple<int, int>(0, 0);
	}
	public override int Accuracy()
	{
		return 0;
	}

	protected override void ResetImpl()
	{
		name = "Armure";

		Charges = -1;
		CooldownTime = 4;
	}

	public override string Description()
	{
		return "Ajoute un bouclier supplémentaire.";
	}

	protected override void UseImpl()
	{
		shieldSprite = GameObject.Find("EffectPrefabRoot").transform.Find("Boss1ShieldEffect").gameObject;

		GameObject.Find("GlobalScripts").GetComponent<CoroutineUtil>().StartCoroutine(ShieldAnimation());
	}

	IEnumerator ShieldAnimation()
	{
		effector.obj.GetComponent<DamagedShake>().Shake();
		effector.MaxArmor = 150;
		effector.Armor = 150;
		effector.ArmorVulnerableToElements = true;
		
		/*
		if (ActorAnimator().parameters.Any(x => x.name == "Shield"))
		{
			ActorAnimator().SetTrigger("Shield");
			yield return new WaitForSeconds(0.75f);
		}
		*/

		GameObject shield = GameObject.Instantiate(shieldSprite, new Vector3(1, 0, 0), Quaternion.identity, target.obj.transform);
		shield.SetActive(true);
		shield.transform.localPosition = new Vector3(-0.30f, 0, 0);

		yield return new WaitForSeconds(2);

		// Using this attack implies losing his chance at doing crits
		effector.SetBaseCrit(0);

		ActionOver();
	}
}
