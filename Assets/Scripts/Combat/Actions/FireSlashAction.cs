using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using System.Linq;

[ActionData("10�me mouvement", "Attaque de feu, inflige 60 pt de d�g�ts.")]
public class FireSlashAction : Action
{
	GameObject projectile;

	public int accuracy = 95;

	[ActionMainStat("D�g�ts d'attaque", 60, +12)]
	public int damage = 60;
	public FireSlashAction()
	{
		Reset();
	}

	public override AttackType DamageType()
	{
		return AttackType.Fire;
	}

	public override System.Tuple<int, int> Damage()
	{
		return new System.Tuple<int, int>(damage, damage);
	}
	public override int Accuracy()
	{
		return (int)accuracy;
	}
	protected override void ResetImpl()
	{
		name = "10�me mouvement";

		Charges = -1;
		CooldownTime = 1 + Tweakables.GetSingle("fire_slash_cooldown", 1);

		projectile = GameObject.Find("EffectPrefabRoot").transform.Find("FireSlashLinear").gameObject;
	}

	public override string Description()
	{
		return "Attaque de feu, inflige " + damage + " pt de d�g�ts.";
	}

	protected override void UseImpl()
	{
		GameObject.Find("GlobalScripts").GetComponent<CoroutineUtil>().StartCoroutine(SlashAnimation());
	}

	IEnumerator SlashAnimation()
	{
		var monitor = GameObject.FindObjectOfType<SceneMouseHoverMonitor>();
		bool UIPickedTarget = false;
		if (target == null) // Ask the user for a target using the UI
		{
			UIPickedTarget = true;
			GameObject.FindObjectOfType<SceneMouseHoverMonitor>().TargetSelectionMode("Enemy");

			// wait until a target has been picked
			yield return new WaitUntil(() => monitor.SelectedTarget() != null);
			target = monitor.SelectedTarget().GetComponent<ActorSpriteUpdater>().actor;
		}

		Vector3 basePos = effector.obj.gameObject.transform.position;
		LeanTween.cancel(effector.obj.gameObject);

		Vector3 direction = (target.obj.transform.position - effector.obj.transform.position).normalized;
		// Go near the target, but not right on top of it
		// Move to
		LeanTween.move(effector.obj.gameObject, target.obj.transform.position - 1.0f*direction, 0.75f).setEaseInOutQuad();
		yield return new WaitUntil(() => !LeanTween.isTweening(effector.obj.gameObject));

		// Attack
		if (ActorAnimator().parameters.Any(x => x.name == "Slash"))
		{
			ActorAnimator().SetTrigger("Slash");
			yield return new WaitForSeconds(0.4f);
		}
		bool hit = GameObject.FindObjectOfType<CombatManager>().AttackActor(effector, target, damage, accuracy + effector.EffectiveAccuracyModifier(),
			AttackType.Fire);
		if (hit)
		{
			effector.obj.GetComponent<AudioSource>().PlayOneShot(GameObject.FindObjectOfType<SoundFXList>().slash);
			GameObject.Instantiate(projectile, target.obj.transform.position, Quaternion.identity).SetActive(true);
		}
		else
		{
			effector.obj.GetComponent<AudioSource>().PlayOneShot(GameObject.FindObjectOfType<SoundFXList>().swoosh);
		}

		yield return new WaitForSeconds(1);

		// Move back
		LeanTween.move(effector.obj.gameObject, basePos, 0.75f).setEaseInOutQuad();
		yield return new WaitUntil(() => !LeanTween.isTweening(effector.obj.gameObject));

		yield return new WaitForSeconds(0.5f);
		if (UIPickedTarget)
			monitor.ExitTargetMode();
		ActionOver();
	}
}
