using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using System.Linq;

[ActionData("Flash", "Tout membre de l��quipe adverse a 50% de chance d��tre aveugl�.\n(-60% de pr�cision)")]
public class FlashAction : Action
{
	GameObject effect;
	[ActionMainStat("Chance d'aveugler", 50, +10)]
	int hitChance = 50;
	[ActionMainStat("Pr�cision r�duite", 60, +5)]
	int blindness = 60;

	public FlashAction()
	{
		Reset();
	}

	public override AttackType DamageType()
	{
		return AttackType.Harmless;
	}
	public override System.Tuple<int, int> Damage()
	{
		return new System.Tuple<int, int>(0, 0);
	}
	public override int Accuracy()
	{
		return (int)hitChance;
	}
	protected override void ResetImpl()
	{
		name = "Flash";

		Charges = -1;
		CooldownTime = 1 + Tweakables.GetSingle("flash_cooldown", 1);

		effect = GameObject.Find("EffectPrefabRoot").transform.Find("FlashbangPrefab").gameObject;
	}

	public override string Description()
	{
		return "Tout membre de l��quipe adverse a " + hitChance + "% de chance d��tre aveugl�.\n(-" + blindness + "% de pr�cision)";
	}

	protected override void UseImpl()
	{
		//Debug.Log("Grenade action called : " + name);
		GameObject.Find("GlobalScripts").GetComponent<CoroutineUtil>().StartCoroutine(GrenadeAnimation());
	}

	IEnumerator GrenadeAnimation()
	{
		// Target either the center of enemy party or the center of the player party
		var positionTransform = GameObject.Find(effector.Enemy ? "PlayerPartyCenter" : "EnemyPartyCenter").transform;

		effector.obj.GetComponent<AudioSource>().PlayOneShot(GameObject.FindObjectOfType<SoundFXList>().grenadeThrow);
		if (ActorAnimator().parameters.Any(x => x.name == "Throw"))
			ActorAnimator().SetTrigger("Throw");
		yield return new WaitForSeconds(0.9f);

		effector.obj.GetComponent<AudioSource>().PlayOneShot(GameObject.FindObjectOfType<SoundFXList>().grenadeBoom);
		var obj = GameObject.Instantiate(effect, positionTransform);
		obj.SetActive(true);

		Assert.IsNotNull(effector);

		// Roll for each opponent and see if we affect them
		List<Actor> listCopy = new List<Actor>(effector.oppositeParty); // Make a copy of the list object (not its content) to iterate over it, as DamageActor might delete actors which might break the iteration
		foreach (var actor in listCopy)
			if (GameObject.FindObjectOfType<CombatManager>().AttackActor(effector, actor, 0, hitChance + effector.EffectiveAccuracyModifier(), AttackType.Harmless)) // 0 Damage, just see if we hit them or not
			{
				GameObject.Instantiate(GameObject.Find("EffectPrefabRoot").transform.Find("BlindedMsg").gameObject, actor.obj.transform).SetActive(true);
				
				actor.AddEffect(new BlindEffect(blindness)); // Add a blinded status if hit
			}

		yield return new WaitForSeconds(2);
		ActionOver();
	}
}
