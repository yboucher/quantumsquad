using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

[ActionData("Bouclier humain", "Appliqu� � un alli�, le prot�ge et prends 50% de points de d�g�ts � sa place")]
public class HumanShieldAction : Action
{
	[ActionMainStat("Pourcentage absorb�", 50, +5)]
	int HPRedirected = 50;

	GameObject shieldSprite;
	public HumanShieldAction()
	{
		Reset();
	}

	public override AttackType DamageType()
	{
		return AttackType.Harmless;
	}
	public override System.Tuple<int, int> Damage()
	{
		return new System.Tuple<int, int>(0, 0);
	}
	public override int Accuracy()
	{
		return 0;
	}
	protected override void ResetImpl()
	{
		name = "Bouclier humain";

		Charges = -1;
		CooldownTime = 1 + Tweakables.GetSingle("human_shield_cooldown", 1);

		shieldSprite = GameObject.Find("EffectPrefabRoot").transform.Find("MastoShield").gameObject;
	}

	public override string Description()
	{
		return "Appliqu� � un alli�, le prot�ge des attaques et subit " + HPRedirected + " % de points de d�g�ts.";
	}

	protected override void UseImpl()
	{
		GameObject.Find("GlobalScripts").GetComponent<CoroutineUtil>().StartCoroutine(HealAnimation());
	}

	IEnumerator HealAnimation()
	{
		var monitor = GameObject.FindObjectOfType<SceneMouseHoverMonitor>();
		bool UIPickedTarget = false;
		if (target == null) // Ask the user for a target using the UI
		{
			UIPickedTarget = true;
			GameObject.FindObjectOfType<SceneMouseHoverMonitor>().TargetSelectionMode("Soldier");

			// wait until a target has been picked
			yield return new WaitUntil(() => monitor.SelectedTarget() != null);
			target = monitor.SelectedTarget().GetComponent<ActorSpriteUpdater>().actor;
		}

		ActorAnimator().SetTrigger("Shield");
		yield return new WaitForSeconds(1.1f);

		effector.obj.GetComponent<AudioSource>().PlayOneShot(GameObject.FindObjectOfType<SoundFXList>().shieldPowerup);

		Assert.IsNotNull(target);
		target.AddEffect(new DamageRedirectEffect(effector, HPRedirected / 100.0f));
		GameObject.Instantiate(GameObject.Find("EffectPrefabRoot").transform.Find("ProtectMsg").gameObject, target.obj.transform).SetActive(true);
		GameObject shield = GameObject.Instantiate(shieldSprite, new Vector3(1, 0, 0), Quaternion.identity, target.obj.transform);
		shield.SetActive(true);
		shield.transform.localPosition = new Vector3(1.5f, 0, 0);

		if (UIPickedTarget)
			monitor.ExitTargetMode();
		ActionOver();
	}
}
