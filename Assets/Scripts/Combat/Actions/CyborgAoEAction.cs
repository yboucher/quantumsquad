using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

[ActionData("Vague incendiaire", "Lib�re une vague incendiant tout sur son passage.")]
public class CyborgAoEAction : Action
{
	GameObject projectile;

	[ActionMainStat("D�g�ts", 10, +5)]
	public int damage = 10;

	public int accuracy = 90;

	public CyborgAoEAction()
	{
		Reset();
	}

	public override AttackType DamageType()
	{
		return AttackType.Fire;
	}
	public override System.Tuple<int, int> Damage()
	{
		return new System.Tuple<int, int>(damage, damage + 3);
	}
	public override int Accuracy()
	{
		return (int)accuracy;
	}

	protected override void ResetImpl()
	{
		name = "Vague incendiaire";

		Charges = -1;
		CooldownTime = 1 + Tweakables.GetSingle("fire_wave_cooldown", 2);

		projectile = GameObject.Find("EffectPrefabRoot").transform.Find("ProjectilePrefab").gameObject;
	}

	public override string Description()
	{
		return "Lib�re une vague incendiant tout sur son passage.\n" + 
			"Inflige " + damage + "-" + (damage+3) + "pt de d�g�ts � tous les ennemis.";
	}

	protected override void UseImpl()
	{
		Debug.Log("Shooting action called : " + name);
		GameObject.Find("GlobalScripts").GetComponent<CoroutineUtil>().StartCoroutine(ShootAnimation());
	}

	IEnumerator ShootAnimation()
	{
		ActorAnimator().SetTrigger("AoE");
		effector.obj.GetComponent<AudioSource>().PlayOneShot(GameObject.FindObjectOfType<SoundFXList>().fireball);
		yield return new WaitForSeconds(0.4f);

		int actualDamage = damage;
		actualDamage += Random.Range(0, 4);

		// Roll for each opponent and see if we affect them
		List<Actor> listCopy = new List<Actor>(effector.oppositeParty); // Make a copy of the list object (not its content) to iterate over it, as DamageActor might delete actors which might break the iteration
		foreach (var actor in listCopy)
			GameObject.FindObjectOfType<CombatManager>().AttackActor(effector, actor, actualDamage, accuracy + effector.EffectiveAccuracyModifier(), AttackType.Fire); // 0 Damage, just see if we hit them or not

		yield return new WaitForSeconds(2);

		ActionOver();
	}
}
