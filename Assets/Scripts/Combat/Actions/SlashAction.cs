using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using System.Linq;

[ActionData("Slash", "Inflige 40 pts de d�g�ts.")]
public class SlashAction : Action
{
	GameObject projectile;

	[ActionMainStat("D�g�ts", 40, +8)]
	public int damage = 40;

	public int accuracy = 90;
	public SlashAction()
	{
		Reset();
	}

	public override System.Tuple<int, int> Damage()
	{
		return new System.Tuple<int, int>(damage, damage);
	}
	public override int Accuracy()
	{
		return (int)accuracy;
	}

	public override AttackType DamageType()
	{
		return AttackType.Melee;
	}
	protected override void ResetImpl()
	{
		name = "Slash";

		Charges = -1;
		CooldownTime = 0 + Tweakables.GetSingle("slash_cooldown", 0);

		projectile = GameObject.Find("EffectPrefabRoot").transform.Find("BlueSlashLinear").gameObject;
	}

	public override string Description()
	{
		return "Inflige " + damage + " pts de d�g�ts.";
	}

	protected override void UseImpl()
	{
		GameObject.Find("GlobalScripts").GetComponent<CoroutineUtil>().StartCoroutine(SlashAnimation());
	}

	IEnumerator SlashAnimation()
	{
		var monitor = GameObject.FindObjectOfType<SceneMouseHoverMonitor>();
		bool UIPickedTarget = false;
		if (target == null) // Ask the user for a target using the UI
		{
			UIPickedTarget = true;
			GameObject.FindObjectOfType<SceneMouseHoverMonitor>().TargetSelectionMode("Enemy");

			// wait until a target has been picked
			yield return new WaitUntil(() => monitor.SelectedTarget() != null);
			target = monitor.SelectedTarget().GetComponent<ActorSpriteUpdater>().actor;
		}

		Vector3 basePos = effector.obj.gameObject.transform.position;
		LeanTween.cancel(effector.obj.gameObject);

		Vector3 direction = (target.obj.transform.position - effector.obj.transform.position).normalized;
		// Go near the target, but not right on top of it
		// Move to
		LeanTween.move(effector.obj.gameObject, target.obj.transform.position - 1.0f*direction, 0.75f).setEaseInOutQuad();


		yield return new WaitUntil(() => !LeanTween.isTweening(effector.obj.gameObject));

		// Attack
		//effector.obj.GetComponent<AudioSource>().PlayOneShot(GameObject.FindObjectOfType<SoundFXList>().moveSwoosh);
		if (ActorAnimator().parameters.Any(x => x.name == "Slash"))
		{
			ActorAnimator().SetTrigger("Slash");
			yield return new WaitForSeconds(0.4f);
		}
		bool hit = GameObject.FindObjectOfType<CombatManager>().AttackActor(effector, target, damage, accuracy + effector.EffectiveAccuracyModifier(),
																													AttackType.Melee);
		if (hit)
		{
			effector.obj.GetComponent<AudioSource>().PlayOneShot(GameObject.FindObjectOfType<SoundFXList>().slash);
			GameObject.Instantiate(projectile, target.obj.transform.position, Quaternion.identity).SetActive(true);
		}
		else
		{
			effector.obj.GetComponent<AudioSource>().PlayOneShot(GameObject.FindObjectOfType<SoundFXList>().swoosh);
		}

		yield return new WaitForSeconds(1);
		//effector.obj.GetComponent<AudioSource>().PlayOneShot(GameObject.FindObjectOfType<SoundFXList>().moveSwoosh);

		// Move back
		LeanTween.move(effector.obj.gameObject, basePos, 0.75f).setEaseInOutQuad();
		yield return new WaitUntil(() => !LeanTween.isTweening(effector.obj.gameObject));

		yield return new WaitForSeconds(0.5f);
		if (UIPickedTarget)
			monitor.ExitTargetMode();
		ActionOver();
	}
}
