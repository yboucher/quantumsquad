using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using System.Linq;

public class DeathScreamAction : Action
{
	GameObject effect;


	public DeathScreamAction()
	{
		Reset();
	}

	public override AttackType DamageType()
	{
		return AttackType.Harmless;
	}
	public override System.Tuple<int, int> Damage()
	{
		return new System.Tuple<int, int>(0, 0);
	}
	public override int Accuracy()
	{
		return 0;
	}
	protected override void ResetImpl()
	{
		name = "Hurlement";

		Charges = -1;
		CooldownTime = 1 + Tweakables.GetSingle("death_scream_cooldown", 1);

		effect = GameObject.Find("EffectPrefabRoot").transform.Find("FlashbangPrefab").gameObject;
	}

	public override string Description()
	{
		return "Tout membre de l��quipe adverse a 50% de chance d��tre effray�.\n(Pr�cision globale r�duite de 15%)";
	}

	protected override void UseImpl()
	{
		//Debug.Log("Grenade action called : " + name);
		GameObject.Find("GlobalScripts").GetComponent<CoroutineUtil>().StartCoroutine(ScreamAnimation());
	}

	IEnumerator ScreamAnimation()
	{
		effector.obj.GetComponent<DamagedShake>().Shake();
		effector.obj.GetComponent<AudioSource>().PlayOneShot(GameObject.FindObjectOfType<SoundFXList>().demonScream);
		if (ActorAnimator().parameters.Any(x => x.name == "Scream"))
		{
			ActorAnimator().SetTrigger("Scream");
		}

		yield return new WaitForSeconds(3.0f);

		Assert.IsNotNull(effector);

		// Roll for each opponent and see if we affect them
		List<Actor> listCopy = new List<Actor>(effector.oppositeParty); // Make a copy of the list object (not its content) to iterate over it, as DamageActor might delete actors which might break the iteration
		foreach (var actor in listCopy)
			if (GameObject.FindObjectOfType<CombatManager>().AttackActor(effector, actor, 0, 50, AttackType.Harmless)) // 0 Damage, just see if we hit them or not
			{
				GameObject.Instantiate(GameObject.Find("EffectPrefabRoot").transform.Find("ScaredMsg").gameObject, actor.obj.transform).SetActive(true);
				actor.AddEffect(new ScaredEffect(50));
			}

		yield return new WaitForSeconds(2);
		ActionOver();
	}
}
