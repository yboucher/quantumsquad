using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using System.Linq;

public class WarCryAction : Action
{
	[ActionMainStat("Chance de fonctionner", 33, +5)]
	int BuffChance = 33;
	[ActionMainStat("Bonus de critique", 15, +5)]
	int CritBonus = 15;

	public WarCryAction()
	{
		Reset();
	}

	public override AttackType DamageType()
	{
		return AttackType.Harmless;
	}
	public override System.Tuple<int, int> Damage()
	{
		return new System.Tuple<int, int>(0, 0);
	}
	public override int Accuracy()
	{
		return 0;
	}
	protected override void ResetImpl()
	{
		name = "Cri de guerre";

		Charges = -1;
		CooldownTime = 1 + Tweakables.GetSingle("warcry_action_cooldown", 1);
	}

	public override string Description()
	{
		return "";
	}

	protected override void UseImpl()
	{
		GameObject.Find("GlobalScripts").GetComponent<CoroutineUtil>().StartCoroutine(CryAnimation());
	}

	IEnumerator CryAnimation()
	{
		effector.obj.GetComponent<AudioSource>().PlayOneShot(GameObject.FindObjectOfType<SoundFXList>().alienBattlecry);
		if (ActorAnimator().parameters.Any(x => x.name == "Cry"))
		{
			ActorAnimator().SetTrigger("Cry");
			yield return new WaitForSeconds(1.0f);
		}
		else
			yield return new WaitForSeconds(1.5f);

		foreach (var ally in effector.party.FindAll(x => x != effector))
		{
			int roll = Random.Range(0, 100);
			if (roll <= BuffChance)
			{
				GameObject.Instantiate(GameObject.Find("EffectPrefabRoot").transform.Find("InspiredMsg").gameObject, ally.obj.transform).SetActive(true);
				ally.AddEffect(new CritModEffect(CritBonus));
			}
			else
				GameObject.Instantiate(GameObject.Find("EffectPrefabRoot").transform.Find("MissMsg").gameObject, ally.obj.transform).SetActive(true);
		}

		yield return new WaitForSeconds(1.5f);

		ActionOver();
	}
}
