using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using System.Linq;
public class EnemySpawnAction : Action
{
	public delegate Enemy EnemySpawnCallback();

	private EnemySpawnCallback callback;
	private int partyCountThreshold;

	public EnemySpawnAction(Actor effector, int PartyCountThreshold, EnemySpawnCallback callback)
	{
		this.effector = effector;
		this.partyCountThreshold = PartyCountThreshold;
		this.callback = callback;
		Reset();
	}

	public override System.Tuple<int, int> Damage()
	{
		return new System.Tuple<int, int>(0, 0);
	}
	public override int Accuracy()
	{
		return (int)0;
	}

	public override AttackType DamageType()
	{
		return AttackType.Harmless;
	}
	public override bool CanUseImpl()
	{
		return effector.party.Count < partyCountThreshold;
	}

	protected override void ResetImpl()
	{
		name = "Appel de renforts";

		Charges = -1;
		CooldownTime = 1 + Tweakables.GetSingle("reinforcements_action_cooldown", 2);
	}

	public override string Description()
	{
		return "";
	}

	protected override void UseImpl()
	{
		GameObject.Find("GlobalScripts").GetComponent<CoroutineUtil>().StartCoroutine(SpawnAnimation());
	}

	IEnumerator SpawnAnimation()
	{

		if (ActorAnimator().parameters.Any(x => x.name == "Summon"))
		{
			ActorAnimator().SetTrigger("Summon");
			yield return new WaitForSeconds(0.6f);
		}
		else
			yield return new WaitForSeconds(0.5f);

		var enemy = callback();
		enemy.Actions = 0; // Prevent that enemy for playing on the turn they spawned in
		GameObject.FindObjectOfType<CombatManager>().SpawnEnemy(enemy);
		GameObject.Instantiate(GameObject.Find("EffectPrefabRoot").transform.Find("SummonMsg").gameObject, enemy.obj.transform).SetActive(true);

		yield return new WaitForSeconds(1.5f);

		ActionOver();
	}
}
