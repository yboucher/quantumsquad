using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using System.Linq;

public class RadSprayAction : Action
{
	GameObject spikes;
	public RadSprayAction()
	{
		Reset();
	}

	protected override void ResetImpl()
	{
		name = "Rad Spray";

		Charges = -1;
		CooldownTime = 1 + Tweakables.GetSingle("rad_spray_cooldown", 0);

		spikes = GameObject.Find("EffectPrefabRoot").transform.Find("RadBeastAttackEffect").gameObject;
	}

	public override AttackType DamageType()
	{
		return AttackType.Radioactive;
	}

	public override System.Tuple<int, int> Damage()
	{
		int damage = Tweakables.GetSingle("rad_spray_damage", 10);
		return new System.Tuple<int, int>(damage, damage);
	}
	public override int Accuracy()
	{
		return (int)Tweakables.GetSingle("rad_spray_accuracy", 87);
	}

	public override string Description()
	{
		return "";
	}

	protected override void UseImpl()
	{
		GameObject.Find("GlobalScripts").GetComponent<CoroutineUtil>().StartCoroutine(SprayAnimation());
	}

	IEnumerator SprayAnimation()
	{
		if (ActorAnimator().parameters.Any(x => x.name == "Spray"))
		{
			ActorAnimator().SetTrigger("Spray");
			yield return new WaitForSeconds(1.0f);
		}

		var targets = effector.oppositeParty.OrderBy(a => Random.Range(0, 100000000)).Take(2);
		bool anyHit = false;
		foreach (var target in targets)
		{
			GameObject.Instantiate(spikes, target.obj.transform.position, Quaternion.identity).SetActive(true);

			int damage = Tweakables.GetSingle("rad_spray_damage", 10);
			int accuracy = Tweakables.GetSingle("rad_spray_accuracy", 87);
			bool hit = GameObject.FindObjectOfType<CombatManager>().AttackActor(effector, target, damage, accuracy, AttackType.Radioactive);
			if (hit)
			{
				anyHit = true;
				target.AddEffect(new RadioactiveEffect(15));
			}
		}
		if (anyHit)
			effector.obj.GetComponent<AudioSource>().PlayOneShot(GameObject.FindObjectOfType<SoundFXList>().slash);
		else
			effector.obj.GetComponent<AudioSource>().PlayOneShot(GameObject.FindObjectOfType<SoundFXList>().swoosh);

		yield return new WaitForSeconds(2);

		ActionOver();
	}
}
