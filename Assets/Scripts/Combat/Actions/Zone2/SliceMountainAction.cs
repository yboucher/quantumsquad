using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using System.Linq;

public class SliceMountainAction : Action
{
	GameObject effect;


	public SliceMountainAction(Actor effector)
	{
		this.effector = effector;
		Reset();
	}

	public override AttackType DamageType()
	{
		return AttackType.Harmless;
	}
	public override bool CanUseImpl()
	{
		return effector.Armor == 0; // Second phase
	}

	public override System.Tuple<int, int> Damage()
	{
		return new System.Tuple<int, int>(25, 30);
	}
	public override int Accuracy()
	{
		return 85;
	}
	protected override void ResetImpl()
	{
		name = "Tranche montagne";

		Charges = -1;
		CooldownTime = 1 + Tweakables.GetSingle("grenade_cooldown", 3);

		effect = GameObject.Find("EffectPrefabRoot").transform.Find("GrenadePrefab").gameObject;
	}

	public override string Description()
	{
		return "";
	}

	protected override void UseImpl()
	{
		//Debug.Log("Grenade action called : " + name);
		GameObject.Find("GlobalScripts").GetComponent<CoroutineUtil>().StartCoroutine(AttackAnimation());
	}

	IEnumerator AttackAnimation()
	{
		// Target either the center of enemy party or the center of the player party
		var positionTransform = GameObject.Find(effector.Enemy ? "PlayerPartyCenter" : "EnemyPartyCenter").transform;

		if (ActorAnimator().parameters.Any(x => x.name == "Slice"))
		{
			ActorAnimator().SetTrigger("Slice");
			yield return new WaitForSeconds(0.4f);
		}

		int damage = Random.Range(25, 30);

		bool anyHit = false;

		Assert.IsNotNull(effector);
		var listCopy = new List<Actor>(effector.oppositeParty);
		foreach (var soldier in listCopy)
		{ 
			bool hit = GameObject.FindObjectOfType<CombatManager>().AttackActor(effector, soldier, damage, 85, AttackType.Bleeding);
			int roll = Random.Range(0, 100);
			if (hit)
				anyHit = true;
			if (hit && roll <= 70)
			{
				soldier.AddEffect(new BleedingEffect(10));
			}
		}

		if (anyHit)
			effector.obj.GetComponent<AudioSource>().PlayOneShot(GameObject.FindObjectOfType<SoundFXList>().slash);
		else
			effector.obj.GetComponent<AudioSource>().PlayOneShot(GameObject.FindObjectOfType<SoundFXList>().swoosh);

		yield return new WaitForSeconds(2);
		ActionOver();
	}
}
