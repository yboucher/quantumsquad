using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using System.Linq;

public class HeavyPunchAction : Action
{
	GameObject projectile;

	public int accuracy = 82;

	[ActionMainStat("D�g�ts d'attaque", 60, +5)]
	public int damage = 60;
	public HeavyPunchAction()
	{
		Reset();
	}

	public override System.Tuple<int, int> Damage()
	{
		return new System.Tuple<int, int>(damage, damage + 7);
	}
	public override int Accuracy()
	{
		return (int)accuracy;
	}

	public override AttackType DamageType()
	{
		return AttackType.Melee;
	}

	protected override void ResetImpl()
	{
		name = "Coup de poing lourd";

		Charges = -1;
		CooldownTime = 1 + Tweakables.GetSingle("heavy_punch_cooldown", 2);

		projectile = GameObject.Find("EffectPrefabRoot").transform.Find("FireSlashLinear").gameObject;
	}

	public override string Description()
	{
		return "Coup lourd, inflige " + damage + " pt de d�g�ts.";
	}

	protected override void UseImpl()
	{
		GameObject.Find("GlobalScripts").GetComponent<CoroutineUtil>().StartCoroutine(PunchAnimation());
	}

	IEnumerator PunchAnimation()
	{
		var monitor = GameObject.FindObjectOfType<SceneMouseHoverMonitor>();
		bool UIPickedTarget = false;
		if (target == null) // Ask the user for a target using the UI
		{
			UIPickedTarget = true;
			GameObject.FindObjectOfType<SceneMouseHoverMonitor>().TargetSelectionMode("Enemy");

			// wait until a target has been picked
			yield return new WaitUntil(() => monitor.SelectedTarget() != null);
			target = monitor.SelectedTarget().GetComponent<ActorSpriteUpdater>().actor;
		}

		Vector3 basePos = effector.obj.gameObject.transform.position;
		LeanTween.cancel(effector.obj.gameObject);

		Vector3 direction = (target.obj.transform.position - effector.obj.transform.position).normalized;
		// Go near the target, but not right on top of it
		// Move to
		LeanTween.move(effector.obj.gameObject, target.obj.transform.position - 1.0f*direction, 0.75f).setEaseInOutQuad();
		yield return new WaitUntil(() => !LeanTween.isTweening(effector.obj.gameObject));

		// Attack
		if (ActorAnimator().parameters.Any(x => x.name == "Punch"))
		{
			ActorAnimator().SetTrigger("Punch");
			yield return new WaitForSeconds(0.4f);
		}
		int lo = Tweakables.GetSingle("protector_punch_min", 20);
		int hi = Tweakables.GetSingle("protector_punch_max", 27);
		int actualDamage = Random.Range(lo, hi);
		bool hit = GameObject.FindObjectOfType<CombatManager>().AttackActor(effector, target, actualDamage, accuracy + effector.EffectiveAccuracyModifier(),
			AttackType.Melee);
		if (hit)
		{
			effector.obj.GetComponent<AudioSource>().PlayOneShot(GameObject.FindObjectOfType<SoundFXList>().slash);
			GameObject.Instantiate(projectile, target.obj.transform.position, Quaternion.identity).SetActive(true);
		}
		else
		{
			effector.obj.GetComponent<AudioSource>().PlayOneShot(GameObject.FindObjectOfType<SoundFXList>().swoosh);
		}

		yield return new WaitForSeconds(1);

		// Move back
		LeanTween.move(effector.obj.gameObject, basePos, 0.75f).setEaseInOutQuad();
		yield return new WaitUntil(() => !LeanTween.isTweening(effector.obj.gameObject));

		yield return new WaitForSeconds(0.5f);
		if (UIPickedTarget)
			monitor.ExitTargetMode();
		ActionOver();
	}
}
