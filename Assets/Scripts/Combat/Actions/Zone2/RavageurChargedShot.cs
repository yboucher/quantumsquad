using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class RavageurChargedAction : Action
{
	GameObject projectile;

	[ActionMainStat("D�g�ts", 10, +5)]
	public int damage = 10;

	public bool useRange = false;
	public int damageMin = 10;
	public int damageMax = 10;
	public int accuracy = 75;

	public RavageurChargedAction()
	{
		Reset();
	}

	public override System.Tuple<int, int> Damage()
	{
		return new System.Tuple<int, int>(damage, damage);
	}
	public override int Accuracy()
	{
		return (int)accuracy;
	}

	public override AttackType DamageType()
	{
		return AttackType.Ballistic;
	}
	protected override void ResetImpl()
	{
		name = "Tir charg�";

		Charges = -1;
		CooldownTime = 1 + Tweakables.GetSingle("ravageur_attack_cooldown", 1);

		projectile = GameObject.Find("EffectPrefabRoot").transform.Find("ProjectilePrefab").gameObject;
	}

	public override string Description()
	{
		return "Inflige " + damage + "pt de d�g�ts � tous les ennemis.";
	}

	protected override void UseImpl()
	{
		GameObject.Find("GlobalScripts").GetComponent<CoroutineUtil>().StartCoroutine(ShootAnimation());
	}

	IEnumerator ShootAnimation()
	{
		effector.obj.GetComponent<AudioSource>().PlayOneShot(GameObject.FindObjectOfType<SoundFXList>().chargedCannon);
		yield return new WaitForSeconds(0.9f);
		ActorAnimator().SetTrigger("ChargedShot");
		yield return new WaitForSeconds(0.8f);

		int actualDamage = damage;
		if (useRange)
			actualDamage = Random.Range(damageMin, damageMax);

		// Roll for each opponent and see if we affect them
		List<Actor> listCopy = new List<Actor>(effector.oppositeParty); // Make a copy of the list object (not its content) to iterate over it, as DamageActor might delete actors which might break the iteration
		foreach (var actor in listCopy)
			GameObject.FindObjectOfType<CombatManager>().AttackActor(effector, actor, actualDamage, accuracy + effector.EffectiveAccuracyModifier(), AttackType.Ballistic); // 0 Damage, just see if we hit them or not
		
		yield return new WaitForSeconds(2);

		ActionOver();
	}
}
