using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using System.Linq;

public class ProtectorAction : Action
{
	[ActionMainStat("Bonus d'equive pour les alli�s", 20, +5)]
	int AlliedDodgeBonus = 20;
	[ActionMainStat("Malus d'esquive pour le personnage", -50, -5)]
	int SelfDodgeMalus = -50;

	GameObject msg;
	public ProtectorAction()
	{
		Reset();
	}

	public override AttackType DamageType()
	{
		return AttackType.Harmless;
	}

	public override System.Tuple<int, int> Damage()
	{
		return new System.Tuple<int, int>(0, 0);
	}
	public override int Accuracy()
	{
		return 0;
	}
	protected override void ResetImpl()
	{
		name = "Protecteur";

		Charges = -1;
		CooldownTime = 1 + Tweakables.GetSingle("protector_action_cooldown", 1);

		msg = GameObject.Find("EffectPrefabRoot").transform.Find("ProtectMsg").gameObject;
	}

	public override string Description()
	{
		return "";
	}

	protected override void UseImpl()
	{
		GameObject.Find("GlobalScripts").GetComponent<CoroutineUtil>().StartCoroutine(ShieldAnimation());
	}

	IEnumerator ShieldAnimation()
	{
		effector.obj.GetComponent<AudioSource>().PlayOneShot(GameObject.FindObjectOfType<SoundFXList>().robotSounds);

		if (effector.movedForward)
		{
			// Move back
			LeanTween.move(effector.obj.gameObject, effector.basePos, 0.75f).setEaseInOutQuad();
			yield return new WaitUntil(() => !LeanTween.isTweening(effector.obj.gameObject));
			effector.movedForward = false;
		}

		Vector3 basePos = effector.obj.gameObject.transform.position;
		LeanTween.cancel(effector.obj.gameObject);

		Vector3 direction = (target.obj.transform.position - effector.obj.transform.position).normalized;
		// Go near the target, but not right on top of it
		// Move to
		LeanTween.move(effector.obj.gameObject, GameObject.Find("PlayfieldCenter").transform.position, 0.75f).setEaseInOutQuad();
		yield return new WaitUntil(() => !LeanTween.isTweening(effector.obj.gameObject));

		if (ActorAnimator().parameters.Any(x => x.name == "Protect"))
		{
			ActorAnimator().SetTrigger("Protect");
			yield return new WaitForSeconds(0.4f);
		}

		yield return new WaitForSeconds(0.4f);

		effector.AddEffect(new ProtectorEffect(SelfDodgeMalus));

		foreach (var ally in effector.party)
			if (ally != effector)
			{
				GameObject.Instantiate(msg, ally.obj.transform).SetActive(true);
				ally.AddEffect(new ProtectorEffect(AlliedDodgeBonus));
			}

		effector.movedForward = true;
		effector.basePos = basePos;

		yield return new WaitForSeconds(1.5f);

		ActionOver();
	}
}
