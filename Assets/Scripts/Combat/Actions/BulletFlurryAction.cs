using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

[ActionData("Pluie de balles", "Inflige 13-17 pt de d�g�ts � tous les ennemis.")]
public class BulletFlurryAction : Action
{
	GameObject projectile;

	[ActionMainStat("D�g�ts minimaux", 13, +5)]
	public int damage = 13;

	public bool useRange = false;
	public int damageMin = 10;
	public int damageMax = 10;
	public int accuracy = 82;

	public BulletFlurryAction()
	{
		Reset();
	}

	public override AttackType DamageType()
	{
		return AttackType.Ballistic;
	}
	public override System.Tuple<int, int> Damage()
	{
		return new System.Tuple<int, int>(damage, damage + 5);
	}
	public override int Accuracy()
	{
		return (int)accuracy;
	}
	protected override void ResetImpl()
	{
		name = "Pluie de balles";

		Charges = -1;
		CooldownTime = 0 + Tweakables.GetSingle("pluie_balles_cooldown", 0);

		projectile = GameObject.Find("EffectPrefabRoot").transform.Find("ProjectilePrefab").gameObject;
	}

	public override string Description()
	{
		return "Inflige " + damage + "-" + (damage+5) + "pt de d�g�ts � tous les ennemis.";
	}

	protected override void UseImpl()
	{
		Debug.Log("Shooting action called : " + name);
		GameObject.Find("GlobalScripts").GetComponent<CoroutineUtil>().StartCoroutine(ShootAnimation());
	}

	IEnumerator ShootAnimation()
	{
		ActorAnimator().SetTrigger("Spray");
		effector.obj.GetComponent<AudioSource>().PlayOneShot(GameObject.FindObjectOfType<SoundFXList>().machineGun);
		yield return new WaitForSeconds(0.4f);

		int actualDamage = damage;
		if (useRange)
			actualDamage = Random.Range(damageMin, damageMax);
		else
			actualDamage += Random.Range(0, 6);

		// Roll for each opponent and see if we affect them
		List<Actor> listCopy = new List<Actor>(effector.oppositeParty); // Make a copy of the list object (not its content) to iterate over it, as DamageActor might delete actors which might break the iteration
		foreach (var actor in listCopy)
			GameObject.FindObjectOfType<CombatManager>().AttackActor(effector, actor, actualDamage, accuracy + effector.EffectiveAccuracyModifier(), AttackType.Melee); // 0 Damage, just see if we hit them or not

		yield return new WaitForSeconds(2);

		ActionOver();
	}
}
