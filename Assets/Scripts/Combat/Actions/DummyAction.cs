using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DummyAction : Action
{
	public DummyAction(string in_name, int charges)
	{
		name = in_name;

		Charges = charges;

		Reset();
	}

	public override AttackType DamageType()
	{
		return AttackType.Harmless;
	}
	public override System.Tuple<int, int> Damage()
	{
		return new System.Tuple<int, int>(0, 0);
	}
	public override int Accuracy()
	{
		return 0;
	}
	protected override void ResetImpl()
	{
		CooldownTime = 5;
	}

	protected override void UseImpl()
	{
		Debug.Log("Dummy action called : " + name);
		ActionOver();
	}
}
