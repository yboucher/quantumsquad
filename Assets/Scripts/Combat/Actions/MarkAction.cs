using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using System.Linq;

//[ActionData("Marquer", "Marque une cible, la rendant plus vulnérable aux attaques.")]
public class MarkAction : Action
{
	public GameObject crossObject;
	public MarkAction()
	{
		Reset();
	}

	public override AttackType DamageType()
	{
		return AttackType.Harmless;
	}
	public override System.Tuple<int, int> Damage()
	{
		return new System.Tuple<int, int>(0, 0);
	}
	public override int Accuracy()
	{
		return 0;
	}
	protected override void ResetImpl()
	{
		name = "Marquer";

		Charges = -1;
		CooldownTime = 1 + Tweakables.GetSingle("marquer_cooldown", 1);

		crossObject = GameObject.Find("EffectPrefabRoot").transform.Find("Cross").gameObject;
	}

	public override string Description()
	{
		return "Marque une cible, la rendant plus vulnérable aux attaques.";
	}

	protected override void UseImpl()
	{
		GameObject.Find("GlobalScripts").GetComponent<CoroutineUtil>().StartCoroutine(MarkAnimation());
	}

	IEnumerator MarkAnimation()
	{
		var monitor = GameObject.FindObjectOfType<SceneMouseHoverMonitor>();
		bool UIPickedTarget = false;
		if (target == null) // Ask the user for a target using the UI
		{
			UIPickedTarget = true;
			GameObject.FindObjectOfType<SceneMouseHoverMonitor>().TargetSelectionMode("Enemy");

			// wait until a target has been picked
			yield return new WaitUntil(() => monitor.SelectedTarget() != null);
			target = monitor.SelectedTarget().GetComponent<ActorSpriteUpdater>().actor;
		}

		if (ActorAnimator().parameters.Any(x => x.name == "Mark"))
		{
			ActorAnimator().SetTrigger("Mark");
			yield return new WaitForSeconds(0.75f);
		}


		Assert.IsNotNull(target);
		bool hit = GameObject.FindObjectOfType<CombatManager>().AttackActor(effector, target, 0, 100 + effector.EffectiveAccuracyModifier(),
			AttackType.Harmless);
		if (hit)
		{
			GameObject.Instantiate(crossObject, target.obj.transform.position + new Vector3(0, 0.2f, 0), Quaternion.identity).SetActive(true);
			GameObject.Instantiate(GameObject.Find("EffectPrefabRoot").transform.Find("MarkMsg").gameObject, target.obj.transform).SetActive(true);
			target.AddEffect(new MarkedEffect());
		}

		yield return new WaitForSeconds(2);
		if (UIPickedTarget)
			monitor.ExitTargetMode();
		ActionOver();
	}
}
