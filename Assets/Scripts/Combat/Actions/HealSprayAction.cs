using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

[ActionData("Vapeur bonifiante", "Rend entre 40 et 80 HP � un alli�.")]
public class HealSprayAction : Action
{
	[ActionMainStat("Soins minimum", 40, +8)]
	int healingBase = 40;

	public HealSprayAction()
	{
		Reset();
	}

	public override AttackType DamageType()
	{
		return AttackType.Harmless;
	}

	public override System.Tuple<int, int> Damage()
	{
		return new System.Tuple<int, int>(0, 0);
	}
	public override int Accuracy()
	{
		return 0;
	}
	protected override void ResetImpl()
	{
		name = "Vapeur bonifiante";

		Charges = 2;
		CooldownTime = 1 + Tweakables.GetSingle("heal_spray_cooldown", 1);
	}

	public override string Description()
	{
		return "Rend entre " + healingBase + " et " + (healingBase+40) + " HP � un alli�.";
	}

	protected override void UseImpl()
	{
		GameObject.Find("GlobalScripts").GetComponent<CoroutineUtil>().StartCoroutine(HealAnimation());
	}

	IEnumerator HealAnimation()
	{
		var monitor = GameObject.FindObjectOfType<SceneMouseHoverMonitor>();
		bool UIPickedTarget = false;
		if (target == null) // Ask the user for a target using the UI
		{
			UIPickedTarget = true;
			GameObject.FindObjectOfType<SceneMouseHoverMonitor>().TargetSelectionMode("Soldier");

			// wait until a target has been picked
			yield return new WaitUntil(() => monitor.SelectedTarget() != null);
			target = monitor.SelectedTarget().GetComponent<ActorSpriteUpdater>().actor;
		}

		int HPRoll = Random.Range(healingBase, healingBase + 40);

		Assert.IsNotNull(target);
		GameObject.FindObjectOfType<CombatManager>().HealActor(effector, target, HPRoll);
		effector.obj.GetComponent<AudioSource>().PlayOneShot(GameObject.FindObjectOfType<SoundFXList>().spray);

		// Ultimate effects
		//target.RemoveEffect<BleedingEffect>();
		//target.RemoveEffect<RadioactiveEffect>();

		yield return new WaitForSeconds(2);
		if (UIPickedTarget)
			monitor.ExitTargetMode();
		ActionOver();
	}
}
