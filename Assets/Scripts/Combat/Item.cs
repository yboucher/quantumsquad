using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;
public enum ItemUsage
{
	DuringCombat,
	DuringExploration,
	Passive
}

public enum ItemRarity
{
	Common,
	Rare,
	Legendary
}

[System.AttributeUsage(System.AttributeTargets.Class |
					   System.AttributeTargets.Struct)
]
public class LootInfoAttribute : System.Attribute
{
	public ItemRarity itemRarity;
	public float probWeight = 1.0f; // coefficient changing the likelihood this item is picked when generating loot of this rarity level
	public LootInfoAttribute(ItemRarity rarity = ItemRarity.Common, float probWeight = 1.0f)
	{
		this.itemRarity = rarity;
		this.probWeight = probWeight;
	}
}

public class CurioItemAttribute : System.Attribute
{
	public CurioItemAttribute()
	{
	}
}

[System.Serializable]
public abstract class Item
{
	public string name = "<MISSINGNO>";
	public UseItemAction action;

	public Actor effector;

	public bool animationActive = false;
	public bool used = false;

	public virtual string Description()
	{
		return "<MISSINGDESC>";
	}

	public virtual ItemUsage Usage()
	{
		return ItemUsage.DuringCombat;
	}

	public void Use()
	{
		Assert.IsFalse(used);
		Assert.IsFalse(animationActive);

		animationActive = true;
		used = true;

		UseImpl();
	}

	public bool ActionAnimationOver()
	{
		return animationActive;
	}

	public void EndOfTurn()
	{
		EndOfTurnImpl();
	}
	protected abstract void UseImpl();

	protected void ActionOver()
	{
		animationActive = false;
	}

	protected virtual void EndOfTurnImpl()
	{

	}

	static public List<string> ItemsByRarity(ItemRarity rarity)
	{
		List<string> items = new List<string>();

		// List Item subclasses
		var subclassTypes = Assembly
		   .GetAssembly(typeof(Item))
		   .GetTypes()
		   .Where(t => t.IsSubclassOf(typeof(Item)));

		foreach (var type in subclassTypes)
		{
			var attrs = System.Attribute.GetCustomAttributes(type, typeof(LootInfoAttribute));
			if (attrs.Length == 0)
				continue;
			var attr = (LootInfoAttribute)attrs[0];
			if (attr.itemRarity != rarity)
				continue;

			items.Add(type.Name);
		}

		return items;
	}

	static public List<string> CurioItems()
	{
		List<string> items = new List<string>();

		// List Item subclasses
		var subclassTypes = Assembly
		   .GetAssembly(typeof(Item))
		   .GetTypes()
		   .Where(t => t.IsSubclassOf(typeof(Item)));

		foreach (var type in subclassTypes)
		{
			var attrs = System.Attribute.GetCustomAttributes(type, typeof(CurioItemAttribute));
			if (attrs.Length == 0)
				continue;

			items.Add(type.Name);
		}

		return items;
	}

	static public Item GenerateRandomItem()
	{
		int roll = Random.Range(0, 100);
		List<string> lootList;
		if (roll <= 75)
			lootList = ItemsByRarity(ItemRarity.Common);
		else if (roll <= 95)
			lootList = ItemsByRarity(ItemRarity.Rare);
		else
			lootList = ItemsByRarity(ItemRarity.Legendary);

		// If loot lists are empty, default to the one below
		if (lootList.Count == 0)
			lootList = ItemsByRarity(ItemRarity.Rare);
		if (lootList.Count == 0)
			lootList = ItemsByRarity(ItemRarity.Common);

		Assert.IsTrue(lootList.Count > 0);

		var className = lootList[Random.Range(0, lootList.Count)];

		// Instantiate
		var assembly = Assembly.GetExecutingAssembly();

		var type = assembly.GetTypes()
			.First(t => t.Name == className);

		return (Item)System.Activator.CreateInstance(type);
	}
}
