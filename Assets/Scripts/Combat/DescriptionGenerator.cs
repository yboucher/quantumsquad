using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DescriptionGenerator : MonoBehaviour
{
    public TextMeshPro descText;
    public TextMeshPro actionText;

    float startTime;
    string currentMasterText;

    SceneMouseHoverMonitor hoverMonitor;

    // Start is called before the first frame update
    void Start()
    {
        hoverMonitor = FindObjectOfType<SceneMouseHoverMonitor>();
        startTime = Time.time;
    }

    public string GetDescriptionText()
	{
        string text = "Survoler un personnage ou un ennemi\n" +
                      "pour afficher ses caractéristiques\n" +
                      "et les effets qui lui sont appliqués";

        Actor actor = null;
        if (FindObjectOfType<CombatManager>().selectedSoldier != null)
            actor = FindObjectOfType<CombatManager>().selectedSoldier;
        if (hoverMonitor.HoveredObject() && hoverMonitor.HoveredObject().GetComponent<ActorSpriteUpdater>())
        {
            actor = hoverMonitor.HoveredObject().GetComponent<ActorSpriteUpdater>().actor;
        }

        if (actor != null)
        {
            text = "";
            if (actor == FindObjectOfType<CombatManager>().selectedSoldier)
                text += "Soldat actuel : ";
            else
                text += "Sélection : ";

            if (actor.Enemy)
                text += "<color=red>";
            else
                text += "<color=#7802f5>";
            text += actor.ActorName + "<#FFFFFF>\n";
            if (!actor.Enemy)
                text += "\nClasse : <color=green>" + actor.Class + "</color>\n";
            text += "<color=red><voffset=0.3em><size=200%><sprite tint=1 name=\"heart\"></size></voffset>  " + actor.HP + " / " + actor.MaxHP + "</color>  ";
            if (actor.Armor > 0)
                text += "<color=#00a0ff><voffset=0.3em><size=200%><sprite tint=1 name=\"shield\"></size></voffset>  " + actor.Armor + " / " + actor.MaxArmor + "</color>";
            text += "\n";
            text += "Esquive : " + actor.EffectiveDodgeChance() + " %\n";

            foreach (var effect in actor.GetEffects())
            {
                text += "<color=yellow>" + effect.Name + "</color> : " + effect.Description + "\n";
            }
            if (FindObjectOfType<CombatManager>().revealWeaknesses && actor.Enemy)
            {
                foreach (var type in actor.Vulnerabilities)
                {
                    text += "<color=red>Vulnérable</color> " + AttackTypeUtils.TypeToVulnerabilityString(type.type) + "\n";
                }
                foreach (var type in actor.Resistances)
                {
                    text += "<color=red>Résistant</color> " + AttackTypeUtils.TypeToVulnerabilityString(type.type) + "\n";
                }
            }
        }

        text = "<mspace=0.5em>" + text;
        return text;
	}

    float counter = 0;
    // Update is called once per frame
    void Update()
    {
        counter += Time.deltaTime;

        string text = GetDescriptionText();
        if (currentMasterText != text)
        {
            startTime = Time.time;

            currentMasterText = text;
            descText.text = text;
        }

        if (counter > 0.025f)
        {
            counter = 0;

            char[] itemName = currentMasterText.ToCharArray();
            float progress = Mathf.Clamp((Time.time - startTime) / 0.25f, 0, 1);
            //progress = EasingFunction.EaseInSine(0, 1, progress);
            int randLimit = Mathf.RoundToInt(itemName.Length * (1.0f - progress));

            int caretDepth = 0;

            for (int j = 0; j < randLimit; ++j)
            {
                if(itemName[j] == '<')
				{
                    ++caretDepth;
                    continue;
				}
                if (itemName[j] == '>')
                {
                    --caretDepth;
                    continue;
                }

                if (caretDepth == 0 && char.IsLetterOrDigit(itemName[j]))
                    itemName[j] = (char)(Random.Range(0x20, 0x7F)); // Random char effect, a la Matrix
            }

            descText.text = new string(itemName);
        }
    }
}
