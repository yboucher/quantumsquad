using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

public class ItemButton : MonoBehaviour
{
    public Item item;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetInteractable(bool interactable)
    {
        GetComponent<Button>().interactable = interactable;
    }

    public void SetItem(Item in_item, int count)
	{
        Assert.IsNotNull(in_item);
        item = in_item;
        GetComponentInChildren<TMPro.TextMeshProUGUI>().text = item.name;
        if (count > 1)
            GetComponentInChildren<TMPro.TextMeshProUGUI>().text += " (" + count + ")";
        GetComponent<Button>().interactable = true;

        var colorBlock = GetComponent<Button>().colors;
        colorBlock.disabledColor = Color.white;
        GetComponent<Button>().colors = colorBlock;
        GetComponentInChildren<TMPro.TextMeshProUGUI>().color = Color.white;
    }

    public void Pressed()
	{
        if (item != null)
        {
            item.Use();
        }
	}
}
