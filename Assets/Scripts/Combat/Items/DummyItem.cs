using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DummyItem : Item
{
	// Dummy item that doesn't do anything and returns to the inventory
	public DummyItem()
	{
		name = "Retour";
	}

	public override string Description()
	{
		return "Fermer l'inventaire";
	}

	protected override void UseImpl()
	{
		action.Cancel();
	}
}
