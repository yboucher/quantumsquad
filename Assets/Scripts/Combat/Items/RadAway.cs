using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[LootInfo(ItemRarity.Common)]
public class RadAway : Item
{
	public RadAway()
	{
		name = "RadAway";
	}

	public override string Description()
	{
		return "Retire l'effet 'irradi�'.";
	}

	protected override void UseImpl()
	{
		action.effector.RemoveEffect<RadioactiveEffect>();
		action.ItemUsed();
	}
}
