using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[LootInfo(ItemRarity.Common)]
public class Adrenaline : Item
{
	public Adrenaline()
	{
		name = "Adrénaline";
	}

	public override string Description()
	{
		return "Boost de précision (+20%) pour ce combat.";
	}

	public override ItemUsage Usage()
	{
		return ItemUsage.DuringCombat;
	}

	protected override void UseImpl()
	{
		foreach (var soldier in SharedData.soldiers)
			soldier.AddEffect(new PrecisionEffect(20), true);

		action.ItemUsed();
	}
}

[LootInfo(ItemRarity.Common)]
public class ExoMod : Item
{
	public ExoMod()
	{
		name = "Module d'exosquelette";
	}

	public override string Description()
	{
		return "Boost d'esquive (+20%) pour ce combat.";
	}

	public override ItemUsage Usage()
	{
		return ItemUsage.DuringCombat;
	}

	protected override void UseImpl()
	{
		foreach (var soldier in SharedData.soldiers)
			soldier.AddEffect(new ExoModEffect(20), true);

		action.ItemUsed();
	}
}