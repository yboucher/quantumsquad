using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScanItem : Item
{
	public EnemyScanItem()
	{
		name = "Scouter";
	}

	public override ItemUsage Usage()
	{
		return ItemUsage.Passive;
	}

	public override string Description()
	{
		return "Mini-ordinateur extraterrestre automatique, capable de révéler les résistances et les faiblesses des ennemis lorsqu'ils sont survolés.";
	}

	protected override void UseImpl()
	{
		GameObject.FindObjectOfType<CombatManager>().revealWeaknesses = true;
		action.ItemUsed();
	}
}
