using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[LootInfo(ItemRarity.Rare)]
public class NFTItem : Item
{
	public NFTItem()
	{
		name = "NFT � identifier";
	}

	public override ItemUsage Usage()
	{
		return ItemUsage.Passive;
	}

	public override string Description()
	{
		return "Un ticket num�rique associ� � une oeuvre d'art myst�rieuse : une expertise plus pouss�e permettra de d�terminer sa nature et sa valeur.";
	}

	protected override void UseImpl()
	{
	}
}
