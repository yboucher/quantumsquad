using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CurioItem]
[LootInfo(ItemRarity.Common)]
public class BadgeItem : Item
{
	public BadgeItem()
	{
		name = "Badge";
	}

	public override string Description()
	{
		return "Un badge. Cet item pourrait se r�v�ler utile dans certains types de salles !";
	}

	public override ItemUsage Usage()
	{
		return ItemUsage.Passive;
	}

	// Shouldn't be called
	protected override void UseImpl()
	{
		action.ItemUsed();
	}
}

[CurioItem]
[LootInfo(ItemRarity.Common)]
public class AccessCardItem : Item
{
	public AccessCardItem()
	{
		name = "Carte d'acc�s";
	}

	public override string Description()
	{
		return "Une carte d'acc�s. Cet item pourrait se r�v�ler utile dans certains types de salles !";
	}

	public override ItemUsage Usage()
	{
		return ItemUsage.Passive;
	}

	// Shouldn't be called
	protected override void UseImpl()
	{
		action.ItemUsed();
	}
}

[CurioItem]
[LootInfo(ItemRarity.Common)]
public class GourdeItem : Item
{
	public GourdeItem()
	{
		name = "Gourde";
	}

	public override string Description()
	{
		return "Une gourde. Cet item pourrait se r�v�ler utile dans certains types de salles !";
	}
	public override ItemUsage Usage()
	{
		return ItemUsage.Passive;
	}

	// Shouldn't be called
	protected override void UseImpl()
	{
		action.ItemUsed();
	}
}

[CurioItem]
[LootInfo(ItemRarity.Common)]
public class GeigerItem : Item
{
	public GeigerItem()
	{
		name = "Compteur Geiger";
	}

	public override string Description()
	{
		return "Un compteur Geiger. Cet item pourrait se r�v�ler utile dans certains types de salles !";
	}
	public override ItemUsage Usage()
	{
		return ItemUsage.Passive;
	}

	// Shouldn't be called
	protected override void UseImpl()
	{
		action.ItemUsed();
	}
}

/*
[LootInfo(ItemRarity.Common)]
public class ArtOfWarItem : Item
{
	public ArtOfWarItem()
	{
		name = "\"L'art de la guerre \"";
	}

	public override ItemUsage Usage()
	{
		return ItemUsage.Passive;
	}

	// Shouldn't be called
	protected override void UseImpl()
	{
		action.ItemUsed();
	}
}
*/