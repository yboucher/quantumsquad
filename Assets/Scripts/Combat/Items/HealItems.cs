using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[LootInfo(ItemRarity.Common)]
public class HealHerbs : Item
{
	public HealHerbs()
	{
		name = "Herbes médicinales";
	}

	public override string Description()
	{
		return "Rend 30HP au soldat actuel.";
	}

	protected override void UseImpl()
	{
		GameObject.FindObjectOfType<CombatManager>().HealActor(action.effector, action.effector, 30);
		action.ItemUsed();
	}
}

[LootInfo(ItemRarity.Common)]
public class MedKitItem : Item
{
	public MedKitItem()
	{
		name = "Médikit";
	}

	public override string Description()
	{
		return "Rend 60HP au soldat actuel.";
	}

	protected override void UseImpl()
	{
		GameObject.FindObjectOfType<CombatManager>().HealActor(action.effector, action.effector, 60);
		action.ItemUsed();
	}
}

[LootInfo(ItemRarity.Rare)]
public class SuperMedKit : Item
{
	public SuperMedKit()
	{
		name = "Super Médikit";
	}

	public override string Description()
	{
		return "Rend 90HP au soldat actuel.";
	}

	protected override void UseImpl()
	{
		GameObject.FindObjectOfType<CombatManager>().HealActor(action.effector, action.effector, 90);
		action.ItemUsed();
	}
}