using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[LootInfo(ItemRarity.Common)]
public class Bandage : Item
{
	public Bandage()
	{
		name = "Bandage";
	}

	public override string Description()
	{
		return "Retire l'effet 'saignement'.";
	}

	protected override void UseImpl()
	{
		action.effector.RemoveEffect<BleedingEffect>();
		action.ItemUsed();
	}
}
