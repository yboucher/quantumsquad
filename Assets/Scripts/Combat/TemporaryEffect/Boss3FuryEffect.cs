using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class Boss3FuryEffect : TemporaryEffect
{
	public Boss3FuryEffect()
	{
		Name = "Furie";
		turns = Tweakables.GetSingle("boss3_fury_duration", 2);
		accuracyMod = Tweakables.GetSingle("boss3_fury_accuracy", 15);
		dodgeMod = Tweakables.GetSingle("boss3_fury_dodge", 25);
		canCounterattack = true;
		Description = "Le boss gagne " + accuracyMod + "% de pr�cision et " + dodgeMod + "% d�esquive.\n" +
			"Le boss effectue une contre-attaque quand il esquive un coup.\n" +
			"Les attaques de type feu mettent fin au mode furie.";

		Shader.SetGlobalFloat("_FuryMode", 1.0f);
	}

	public override void DoExpireCleanup(Actor act)
	{
		Debug.Log("Called");
		Shader.SetGlobalFloat("_FuryMode", 0.0f);
	}

	public override GameObject GetIcon()
	{
		return GameObject.Find("FireIcon");
	}
}
