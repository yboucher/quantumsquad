using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class ScaredEffect : TemporaryEffect
{
	public ScaredEffect(int mod = 60)
	{
		Name = "Effray�";
		accuracyMod = -mod; // Fear reduces the accuracy by 60%
		turns = 2;
		Description = "La cible est effray�e et subit une p�nalit� de pr�cision de " + mod + "%. Dure un tour.";
	}

	public override GameObject GetIcon()
	{
		return GameObject.Find("StunnedIcon");
	}
}
