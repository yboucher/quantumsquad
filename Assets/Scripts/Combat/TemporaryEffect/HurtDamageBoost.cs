using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class HurtDamageBoost : TemporaryEffect
{
	public HurtDamageBoost(float mult = 1.4f)
	{
		Name = "Berserk";
		turns = 100000;
		inflictedDamageMult = mult;
		Description = "Inflige 40% de d�g�ts en plus quand la sant� du soldat est inf�rieure � 20%.";
	}

	public override GameObject GetIcon()
	{
		return GameObject.Find("FireIcon");
	}
}
