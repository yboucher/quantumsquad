using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class MarkedEffect : TemporaryEffect
{
	public MarkedEffect()
	{

		Name = "Marqu�";
		turns = 2;
		Description = "Les attaques ennemies font plus de d�g�ts.";
	}

	public override GameObject GetIcon()
	{
		return GameObject.Find("TargetedIcon");
	}
}
