using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class StunnedEffect : TemporaryEffect
{
	public StunnedEffect(int in_turns)
	{
		Name = "�tourdi";
		turns = in_turns;
		Description = "La cible passe " + turns + "tour(s).";	
	}

	public override GameObject GetIcon()
	{
		return GameObject.Find("StunnedIcon");
	}
	protected override void DoEffectImpl(Actor act)
	{
		if ((turns - elapsedTurns) > 1)
		{
			act.Actions = 0;
		}
	}
}
