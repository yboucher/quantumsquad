using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class IntimidatedEffect : TemporaryEffect
{
	public IntimidatedEffect(int mod = 60)
	{
		Name = "Intimid�";
		inflictedDamageMult = (100 - mod) / 100.0f;
		turns = Tweakables.GetSingle("intimidation_turns", 1);
		Description = "La cible est intimid�e et subit une p�nalit� de d�g�ts de " + mod + "%. Dure un tour.";
	}

	public override GameObject GetIcon()
	{
		return GameObject.Find("StunnedIcon");
	}
}
