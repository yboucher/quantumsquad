using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class TemporaryEffect
{
	public string Name = "<PLACEHOLDER>";
	public string Description = "<PLACEHOLDER>";

	public float accuracyMod = 0;
	public float dodgeMod = 0;
	public float critMod = 0;
	public float receivedDamageMult = 1;
	public float inflictedDamageMult = 1;
	public int turns = 1;
	public bool permanent = false; // Persistent between combats
	public bool canCounterattack = false;

	public virtual GameObject GetIcon()
	{ return null; }

	protected int elapsedTurns = 0;
	public void DoEffect(Actor act)
	{
		Assert.IsTrue(elapsedTurns < turns);

		DoEffectImpl(act);

		++elapsedTurns;
	}

	protected virtual void DoEffectImpl(Actor act)
	{ } // Do Nothing by default

	public virtual void DoExpireCleanup(Actor act)
	{ } // Do Nothing by default

	public virtual bool EffectAppliedOnEnemyTurnStart()
	{
		return false;
	}

	public bool Expired()
	{
		return elapsedTurns >= turns;
	}
}
