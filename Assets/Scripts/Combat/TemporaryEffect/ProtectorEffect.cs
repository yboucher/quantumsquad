using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class ProtectorEffect : TemporaryEffect
{
	public ProtectorEffect(float dodgeBonus)
	{
		if (dodgeBonus > 0)
			Name = "Prot�g�";
		else
			Name = "Protecteur";
		turns = 2;
		dodgeMod = (dodgeBonus+100) / 100.0f;
		if (dodgeBonus > 0)
			Description = "Bonus de +" + dodgeBonus + " d'esquive.";
		else
			Description = "Malus de " + dodgeBonus + " d'esquive.";
	}

	public override GameObject GetIcon()
	{
		if (dodgeMod >= 1.0f)
			return GameObject.Find("ShieldIcon");
		else
			return GameObject.Find("TargetedIcon");
	}

	// Remove protector effects if the effect is expired or its actor is dead
	public override void DoExpireCleanup(Actor act)
	{
		// Protection effect source
		if (dodgeMod < 1.0f)
		{
			foreach (var ally in act.party)
				if (ally != act)
					ally.RemoveEffect<ProtectorEffect>();
		}
	}
}
