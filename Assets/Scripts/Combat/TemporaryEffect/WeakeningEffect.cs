using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class WeakeningEffect : TemporaryEffect
{
	public WeakeningEffect(float mult = 2.0f)
	{
		Name = "Affaibli";
		turns = 1;
		receivedDamageMult = mult;
		Description = "Subit 50% de d�g�ts en plus.";
	}

	public override GameObject GetIcon()
	{
		return GameObject.Find("WeakenedIcon");
	}
}
