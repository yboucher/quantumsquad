using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class RadioactiveEffect : TemporaryEffect
{
	int damage = 15;
	public RadioactiveEffect(int in_damage, int turns = 2)
	{
		Name = "Irradi�";
		damage = in_damage;
		this.turns = turns;
		Description = "La cible subit " + damage + "HP par tour. Dure deux tours.";
	}

	public override GameObject GetIcon()
	{
		return GameObject.Find("RadioactiveIcon");
	}

	public override bool EffectAppliedOnEnemyTurnStart()
	{
		return true;
	}

	protected override void DoEffectImpl(Actor act)
	{
		GameObject.FindObjectOfType<CombatManager>().DamageActor(null, act, damage, AttackType.Radioactive);
	}
}
