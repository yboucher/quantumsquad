using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class BleedingEffect : TemporaryEffect
{
	int damage = 15;
	public BleedingEffect(int in_damage, int turns = 2)
	{
		Name = "Saignement";
		damage = in_damage;
		this.turns = turns;
		Description = "La cible subit " + damage + "HP par tour. Dure deux tours.";
	}

	public override GameObject GetIcon()
	{
		return GameObject.Find("BleedingIcon");
	}

	public override bool EffectAppliedOnEnemyTurnStart()
	{
		return true;
	}

	protected override void DoEffectImpl(Actor act)
	{
		GameObject.FindObjectOfType<CombatManager>().DamageActor(null, act, damage, AttackType.Bleeding);
	}
}
