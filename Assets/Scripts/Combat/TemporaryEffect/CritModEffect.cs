using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class CritModEffect : TemporaryEffect
{
	int mod;
	public CritModEffect(int mod)
	{
		this.mod = mod;
		if (mod > 0)
			Name = "Inspir�";
		else
			Name = "Affaibli";
		critMod = mod;
		turns = 2;
		if (mod > 0)
			Description = "La cr�ature a " + mod + " % de chance en plus de faire un coup critique.";
		else
			Description = "La cr�ature a un malus de chance de critique de " + mod + ".";
	}

	public override GameObject GetIcon()
	{
		if (mod > 0)
			return GameObject.Find("FireIcon");
		else
			return GameObject.Find("WeakenedIcon");
	}
}
