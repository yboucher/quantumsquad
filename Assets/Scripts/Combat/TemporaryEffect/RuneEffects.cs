using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class WisdomEffect : TemporaryEffect
{
	public WisdomEffect()
	{
		Name = "Sagesse";

		turns = 10000;
		accuracyMod = 20;
		inflictedDamageMult = 0.9f;

		Description = "+20% de pr�cision, -10% de d�gats inflig� � l�ennemi.";
	}

	public override GameObject GetIcon()
	{
		return null; // TODO
	}
}