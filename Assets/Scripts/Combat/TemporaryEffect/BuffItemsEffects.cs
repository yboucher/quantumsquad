using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrecisionEffect : TemporaryEffect
{
	public PrecisionEffect(int mod)
	{
		if (mod >= 0)
			Name = "Adr�naline";
		else
			Name = "Malus de pr�cision";
		turns = 10000;
		accuracyMod = mod;
		if (mod >= 0)
			Description = "Pr�cision augment�e de " + mod + "%";
		else
			Description = "Pr�cision r�duite de " + (-mod) + "%";
	}

	public override GameObject GetIcon()
	{
		// TODO
		return null;
	}
}

public class ExoModEffect : TemporaryEffect
{
	public ExoModEffect(int mod)
	{
		if (mod >= 0)
			Name = "Module d'exosquelette";
		else
			Name = "Malus d'esquive";
		turns = 10000;
		dodgeMod = mod;
		if (mod >= 0)
			Description = "Esquive augment�e de " + mod + "%";
		else
			Description = "Esquive r�duite de " + (-mod) + "%";
	}

	public override GameObject GetIcon()
	{
		return null; // TODO
	}
}