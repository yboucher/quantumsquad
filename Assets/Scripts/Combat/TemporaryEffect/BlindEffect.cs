using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class BlindEffect : TemporaryEffect
{
	public BlindEffect(int mod = 60)
	{
		Name = "Aveugl�";
		accuracyMod = -mod; // Blinding reduces the accuracy by 60%
		turns = 1;
		Description = "La cible est aveugl�e et subit une p�nalit� de pr�cision de " + mod + "%. Dure un tour.";
	}

	public override GameObject GetIcon()
	{
		return GameObject.Find("StunnedIcon");
	}
}
