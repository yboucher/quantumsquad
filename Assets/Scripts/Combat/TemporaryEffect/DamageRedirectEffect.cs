using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class DamageRedirectEffect : TemporaryEffect
{
	public Actor benefactor;
	public float redirectedPortion;

	public DamageRedirectEffect(Actor in_benefactor, float in_redirectedPortion)
	{
		benefactor = in_benefactor;
		redirectedPortion = in_redirectedPortion;

		Name = "Protection";
		turns = 1;
		Description = "Un alli� prot�ge ce personnage et prendra une partie des d�g�ts qui lui seront inflig�s � sa place.";
	}

	public override GameObject GetIcon()
	{
		return GameObject.Find("ShieldIcon");
	}

	public override void DoExpireCleanup(Actor act)
	{
		foreach (Transform child in act.obj.transform)
			if (child.CompareTag("EffectVfx"))
				GameObject.Destroy(child.gameObject);
	}
}
