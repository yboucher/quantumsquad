using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class NanomachineEffect : TemporaryEffect
{
	public int radChance = 33;
	public int accDebuff = 25;
	public Actor effector;
	public NanomachineEffect(Actor effector, int radChance, int accDebuff)
	{
		Name = "Lucioles";
		this.effector = effector;
		this.radChance = radChance;
		accuracyMod = -accDebuff;
		turns = 99999;
		Description = "La cible subit a " + radChance + "% de chance d'�tre irradi�e � chaque tour, et perd " + accDebuff + "% de pr�cision.";
	}

	public override GameObject GetIcon()
	{
		return GameObject.Find("TargetedIcon");
	}

	protected override void DoEffectImpl(Actor act)
	{
		int radDamage = Random.Range(14, 17);
		int radRoll = Random.Range(0, 100);
		if (radRoll <= radChance)
		{ 
			GameObject.FindObjectOfType<CombatManager>().DamageActor(null, act, radDamage, AttackType.Radioactive);
			act.AddEffect(new RadioactiveEffect(radDamage));
		}
		int roll = Random.Range(0, 100);
		if (roll < 50)
			turns = 0; // Will disappear next turn
	}

	public override void DoExpireCleanup(Actor act)
	{
		foreach (Transform child in act.obj.transform)
			if (child.CompareTag("Lucioles"))
				GameObject.Destroy(child.gameObject);
	}
}
