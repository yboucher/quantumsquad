using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using System;
using System.Linq;
using System.Reflection;

[System.AttributeUsage(System.AttributeTargets.Class |
					   System.AttributeTargets.Struct)
]
public class ActionDataAttribute : System.Attribute
{
	public string name, desc;
	public double version;

	public ActionDataAttribute(string name, string desc)
	{
		this.name = name;
		this.desc = desc;
	}
}

[System.AttributeUsage(System.AttributeTargets.Field)]
public class ActionMainStatAttribute : System.Attribute
{
	public string name;
	public int baseValue;
	public int step = +5;
	public ActionMainStatAttribute(string name, int baseValue, int step)
	{
		this.name = name;
		this.baseValue = baseValue;
		this.step = step;
	}
}

[Serializable]
public abstract class Action
{
	public string name = "<MISSINGNO>";
	public int Charges = -1; // Negative amount of charges indicates an undepletable action
	public int ChargesUsed = 0;
	public int CooldownTime = 0;
	public int CooldownRemaining = 0;
	public bool turnEnding = false;
	public int Tier = 0;
	public int ActionCost = 1;

	public Actor effector;
	public Actor target;

	bool animationActive = false;


	public bool CanUse()
	{
		if (!CanUseImpl())
			return false;
		if (Charges >= 0)
			return ChargesUsed < Charges && CooldownRemaining == 0;
		else
			return CooldownRemaining == 0;
	}

	public virtual bool CanUseImpl()
	{
		return true;
	}

	public virtual string Description()
	{
		return "<MISSINGDESC>";
	}

	public abstract Tuple<int, int> Damage();
	public abstract int Accuracy();
	public abstract AttackType DamageType();

	public void SetTier(int tier)
	{
		this.Tier = tier;

		var mainStats = this.GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance)
							.Where(prop => prop.IsDefined(typeof(ActionMainStatAttribute), false));
		foreach (var stat in mainStats)
		{
			var statAttr = stat.GetCustomAttribute<ActionMainStatAttribute>();
			//Debug.Log(stat.Name + " / " + statAttr.name + " : " + statAttr.baseValue + " (" + statAttr.step + ")");
			stat.SetValue(this, Tweakables.GetSingle(GetType().ToString() + "_" + statAttr.name, statAttr.baseValue) + statAttr.step * tier);
			//Debug.Log("Setting value " + (statAttr.baseValue + statAttr.step * tier) + "(" + tier + ") for " + statAttr.name + " of " + name);
		}
	}

	public void Reset()
	{
		ChargesUsed = 0;
		CooldownRemaining = 0;
		if (GameObject.FindObjectOfType<CombatManager>())
			ResetImpl();
	}

	protected abstract void ResetImpl();

	public void Use()
	{
		Assert.IsTrue(CanUse());
		Assert.IsFalse(animationActive);

		GameObject.FindObjectOfType<CombatManager>().ActionStarted(this);

		animationActive = true;

		if (Charges >= 0)
			++ChargesUsed;
		CooldownRemaining = CooldownTime;

		UseImpl();
	}

	public bool ActionAnimationOver()
	{
		return animationActive;
	}

	public void EndOfTurn()
	{
		if (CooldownRemaining > 0)
			CooldownRemaining--;
		EndOfTurnImpl();
	}
	protected abstract void UseImpl();

	protected void ActionOver()
	{
		animationActive = false;
		GameObject.Find("GlobalScripts").GetComponent<CoroutineUtil>().StartCoroutine(GameObject.FindObjectOfType<CombatManager>().ActionFinished(this));
	}

	protected virtual void EndOfTurnImpl()
	{

	}

	protected Animator ActorAnimator()
	{
		return effector.obj.GetComponent<Animator>();
	}
}
