using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using System.Linq;

public class ActorSpriteUpdater : MonoBehaviour
{
    public StatusBar armorBar;
    public StatusBar healthBar;
    public GameObject arrow;
    public GameObject baseIndicatorPos;
    public Actor actor;

    List<TemporaryEffect> lastEffects = new List<TemporaryEffect>();

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void UpdateState()
	{
        Assert.IsNotNull(actor);

        armorBar.gameObject.SetActive(actor.Armor > 0);
        armorBar.maxValue = actor.MaxArmor;
        armorBar.value = actor.Armor;
        healthBar.maxValue = actor.MaxHP;
        healthBar.value = actor.HP;

        //arrow.SetActive(actor.active);

        if (!Enumerable.SequenceEqual(actor.GetEffects(), lastEffects))
        {
            // Remove previous indicators
            foreach (Transform child in baseIndicatorPos.transform)
            {
                GameObject.Destroy(child.gameObject);
            }

            int shownIndicators = 0;
            foreach (var effect in actor.GetEffects())
            {
                if (effect.GetIcon() == null)
                    continue;

                var obj = GameObject.Instantiate(effect.GetIcon(), baseIndicatorPos.transform);
                // If there's no armor bar to show, lower the indicators a lil' bit
                var yOffset = armorBar.value == 0 ? -0.485f : 0;
                // Translate to the right to show next to other indicators
                obj.transform.localPosition = new Vector3(0.5f * shownIndicators, yOffset, 0);
                ++shownIndicators;
            }

            lastEffects = new List<TemporaryEffect>(actor.GetEffects());
        }
	}

    // Update is called once per frame
    void Update()
    {
        // Reposition the status bar
        var newStatusPos = transform.position;
        var highestPoint = new Vector3(0, GetComponent<PolygonCollider2D>().points.Max(x => x.y), 0);
        highestPoint = transform.TransformPoint(highestPoint);
        newStatusPos.y = highestPoint.y;
        transform.Find("Status").transform.position = newStatusPos;
        //Debug.DrawLine(Vector3.zero, newStatusPos);
    }
}
