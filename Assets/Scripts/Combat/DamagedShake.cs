using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamagedShake : MonoBehaviour
{
    float animSpeed = 1.0f;
    public float redDuration = 1.5f;
    public float hitTime = -1;

    Vector3 basePos;
    bool showTint = true;

    // Start is called before the first frame update
    void Start()
    {
        //Hit();
    }

    public void Hit()
	{
        hitTime = Time.time;
        basePos = transform.position;
        showTint = true;
	}

    public void Shake()
	{
        Hit();
        showTint = false;
	}

    // Update is called once per frame
    void Update()
    {
        if (hitTime >= 0)
		{
            float progress = Mathf.Min(1.0f, (Time.time - hitTime) / animSpeed);
            transform.position = basePos + new Vector3(Mathf.Sin(progress * 50) * 0.1f, 0, 0) * (1 - progress);

            if (showTint)
            {
                GetComponent<SpriteRenderer>().color = Color.Lerp(Color.red, Color.white,
                    EasingFunction.EaseInCirc(0, 1, Mathf.Min(1, (Time.time - hitTime) / redDuration)));
            }
            if (Time.time - hitTime >= redDuration)
                hitTime = -1;
        }
    }
}
