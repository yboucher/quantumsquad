using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.Assertions;

[Serializable]
public class Weapon
{
	public string Name;
	public OffensiveAction action;
}
