using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetIndicator : MonoBehaviour
{
    public float baseRotSpeed = 100.0f;
    public float period = 5.0f;
    public CombatManager mgr;

    int currentEnemyIdx = 0;
    float previousAxisSign = 0;

    // Start is called before the first frame update
    void Start()
    {
        mgr = FindObjectOfType<CombatManager>();
    }

    List<Actor> ActiveParty()
	{
        if (FindObjectOfType<SceneMouseHoverMonitor>().targetTag == "Enemy")
            return mgr.EnemyParty();
        else
            return mgr.PlayerParty();
	}

    public void SelectRight()
	{
        currentEnemyIdx = (currentEnemyIdx + 1) % ActiveParty().Count;
        LeanTween.cancel(gameObject);
        LeanTween.move(gameObject, ActiveParty()[currentEnemyIdx].obj.transform.position, 0.25f).setEaseInOutQuad();
    }

    public void SelectLeft()
	{
        currentEnemyIdx = (currentEnemyIdx - 1) % ActiveParty().Count;
        if (currentEnemyIdx < 0) // Correct negative modulos
            currentEnemyIdx += ActiveParty().Count;
        LeanTween.cancel(gameObject);
        LeanTween.move(gameObject, ActiveParty()[currentEnemyIdx].obj.transform.position, 0.25f).setEaseInOutQuad();
    }

    public void SelectEnemy(GameObject enemy)
	{
        for (int i = 0; i < ActiveParty().Count; i++)
		{
            if (ActiveParty()[i].obj.gameObject == enemy && currentEnemyIdx != i)
			{
                currentEnemyIdx = i;
                LeanTween.cancel(gameObject);
                LeanTween.move(gameObject, ActiveParty()[currentEnemyIdx].obj.transform.position, 0.25f).setEaseInOutQuad();
                return;
            }
		}
	}

    public void ResetSelection()
	{
        if (mgr == null || ActiveParty() == null || ActiveParty().Count <= 0)
            return;

        currentEnemyIdx = 0;
        LeanTween.cancel(gameObject);
        LeanTween.move(gameObject, ActiveParty()[currentEnemyIdx].obj.transform.position, 0.25f).setEaseInOutQuad();
    }

    public GameObject SelectedEnemy()
	{
        if (ActiveParty().Count <= 0)
            return null;
        if (currentEnemyIdx < 0 || currentEnemyIdx >= ActiveParty().Count)
            currentEnemyIdx = 0;
        return ActiveParty()[currentEnemyIdx].obj.gameObject;
	}

    // Update is called once per frame
    void Update()
    {
        //transform.position = ActiveParty()[currentEnemyIdx].obj.transform.position;
        transform.Rotate(0, 0, Mathf.Sin(Time.time * period) * baseRotSpeed * Time.deltaTime);

        bool leftPressed = false;
        bool rightPressed = false;
        //Debug.Log(Input.GetAxisRaw("Horizontal") + " / " + previousAxisSign);
        if (Input.GetKeyDown("left") || Input.GetAxisRaw("Horizontal") < 0 && previousAxisSign >= 0)
		{
            leftPressed = true;
		}
        else if (Input.GetKeyDown("right") || Input.GetAxisRaw("Horizontal") > 0 && previousAxisSign <= 0)
        {
            rightPressed = true;
        }

        if (leftPressed)
		{
            SelectLeft();
		}
        else if (rightPressed)
        {
            SelectRight();
        }

        previousAxisSign = Mathf.Sign(Input.GetAxisRaw("Horizontal"));
        if (Mathf.Abs(Input.GetAxisRaw("Horizontal")) < 0.05f)
            previousAxisSign = 0;
    }
}
