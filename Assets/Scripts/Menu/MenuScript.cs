using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MenuScript : MonoBehaviour
{
    public LevelLoader loader;
    public GameObject overlay;
    public TextMeshProUGUI difficultyText;
    public Button difficultyButton;
    public GameObject swipeObject;

    // Start is called before the first frame update
    void Start()
    {
        // TODO REMOVE AFTER DEMO
        //Random.InitState(2337);


        if (FindObjectOfType<TransitionSwipeScript>() == null)
            GameObject.Instantiate(swipeObject).SetActive(true);

        bool firstTime = SharedData.firstTime;
        // Reset Shared Data
        SharedData.Reset();

        // Carry over the "first time?" state to the reset shared data
        SharedData.firstTime = firstTime;

        // Generate the universe name
        // Template : #37AF-42
        SharedData.universeName = "#" + (char)(Random.Range(0, 9) + '0') + (char)(Random.Range(0, 9) + '0')
                                  + (char)(Random.Range(0, 25) + 'A') + (char)(Random.Range(0, 25) + 'A') + "-" +
                                  (char)(Random.Range(0, 9) + '0') + (char)(Random.Range(0, 9) + '0');

        Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);

        SaveData save = SaveData.Load();

        if (save.difficultyMode == 1)
        {
            difficultyText.text = "Hardcore";
        }
        else
            difficultyText.text = "Normale";

        // Play intro if it had never played before
        if (!save.introPlayed)
        {
            save.introPlayed = true;
            save.Save();
            FindObjectOfType<LevelLoader>().LoadLevelByName("IntroCinematic");
        }
        else
        {
            overlay.SetActive(false);
            GetComponent<MenuAnimationSequencer>().enabled = true;
        }
    }

    public void SwitchDifficulty()
	{
        SaveData save = SaveData.Load();
        if (save.difficultyMode == 1)
        {
            save.difficultyMode = 0;
            difficultyText.text = "Normale";
        }
        else
        {
            save.difficultyMode = 1;
            difficultyText.text = "Hardcore";
        }

        save.Save();
    }
    public void ExitGame()
	{
        Application.Quit();
	}
    public void LoadGame()
	{
        loader.LoadLevelByName("BeforeGameScene");
	}

    public void OpenOptionMenu()
	{
        difficultyButton.Select();
	}

    // Update is called once per frame
    void Update()
    {
        
    }
}
