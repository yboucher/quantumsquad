using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MenuAnimationSequencer : MonoBehaviour
{
    public Material starfieldMaterial;
    public TMP_Text introText;
    public float textSpeed = 0.02f;
    public MessageBox msgBox;
    public RectTransform menu;
    public AudioClip titleTheme;
    public MusicManager musicManager;
    public Button playButton;

    // Start is called before the first frame update
    void Start()
    {
        starfieldMaterial.SetFloat("_H_speed", 5);
        starfieldMaterial.SetFloat("_V_speed", 5);
        starfieldMaterial.SetFloat("_Darken", 0.6f);

        introText.text = string.Format(introText.text, SharedData.universeName);

        StartCoroutine(SequenceCoroutine());
        if (SharedData.firstTime)
            GetComponent<TextWriter>().AddWriter(introText, introText.text, textSpeed, true, null, 0);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator SequenceCoroutine()
	{
        if (SharedData.firstTime)
        {
            msgBox.gameObject.SetActive(true);

            for (int i = 0; i < 120; ++i)
            {
                starfieldMaterial.SetFloat("_H_speed", Mathf.Lerp(5, 500, Mathf.Sin(i / 120.0f * (Mathf.PI / 2))));
                if (Input.GetButton("Fire1"))
                    break;
                yield return new WaitForSeconds(2 / 120.0f);
            }

            float startTime = Time.time;
            yield return new WaitUntil(() => Time.time - startTime > 9 || Input.GetButton("Fire1"));
            GetComponent<TextWriter>().Reset();

            msgBox.Close();

            SharedData.firstTime = false;
        }

        for (int i = 0; i < 120; ++i)
        {
            starfieldMaterial.SetFloat("_H_speed", Mathf.Lerp(500, 5, Mathf.Sin(i / 120.0f * (Mathf.PI / 2))));
            starfieldMaterial.SetFloat("_V_speed", Mathf.Lerp(5, 50, Mathf.Sin(i / 120.0f * (Mathf.PI / 2))));
            yield return new WaitForSeconds(1 / 120.0f);
        }

        musicManager.PlayMusic(titleTheme);
        yield return new WaitForSeconds(1.5f);

        float menuBaseY = menu.anchoredPosition.y;
        for (int i = 0; i < 120; ++i)
        {
            //starfieldMaterial.SetFloat("_V_speed", Mathf.Lerp(500, 5, Mathf.Sin(i / 120.0f * (Mathf.PI / 2))));
            menu.anchoredPosition = new Vector2(0, EasingFunction.EaseOutSine(menuBaseY, 0, i / 120.0f));
            yield return new WaitForSeconds(1.0f / 120.0f);
        }

        FindObjectOfType<EventSystem>().firstSelectedGameObject = playButton.gameObject;
        playButton.Select();
    }
}
