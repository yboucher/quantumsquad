using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingCredits : MonoBehaviour
{
    public float delay = 4;
    public float speed = 5;
    public float topThreshold = 250;
    public float baseY = -614;

    private float delayAcc;

    // Start is called before the first frame update
    void Start()
    {
        delayAcc = delay;
        //baseY = GetComponent<RectTransform>().anchoredPosition.y;
    }

    public void ScrollReset()
	{
        delayAcc = delay;
        GetComponent<RectTransform>().anchoredPosition = new Vector3(GetComponent<RectTransform>().anchoredPosition.x,
                                                            baseY);
	}

    // Update is called once per frame
    void Update()
    {
        if (delayAcc > 0)
		{
            delayAcc -= Time.deltaTime;
            return;
		}

        GetComponent<RectTransform>().Translate(0, speed * Time.deltaTime, 0);
        if (GetComponent<RectTransform>().anchoredPosition.y > topThreshold)
            ScrollReset();
    }
}
