using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;
using System.Linq;
using TMPro;

public class ShopItem
{
    public string Name;
    public int count;
    public int price;

    public string description;

    public virtual void Purchased()
	{
        Debug.Log("Item " + Name + " purchased");
	}
}

public class ShopInventoryItem : ShopItem
{
    public Item item;
    public ShopInventoryItem(Item item, int price, int count)
	{
        this.item = item;
        this.price = price;
        this.count = count;
        Name = item.name;
        description = item.Description();
	}

	public override void Purchased()
	{
        SharedData.sharedInventory.Add(item);
	}
}

public class NewClassItem : ShopItem
{
    public string classId;
    SaveData saveData;
    public NewClassItem(string name, int price, SaveData save)
    {
        this.saveData = save;
        this.price = price;
        this.count = 1;
        this.classId = name;
        Name = "Classe : <color=#00A9FF>" + name;
        description = "D�bloque la classe : <color=#00A9FF>" + name + "</color>\n\n" + ClassInfo.Classes()[name].desc;
    }

    public override void Purchased()
    {
        saveData.unlockedClasses.Add(classId);
        saveData.Save();
    }
}

public class ShopScript : MonoBehaviour
{
    public TextMeshPro desc;
    public TextMeshPro goldText;
    public GameObject msgBoxOfThePoor;
    public GameObject classIndicator;
    public AudioSource cashRegisterEffect;
    public Button firstItem;

    List<GameObject> itemObjs = new List<GameObject>();
    List<Button> buttons = new List<Button>();

    SaveData saveData;
    int currentPageOffset = -1;
    bool beingBerated = false;

    // Start is called before the first frame update
    void Start()
    {
        saveData = SaveData.Load();

        UpdateGoldCount();

        for (int i = 0; i <= 8; ++i)
        {
            itemObjs.Add(GameObject.Find("Item" + i));
            buttons.Add(GameObject.Find("ItemButton" + i).GetComponent<Button>());
        }

        if (SharedData.shopItems == null)
            GenerateShopItems();

        LoadPage(0);

        firstItem.Select();
    }

    void GenerateShopItems()
	{
        SharedData.shopItems = new List<ShopItem>();
        if (!saveData.unlockedClasses.Contains("Savon"))
            SharedData.shopItems.Add(new NewClassItem("Savon", 100, saveData));
        if (!saveData.unlockedClasses.Contains("Cyborg") && saveData.highestBeatenBoss >= 1)
            SharedData.shopItems.Add(new NewClassItem("Cyborg", 500, saveData));
        // TODO : Guardian
        //if (!saveData.unlockedClasses.Contains("Guardian") && saveData.highestBeatenBoss >= 2)
        //    SharedData.shopItems.Add(new NewClassItem("Guardian", 500));
        SharedData.shopItems.Add(new ShopInventoryItem(new HealHerbs(), 150, 3));
        SharedData.shopItems.Add(new ShopInventoryItem(new SuperMedKit(), 400, 1));
        SharedData.shopItems.Add(new ShopInventoryItem(new RadAway(), 100, 2));
        SharedData.shopItems.Add(new ShopInventoryItem(new Bandage(), 100, 2));
        SharedData.shopItems.Add(new ShopInventoryItem(new Adrenaline(), 175, 1));
        SharedData.shopItems.Add(new ShopInventoryItem(new ExoMod(), 175, 1));
        SharedData.shopItems.Add(new ShopInventoryItem(new EnemyScanItem(), 75, 3));

        var curioItems = Item.CurioItems();
        foreach (var curioName in curioItems.OrderBy(a => Random.Range(0, 100000000)).Take(2))
		{
            var type = System.Type.GetType(curioName);
            var myObject = (Item)System.Activator.CreateInstance(type);
            SharedData.shopItems.Add(new ShopInventoryItem(myObject, 200, 1));
        }

        SharedData.shopItems.Add(new ShopInventoryItem(new NFTItem(), 100, 3));

        /*
        for (int i = 0; i <= 4; ++i)
        {
            SharedData.shopItems.Add(new ShopItem
            {
                Name = "Item number " + i,
                count = i % 3 + 1,
                price = ((i + 2) % 4 + 1) * 100,
                description = "Description test numero " + i
            });
        }
        */
    }

    public void PickItem(int screenIdx)
	{
        int idx = (screenIdx + currentPageOffset);
        Assert.IsTrue(idx < SharedData.shopItems.Count);

        if (beingBerated)
            return;

        if (SharedData.shopItems[idx].price > saveData.gold)
		{
            StartCoroutine(BerateForBeingPoor());
            return;
		}

        cashRegisterEffect.Play();

        saveData.gold -= SharedData.shopItems[idx].price;
        saveData.Save();

        UpdateGoldCount();

        SharedData.shopItems[idx].Purchased();

        if (SharedData.shopItems[idx].count > 1)
        {
            SharedData.shopItems[idx].count--;
            itemObjs[screenIdx].transform.Find("Count").GetComponent<TextMeshPro>().text = "x" + SharedData.shopItems[idx].count;
        }
        else
            RemoveItem(idx);
	}

    public void UpdateGoldCount()
    {
        var newData = SaveData.Load();
        saveData.gold = newData.gold;
        goldText.text = System.String.Format("Gold : <color=yellow>{0}</color> po", saveData.gold);
    }

    void RemoveItem(int idx)
	{
        SharedData.shopItems.RemoveAt(idx);
        // Reload page
        if (SharedData.shopItems.Count > currentPageOffset || SharedData.shopItems.Count == 0)
            LoadPage(currentPageOffset);
        else
            PrevPage();
    }

    public void UpdateDesc(int screenIdx)
	{
        int idx = (screenIdx + currentPageOffset);
        Assert.IsTrue(idx < SharedData.shopItems.Count);

        desc.text = SharedData.shopItems[idx].description;

        if (SharedData.shopItems[idx].GetType() == typeof(NewClassItem))
		{
            NewClassItem classItem = (NewClassItem)SharedData.shopItems[idx];

            classIndicator.SetActive(true);
            classIndicator.transform.Find("ClassName").GetComponent<TextMeshPro>().text = classItem.classId;

            var anim = FindObjectOfType<AnimList>().anims.Find(x => x.name == classItem.classId);
            classIndicator.GetComponent<Animator>().runtimeAnimatorController = anim.anim;
            classIndicator.GetComponent<Animator>().Update(0.0f);
        }
        else
            classIndicator.SetActive(false);
    }

    public void HideDesc()
	{
        desc.text = "Survolez un item pour voir sa description.";
        classIndicator.SetActive(false);
    }

    IEnumerator BerateForBeingPoor()
    {
        beingBerated = true;

        var msg = GameObject.Instantiate(msgBoxOfThePoor, transform.parent);
        msg.GetComponent<MessageBox>().Show();
        yield return new WaitUntil(() => msg.GetComponent<MessageBox>().closing);
        msg.GetComponent<MessageBox>().Close();

        beingBerated = false;
    }

    // Update is called once per frame
    void Update()
    {
        // Paging
        if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetButtonDown("SwitchLeft"))
            PrevPage();
        else if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetButtonDown("SwitchRight"))
            NextPage();
    }

    void LoadPage(int pageOffset)
	{
        //if (pageOffset == currentPageOffset)
        //    return;

        currentPageOffset = pageOffset;

        // Load skill info
        for (int i = 0; i < itemObjs.Count; ++i)
        {
            if (i + currentPageOffset < SharedData.shopItems.Count)
            {
                buttons[i].gameObject.SetActive(true);
                itemObjs[i].SetActive(true);
                itemObjs[i].transform.Find("ItemName").GetComponent<TextMeshPro>().text = SharedData.shopItems[i + currentPageOffset].Name;
                itemObjs[i].transform.Find("Count").GetComponent<TextMeshPro>().text = "x" + SharedData.shopItems[i + currentPageOffset].count;
                itemObjs[i].transform.Find("Cost").GetComponent<TextMeshPro>().text = "<color=yellow>" + SharedData.shopItems[i + currentPageOffset].price + " po";
            }
            else
            {
                itemObjs[i].SetActive(false);
                buttons[i].gameObject.SetActive(false);
            }
        }

        buttons[0].GetComponent<Button>().Select();
    }

    public void PrevPage()
    {
        if (currentPageOffset > 0)
            LoadPage(Mathf.Max(0, currentPageOffset - itemObjs.Count));
    }

    public void NextPage()
    {
        if ((currentPageOffset + itemObjs.Count) < SharedData.shopItems.Count)
            LoadPage(Mathf.Min(currentPageOffset + itemObjs.Count, SharedData.shopItems.Count - 1));
    }
}
