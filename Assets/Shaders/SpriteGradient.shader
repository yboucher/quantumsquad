// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/SpriteGradient" {
    Properties{
        [PerRendererData] _MainTex("Sprite Texture", 2D) = "white" {}
        _Color("Left Color", Color) = (1,1,1,1)
        _Color2("Right Color", Color) = (1,1,1,1)
        _Scale("Scale", Float) = 1
        _Offset("Offset", Float) = 0.8
        _Width("Width", Float) = 0.2
    }

        SubShader{
            Tags
            {
                "Queue" = "Transparent"
                "IgnoreProjector" = "True"
                "RenderType" = "Transparent"
                "PreviewType" = "Plane"
                "CanUseSpriteAtlas" = "True"
            }

            Cull Off
            ZWrite Off
            Blend  SrcAlpha OneMinusSrcAlpha

            Pass {
                CGPROGRAM
                #pragma vertex vert  
                #pragma fragment frag
                #include "UnityCG.cginc"

                fixed4 _Color;
                fixed4 _Color2;
                fixed  _Scale;
                fixed _Offset;
                fixed _Width;

                struct v2f {
                    fixed4 color : COLOR;
                    float4 pos : SV_POSITION;
                    fixed2 uv : TEXCOORD0;
                };

                v2f vert(appdata_full v)
                {
                    v2f o;
                    o.pos = UnityObjectToClipPos(v.vertex);
                    o.uv = v.texcoord;
                    o.color = v.color;
                    return o;
                }


                float4 frag(v2f i) : COLOR {
                    float t = clamp((i.uv.y - _Offset) / _Width, 0, 1);
                    float4 c = lerp(_Color, i.color, t);
                    c.a = 1;
                    return c;
                }
                    ENDCG
                }
        }
}