Shader "Unlit/TransitionSwipe"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _StartTime("Start Time", Float) = 0.0
        _GlobalTime("Global Time", Float) = 0.0
        _LogoTex("Logo Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "Queue" = "Transparent" "RenderType" = "Transparent" }
        LOD 100
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            int _revealedTileCount;
            float _revealedTiles[256*3]; // X, Y, revealTime

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            sampler2D _LogoTex;
            float4 _MainTex_ST;
            fixed _StartTime;
            fixed _GlobalTime;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

#define PI 3.14159265358979323846

            float2 rotate(float2 vec, float angle)
            {
                float2 rotated;
                rotated.x = vec.x * cos(angle) - vec.y * sin(angle);
                rotated.y = vec.x * sin(angle) + vec.y * cos(angle);

                return rotated;
            }

            float astroid(float2 coords, float radius)
            {
                float dist = pow(coords.x * coords.x + coords.y * coords.y - radius * radius, 3.f)
                    + 27.f * radius * radius * coords.x * coords.x * coords.y * coords.y; // astroid
                return step(dist, 0.0f);
            }

            fixed4 frag(v2f i) : SV_Target
            {
                const float time = _GlobalTime - _StartTime;
                //const float time = fmod(_Time.y, 3.0f);

                const float invert_time = 1.5f * 0.75f;
                float total_progress = time / 0.75f;
                float progress = min(total_progress, 1.0f);
                if (time > invert_time)
                    progress = 1.0f - (total_progress - 1.5f) / 0.7f;
                if (time > invert_time + 0.5f)
                    progress = 0.0f;

                // Normalized pixel coordinates (from 0 to 1)
                float2 uv2 = i.uv;
                uv2.x *= _ScreenParams.x / _ScreenParams.y;
                float2 uv2_cpy = uv2;

                uv2 += float2(0.0f, sin(progress * 4.f + uv2.y * 2.f) / 40.f);

                float y_coord = time > invert_time ? uv2_cpy.y : (1.0f - uv2_cpy.y);
                float radius = min(progress * 3.f, max(0.0f, 2.f * 3.0f * (progress - (y_coord / 1.5f)))); // sqrt(0.5�+0.5�)
                //radius = mod(iTime, 0.707f);

                float2 mod_coords = 2.f * (fmod(uv2, 0.1f) / 0.1f) - float2(1, 1); // normalized mod coordinates
                float angle = progress * 1.5f * 3.1415f / 2.f;
                float2 coords = rotate(mod_coords, angle);

                float3 pink = float3(0x7e / 255.0, 0x05 / 255.0f, 1);
                //pink *= (progress+2.f+2.f*uv2_cpy.y)/2.9f;
                pink = lerp(pink, pink / 1.5f, (min(0.5f * progress + 2.f * uv2_cpy.y, 1.f)));

                // Output to screen
                fixed4 fragColor = fixed4(pink, astroid(coords, radius));
                fixed4 tex = tex2D(_LogoTex, i.uv);
                if (fragColor.a > 0 && tex.a > 0) // Logo
                {
                    fragColor = tex;
                }
                return fragColor;
            }
            ENDCG
        }
    }
}
