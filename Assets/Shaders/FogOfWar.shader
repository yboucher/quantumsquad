Shader "Unlit/FogOfWar"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "Queue" = "Transparent" "RenderType" = "Transparent" }
        LOD 100
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            int _revealedTileCount;
            float _revealedTiles[256*3]; // X, Y, revealTime

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            float polygonDistance(float2 p, float radius, float angleOffset, int sideCount) {
                float a = atan2(p.y, p.x) + angleOffset;
                float b = 6.28319 / float(sideCount);
                return cos(floor(.5 + a / b) * b - a) * length(p) - radius;
            }

            // from https://www.shadertoy.com/view/4djSRW
#define HASHSCALE1 443.8975
            float hash11(float p) // assumes p in ~0-1 range
            {
                float t = frac(p * HASHSCALE1);
                float3 p3 = float3(t, t, t);
                p3 += dot(p3, p3.yzx + 19.19);
                return frac((p3.x + p3.y) * p3.z);
            }

#define HASHSCALE3 float3(.1031, .1030, .0973)
            float2 hash21(float p) // assumes p in larger integer range
            {
                float3 p3 = float3(p,p,p) * HASHSCALE3;
                p3.r = frac(p3.r); p3.g = frac(p3.g); p3.b = frac(p3.b);
                p3 += dot(p3, p3.yzx + 19.19);
                return frac(float2((p3.x + p3.y) * p3.z, (p3.x + p3.z) * p3.y));
            }

            float revealSquare(float2 uv, int2 tilePos, float progress)
            {
                float alpha = 1;
                float off_x = -0.005/1.2 - 0.005*8.0;
                float off_y = -0.035/1.2 / 1.59;
                float width = 0.0375/1.2 / 1.45;
                float height = 0.0375 / 1.2 / 1.59 / 1.5;
                float opacity_falloff = 0.04;

                float pos_x = tilePos.x * 0.045 / 1.2;
                float pos_y = tilePos.y * -0.045 / 1.2 / 1.59;

                float alpha_mult = progress;

                alpha *= alpha_mult * (smoothstep(pos_x + off_x + -opacity_falloff - width, pos_x + off_x + opacity_falloff - width, uv.x));
                alpha *= alpha_mult * (1 - smoothstep(pos_x + off_x + -opacity_falloff + width, pos_x + off_x + opacity_falloff + width, uv.x));

                alpha *= alpha_mult * (smoothstep(pos_y + off_y - 0.03 - height, pos_y + off_y + 0.03 - height, uv.y));
                alpha *= alpha_mult * (1 - smoothstep(pos_y + off_y - 0.03 + height, pos_y + off_y + 0.03 + height, uv.y));
                return 1 - alpha;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                float2 uv = float2(0.5, 0.5) - i.uv;
                uv.x *= _ScreenParams.x / _ScreenParams.y;

                float alpha = 1.0;

                float2 tileCoords = i.uv * 16;
                //if (tileCoords.y > 8)
                //    alpha = 1 - (tileCoords.y - 8) / 16;
                /*
                alpha *= smoothstep(0.45 - 0.2, 0.5 - 0.2, fragCoord.x / _ScreenParams.x);
                alpha *= 1 - smoothstep(0.45 + 0.1, 0.5 + 0.1, fragCoord.x / _ScreenParams.x);

                float alpha2 = 1;
                alpha2 *= smoothstep(0.45 - 0.4, 0.55 - 0.4, fragCoord.x / _ScreenParams.x);
                alpha2 *= 1 - smoothstep(0.45 - 0.3, 0.55 - 0.3, fragCoord.x / _ScreenParams.x);

                alpha += alpha2;
                */

                for (int j = 0; j < _revealedTileCount; ++j)
                {
                    // pass the time since the tile was revealed as an alpha factor to progressively reveal the tile
                    alpha *= revealSquare(uv, int2(_revealedTiles[j*3], _revealedTiles[j*3+1]), min(_Time.y - _revealedTiles[j * 3 + 2], 1.0f));
                }

                //alpha *= revealSquare(uv, int2(0, 0));
                //alpha *= revealSquare(uv, int2(1, 0));
                //alpha *= revealSquare(uv, int2(2, 0));
                //alpha *= revealSquare(uv, int2(3, 0));
                //alpha *= revealSquare(uv, int2(4, 0));
                //alpha *= revealSquare(uv, int2(5, 0));
                //alpha *= revealSquare(uv, int2(0, 1));
                //alpha *= revealSquare(uv, int2(0, 2));
                //alpha *= revealSquare(uv, int2(0, 3));
                //alpha *= revealSquare(uv, int2(2, 1));
                //alpha *= revealSquare(uv, int2(2, 2));
                //alpha *= revealSquare(uv, int2(2, 3));

                _Time.y *= 0.75;
                
                uv *= 2;

                float accum = 0.;
                for (int i = 0; i < 36; i++) {
                    float fi = float(i);
                    float thisYOffset = fmod(hash11(fi * 0.017) * (_Time.y + 19.) * 0.2, 4.0) - 2.0;
                    float2 center = (hash21(fi) * 2. - 1.) * float2(1.1, 1.0) - float2(0.0, thisYOffset);
                    float radius = 0.5;
                    float2 offset = uv - center;
                    float twistFactor = (hash11(fi * 0.0347) * 2. - 1.) * 1.9;
                    float rotation = 0.1 + _Time.y * 0.2 + sin(_Time.y * 0.1) * 0.9 + (length(offset) / radius) * twistFactor;
                    accum += pow(smoothstep(radius, 0.0, polygonDistance(uv - center, 0.1 + hash11(fi * 2.3) * 0.2, rotation, 5) + 0.1), 3.0);
                }

                float3 subColor = float3(1, 1, 1) - float3(0.4, 0.8, 0.2); //vec3(0.4, 0.2, 0.8);
                subColor *= 0.6;
                float3 addColor = float3(0.3, 0.2, 0.1) * 0.2;//vec3(0.3, 0.1, 0.2);

                fixed4 color = fixed4(accum * subColor + addColor, alpha);

                //if (color.a > 0.5)
                {
                    //color.rgb *= 0.6;
                    // premultiply
                    //color.rgb *= color.a;
                }
                //else
                {
                    //color.a = 0;
                }
                return color;
            }
            ENDCG
        }
    }
}
