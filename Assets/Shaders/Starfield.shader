Shader "Unlit/Starfield"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _H_speed("Horizontal Speed", Float) = 5
        _V_speed("Vertical Speed", Float) = 5
        _Darken("Luminosity", Float) = 0.3
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100
        //Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            float rand(in float2 uv) { return frac(sin(dot(uv, float2(12.4124, 48.4124))) * 48512.41241); }
            const float2 O = float2(0., 1.);
            float noise(in float2 uv) {
                float2 b = floor(uv);
                return lerp(lerp(rand(b), rand(b + O.yx), .5), lerp(rand(b + O), rand(b + O.yy), .5), .5);
            }

            #define DIR_RIGHT -1.
            #define DIR_LEFT 1.
            #define DIRECTION DIR_LEFT

            #define LAYERS 8
            #define SPEED (0.4*10.0*0.7)
            #define SIZE 5.
            #define SCALE 2.0

            float _H_speed = 5;
            float _V_speed = 5;
            float _Darken = 0.3;

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);
                col = fixed4(0.5, 0.5, 0.5, 1);

                float2 uv = i.uv;
                //uv.y /= (_ScreenParams.x / _ScreenParams.y); // Fix the aspect ratio
                uv *= SCALE;

                float stars = 0.;
                float fl, s;
                for (int layer = 2; layer < LAYERS; layer++) {
                    fl = float(layer);
                    s = (400. - fl * 20.);
                    stars += step(.1, pow(noise(fmod(float2(uv.x * s + _Time.y * _H_speed * 0.5 - fl * 200., uv.y * s + _Time.y * _V_speed * 0.5), _ScreenParams.x)), 500.)) * (fl / float(LAYERS));
                }

                col = fixed4(stars, stars, stars, stars);
                col *= _Darken; // darken

                // apply fog
                //UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}
