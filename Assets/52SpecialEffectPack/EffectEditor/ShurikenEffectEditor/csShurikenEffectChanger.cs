﻿using UnityEngine;
using UnityEditor;


public class csShurikenEffectChanger : MonoBehaviour
{
	public float scale = 1;

	public void Start()
	{
		ShurikenParticleScaleChange(scale);
	}

	public void ShurikenParticleScaleChange(float _Value)
	{
		ParticleSystem[] ParticleSystems = GetComponentsInChildren<ParticleSystem>();

        transform.localScale *= _Value;

		#pragma warning disable CS0618
		foreach (ParticleSystem _ParticleSystem in ParticleSystems) {
			_ParticleSystem.startSpeed *= _Value;
			_ParticleSystem.startSize *= _Value;
			_ParticleSystem.gravityModifier *= _Value;

		}
		#pragma warning restore CS0618
	}
}
